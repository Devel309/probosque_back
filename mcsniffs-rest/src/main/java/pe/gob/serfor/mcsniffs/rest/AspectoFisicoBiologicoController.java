package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.*;

import java.util.List;

@RestController
@RequestMapping(Urls.aspectoFisicoFisiografia.BASE)
public class AspectoFisicoBiologicoController extends AbstractRestController{
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(AspectoFisicoBiologicoController.class);

    @Autowired
    private AspectoFisicoFisiografiaService aspectoFisicoFisiografiaService;

    /**
     * @autor: Nelson Coqchi [31-08-2021]
     * @modificado:
     * @descripción: {}
     */

    @PostMapping(path = "/eliminarAspectofisicofisiografia")
    @ApiOperation(value = "eliminarAspectofisicofisiografia" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarAspectofisicofisiografia(@RequestBody AspectoFisicoFisiografiaEntity aspectoFisicoFisiografiaEntity){
        log.info("AspectoFisicoBiologico - eliminarAspectofisicofisiografia",aspectoFisicoFisiografiaEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = aspectoFisicoFisiografiaService.EliminarAspectoFisicoFisiografia(aspectoFisicoFisiografiaEntity);
            log.info("AspectoFisicoBiologico - EliminarAspectoFisicoFisiografia","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service - EliminarAspectoFisicoFisiografia","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //INTERNAMENTE SE REGISTRARA O ELIMINAARA
    @PostMapping(path = "/registrarAspectofisicofisiografia")
    @ApiOperation(value = "registrarAspectofisicofisiografia" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarAspectofisicofisiografia(@RequestBody List<AspectoFisicoFisiografiaEntity> listAspectoFisicoFisiografiaEntity){
        log.info("AspectoFisicoBiologico - registrarAspectofisicofisiografia",listAspectoFisicoFisiografiaEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = aspectoFisicoFisiografiaService.RegistrarAspectoFisicoFisiografia(listAspectoFisicoFisiografiaEntity);
            log.info("AspectoFisicoBiologico - RegistrarAspectoFisicoFisiografia","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service - RegistrarAspectoFisicoFisiografia","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //INTERNAMENTE SE REGISTRARA O ELIMINAARA
    @PostMapping(path = "/listarAspectofisicofisiografia")
    @ApiOperation(value = "listarAspectofisicofisiografia" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarAspectofisicofisiografia(@RequestBody AspectoFisicoFisiografiaEntity aspectoFisicoFisiografiaEntity){
        log.info("AspectoFisicoBiologico - listarAspectofisicofisiografia",aspectoFisicoFisiografiaEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = aspectoFisicoFisiografiaService.ListarAspectoFisicoFisiografia(aspectoFisicoFisiografiaEntity);
            log.info("AspectoFisicoBiologico - ListarAspectoFisicoFisiografia","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service - ListarAspectoFisicoFisiografia","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/configurarAspectofisicofisiografia")
    @ApiOperation(value = "configurarAspectofisicofisiografia" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity configurarAspectofisicofisiografia(@RequestBody AspectoFisicoFisiografiaEntity aspectoFisicoFisiografiaEntity){
        log.info("AspectoFisicoBiologico - configurarAspectofisicofisiografia",aspectoFisicoFisiografiaEntity.toString());
        ResponseEntity result = null;
        ResultClassEntity response;
        try{
            response = aspectoFisicoFisiografiaService.ConfigurarAspectoFisicoFisiografia(aspectoFisicoFisiografiaEntity);
            log.info("AspectoFisicoBiologico - ConfigurarAspectoFisicoFisiografia","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service - ConfigurarAspectoFisicoFisiografia","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
