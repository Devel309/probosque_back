package pe.gob.serfor.mcsniffs.rest;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.Dto.LibroOperaciones.LibroOperacionesDto;
import pe.gob.serfor.mcsniffs.entity.Dto.LibroOperaciones.LibroOperacionesPlanManejoDto;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.LibroOperacionesPlanManejoService;
import pe.gob.serfor.mcsniffs.service.LibroOperacionesService;

import java.util.List;


@RestController
@RequestMapping(Urls.libroOperacionesPlanManejo.BASE)
public class LibroOperacionesPlanManejoController extends AbstractRestController {

    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(LibroOperacionesPlanManejoController.class);

    @Autowired
    private LibroOperacionesPlanManejoService libroOperacionesPlanManejoService;


    @PostMapping(path = "/registrarLibroOperacionesPlanManejo")
    @ApiOperation(value = "registrarLibroOperacionesPlanManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarLibroOperaciones(@RequestBody List<LibroOperacionesPlanManejoDto> obj){
        log.info("Libro OperacionesPlanManejo - registrarLibroOperaciones",obj.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = libroOperacionesPlanManejoService.registrarLibroOperacionesPlanManejo(obj);
            log.info("Libro Operaciones PlanManejo- registrarLibroOperacionesPlanManejo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Libro Operaciones PlanManejo- registrarLibroOperacionesPlanManejo","Ocurrió un error :"+ e.getMessage());

            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/eliminarLibroOperacionesPlanManejo")
    @ApiOperation(value = "eliminarLibroOperacionesPlanManejo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarLibroOperacionesPlanManejo(@RequestBody LibroOperacionesPlanManejoDto obj){
        log.info("Libro Operaciones Plan Manejo - eliminarLibroOperacionesPlanManejo",obj.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = libroOperacionesPlanManejoService.eliminarLibroOperacionesPlanManejo(obj);
            log.info("Libro Operaciones Plan Manejo - eliminarLibroOperacionesPlanManejo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Libro Operaciones Plan Manejo - eliminarLibroOperacionesPlanManejo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}
