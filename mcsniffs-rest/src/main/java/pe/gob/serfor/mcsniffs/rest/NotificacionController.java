package pe.gob.serfor.mcsniffs.rest;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import pe.gob.serfor.mcsniffs.entity.NotificacionEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudSANEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.NotificacionService;

@RestController
@RequestMapping(Urls.notificacion.BASE)
public class NotificacionController extends AbstractRestController{
    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger( NotificacionController.class);

    @Autowired
    private  NotificacionService  notificacionService;

 
 
    @PostMapping(path = "/registrarNotificacion")
    @ApiOperation(value = "Registrar Actividad de  Notificacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarNotificacion(@RequestBody NotificacionEntity notificacion){
        log.info("Notificacion - registrarNotificacion", notificacion.toString());
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response =  notificacionService.registrarNotificacion( notificacion);
            log.info("Notificacion - registrarNotificacion","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("Service - registrarNotificacion","Ocurrió un error :"+ e.getMessage());
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(path = "/listarNotificacion")
    @ApiOperation(value = "listarNotificacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public pe.gob.serfor.mcsniffs.entity.ResponseEntity listarNotificacion(@RequestBody NotificacionEntity param){
        try{
            return  new pe.gob.serfor.mcsniffs.entity.ResponseEntity(true, "ok", notificacionService.listarNotificacion(param));
        }catch (Exception e){
            log.error(e.getMessage());
            return new pe.gob.serfor.mcsniffs.entity.ResponseEntity(false,e.getMessage(),null);
        }
    }



    @PostMapping(path = "/eliminarNotificacion")
    @ApiOperation(value = "eliminarNotificacion" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarNotificacion(@RequestBody NotificacionEntity param){
        log.info("NotificacionEntity - NotificacionEntity",param.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = notificacionService.EliminarNotificacion(param);
            log.info("NotificacionEntity - NotificacionEntity","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("NotificacionEntity - NotificacionEntity","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
  


 
}
