package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.serfor.mcsniffs.entity.*;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.InformacionBasica.UnidadFisiograficaUMFEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.UnidadFisiograficaService;

@RestController
@RequestMapping(Urls.unidadFisiografica.BASE)
public class UnidadFisiograficaController extends AbstractRestController{
    
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(UnidadFisiograficaController.class);

    @Autowired
    private UnidadFisiograficaService service;
    
    /**
     * @autor:  Abner Valdez [23-08-2021]
     * @descripción: {Listar ListarUnidadFisiografica}
     * @param: InformacionBasicaEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @GetMapping(path = "/listarUnidadFisiografica/{idInfBasica}")
    @ApiOperation(value = "listarUnidadFisiografica" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarUnidadFisiografica(@PathVariable Integer idInfBasica){
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = service.listarUnidadFisiografica(idInfBasica);
            log.info("UnidadFisiograficaController - ListarUnidadFisiografica","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("UnidadFisiograficaController -listarUnidadFisiografica","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Abner Valdez [25-08-2021]
     * @descripción: {Registrar UnidadFisiograficaArchivo}
     * @param: UnidadFisiograficaEntity
     * @return: ResponseEntity<ResponseVO>
     */
    @PostMapping(path = "/registrarUnidadFisiograficaArchivo")
    @ApiOperation(value = "registrarUnidadFisiograficaArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarUnidadFisiograficaArchivo(@RequestParam(name ="files", required = false) MultipartFile files ,
                                                                                   @RequestParam(required = false) Integer idInfBasica,
                                                                                   @RequestParam(required = false) Integer idUnidadFisiografica,
                                                                                   @RequestParam(required = false) Double area,
                                                                                   @RequestParam(required = false) Double porcentaje,
                                                                                   @RequestParam(required = false) Boolean accion,
                                                                                   @RequestParam(required = false) Boolean file,
                                                                                   @RequestParam(required = false) Integer idUsuarioRegistro) throws Exception{
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            UnidadFisiograficaUMFEntity unidFisiografica = new UnidadFisiograficaUMFEntity();
            InformacionBasicaEntity infBasica = new InformacionBasicaEntity();
            infBasica.setIdInfBasica(idInfBasica);
            unidFisiografica.setInformacionBasica(infBasica);
            unidFisiografica.setIdUnidadFisiografica(idUnidadFisiografica);
            unidFisiografica.setArea(area);
            unidFisiografica.setPorcentaje(porcentaje);
            unidFisiografica.setAccion(accion);
            unidFisiografica.setFile(file);
            unidFisiografica.setIdUsuarioRegistro(idUsuarioRegistro);
            response = service.registrarUnidadFisiograficaArchivo(unidFisiografica,files);
            log.info("UnidadFisiograficaController - RegistrarUnidadFisiograficaArchivo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("UnidadFisiograficaController -RegistrarUnidadFisiograficaArchivo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @autor:  Abner Valdez [26-08-2021]
     * @descripción: {Eliminar eliminarUnidadFisiograficaArchivo}
     * @param: idInfBasica,idUnidadFisiografica,idUsuario
     * @return: ResponseEntity<ResponseVO>
     */
    @GetMapping(path = "/eliminarUnidadFisiograficaArchivo/{idInfBasica}/{idUnidadFisiografica}/{idUsuario}")
    @ApiOperation(value = "eliminarUnidadFisiograficaArchivo" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarUnidadFisiograficaArchivo(@PathVariable Integer idInfBasica,@PathVariable Integer idUnidadFisiografica,@PathVariable Integer idUsuario){
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = service.eliminarUnidadFisiograficaArchivo(idInfBasica,idUnidadFisiografica,idUsuario);
            log.info("UnidadFisiograficaController - eliminarUnidadFisiografiaArchivo","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("UnidadFisiograficaController -eliminarUnidadFisiograficaArchivo","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
