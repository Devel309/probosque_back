package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.Dto.ImpactoAmbientalPmfi.MedidasPmfiDto;
import pe.gob.serfor.mcsniffs.entity.ImpactoAmbientalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ImpactoAmbientalEntity;
import pe.gob.serfor.mcsniffs.entity.Parametro.ImpactoAmbientalDto;
import pe.gob.serfor.mcsniffs.entity.Dto.ImpactoAmbientalPmfi.ImpactoAmbientalPmfiDto;
import pe.gob.serfor.mcsniffs.entity.ResponseEntity;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.ImpactoAmbientalService;

import java.util.List;


@RestController
@RequestMapping(Urls.impactoAmbiental.BASE)
public class ImpactoAmbientalController extends AbstractRestController{

   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(ImpactoAmbientalController.class);

    @Autowired
    private ImpactoAmbientalService impactoAmbientalService;

    @PostMapping(path = "/listarImpactoAmbiental")
    @ApiOperation(value = "listarImpactoAmbiental" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarImpactoAmbiental(@RequestBody ImpactoAmbientalEntity impactoAmbientalEntity){
        log.info("ImpactoAmbiental - listarImpactoAmbiental", impactoAmbientalEntity);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = impactoAmbientalService.ListarImpactoAmbiental(impactoAmbientalEntity);
            log.info("ImpactoAmbiental - ListarImpactoAmbiental","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - ListarImpactoAmbiental","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarImpactoAmbiental")
    @ApiOperation(value = "registrarImpactoAmbiental" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarImpactoAmbiental(@RequestBody List<ImpactoAmbientalDto> impactoAmbientalDtoList){
        log.info("ImpactoAmbiental - registrarImpactoAmbiental", impactoAmbientalDtoList);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = impactoAmbientalService.RegistrarImpactoAmbiental(impactoAmbientalDtoList);
            log.info("ImpactoAmbiental - RegistrarImpactoAmbiental","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - RegistrarImpactoAmbiental","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarImpactoAmbiental")
    @ApiOperation(value = "eliminarImpactoAmbiental" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarImpactoAmbiental(@RequestBody ImpactoAmbientalEntity impactoAmbientalEntity){
        log.info("ImpactoAmbiental - eliminarImpactoAmbiental", impactoAmbientalEntity);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = impactoAmbientalService.EliminarImpactoAmbiental(impactoAmbientalEntity);
            log.info("ImpactoAmbiental - EliminarImpactoAmbiental","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - EliminarImpactoAmbiental","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/eliminarImpactoAmbientalDetalle")
    @ApiOperation(value = "eliminarImpactoAmbientalDetalle" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity eliminarImpactoAmbientalDetalle(@RequestBody ImpactoAmbientalDetalleEntity impactoAmbientalDetalleEntity){
        log.info("ImpactoAmbiental - eliminarImpactoAmbientalDetalle", impactoAmbientalDetalleEntity);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = impactoAmbientalService.EliminarImpactoAmbientalDetalle(impactoAmbientalDetalleEntity);
            log.info("ImpactoAmbiental - EliminarImpactoAmbientalDetalle","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - EliminarImpactoAmbientalDetalle","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/listarImpactoAmbientalPmfi")
    @ApiOperation(value = "listarImpactoAmbientalPmfi" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity listarImpactoAmbientalPmfi(@RequestBody MedidasPmfiDto medidasPmfiDto){
        log.info("ImpactoAmbiental - listarImpactoAmbientalPmfi", medidasPmfiDto);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = impactoAmbientalService.ListarImpactoAmbientalPmfi(medidasPmfiDto);
            log.info("ImpactoAmbiental - ListarImpactoAmbientalPmfi","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - ListarImpactoAmbientalPmfi","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarImpactoAmbientalPmfi")
    @ApiOperation(value = "registrarImpactoAmbientalPmfi" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public org.springframework.http.ResponseEntity registrarImpactoAmbientalPmfi(@RequestBody ImpactoAmbientalPmfiDto impactoAmbientalPmfiDto){
        log.info("ImpactoAmbiental - registrarImpactoAmbientalPmfi", impactoAmbientalPmfiDto);
        ResponseEntity result = null;
        ResultClassEntity response ;
        try{
            response = impactoAmbientalService.RegistrarImpactoAmbientalPmfi(impactoAmbientalPmfiDto);
            log.info("ImpactoAmbiental - RegistrarImpactoAmbientalPmfi","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            log.error("service - RegistrarImpactoAmbientalPmfi","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new org.springframework.http.ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
