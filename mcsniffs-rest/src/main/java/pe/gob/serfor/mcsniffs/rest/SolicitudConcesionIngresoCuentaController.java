package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionIngresoCuentaDto;
import pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion.SolicitudConcesionResponsableDto;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionIngresoCuentaService;
import pe.gob.serfor.mcsniffs.service.SolicitudConcesionResponsableService;

import java.util.Arrays;
import java.util.List;


@RestController
@RequestMapping(Urls.ingresoCuentaSolicitudConcesion.BASE)
public class SolicitudConcesionIngresoCuentaController extends AbstractRestController {
    private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(SolicitudConcesionIngresoCuentaController.class);

    @Autowired
    private SolicitudConcesionIngresoCuentaService solicitudConcesionIngresoCuentaService;

    @PostMapping(path = "/listarSolicitudConcesionIngresoCuenta")
    @ApiOperation(value = "listar Solicitud Concesion Ingreso Cuenta" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity listarSolicitudConcesionIngresoCuenta(@RequestBody SolicitudConcesionIngresoCuentaDto request) {
        log.info("SolicitudConcesionIngresoCuenta - listarSolicitudConcesionIngresoCuenta", request.toString());
        ResultClassEntity response = new ResultClassEntity<>();
        try {
            response = solicitudConcesionIngresoCuentaService.listarSolicitudConcesionIngresoCuenta(request);
            log.info("SolicitudConcesionIngresoCuenta - listarSolicitudConcesionIngresoCuenta", "Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("SolicitudConcesionIngresoCuenta - listarSolicitudConcesionIngresoCuenta", "Ocurrió un error : " + e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/registrarSolicitudConcesionIngresoCuenta")
    @ApiOperation(value = "Registrar  Solicitud Concesión Ingreso Cuenta" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarSolicitudConcesionIngresoCuenta(@RequestBody List<SolicitudConcesionIngresoCuentaDto> request){
        log.info("SolicitudConcesionIngresoCuenta - registrarSolicitudConcesionIngresoCuenta",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = solicitudConcesionIngresoCuentaService.registrarSolicitudConcesionIngresoCuenta(request);
            log.info("SolicitudConcesionIngresoCuenta - registrarSolicitudConcesionIngresoCuenta","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("SolicitudConcesionIngresoCuenta - registrarSolicitudConcesionIngresoCuenta","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping(path = "/eliminarSolicitudConcesionIngresoCuenta")
    @ApiOperation(value = "Eliminar  Solicitud Concesión Ingreso Cuenta" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity eliminarSolicitudConcesionIngresoCuenta(@RequestBody SolicitudConcesionIngresoCuentaDto request){
        log.info("SolicitudConcesionIngresoCuenta - eliminarSolicitudConcesionIngresoCuenta",request.toString());

        ResultClassEntity response = new ResultClassEntity<>();

        try{
            response = solicitudConcesionIngresoCuentaService.eliminarSolicitudConcesionIngresoCuenta(request);
            log.info("SolicitudConcesionIngresoCuenta - eliminarSolicitudConcesionIngresoCuenta","Proceso realizado correctamente");
            if (!response.getSuccess()) {
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (Exception e){
            log.error("SolicitudConcesionIngresoCuenta - eliminarSolicitudConcesionIngresoCuenta","Ocurrió un error :"+ e.getMessage());
            response.setSuccess(Constantes.STATUS_ERROR);
            response.setMessage(Constantes.MESSAGE_ERROR_500);
            response.setStackTrace(Arrays.toString(e.getStackTrace()));
            response.setMessageExeption(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

}
