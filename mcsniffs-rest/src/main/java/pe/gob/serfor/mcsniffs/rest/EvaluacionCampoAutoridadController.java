package pe.gob.serfor.mcsniffs.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.serfor.mcsniffs.entity.Dto.EvaluacionCampoAutoridad.EvaluacionCampoAutoridadDto;
import pe.gob.serfor.mcsniffs.entity.Parametro.Dropdown;
import pe.gob.serfor.mcsniffs.entity.ResultClassEntity;
import pe.gob.serfor.mcsniffs.repository.util.Constantes;
import pe.gob.serfor.mcsniffs.repository.util.Urls;
import pe.gob.serfor.mcsniffs.service.EvaluacionCampoAutoridadService;
import pe.gob.serfor.mcsniffs.service.OrdenamientoAreaManejoService;

import java.util.List;

@RestController
@RequestMapping(Urls.evaluacionCampoAutoridad.BASE)
public class EvaluacionCampoAutoridadController extends AbstractRestController{
   private static final org.apache.logging.log4j.Logger log = LogManager.getLogger(EvaluacionAmbientalController.class);

    @Autowired
    private EvaluacionCampoAutoridadService evaluacionCampoAutoridadService;
    /**
     * @autor: IVAN MINAYA [13-09-2021]
     * @modificado:
     * @descripción: {Combo por filtro de autoridad}
     * @param:
     */
    @PostMapping(path = "/comboPorFiltroAutoridad")
    @ApiOperation(value = "comboPorFiltroAutoridad", authorizations = @Authorization(value = "JWT"))
    @ApiResponses({@ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity<ResultClassEntity> comboPorFiltroAutoridad(@RequestBody EvaluacionCampoAutoridadDto request){
        ResultClassEntity result = new ResultClassEntity();
        try {
            result = evaluacionCampoAutoridadService.ComboPorFiltroAutoridad(request);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception Ex){
            log.error("EvaluacionCampoAutoridad - comboPorFiltroAutoridad", "Ocurrió un error al generar en: "+Ex.getMessage());
            result.setInnerException("Ocurrió un error.");
            return new ResponseEntity<>(result,HttpStatus.FORBIDDEN);
        }
    }

    /**
     * @autor:  Ivan Minaya [09-06-2021]
     * @descripción: {Registrar Evaluación de Campo Autoridad}
     * @param: List<EvaluacionCampoAutoridadDto>
     * @return: ResultClassEntity<ResponseVO>
     */
    @PostMapping(path = "/registrarEvaluacionCampoAutoridad")
    @ApiOperation(value = "registrarEvaluacionCampoAutoridad" , authorizations = {@Authorization(value="JWT")})
    @ApiResponses({ @ApiResponse(code = 200, message = "Resuelto correctamente"), @ApiResponse(code = 404, message = "No encontrado") })
    public ResponseEntity registrarEvaluacionCampoAutoridad(@RequestBody List<EvaluacionCampoAutoridadDto> list){
        log.info("OrdenamientoAreaManejo - RegistrarEvaluacionCampoAutoridad",list.toString());
        pe.gob.serfor.mcsniffs.entity.ResponseEntity result = null;
        ResultClassEntity response ;
        try{

            response = evaluacionCampoAutoridadService.RegistrarEvaluacionCampoAutoridad(list);
            log.info("EvaluacionCampoAutoridad - RegistrarEvaluacionCampoAutoridad","Proceso realizado correctamente");
            return new org.springframework.http.ResponseEntity(response, HttpStatus.OK);


        }catch (Exception e){
            log.error("EvaluacionCampoAutoridad -RegistrarEvaluacionCampoAutoridad","Ocurrió un error :"+ e.getMessage());
            result = buildResponse(Constantes.STATUS_ERROR, null, Constantes.MESSAGE_ERROR_500, e);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}
