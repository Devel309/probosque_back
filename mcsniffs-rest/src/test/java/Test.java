import java.sql.Timestamp;
import java.time.Instant;

import org.apache.commons.io.FilenameUtils;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
//		Integer codigo = (int)(100000 * Math.random());
//		
//		System.out.println(codigo);
//		
//		String str = "hola";
//		System.out.println(str.charAt(3));
		
		String result = java.util.UUID.randomUUID().toString();

		result = result.replaceAll("-", "");
		result = result.substring(0, 32);

	    System.out.println( result);
	    
	    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	    
	    Instant instant = timestamp.toInstant();
        System.out.println(instant);

        //return number of milliseconds since the epoch of 1970-01-01T00:00:00Z
        System.out.println(instant.toEpochMilli());// 1598463853796 1598463872921
        
        
        String extension = FilenameUtils.getExtension("hola-asas.como.pdf");
        System.out.println(extension);
	}

}
