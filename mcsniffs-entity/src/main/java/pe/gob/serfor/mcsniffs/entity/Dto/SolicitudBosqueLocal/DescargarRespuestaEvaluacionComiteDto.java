package pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import lombok.Data;
import java.io.Serializable;

@Data
public class DescargarRespuestaEvaluacionComiteDto extends AuditoriaEntity implements Serializable{
    private Integer idSolBosqueLocal;
    private Boolean evalGabineteObservado;
    private Boolean evalCampoObservado;
    //private String codigoSeccion;
    //private String codigoSubSeccion;
}
