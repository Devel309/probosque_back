package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

import java.util.List;

public class ZonaEntity extends AuditoriaEntity implements Serializable {
    private Integer idZona;
    private Integer idPlanManejo;
    private String codigoZona;
    private Integer idZonaPadre;
    private String nombre;
    private Double total;
    private Double porcentaje;
    private List<ZonaAnexoEntity> zonaAnexo;
    
    public Integer getIdZona() {
        return idZona;
    }
    public void setIdZona(Integer idZona) {
        this.idZona = idZona;
    }
    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }
    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }
    public String getCodigoZona() {
        return codigoZona;
    }
    public void setCodigoZona(String codigoZona) {
        this.codigoZona = codigoZona;
    }
    public Integer getIdZonaPadre() {
        return idZonaPadre;
    }
    public void setIdZonaPadre(Integer idZonaPadre) {
        this.idZonaPadre = idZonaPadre;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public Double getTotal() {
        return total;
    }
    public void setTotal(Double total) {
        this.total = total;
    }
    public Double getPorcentaje() {
        return porcentaje;
    }
    public void setPorcentaje(Double porcentaje) {
        this.porcentaje = porcentaje;
    }
    public List<ZonaAnexoEntity> getZonaAnexo() {
        return zonaAnexo;
    }
    public void setZonaAnexo(List<ZonaAnexoEntity> zonaAnexo) {
        this.zonaAnexo = zonaAnexo;
    }  
}
