package pe.gob.serfor.mcsniffs.entity.Dto.EvaluacionCampoAutoridad;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;

@Data
public class EvaluacionCampoAutoridadDto extends AuditoriaEntity implements Serializable {

    Integer idEvaluacionCampoAutoridad;
    Integer idEvaluacionCampo;
    String idTipoAutoridad;
    String tipoAutoridad;
    Integer idParametroPadre;
    Boolean activo;
}