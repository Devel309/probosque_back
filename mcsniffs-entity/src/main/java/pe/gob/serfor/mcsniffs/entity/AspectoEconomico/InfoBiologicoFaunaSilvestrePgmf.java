package pe.gob.serfor.mcsniffs.entity.AspectoEconomico;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;


@Data
public class InfoBiologicoFaunaSilvestrePgmf extends AuditoriaEntity{
    Integer idInfobioFaunaSilvestre ;
    Integer idPlanManejo ;
    Integer idEspecieForestal; 
    String nombreComun ;
    String nombreCientifico;
    String categoriaAmenaza;
    String nombreNativo;
    String cites;
    String observacion;
    String estatus ;
}
