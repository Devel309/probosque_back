package pe.gob.serfor.mcsniffs.entity;

public class AnexosPFDMAdjuntosEntity {
    private Integer IdPostulacionPFDM;
    private Integer IdDocumentoAdjunto;
    private Integer IdUsuarioRegistro;
    
    public Integer getIdPostulacionPFDM() {
        return IdPostulacionPFDM;
    }
    public void setIdPostulacionPFDM(Integer idPostulacionPFDM) {
        IdPostulacionPFDM = idPostulacionPFDM;
    }
    public Integer getIdDocumentoAdjunto() {
        return IdDocumentoAdjunto;
    }
    public void setIdDocumentoAdjunto(Integer idDocumentoAdjunto) {
        IdDocumentoAdjunto = idDocumentoAdjunto;
    }
    public Integer getIdUsuarioRegistro() {
        return IdUsuarioRegistro;
    }
    public void setIdUsuarioRegistro(Integer idUsuarioRegistro) {
        IdUsuarioRegistro = idUsuarioRegistro;
    }
    
    
}
