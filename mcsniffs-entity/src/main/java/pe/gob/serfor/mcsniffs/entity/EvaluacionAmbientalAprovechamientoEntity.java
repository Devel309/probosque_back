package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EvaluacionAmbientalAprovechamientoEntity extends AuditoriaEntity implements Serializable {
    private Integer idEvalAprovechamiento;
    private String codTipoAprovechamiento;
    private String subCodTipoAprovechamiento;

    private String tipoAprovechamiento;
    private String tipoNombreAprovechamiento;
    private String nombreAprovechamiento;
    private String impacto;
    private String medidasControl;
    private String medidasMonitoreo;
    private String frecuencia;
    private String acciones;
    private String responsable;
    private String responsableSecundario;
    private String contingencia;
    private String auxiliar;
    private Integer idPlanManejo;
    private String accion;
}
