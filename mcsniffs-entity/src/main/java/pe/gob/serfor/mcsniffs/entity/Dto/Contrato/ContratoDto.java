package pe.gob.serfor.mcsniffs.entity.Dto.Contrato;

import java.util.Date;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
public class ContratoDto extends AuditoriaEntity {

    private Integer idProcesoOferta;
    private Integer documentoGestion;
    private Integer idUsuarioPostulacion;
    private String nombreUsuarioPostulacion;
    private Integer idStatusProcesoPostulacion;
    private String nombreStatusProcesoPostulacion;
    private Date fechaPostulacion;
    private Integer notaTotal;
    private Boolean ganador;
    private Integer idResultadoPP;
    private String tipoDocumentoGestion;
    private String tipoDocumentoGestionDescripcion;
    private String estadoTexto;




    private String codigoTipoPersonaPostulante;
    private String codigoTipoDocumentoPostulante;
    private String numeroDocumentoPostulante;
    private String nombrePostulante;
    private String correoPostulante;
    private String domicilio;
    private String departamento;
    private String provincia;
    private String distrito;
    private String codigoTipoPersonaRepresentante;
    private String codigoTipoDocumentoRepresentante;
    private String numeroDocumentoRepresentante;
    private String nombreRepresentante;
    private String apellidoPaternoPostulante;
    private String apellidoMaternoPostulante;
    private String apellidoPaternoRepresentante;
    private String apellidoMaternoRepresentante;
}
