package pe.gob.serfor.mcsniffs.entity;

public class IngresosCuentasEntity {
    //para obtener el anexo, orden : value1
    private String TipoNumeroCodigoDoc;
    //para obtener el anexo, orden : value2
    private String Descripcion;
    //para obtener el anexo, orden : value3
    private String Monto;
    //para obtener el anexo, orden : value4
    private String IdDocumentoAdjunto;
    
    public String getTipoNumeroCodigoDoc() {
        return TipoNumeroCodigoDoc;
    }
    public void setTipoNumeroCodigoDoc(String tipoNumeroCodigoDoc) {
        TipoNumeroCodigoDoc = tipoNumeroCodigoDoc;
    }
    public String getDescripcion() {
        return Descripcion;
    }
    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }
    public String getIdDocumentoAdjunto() {
        return IdDocumentoAdjunto;
    }
    public void setIdDocumentoAdjunto(String idDocumentoAdjunto) {
        IdDocumentoAdjunto = idDocumentoAdjunto;
    }
    public String getMonto() {
        return Monto;
    }
    public void setMonto(String monto) {
        Monto = monto;
    }

    
}
