package pe.gob.serfor.mcsniffs.entity.Dto.PlanManejo;


public class PlanManejoGeometriaActualizarCatastroDto {
    private Integer idPlanManejoGeometria;
    private Integer idPlanManejo;
    private Integer idUsuarioRegistro;
    private String idsCatastro;
    public Integer getIdPlanManejoGeometria() {
        return idPlanManejoGeometria;
    }
    public void setIdPlanManejoGeometria(Integer idPlanManejoGeometria) {
        this.idPlanManejoGeometria = idPlanManejoGeometria;
    }
    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }
    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }
    public Integer getIdUsuarioRegistro() {
        return idUsuarioRegistro;
    }
    public void setIdUsuarioRegistro(Integer idUsuarioRegistro) {
        this.idUsuarioRegistro = idUsuarioRegistro;
    }
    public String getIdsCatastro() {
        return idsCatastro;
    }
    public void setIdsCatastro(String idsCatastro) {
        this.idsCatastro = idsCatastro;
    }

    
}
