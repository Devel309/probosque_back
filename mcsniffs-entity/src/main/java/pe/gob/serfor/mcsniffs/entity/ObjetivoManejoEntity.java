package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ObjetivoManejoEntity extends AuditoriaEntity implements Serializable {
    private Integer idObjManejo;
    private String general;
    private Integer idPlanManejo;
    private String codigoObjetivo;
    private PlanManejoEntity planManejo;
    private String detalle;
    private String observacion;
    private String descripcion;
    private List<ObjetivoEspecificoManejoEntity> listDetalle;

    
    
    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<ObjetivoEspecificoManejoEntity> getListDetalle() {
        return listDetalle;
    }

    public void setListDetalle(List<ObjetivoEspecificoManejoEntity> listDetalle) {
        this.listDetalle = listDetalle;
    }

    public String getCodigoObjetivo() {
        return codigoObjetivo;
    }

    public void setCodigoObjetivo(String codigoObjetivo) {
        this.codigoObjetivo = codigoObjetivo;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public Integer getIdObjManejo() {
        return idObjManejo;
    }

    public void setIdObjManejo(Integer idObjManejo) {
        this.idObjManejo = idObjManejo;
    }

    public String getGeneral() {
        return general;
    }

    public void setGeneral(String general) {
        this.general = general;
    }

    public PlanManejoEntity getPlanManejo() {
        return planManejo;
    }

    public void setPlanManejo(PlanManejoEntity planManejo) {
        this.planManejo = planManejo;
    }
}
