package pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal;

import lombok.Data;

@Data
public class SolicitudPlantacionForestalEntity {

    private Integer idSolPlantForest;
    private String codigoModalidad;
    private Integer idUsuarioRegistro;
    private Integer idContrato;
}
