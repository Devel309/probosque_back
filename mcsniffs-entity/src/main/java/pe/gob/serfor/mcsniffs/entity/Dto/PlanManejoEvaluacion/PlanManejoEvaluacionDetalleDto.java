package pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class PlanManejoEvaluacionDetalleDto extends AuditoriaEntity implements Serializable {

    private Integer idPlanManejoEvaluacion;
    private Integer idPlanManejoEvalDet;
    private Boolean conforme;
    private String codigoTipo;
    private String observacion;
    private String tituloHabilitante;
    private String estadoValidacion;
    private String codigoTramite;
    private Date fechaRecepcion;
    private Integer idTipoParametro;
    private String descripcion;
    private Integer idArchivo;
    private String descArchivo;
    private String conformidad;
    private List<PlanManejoEvaluacionSubDetalleDto>  lstPlanManejoEvaluacionSubDetalle;

}
