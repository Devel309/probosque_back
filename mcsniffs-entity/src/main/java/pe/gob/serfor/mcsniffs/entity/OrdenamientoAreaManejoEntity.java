package pe.gob.serfor.mcsniffs.entity;

import com.microsoft.sqlserver.jdbc.Geometry;

import java.io.Serializable;

public class OrdenamientoAreaManejoEntity extends AuditoriaEntity implements Serializable {
    private Integer idOrdAreaManejo;
    private String catOrdenamiento;
    private Double area;
    private Double porcentaje;
    private PlanManejoEntity planManejo;
    private Boolean accion;
    private Boolean file;
    private String descripcion;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getFile() {
        return file;
    }

    public void setFile(Boolean file) {
        this.file = file;
    }

    public Integer getIdOrdAreaManejo() {
        return idOrdAreaManejo;
    }

    public void setIdOrdAreaManejo(Integer idOrdAreaManejo) {
        this.idOrdAreaManejo = idOrdAreaManejo;
    }

    public String getCatOrdenamiento() {
        return catOrdenamiento;
    }

    public void setCatOrdenamiento(String catOrdenamiento) {
        this.catOrdenamiento = catOrdenamiento;
    }
    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public Double getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(Double porcentaje) {
        this.porcentaje = porcentaje;
    }

    public PlanManejoEntity getPlanManejo() {
        return planManejo;
    }

    public void setPlanManejo(PlanManejoEntity planManejo) {
        this.planManejo = planManejo;
    }

    public Boolean getAccion() {
        return accion;
    }

    public void setAccion(Boolean accion) {
        this.accion = accion;
    }
}
