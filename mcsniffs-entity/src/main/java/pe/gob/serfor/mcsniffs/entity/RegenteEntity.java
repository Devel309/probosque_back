package pe.gob.serfor.mcsniffs.entity;

import pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoArchivo.PlanManejoArchivoDto;

import java.io.Serializable;
import java.util.List;

public class RegenteEntity extends AuditoriaEntity implements Serializable {
    private Integer idPlanManejoRegente;
    private String codigoProceso;
    private String codigoTipoRegente;
    private String nombres;
    private String apellidos;
    private String numeroDocumento;
    private String domicilioLegal;
    private String estadoRegente;
    private String numeroLicencia;
    private String periodo;
    private String contratoSuscrito;
    private String certificadoHabilitacion;
    private String numeroInscripcion;
    private String adjuntoContrato;
    private String adjuntoCertificado;
    private Integer idPlanManejo;

    private List<PlanManejoArchivoDto> archivos;

    public List<PlanManejoArchivoDto> getArchivos() {
        return archivos;
    }

    public void setArchivos(List<PlanManejoArchivoDto> archivos) {
        this.archivos = archivos;
    }
    
    public Integer getIdPlanManejoRegente() {
        return idPlanManejoRegente;
    }
    public void setIdPlanManejoRegente(Integer idPlanManejoRegente) {
        this.idPlanManejoRegente = idPlanManejoRegente;
    }
    public String getCodigoProceso() {
        return codigoProceso;
    }
    public void setCodigoProceso(String codigoProceso) {
        this.codigoProceso = codigoProceso;
    }
    public String getCodigoTipoRegente() {
        return codigoTipoRegente;
    }
    public void setCodigoTipoRegente(String codigoTipoRegente) {
        this.codigoTipoRegente = codigoTipoRegente;
    }
    public String getNombres() {
        return nombres;
    }
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
    public String getApellidos() {
        return apellidos;
    }
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    public String getNumeroDocumento() {
        return numeroDocumento;
    }
    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }
    public String getDomicilioLegal() {
        return domicilioLegal;
    }
    public void setDomicilioLegal(String domicilioLegal) {
        this.domicilioLegal = domicilioLegal;
    }
    public String getEstadoRegente() {
        return estadoRegente;
    }
    public void setEstadoRegente(String estadoRegente) {
        this.estadoRegente = estadoRegente;
    }
    public String getNumeroLicencia() {
        return numeroLicencia;
    }
    public void setNumeroLicencia(String numeroLicencia) {
        this.numeroLicencia = numeroLicencia;
    }
    public String getPeriodo() {
        return periodo;
    }
    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }
    public String getContratoSuscrito() {
        return contratoSuscrito;
    }
    public void setContratoSuscrito(String contratoSuscrito) {
        this.contratoSuscrito = contratoSuscrito;
    }
    public String getCertificadoHabilitacion() {
        return certificadoHabilitacion;
    }
    public void setCertificadoHabilitacion(String certificadoHabilitacion) {
        this.certificadoHabilitacion = certificadoHabilitacion;
    }
    public String getNumeroInscripcion() {
        return numeroInscripcion;
    }
    public void setNumeroInscripcion(String numeroInscripcion) {
        this.numeroInscripcion = numeroInscripcion;
    }
    public String getAdjuntoContrato() {
        return adjuntoContrato;
    }
    public void setAdjuntoContrato(String adjuntoContrato) {
        this.adjuntoContrato = adjuntoContrato;
    }
    public String getAdjuntoCertificado() {
        return adjuntoCertificado;
    }
    public void setAdjuntoCertificado(String adjuntoCertificado) {
        this.adjuntoCertificado = adjuntoCertificado;
    }
    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }
    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    
}
