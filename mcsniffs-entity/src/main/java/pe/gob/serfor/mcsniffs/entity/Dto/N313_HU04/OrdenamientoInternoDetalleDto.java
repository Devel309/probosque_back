package pe.gob.serfor.mcsniffs.entity.Dto.N313_HU04;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.math.BigDecimal;

public class OrdenamientoInternoDetalleDto extends AuditoriaEntity implements Serializable {
    private Integer idOrdenamientoProteccion;
    private String codTipoOrdenamiento;
    private String anexo;
    private Boolean esSistemaPoliciclico;
    private String justificacion;
    private Integer idOrdenamientoProteccionDet;
    private String codigoTipoOrdenamientoDet;
    private String categoria;
    private String actividad;
    private String meta;
    private BigDecimal areaHA;
    private BigDecimal areaHAPorcentaje;
    private String descripcion;
    private String observacion;
    private String verticeBloque;
    private BigDecimal coordenadaEste;
    private BigDecimal coordenadaNorte;
    private String zonaUTM;
    private String observacionDetalle;
    private String usoPotencial;
    private String actividadesRealizar;

    private Integer idPlanManejo;

    public Integer getIdOrdenamientoProteccion() {
        return idOrdenamientoProteccion;
    }

    public void setIdOrdenamientoProteccion(Integer idOrdenamientoProteccion) {
        this.idOrdenamientoProteccion = idOrdenamientoProteccion;
    }

    public String getCodTipoOrdenamiento() {
        return codTipoOrdenamiento;
    }

    public void setCodTipoOrdenamiento(String codTipoOrdenamiento) {
        this.codTipoOrdenamiento = codTipoOrdenamiento;
    }

    public String getAnexo() {
        return anexo;
    }

    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    public Boolean getEsSistemaPoliciclico() {
        return esSistemaPoliciclico;
    }

    public void setEsSistemaPoliciclico(Boolean esSistemaPoliciclico) {
        this.esSistemaPoliciclico = esSistemaPoliciclico;
    }

    public String getJustificacion() {
        return justificacion;
    }

    public void setJustificacion(String justificacion) {
        this.justificacion = justificacion;
    }

    public Integer getIdOrdenamientoProteccionDet() {
        return idOrdenamientoProteccionDet;
    }

    public void setIdOrdenamientoProteccionDet(Integer idOrdenamientoProteccionDet) {
        this.idOrdenamientoProteccionDet = idOrdenamientoProteccionDet;
    }

    public String getCodigoTipoOrdenamientoDet() {
        return codigoTipoOrdenamientoDet;
    }

    public void setCodigoTipoOrdenamientoDet(String codigoTipoOrdenamientoDet) {
        this.codigoTipoOrdenamientoDet = codigoTipoOrdenamientoDet;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public BigDecimal getAreaHA() {
        return areaHA;
    }

    public void setAreaHA(BigDecimal areaHA) {
        this.areaHA = areaHA;
    }

    public BigDecimal getAreaHAPorcentaje() {
        return areaHAPorcentaje;
    }

    public void setAreaHAPorcentaje(BigDecimal areaHAPorcentaje) {
        this.areaHAPorcentaje = areaHAPorcentaje;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getVerticeBloque() {
        return verticeBloque;
    }

    public void setVerticeBloque(String verticeBloque) {
        this.verticeBloque = verticeBloque;
    }

    public BigDecimal getCoordenadaEste() {
        return coordenadaEste;
    }

    public void setCoordenadaEste(BigDecimal coordenadaEste) {
        this.coordenadaEste = coordenadaEste;
    }

    public BigDecimal getCoordenadaNorte() {
        return coordenadaNorte;
    }

    public void setCoordenadaNorte(BigDecimal coordenadaNorte) {
        this.coordenadaNorte = coordenadaNorte;
    }

    public String getZonaUTM() {
        return zonaUTM;
    }

    public void setZonaUTM(String zonaUTM) {
        this.zonaUTM = zonaUTM;
    }

    public String getObservacionDetalle() {
        return observacionDetalle;
    }

    public void setObservacionDetalle(String observacionDetalle) {
        this.observacionDetalle = observacionDetalle;
    }

    public String getUsoPotencial() {
        return usoPotencial;
    }

    public void setUsoPotencial(String usoPotencial) {
        this.usoPotencial = usoPotencial;
    }

    public String getActividadesRealizar() {
        return actividadesRealizar;
    }

    public void setActividadesRealizar(String actividadesRealizar) {
        this.actividadesRealizar = actividadesRealizar;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }
}
