package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class TipoMonedaEntity implements Serializable {
    private short idTipoMoneda;
    private String descripcion;
    private String codigo;
    public short getIdTipoMoneda() { return idTipoMoneda; }
    public void setIdTipoMoneda(short idTipoMoneda) { this.idTipoMoneda = idTipoMoneda; }
    public String getDescripcion() { return descripcion; }
    public void setDescripcion(String descripcion) { this.descripcion = descripcion; }
    public String getCodigo() { return codigo; }
    public void setCodigo(String codigo) { this.codigo = codigo; }

}
