package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ConflictoManejoEntity extends AuditoriaEntity implements Serializable {
    private Integer idConflictoManejo;
    private String conflicto;
    private String solucion;
    private PlanManejoEntity planManejo;

    public Integer getIdConflictoManejo() {
        return idConflictoManejo;
    }

    public void setIdConflictoManejo(Integer idConflictoManejo) {
        this.idConflictoManejo = idConflictoManejo;
    }

    public String getConflicto() {
        return conflicto;
    }

    public void setConflicto(String conflicto) {
        this.conflicto = conflicto;
    }

    public String getSolucion() {
        return solucion;
    }

    public void setSolucion(String solucion) {
        this.solucion = solucion;
    }

    public PlanManejoEntity getPlanManejo() {
        return planManejo;
    }

    public void setPlanManejo(PlanManejoEntity planManejo) {
        this.planManejo = planManejo;
    }
}
