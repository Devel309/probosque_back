package pe.gob.serfor.mcsniffs.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class DescuentoEntity extends AuditoriaEntity implements Serializable {
    private Integer idDescuento;
    private String codigoDescuento;
    private String descDescuento;
    private BigDecimal porcentaje;
    private String observacion;
    private String detalle;
    private String descripcion;
}
