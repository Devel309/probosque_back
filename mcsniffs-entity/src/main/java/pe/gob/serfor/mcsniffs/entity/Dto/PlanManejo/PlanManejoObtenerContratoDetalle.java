package pe.gob.serfor.mcsniffs.entity.Dto.PlanManejo;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlanManejoObtenerContratoDetalle{
  private Integer idPlanManejo;
  private String descripcion;
  private Integer idSolicitud;
  private Integer idTipoProceso;
  private Integer idTipoPlan;
  private Integer idTipoEscala;
  private String aspectoComplementario;
  private Integer idPlanManejoContrato;
  private Integer idContrato;
  private Integer resProcesoPostulacion;
  private String coditoTituloh;
  private String contratoDescripcion;
  private String dniRepresentante;
  private String nombreRepresentante;
  private String codigoPartidaRegistral;
  private String nroContratoConcesion;
  private Boolean firmado;
  private Integer datosValidados;
  private Integer statusProceso;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
  private Timestamp fechaInicioContrato;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
  private Timestamp fechaFinContrato;
  private Integer idContratoDet;
  private String codigoTipo;
  private String contratoDetalle;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
  private Timestamp fechaRegistro;
  
}
