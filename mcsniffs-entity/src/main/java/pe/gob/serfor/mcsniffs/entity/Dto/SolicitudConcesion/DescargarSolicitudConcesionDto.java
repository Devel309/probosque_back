package pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DescargarSolicitudConcesionDto {
    
    private Integer idSolicitudConcesion;
    private Integer tipoAnexo;
    private String lugar;
    private String fecha;
    private List<DescargarSolicitudConcesionVerticesDto> vertices;
}
