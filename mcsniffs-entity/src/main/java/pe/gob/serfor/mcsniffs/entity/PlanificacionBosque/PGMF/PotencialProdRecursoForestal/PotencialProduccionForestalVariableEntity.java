package pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.math.BigDecimal;
import java.util.List;


public class PotencialProduccionForestalVariableEntity extends AuditoriaEntity {

    private Integer idVariable;
    private String variable;
    private BigDecimal nuTotalTipoBosque;
    private BigDecimal totalHa;
    private Boolean accion;
    private BigDecimal dap;
    private BigDecimal subdap;

    public BigDecimal getDap() {
        return dap;
    }

    public void setDap(BigDecimal dap) {
        this.dap = dap;
    }

    public BigDecimal getSubdap() {
        return subdap;
    }

    public void setSubdap(BigDecimal subdap) {
        this.subdap = subdap;
    }

    public Integer getIdVariable() {
        return idVariable;
    }

    public void setIdVariable(Integer idVariable) {
        this.idVariable = idVariable;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public BigDecimal getNuTotalTipoBosque() {
        return nuTotalTipoBosque;
    }

    public void setNuTotalTipoBosque(BigDecimal nuTotalTipoBosque) {
        this.nuTotalTipoBosque = nuTotalTipoBosque;
    }

    public BigDecimal getTotalHa() {
        return totalHa;
    }

    public void setTotalHa(BigDecimal totalHa) {
        this.totalHa = totalHa;
    }

    public Boolean getAccion() {
        return accion;
    }

    public void setAccion(Boolean accion) {
        this.accion = accion;
    }
}
