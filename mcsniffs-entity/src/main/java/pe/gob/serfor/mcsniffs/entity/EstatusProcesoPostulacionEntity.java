package pe.gob.serfor.mcsniffs.entity;
import java.io.Serializable;

public class EstatusProcesoPostulacionEntity implements Serializable{
    private Integer IdProcesoPostulacion;
    private Integer IdStatus;
    private Integer IdUsuarioModificacion;
    private String CorreoPostulante;
    private Integer IdProcesoOferta;
    private Integer idArchivoResolucion;
    private Integer IdPostulante;
    

    
    public Integer getIdProcesoOferta() {
        return IdProcesoOferta;
    }
    public void setIdProcesoOferta(Integer idProcesoOferta) {
        IdProcesoOferta = idProcesoOferta;
    }
    public Integer getIdProcesoPostulacion() {
        return IdProcesoPostulacion;
    }
    public void setIdProcesoPostulacion(Integer idProcesoPostulacion) {
        IdProcesoPostulacion = idProcesoPostulacion;
    }
    public Integer getIdStatus() {
        return IdStatus;
    }
    public void setIdStatus(Integer idStatus) {
        IdStatus = idStatus;
    }
    public Integer getIdUsuarioModificacion() {
        return IdUsuarioModificacion;
    }
    public void setIdUsuarioModificacion(Integer idUsuarioModificacion) {
        IdUsuarioModificacion = idUsuarioModificacion;
    }
    public String getCorreoPostulante() {
        return CorreoPostulante;
    }
    public void setCorreoPostulante(String correoPostulante) {
        CorreoPostulante = correoPostulante;
    }

    public Integer getIdArchivoResolucion() {
        return idArchivoResolucion;
    }

    public void setIdArchivoResolucion(Integer idArchivoResolucion) {
        this.idArchivoResolucion = idArchivoResolucion;
    }

    public Integer getIdPostulante() {
        return IdPostulante;
    }

    public void setIdPostulante(Integer idPostulante) {
        this.IdPostulante = idPostulante;
    }
}
