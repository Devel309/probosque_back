package pe.gob.serfor.mcsniffs.entity;

public class ImpugnacionNuevoGanadorEntity {
    private Integer IdProcesoOferta;
    private Integer IdProcesoPostulacion;
    private Integer IdResultadoPP;
    private Integer NotaTotal;
    private Boolean Ganador;
    private Integer IdStatusPostulacion;
    private String DescripcionStatusProceso;
    private Integer IdSolicitud;
    private String  NombreUsuarioPostulante;
    
    public Integer getIdProcesoOferta() {
        return IdProcesoOferta;
    }
    public void setIdProcesoOferta(Integer idProcesoOferta) {
        IdProcesoOferta = idProcesoOferta;
    }
    public Integer getIdProcesoPostulacion() {
        return IdProcesoPostulacion;
    }
    public void setIdProcesoPostulacion(Integer idProcesoPostulacion) {
        IdProcesoPostulacion = idProcesoPostulacion;
    }
    public Integer getIdResultadoPP() {
        return IdResultadoPP;
    }
    public void setIdResultadoPP(Integer idResultadoPP) {
        IdResultadoPP = idResultadoPP;
    }
    public Integer getNotaTotal() {
        return NotaTotal;
    }
    public void setNotaTotal(Integer notaTotal) {
        NotaTotal = notaTotal;
    }
    public Boolean getGanador() {
        return Ganador;
    }
    public void setGanador(Boolean ganador) {
        Ganador = ganador;
    }
    public Integer getIdStatusPostulacion() {
        return IdStatusPostulacion;
    }
    public void setIdStatusPostulacion(Integer idStatusPostulacion) {
        IdStatusPostulacion = idStatusPostulacion;
    }
    public String getDescripcionStatusProceso() {
        return DescripcionStatusProceso;
    }
    public void setDescripcionStatusProceso(String descripcionStatusProceso) {
        DescripcionStatusProceso = descripcionStatusProceso;
    }
    public Integer getIdSolicitud() {
        return IdSolicitud;
    }
    public void setIdSolicitud(Integer idSolicitud) {
        IdSolicitud = idSolicitud;
    }
    public String getNombreUsuarioPostulante() {
        return NombreUsuarioPostulante;
    }
    public void setNombreUsuarioPostulante(String nombreUsuarioPostulante) {
        NombreUsuarioPostulante = nombreUsuarioPostulante;
    }
    

}
