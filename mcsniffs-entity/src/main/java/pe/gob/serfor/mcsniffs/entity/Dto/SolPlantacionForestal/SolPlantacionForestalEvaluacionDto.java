package pe.gob.serfor.mcsniffs.entity.Dto.SolPlantacionForestal;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.util.Date;

@Data
public class SolPlantacionForestalEvaluacionDto  extends AuditoriaEntity implements Serializable {

    private Integer idSolPlantacionForestalEvaluacion;
    private Integer idSolPlantacionForestal;
    private Date fechaInicio;
    private Date fechaFin;
    private Integer diasEvaluacion;
    private String estado;
    private String codigoResultadoEvaluacion;
    private String observacion;
    private Boolean comunicadoEnviado;
    private String strFechaInicio;
    private String strFechaFin;
    private Integer idArchivoCertificado;
}
