package pe.gob.serfor.mcsniffs.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class DivisionAdministrativaEntity  implements Serializable {

    private String fuente;
    private String docreg ;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date   fecreg;
    private String observ;
    private String zonutm;
    private String origen;
    private String numpc;
    private String tipoth;
    private String nrotth;
    private String numblo;
    private String docleg;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fecleg;
    private String supafp;
    private String nomrgt;
    private String estado;
    private String tiempo;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fecest;
    private String numrgt;
    private Integer idPlanManejo;
}
