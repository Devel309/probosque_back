package pe.gob.serfor.mcsniffs.entity;
import java.io.Serializable;

public class CapacitacionEntity extends AuditoriaEntity implements Serializable {

    private Integer idCapacitacion;
    private String codTipoCapacitacion;
    private String tema;
    private String personaCapacitar;
    private String idTipoModalidad;
    private String lugar;
    private String periodo;
    private String responsable;
    private String adjunto;
    private PlanManejoEntity planManejo;
    private Integer idPlanManejo;

    public Integer getIdCapacitacion() {
        return idCapacitacion;
    }

    public void setIdCapacitacion(Integer idCapacitacion) {
        this.idCapacitacion = idCapacitacion;
    }

    public String getCodTipoCapacitacion() {
        return codTipoCapacitacion;
    }

    public void setCodTipoCapacitacion(String codTipoCapacitacion) {
        this.codTipoCapacitacion = codTipoCapacitacion;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public String getPersonaCapacitar() {
        return personaCapacitar;
    }

    public void setPersonaCapacitar(String personaCapacitar) {
        this.personaCapacitar = personaCapacitar;
    }

    public String getIdTipoModalidad() {
        return idTipoModalidad;
    }

    public void setIdTipoModalidad(String idTipoModalidad) {
        this.idTipoModalidad = idTipoModalidad;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public PlanManejoEntity getPlanManejo() {
        return planManejo;
    }

    public void setPlanManejo(PlanManejoEntity planManejo) {
        this.planManejo = planManejo;
    }

    public String getAdjunto() {
        return adjunto;
    }

    public void setAdjunto(String adjunto) {
        this.adjunto = adjunto;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public CapacitacionEntity setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
        return this;
    }
}