package pe.gob.serfor.mcsniffs.entity;

public class AnexosPFDMEntity extends AuditoriaEntity{
    private Integer IdAnexosPFDM;
    private String CodigoAnexo;
    private Boolean Grilla;
    private String CodGrilla;
    private String Descripcion;
    private String Value1;
    private String Value2;
    private String Value3;
    private String Value4;
    private String Value5;
    private String Value6;
    private Integer IdPostulacionPFDM;
    private Integer IdUsuarioPostulacion;
    
    public Integer getIdAnexosPFDM() {
        return IdAnexosPFDM;
    }
    public void setIdAnexosPFDM(Integer idAnexosPFDM) {
        IdAnexosPFDM = idAnexosPFDM;
    }
    public String getCodigoAnexo() {
        return CodigoAnexo;
    }
    public void setCodigoAnexo(String codigoAnexo) {
        CodigoAnexo = codigoAnexo;
    }
    public Boolean getGrilla() {
        return Grilla;
    }
    public void setGrilla(Boolean grilla) {
        Grilla = grilla;
    }
    public String getCodGrilla() {
        return CodGrilla;
    }
    public void setCodGrilla(String codGrilla) {
        CodGrilla = codGrilla;
    }
    public String getDescripcion() {
        return Descripcion;
    }
    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }
    public String getValue1() {
        return Value1;
    }
    public void setValue1(String value1) {
        Value1 = value1;
    }
    public String getValue2() {
        return Value2;
    }
    public void setValue2(String value2) {
        Value2 = value2;
    }
    public String getValue3() {
        return Value3;
    }
    public void setValue3(String value3) {
        Value3 = value3;
    }
    public String getValue4() {
        return Value4;
    }
    public void setValue4(String value4) {
        Value4 = value4;
    }
    public String getValue5() {
        return Value5;
    }
    public void setValue5(String value5) {
        Value5 = value5;
    }
    public String getValue6() {
        return Value6;
    }
    public void setValue6(String value6) {
        Value6 = value6;
    }
    public Integer getIdPostulacionPFDM() {
        return IdPostulacionPFDM;
    }
    public void setIdPostulacionPFDM(Integer idPostulacionPFDM) {
        IdPostulacionPFDM = idPostulacionPFDM;
    }
    public Integer getIdUsuarioPostulacion() {
        return IdUsuarioPostulacion;
    }
    public void setIdUsuarioPostulacion(Integer idUsuarioPostulacion) {
        IdUsuarioPostulacion = idUsuarioPostulacion;
    }

    
}
