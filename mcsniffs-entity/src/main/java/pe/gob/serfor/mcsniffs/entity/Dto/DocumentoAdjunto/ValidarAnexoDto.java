package pe.gob.serfor.mcsniffs.entity.Dto.DocumentoAdjunto;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ValidarAnexoDto {

   
    private Integer idProcesoPostulacion;
    private String descripcion;
    private Boolean devolucion;  
    private String observacion; 

}
