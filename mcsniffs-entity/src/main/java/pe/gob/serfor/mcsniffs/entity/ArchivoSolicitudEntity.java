package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ArchivoSolicitudEntity extends AuditoriaEntity implements Serializable {
    private Integer idArchivo;
    private Integer idSolicitud;
    private Integer idArchivoSolicitud;
    private String tipoDocumento;
    private String nombreArchivo;
    private String extensionArchivo;
    private String rutaArchivo;
    private String nombreGenerado;
    private String descripcionArchivo;
    private byte[] file;

    public Integer getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }

    public Integer getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(Integer idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public Integer getIdArchivoSolicitud() {
        return idArchivoSolicitud;
    }

    public void setIdArchivoSolicitud(Integer idArchivoSolicitud) {
        this.idArchivoSolicitud = idArchivoSolicitud;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getExtensionArchivo() {
        return extensionArchivo;
    }

    public void setExtensionArchivo(String extensionArchivo) {
        this.extensionArchivo = extensionArchivo;
    }

    public String getRutaArchivo() {
        return rutaArchivo;
    }

    public void setRutaArchivo(String rutaArchivo) {
        this.rutaArchivo = rutaArchivo;
    }

    public String getNombreGenerado() {
        return nombreGenerado;
    }

    public void setNombreGenerado(String nombreGenerado) {
        this.nombreGenerado = nombreGenerado;
    }

    public String getDescripcionArchivo() {
        return descripcionArchivo;
    }

    public void setDescripcionArchivo(String descripcionArchivo) {
        this.descripcionArchivo = descripcionArchivo;
    }
}
