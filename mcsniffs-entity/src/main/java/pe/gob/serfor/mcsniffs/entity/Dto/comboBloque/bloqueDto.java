package pe.gob.serfor.mcsniffs.entity.Dto.comboBloque;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter; 

@Getter
@Setter
public class bloqueDto {
    private Integer Id;
    private String ET_FromAtt;
    private String ET_ToAtt;

    private String ET_ID;
    private Integer escale;
    private Integer ORDEN;

    private String ESC;
    private Integer OBJECTID;
    private BigDecimal area;
}
