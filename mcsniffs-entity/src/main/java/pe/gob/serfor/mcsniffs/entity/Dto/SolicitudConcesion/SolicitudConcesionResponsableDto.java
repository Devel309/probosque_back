package pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResponsableFormacionAcademicaEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionArchivoEntity;

import java.io.Serializable;
import java.util.List;

@Data
public class SolicitudConcesionResponsableDto extends AuditoriaEntity implements Serializable {

    private Integer idSolicitudConcesionResponsable;
    private Integer idSolicitudConcesion;
    private String nombres;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String codigoTipoDocumento;
    private String numeroDocumento;
    private String sector;
    private Integer ubigeo;
    private String telefonoFijo;
    private String telefonoCelular;
    private String email;
    private String profesion;
    private String numeroColegiatura;

    private Integer idDepartamento;
    private Integer idProvincia;
    private Integer idDistrito;
    private String nombreDepartamento;
    private String nombreProvincia;
    private String nombreDistrito;
    private String direccion;
    private List<ResponsableFormacionAcademicaEntity> lstResponsableFormacionAcademica;
    private List<SolicitudConcesionArchivoEntity> lstResponsableFormacionAcademicaArchivo;

    private List<ResponsableExperienciaLaboralDto> lstResponsableExperienciaLaboral;
    private List<SolicitudConcesionArchivoEntity> lstResponsableExperienciaLaboralArchivo;
}
