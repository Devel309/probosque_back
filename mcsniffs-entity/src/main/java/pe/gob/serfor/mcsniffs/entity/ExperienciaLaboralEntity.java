package pe.gob.serfor.mcsniffs.entity;

public class ExperienciaLaboralEntity {
    //para obtener el anexo, orden : value1
    private String entidadEmpresaOtros;
    //para obtener el anexo, orden : value2
    private String cargo;
    //para obtener el anexo, orden : value3
    private String tiempoanios;
    //para obtener el anexo, orden : value4
    private String descActvRealizadas;
    //para obtener el anexo, orden : value5
    private String datosContactoEmpresa;
    
    public String getEntidadEmpresaOtros() {
        return entidadEmpresaOtros;
    }
    public void setEntidadEmpresaOtros(String entidadEmpresaOtros) {
        this.entidadEmpresaOtros = entidadEmpresaOtros;
    }
    public String getCargo() {
        return cargo;
    }
    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
    public String getTiempoanios() {
        return tiempoanios;
    }
    public void setTiempoanios(String tiempoanios) {
        this.tiempoanios = tiempoanios;
    }
    public String getDescActvRealizadas() {
        return descActvRealizadas;
    }
    public void setDescActvRealizadas(String descActvRealizadas) {
        this.descActvRealizadas = descActvRealizadas;
    }
    public String getDatosContactoEmpresa() {
        return datosContactoEmpresa;
    }
    public void setDatosContactoEmpresa(String datosContactoEmpresa) {
        this.datosContactoEmpresa = datosContactoEmpresa;
    }

    
}
