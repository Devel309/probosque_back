package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class TipoBosqueEntity implements Serializable{
    private Short idTipoBosque;
    private String descripcion;
    private String region;

    public Short getIdTipoBosque() {
        return idTipoBosque;
    }
    public void setIdTipoBosque(Short idTipoBosque) {
        this.idTipoBosque = idTipoBosque;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String getRegion() {
        return region;
    }
    public void setRegion(String region) {
        this.region = region;
    }

}

