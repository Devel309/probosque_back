package pe.gob.serfor.mcsniffs.entity;
import java.io.Serializable;
import java.util.Date;

public class PGMFAbreviadoDetalleRegistrarEntity extends AuditoriaEntity implements Serializable {
    private Number idPGMF;
    private Integer idOrganizacionManejo;
    private String codigoSeccion;
    private String codigoTipoDetalle;
    private String CodigoSubTipoDetalle;
    private String descripcion;
    private String mecanismoParticipacion;
    private String metodologia;
    private String lugar;
    private Date fecha;
    private String personalCapacitar; 
    private String modalidadCapacitar; 
    
    private String medidaControl;
	private String medidaMonitoreo ;
	private String frecuencia ;
	private String responsable;
	private String contingencia;
	private String accionRealizada ;
	private String operacion;
	private String nombreTipoDetalle;
	    
    private String funcion;
    private Integer numero;
    private String maquinaEquipo;
    private String codigoTipoActividad;
    private String tipoActividad;
    private boolean cabecera;


    public PGMFAbreviadoDetalleRegistrarEntity(){}

    
    





    public boolean isCabecera() {
        return cabecera;
    }








    public void setCabecera(boolean cabecera) {
        this.cabecera = cabecera;
    }


    public Integer getIdOrganizacionManejo() {
        return idOrganizacionManejo;
    }

    public void setIdOrganizacionManejo(Integer idOrganizacionManejo) {
        this.idOrganizacionManejo = idOrganizacionManejo;
    }

    public String getFuncion() {
        return funcion;
    }


    public void setFuncion(String funcion) {
        this.funcion = funcion;
    }


    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getMaquinaEquipo() {
        return maquinaEquipo;
    }


    public void setMaquinaEquipo(String maquinaEquipo) {
        this.maquinaEquipo = maquinaEquipo;
    }

    public String getCodigoTipoActividad() {
        return codigoTipoActividad;
    }


    public void setCodigoTipoActividad(String codigoTipoActividad) {
        this.codigoTipoActividad = codigoTipoActividad;
    }

    public String getTipoActividad() {
        return tipoActividad;
    }


    public void setTipoActividad(String tipoActividad) {
        this.tipoActividad = tipoActividad;
    }


    public String getCodigoSeccion() {
        return codigoSeccion;
    }

    public void setCodigoSeccion(String codigoSeccion) {
        this.codigoSeccion = codigoSeccion;
    }

    public Number getIdPGMF() {
        return idPGMF;
    }

    public void setIdPGMF(Number idPGMF) {
        this.idPGMF = idPGMF;
    }

    public String getCodigoTipoDetalle() {
        return codigoTipoDetalle;
    }

    public void setCodigoTipoDetalle(String codigoTipoDetalle) {
        this.codigoTipoDetalle = codigoTipoDetalle;
    }

    public String getCodigoSubTipoDetalle() {
        return CodigoSubTipoDetalle;
    }

    public void setCodigoSubTipoDetalle(String codigoSubTipoDetalle) {
        CodigoSubTipoDetalle = codigoSubTipoDetalle;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMecanismoParticipacion() {
        return mecanismoParticipacion;
    }

    public void setMecanismoParticipacion(String mecanismoParticipacion) {
        this.mecanismoParticipacion = mecanismoParticipacion;
    }

    public String getMetodologia() {
        return metodologia;
    }

    public void setMetodologia(String metodologia) {
        this.metodologia = metodologia;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getPersonalCapacitar() {
        return personalCapacitar;
    }

    public void setPersonalCapacitar(String personalCapacitar) {
        this.personalCapacitar = personalCapacitar;
    }

    public String getModalidadCapacitar() {
        return modalidadCapacitar;
    }

    public void setModalidadCapacitar(String modalidadCapacitar) {
        this.modalidadCapacitar = modalidadCapacitar;
    }

    public String getMedidaControl() {
        return medidaControl;
    }

    public void setMedidaControl(String medidaControl) {
        this.medidaControl = medidaControl;
    }

    public String getMedidaMonitoreo() {
        return medidaMonitoreo;
    }

    public void setMedidaMonitoreo(String medidaMonitoreo) {
        this.medidaMonitoreo = medidaMonitoreo;
    }

    public String getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(String frecuencia) {
        this.frecuencia = frecuencia;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getContingencia() {
        return contingencia;
    }

    public void setContingencia(String contingencia) {
        this.contingencia = contingencia;
    }

    public String getAccionRealizada() {
        return accionRealizada;
    }

    public void setAccionRealizada(String accionRealizada) {
        this.accionRealizada = accionRealizada;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public String getNombreTipoDetalle() {
        return nombreTipoDetalle;
    }

    public void setNombreTipoDetalle(String nombreTipoDetalle) {
        this.nombreTipoDetalle = nombreTipoDetalle;
    }


}