package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrganizacionActividadArchivoEntity extends AuditoriaEntity implements Serializable {
    private Integer idOrgActividadArchivo;
    private Integer idPlanManejo;
    private String nombreArchivo;

    private Integer idArchivo;

    private String descripcion;
    private String observacion;
    // ArchivoEntity archivo;

}
