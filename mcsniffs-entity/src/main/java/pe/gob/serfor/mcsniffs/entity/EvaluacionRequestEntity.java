package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EvaluacionRequestEntity implements Serializable {
    
    private Integer idProcesoPostulacion;
    private Integer idEstadoPostulacion;
    private Integer IdUsuarioPostulacion;    
    private Integer IdProcesoOferta;
    private Boolean Ganador;
    private Integer pageNum;
    private Integer pageSize;
 
}
