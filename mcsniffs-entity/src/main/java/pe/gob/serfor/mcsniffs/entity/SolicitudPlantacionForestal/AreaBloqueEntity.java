package pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
public class AreaBloqueEntity extends AuditoriaEntity implements Serializable {

    private Integer idSolPlantForest;

    private Integer idSolPlantForestArea;
    
    private Integer idAreaBloque;

    private String bloque;

    private BigDecimal area; 

    private BigDecimal alturaPromedio;

    private List<AreaBloqueDetalleEntity> areaBloqueDetalle;

}
