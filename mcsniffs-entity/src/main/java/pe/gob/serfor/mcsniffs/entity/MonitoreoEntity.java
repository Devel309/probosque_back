package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.List;
public class MonitoreoEntity extends AuditoriaEntity implements Serializable {
    private Integer idMonitoreo;
    private String descripcion;
    private Integer idPlanManejo;
    private String codigoMonitoreo;
    private String observacion;
    private List<MonitoreoDetalleEntity> lstDetalle;
    private String codigoMonitoreoDet;
    private Integer idMonitoreoDet;
    private String indicador;
    private String frecuencia;
    private String monitoreo;
    private String responsable;

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getIndicador() {
        return indicador;
    }

    public void setIndicador(String indicador) {
        this.indicador = indicador;
    }

    public String getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(String frecuencia) {
        this.frecuencia = frecuencia;
    }

    public String getMonitoreo() {
        return monitoreo;
    }

    public void setMonitoreo(String monitoreo) {
        this.monitoreo = monitoreo;
    }

    public String getCodigoMonitoreoDet() {
        return codigoMonitoreoDet;
    }

    public void setCodigoMonitoreoDet(String codigoMonitoreoDet) {
        this.codigoMonitoreoDet = codigoMonitoreoDet;
    }

    public Integer getIdMonitoreoDet() {
        return idMonitoreoDet;
    }

    public void setIdMonitoreoDet(Integer idMonitoreoDet) {
        this.idMonitoreoDet = idMonitoreoDet;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public String getCodigoMonitoreo() {
        return codigoMonitoreo;
    }

    public void setCodigoMonitoreo(String codigoMonitoreo) {
        this.codigoMonitoreo = codigoMonitoreo;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public List<MonitoreoDetalleEntity> getLstDetalle() {
        return lstDetalle;
    }

    public void setLstDetalle(List<MonitoreoDetalleEntity> lstDetalle) {
        this.lstDetalle = lstDetalle;
    }

    public PlanManejoEntity getPlanManejo() {
        return planManejo;
    }

    public void setPlanManejo(PlanManejoEntity planManejo) {
        this.planManejo = planManejo;
    }

    private PlanManejoEntity planManejo;
    public Integer getIdMonitoreo() {
        return idMonitoreo;
    }

    public void setIdMonitoreo(Integer idMonitoreo) {
        this.idMonitoreo = idMonitoreo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
