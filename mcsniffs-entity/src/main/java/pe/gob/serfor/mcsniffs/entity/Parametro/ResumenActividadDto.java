package pe.gob.serfor.mcsniffs.entity.Parametro;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.ResumenActividadPoDetalleEntity;

import java.io.Serializable;
import java.util.List;

public class ResumenActividadDto extends AuditoriaEntity implements Serializable {
    private Integer idResumenAct;
    private Integer idPlanManejo;
    private String codTipoResumen;
    private Integer nroResolucion;
    private Integer nroPcs;
    private Double areaHa;
    private Integer idResumenActDet;
    private Integer tipoResumen;
    private String actividad;
    private String indicador;
    private String programado;
    private String realizado;
    private String resumen;
    private String aspectoRecomendacion;
    private String aspectoPositivo;
    private String aspectoNegativo;
    private String codigoResumen;
    private String codigoSubResumen;

    private List<ResumenActividadPoDetalleEntity> listResumenActividadDetalle;

    public String getCodigoResumen() {
        return codigoResumen;
    }

    public void setCodigoResumen(String codigoResumen) {
        this.codigoResumen = codigoResumen;
    }

    public String getCodigoSubResumen() {
        return codigoSubResumen;
    }

    public void setCodigoSubResumen(String codigoSubResumen) {
        this.codigoSubResumen = codigoSubResumen;
    }

    public Integer getIdResumenAct() {
        return idResumenAct;
    }

    public ResumenActividadDto setIdResumenAct(Integer idResumenAct) {
        this.idResumenAct = idResumenAct;
        return this;
    }

    public String getAspectoRecomendacion() {
        return aspectoRecomendacion;
    }

    public void setAspectoRecomendacion(String aspectoRecomendacion) {
        this.aspectoRecomendacion = aspectoRecomendacion;
    }

    public String getAspectoPositivo() {
        return aspectoPositivo;
    }

    public void setAspectoPositivo(String aspectoPositivo) {
        this.aspectoPositivo = aspectoPositivo;
    }

    public String getAspectoNegativo() {
        return aspectoNegativo;
    }

    public void setAspectoNegativo(String aspectoNegativo) {
        this.aspectoNegativo = aspectoNegativo;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public ResumenActividadDto setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
        return this;
    }

    public String getCodTipoResumen() {
        return codTipoResumen;
    }

    public ResumenActividadDto setCodTipoResumen(String codTipoResumen) {
        this.codTipoResumen = codTipoResumen;
        return this;
    }

    public Integer getNroResolucion() {
        return nroResolucion;
    }

    public ResumenActividadDto setNroResolucion(Integer nroResolucion) {
        this.nroResolucion = nroResolucion;
        return this;
    }

    public Integer getNroPcs() {
        return nroPcs;
    }

    public ResumenActividadDto setNroPcs(Integer nroPcs) {
        this.nroPcs = nroPcs;
        return this;
    }

    public Double getAreaHa() {
        return areaHa;
    }

    public ResumenActividadDto setAreaHa(Double areaHa) {
        this.areaHa = areaHa;
        return this;
    }

    public Integer getIdResumenActDet() {
        return idResumenActDet;
    }

    public ResumenActividadDto setIdResumenActDet(Integer idResumenActDet) {
        this.idResumenActDet = idResumenActDet;
        return this;
    }

    public Integer getTipoResumen() {
        return tipoResumen;
    }

    public ResumenActividadDto setTipoResumen(Integer tipoResumen) {
        this.tipoResumen = tipoResumen;
        return this;
    }

    public String getActividad() {
        return actividad;
    }

    public ResumenActividadDto setActividad(String actividad) {
        this.actividad = actividad;
        return this;
    }

    public String getIndicador() {
        return indicador;
    }

    public ResumenActividadDto setIndicador(String indicador) {
        this.indicador = indicador;
        return this;
    }

    public String getProgramado() {
        return programado;
    }

    public ResumenActividadDto setProgramado(String programado) {
        this.programado = programado;
        return this;
    }

    public String getRealizado() {
        return realizado;
    }

    public ResumenActividadDto setRealizado(String realizado) {
        this.realizado = realizado;
        return this;
    }

    public String getResumen() {
        return resumen;
    }

    public ResumenActividadDto setResumen(String resumen) {
        this.resumen = resumen;
        return this;
    }

    public List<ResumenActividadPoDetalleEntity> getListResumenActividadDetalle() {
        return listResumenActividadDetalle;
    }

    public void setListResumenActividadDetalle(List<ResumenActividadPoDetalleEntity> listResumenActividadDetalle) {
        this.listResumenActividadDetalle = listResumenActividadDetalle;
    }
}
