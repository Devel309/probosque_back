package pe.gob.serfor.mcsniffs.entity.AspectoEconomico;

import lombok.Data;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
public class InfoEconomicaConfictosPgmf extends AuditoriaEntity{
    Integer idInfoEconomicaConfictos ;
    Integer idInformacionSocioEconomica;

    String tipoConficto ;
    String propuestaSolucion ;
    Boolean activo;
}
