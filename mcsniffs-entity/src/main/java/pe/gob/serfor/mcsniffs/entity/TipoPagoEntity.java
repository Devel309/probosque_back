package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class TipoPagoEntity implements Serializable {
    private short IdTipoPago;
    private String descripcion;
    private String estado;
    public short getIdTipoPago() { return IdTipoPago; }
    public void setIdTipoPago(short IdTipoPago) { this.IdTipoPago = IdTipoPago; }
    public String getDescripcion() { return descripcion; }
    public void setDescripcion(String descripcion) { this.descripcion = descripcion; }
    public String getEstado() { return estado; }
    public void setEstado(String estado) { this.estado = estado; }

}