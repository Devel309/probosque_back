package pe.gob.serfor.mcsniffs.entity.Parametro;

public class ParcelaCorteDto {
    String numArbolesPC;
    String  volumenPC;
    public String getNumArbolesPC() {
        return numArbolesPC;
    }

    public void setNumArbolesPC(String numArbolesPC) {
        this.numArbolesPC = numArbolesPC;
    }

    public String getVolumenPC() {
        return volumenPC;
    }

    public void setVolumenPC(String volumenPC) {
        this.volumenPC = volumenPC;
    }

}
