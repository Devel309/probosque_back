package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class TipoDocumentoEntity implements Serializable {
    private short idDocumentoTipo;
    private String descripcionCorta;
    private String descripcion;
    private Integer IdTipoDocumento;
    private String codigoDoc;
    private String codigoDocSub;

    public Integer getIdTipoDocumento() {
        return IdTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        IdTipoDocumento = idTipoDocumento;
    }

    public short getIdDocumentoGestion() { return idDocumentoTipo; }
    public void setIdDocumentoGestion(short idDocumentoTipo) { this.idDocumentoTipo = idDocumentoTipo; }
    public String getDescripcionCorta() { return descripcionCorta; }
    public void setDescripcionCorta(String descripcionCorta) { this.descripcionCorta = descripcionCorta; }
    public String getDescripcion() { return descripcion; }
    public void setDescripcion(String descripcion) { this.descripcion = descripcion; }

    
    public short getIdDocumentoTipo() {
        return idDocumentoTipo;
    }

    public void setIdDocumentoTipo(short idDocumentoTipo) {
        this.idDocumentoTipo = idDocumentoTipo;
    }

    public String getCodigoDoc() {
        return codigoDoc;
    }

    public void setCodigoDoc(String codigoDoc) {
        this.codigoDoc = codigoDoc;
    }

    public String getCodigoDocSub() {
        return codigoDocSub;
    }

    public void setCodigoDocSub(String codigoDocSub) {
        this.codigoDocSub = codigoDocSub;
    }

    
}
