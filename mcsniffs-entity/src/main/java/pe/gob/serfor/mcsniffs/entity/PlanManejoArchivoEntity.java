package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

@Data
public class PlanManejoArchivoEntity {
  private Integer idPlanManejoArchivo;
  private String codigoProceso;
  private String codigoSubTipo;
  private String codigoTipo;
  private String Descripcion;
  private String observacion;
  private Integer idPlanManejo;
  private Integer idArchivo;
  private String nombre;
  private String extension;
  private String tipoDocumento;
  private String nombreArchivo;
  private String extensionArchivo;
  private String idTipoDocumento;
  private byte[] documento;
  private Boolean conforme;
  private String asunto;
  private String acapite;
  private String detalle;
  private String item;
  private String subCodigoProceso;
}
