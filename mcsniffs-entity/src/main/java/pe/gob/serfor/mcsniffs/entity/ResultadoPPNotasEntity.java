package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ResultadoPPNotasEntity implements Serializable {
        private Integer idProcesoPostulacion;
        private Integer idUsuarioRegistro;
        private Integer puntaje11;
        private Integer puntaje12;
        private Integer puntaje21;
        private Integer puntaje22;
        private Integer puntajeAdecMeta311;
        private Integer puntajeAdecMeta3121;
        private Integer puntajeAdecMeta3122;
        private Integer puntajeAdecMeta3123;
        private Integer puntajeAdecMeta3131;
        private Integer puntajeAdecMeta3132;
        private Integer puntajeAdecMeta3141;
        private Integer puntajeAdecMeta3142;
        private Integer puntajeAdecMeta3211;
        private Integer puntajeAdecMeta3212;
        private Integer puntajeAdecMeta3213;
        private Integer puntajeAdecMeta3221;
        private Integer puntajeAdecMeta3222;
        private Integer puntajeNoAdecMeta311;
        private Integer puntajeNoAdecMeta3121;
        private Integer puntajeNoAdecMeta3122;
        private Integer puntajeNoAdecMeta3123;
        private Integer puntajeNoAdecMeta3131;
        private Integer puntajeNoAdecMeta3132;
        private Integer puntajeNoAdecMeta3141;
        private Integer puntajeNoAdecMeta3142;
        private Integer puntajeNoAdecMeta3211;
        private Integer puntajeNoAdecMeta3212;
        private Integer puntajeNoAdecMeta3213;
        private Integer puntajeNoAdecMeta3221;
        private Integer puntajeNoAdecMeta3222;
        private Integer puntajeNoTiene311;
        private Integer puntajeNoTiene3121;
        private Integer puntajeNoTiene3122;
        private Integer puntajeNoTiene3123;
        private Integer puntajeNoTiene3131;
        private Integer puntajeNoTiene3132;
        private Integer puntajeNoTiene3141;
        private Integer puntajeNoTiene3142;
        private Integer puntajeNoTiene3211;
        private Integer puntajeNoTiene3212;
        private Integer puntajeNoTiene3213;
        private Integer puntajeNoTiene3221;
        private Integer puntajeNoTiene3222;
        private Integer puntajeCoherente4111;
        private Integer puntajeCoherente4112;
        private Integer puntajeCoherente412;
        private Integer puntajeCoherente413;
        private Integer puntajeCoherente414;
        private Integer puntajeCoherente415;
        private Integer puntajeCoherente416;
        private Integer puntajeCoherente417;
        private Integer puntajeCoherente418;
        private Integer puntajeCoherente419;
        private Integer puntajeCoherente4110;
        private Integer puntajeCoherente421;
        private Integer puntajeCoherente4221;
        private Integer puntajeCoherente4222;
        private Integer puntajeNoCoherente4111;
        private Integer puntajeNoCoherente4112;
        private Integer puntajeNoCoherente412;
        private Integer puntajeNoCoherente413;
        private Integer puntajeNoCoherente414;
        private Integer puntajeNoCoherente415;
        private Integer puntajeNoCoherente416;
        private Integer puntajeNoCoherente417;
        private Integer puntajeNoCoherente418;
        private Integer puntajeNoCoherente419;
        private Integer puntajeNoCoherente4110;
        private Integer puntajeNoCoherente421;
        private Integer puntajeNoCoherente4221;
        private Integer puntajeNoCoherente4222;
        private Integer puntajeNoConsigna4111;
        private Integer puntajeNoConsigna4112;
        private Integer puntajeNoConsigna412;
        private Integer puntajeNoConsigna413;
        private Integer puntajeNoConsigna414;
        private Integer puntajeNoConsigna415;
        private Integer puntajeNoConsigna416;
        private Integer puntajeNoConsigna417;
        private Integer puntajeNoConsigna418;
        private Integer puntajeNoConsigna419;
        private Integer puntajeNoConsigna4110;
        private Integer puntajeNoConsigna421;
        private Integer puntajeNoConsigna4221;
        private Integer puntajeNoConsigna4222;

        public Integer getIdProcesoPostulacion() {
                return idProcesoPostulacion;
        }

        public void setIdProcesoPostulacion(Integer idProcesoPostulacion) {
                this.idProcesoPostulacion = idProcesoPostulacion;
        }

        public Integer getIdUsuarioRegistro() {
                return idUsuarioRegistro;
        }

        public void setIdUsuarioRegistro(Integer idUsuarioRegistro) {
                this.idUsuarioRegistro = idUsuarioRegistro;
        }

        public Integer getPuntaje11() {
                return puntaje11;
        }

        public void setPuntaje11(Integer puntaje11) {
                this.puntaje11 = puntaje11;
        }

        public Integer getPuntaje12() {
                return puntaje12;
        }

        public void setPuntaje12(Integer puntaje12) {
                this.puntaje12 = puntaje12;
        }

        public Integer getPuntaje21() {
                return puntaje21;
        }

        public void setPuntaje21(Integer puntaje21) {
                this.puntaje21 = puntaje21;
        }

        public Integer getPuntaje22() {
                return puntaje22;
        }

        public void setPuntaje22(Integer puntaje22) {
                this.puntaje22 = puntaje22;
        }

        public Integer getPuntajeAdecMeta311() {
                return puntajeAdecMeta311;
        }

        public void setPuntajeAdecMeta311(Integer puntajeAdecMeta311) {
                this.puntajeAdecMeta311 = puntajeAdecMeta311;
        }

        public Integer getPuntajeAdecMeta3121() {
                return puntajeAdecMeta3121;
        }

        public void setPuntajeAdecMeta3121(Integer puntajeAdecMeta3121) {
                this.puntajeAdecMeta3121 = puntajeAdecMeta3121;
        }

        public Integer getPuntajeAdecMeta3122() {
                return puntajeAdecMeta3122;
        }

        public void setPuntajeAdecMeta3122(Integer puntajeAdecMeta3122) {
                this.puntajeAdecMeta3122 = puntajeAdecMeta3122;
        }

        public Integer getPuntajeAdecMeta3123() {
                return puntajeAdecMeta3123;
        }

        public void setPuntajeAdecMeta3123(Integer puntajeAdecMeta3123) {
                this.puntajeAdecMeta3123 = puntajeAdecMeta3123;
        }

        public Integer getPuntajeAdecMeta3131() {
                return puntajeAdecMeta3131;
        }

        public void setPuntajeAdecMeta3131(Integer puntajeAdecMeta3131) {
                this.puntajeAdecMeta3131 = puntajeAdecMeta3131;
        }

        public Integer getPuntajeAdecMeta3132() {
                return puntajeAdecMeta3132;
        }

        public void setPuntajeAdecMeta3132(Integer puntajeAdecMeta3132) {
                this.puntajeAdecMeta3132 = puntajeAdecMeta3132;
        }

        public Integer getPuntajeAdecMeta3141() {
                return puntajeAdecMeta3141;
        }

        public void setPuntajeAdecMeta3141(Integer puntajeAdecMeta3141) {
                this.puntajeAdecMeta3141 = puntajeAdecMeta3141;
        }

        public Integer getPuntajeAdecMeta3142() {
                return puntajeAdecMeta3142;
        }

        public void setPuntajeAdecMeta3142(Integer puntajeAdecMeta3142) {
                this.puntajeAdecMeta3142 = puntajeAdecMeta3142;
        }

        public Integer getPuntajeAdecMeta3211() {
                return puntajeAdecMeta3211;
        }

        public void setPuntajeAdecMeta3211(Integer puntajeAdecMeta3211) {
                this.puntajeAdecMeta3211 = puntajeAdecMeta3211;
        }

        public Integer getPuntajeAdecMeta3212() {
                return puntajeAdecMeta3212;
        }

        public void setPuntajeAdecMeta3212(Integer puntajeAdecMeta3212) {
                this.puntajeAdecMeta3212 = puntajeAdecMeta3212;
        }

        public Integer getPuntajeAdecMeta3213() {
                return puntajeAdecMeta3213;
        }

        public void setPuntajeAdecMeta3213(Integer puntajeAdecMeta3213) {
                this.puntajeAdecMeta3213 = puntajeAdecMeta3213;
        }

        public Integer getPuntajeAdecMeta3221() {
                return puntajeAdecMeta3221;
        }

        public void setPuntajeAdecMeta3221(Integer puntajeAdecMeta3221) {
                this.puntajeAdecMeta3221 = puntajeAdecMeta3221;
        }

        public Integer getPuntajeAdecMeta3222() {
                return puntajeAdecMeta3222;
        }

        public void setPuntajeAdecMeta3222(Integer puntajeAdecMeta3222) {
                this.puntajeAdecMeta3222 = puntajeAdecMeta3222;
        }

        public Integer getPuntajeNoAdecMeta311() {
                return puntajeNoAdecMeta311;
        }

        public void setPuntajeNoAdecMeta311(Integer puntajeNoAdecMeta311) {
                this.puntajeNoAdecMeta311 = puntajeNoAdecMeta311;
        }

        public Integer getPuntajeNoAdecMeta3121() {
                return puntajeNoAdecMeta3121;
        }

        public void setPuntajeNoAdecMeta3121(Integer puntajeNoAdecMeta3121) {
                this.puntajeNoAdecMeta3121 = puntajeNoAdecMeta3121;
        }

        public Integer getPuntajeNoAdecMeta3122() {
                return puntajeNoAdecMeta3122;
        }

        public void setPuntajeNoAdecMeta3122(Integer puntajeNoAdecMeta3122) {
                this.puntajeNoAdecMeta3122 = puntajeNoAdecMeta3122;
        }

        public Integer getPuntajeNoAdecMeta3123() {
                return puntajeNoAdecMeta3123;
        }

        public void setPuntajeNoAdecMeta3123(Integer puntajeNoAdecMeta3123) {
                this.puntajeNoAdecMeta3123 = puntajeNoAdecMeta3123;
        }

        public Integer getPuntajeNoAdecMeta3131() {
                return puntajeNoAdecMeta3131;
        }

        public void setPuntajeNoAdecMeta3131(Integer puntajeNoAdecMeta3131) {
                this.puntajeNoAdecMeta3131 = puntajeNoAdecMeta3131;
        }

        public Integer getPuntajeNoAdecMeta3132() {
                return puntajeNoAdecMeta3132;
        }

        public void setPuntajeNoAdecMeta3132(Integer puntajeNoAdecMeta3132) {
                this.puntajeNoAdecMeta3132 = puntajeNoAdecMeta3132;
        }

        public Integer getPuntajeNoAdecMeta3141() {
                return puntajeNoAdecMeta3141;
        }

        public void setPuntajeNoAdecMeta3141(Integer puntajeNoAdecMeta3141) {
                this.puntajeNoAdecMeta3141 = puntajeNoAdecMeta3141;
        }

        public Integer getPuntajeNoAdecMeta3142() {
                return puntajeNoAdecMeta3142;
        }

        public void setPuntajeNoAdecMeta3142(Integer puntajeNoAdecMeta3142) {
                this.puntajeNoAdecMeta3142 = puntajeNoAdecMeta3142;
        }

        public Integer getPuntajeNoAdecMeta3211() {
                return puntajeNoAdecMeta3211;
        }

        public void setPuntajeNoAdecMeta3211(Integer puntajeNoAdecMeta3211) {
                this.puntajeNoAdecMeta3211 = puntajeNoAdecMeta3211;
        }

        public Integer getPuntajeNoAdecMeta3212() {
                return puntajeNoAdecMeta3212;
        }

        public void setPuntajeNoAdecMeta3212(Integer puntajeNoAdecMeta3212) {
                this.puntajeNoAdecMeta3212 = puntajeNoAdecMeta3212;
        }

        public Integer getPuntajeNoAdecMeta3213() {
                return puntajeNoAdecMeta3213;
        }

        public void setPuntajeNoAdecMeta3213(Integer puntajeNoAdecMeta3213) {
                this.puntajeNoAdecMeta3213 = puntajeNoAdecMeta3213;
        }

        public Integer getPuntajeNoAdecMeta3221() {
                return puntajeNoAdecMeta3221;
        }

        public void setPuntajeNoAdecMeta3221(Integer puntajeNoAdecMeta3221) {
                this.puntajeNoAdecMeta3221 = puntajeNoAdecMeta3221;
        }

        public Integer getPuntajeNoAdecMeta3222() {
                return puntajeNoAdecMeta3222;
        }

        public void setPuntajeNoAdecMeta3222(Integer puntajeNoAdecMeta3222) {
                this.puntajeNoAdecMeta3222 = puntajeNoAdecMeta3222;
        }

        public Integer getPuntajeNoTiene311() {
                return puntajeNoTiene311;
        }

        public void setPuntajeNoTiene311(Integer puntajeNoTiene311) {
                this.puntajeNoTiene311 = puntajeNoTiene311;
        }

        public Integer getPuntajeNoTiene3121() {
                return puntajeNoTiene3121;
        }

        public void setPuntajeNoTiene3121(Integer puntajeNoTiene3121) {
                this.puntajeNoTiene3121 = puntajeNoTiene3121;
        }

        public Integer getPuntajeNoTiene3122() {
                return puntajeNoTiene3122;
        }

        public void setPuntajeNoTiene3122(Integer puntajeNoTiene3122) {
                this.puntajeNoTiene3122 = puntajeNoTiene3122;
        }

        public Integer getPuntajeNoTiene3123() {
                return puntajeNoTiene3123;
        }

        public void setPuntajeNoTiene3123(Integer puntajeNoTiene3123) {
                this.puntajeNoTiene3123 = puntajeNoTiene3123;
        }

        public Integer getPuntajeNoTiene3131() {
                return puntajeNoTiene3131;
        }

        public void setPuntajeNoTiene3131(Integer puntajeNoTiene3131) {
                this.puntajeNoTiene3131 = puntajeNoTiene3131;
        }

        public Integer getPuntajeNoTiene3132() {
                return puntajeNoTiene3132;
        }

        public void setPuntajeNoTiene3132(Integer puntajeNoTiene3132) {
                this.puntajeNoTiene3132 = puntajeNoTiene3132;
        }

        public Integer getPuntajeNoTiene3141() {
                return puntajeNoTiene3141;
        }

        public void setPuntajeNoTiene3141(Integer puntajeNoTiene3141) {
                this.puntajeNoTiene3141 = puntajeNoTiene3141;
        }

        public Integer getPuntajeNoTiene3142() {
                return puntajeNoTiene3142;
        }

        public void setPuntajeNoTiene3142(Integer puntajeNoTiene3142) {
                this.puntajeNoTiene3142 = puntajeNoTiene3142;
        }

        public Integer getPuntajeNoTiene3211() {
                return puntajeNoTiene3211;
        }

        public void setPuntajeNoTiene3211(Integer puntajeNoTiene3211) {
                this.puntajeNoTiene3211 = puntajeNoTiene3211;
        }

        public Integer getPuntajeNoTiene3212() {
                return puntajeNoTiene3212;
        }

        public void setPuntajeNoTiene3212(Integer puntajeNoTiene3212) {
                this.puntajeNoTiene3212 = puntajeNoTiene3212;
        }

        public Integer getPuntajeNoTiene3213() {
                return puntajeNoTiene3213;
        }

        public void setPuntajeNoTiene3213(Integer puntajeNoTiene3213) {
                this.puntajeNoTiene3213 = puntajeNoTiene3213;
        }

        public Integer getPuntajeNoTiene3221() {
                return puntajeNoTiene3221;
        }

        public void setPuntajeNoTiene3221(Integer puntajeNoTiene3221) {
                this.puntajeNoTiene3221 = puntajeNoTiene3221;
        }

        public Integer getPuntajeNoTiene3222() {
                return puntajeNoTiene3222;
        }

        public void setPuntajeNoTiene3222(Integer puntajeNoTiene3222) {
                this.puntajeNoTiene3222 = puntajeNoTiene3222;
        }

        public Integer getPuntajeCoherente4111() {
                return puntajeCoherente4111;
        }

        public void setPuntajeCoherente4111(Integer puntajeCoherente4111) {
                this.puntajeCoherente4111 = puntajeCoherente4111;
        }

        public Integer getPuntajeCoherente4112() {
                return puntajeCoherente4112;
        }

        public void setPuntajeCoherente4112(Integer puntajeCoherente4112) {
                this.puntajeCoherente4112 = puntajeCoherente4112;
        }

        public Integer getPuntajeCoherente412() {
                return puntajeCoherente412;
        }

        public void setPuntajeCoherente412(Integer puntajeCoherente412) {
                this.puntajeCoherente412 = puntajeCoherente412;
        }

        public Integer getPuntajeCoherente413() {
                return puntajeCoherente413;
        }

        public void setPuntajeCoherente413(Integer puntajeCoherente413) {
                this.puntajeCoherente413 = puntajeCoherente413;
        }

        public Integer getPuntajeCoherente414() {
                return puntajeCoherente414;
        }

        public void setPuntajeCoherente414(Integer puntajeCoherente414) {
                this.puntajeCoherente414 = puntajeCoherente414;
        }

        public Integer getPuntajeCoherente415() {
                return puntajeCoherente415;
        }

        public void setPuntajeCoherente415(Integer puntajeCoherente415) {
                this.puntajeCoherente415 = puntajeCoherente415;
        }

        public Integer getPuntajeCoherente416() {
                return puntajeCoherente416;
        }

        public void setPuntajeCoherente416(Integer puntajeCoherente416) {
                this.puntajeCoherente416 = puntajeCoherente416;
        }

        public Integer getPuntajeCoherente417() {
                return puntajeCoherente417;
        }

        public void setPuntajeCoherente417(Integer puntajeCoherente417) {
                this.puntajeCoherente417 = puntajeCoherente417;
        }

        public Integer getPuntajeCoherente418() {
                return puntajeCoherente418;
        }

        public void setPuntajeCoherente418(Integer puntajeCoherente418) {
                this.puntajeCoherente418 = puntajeCoherente418;
        }

        public Integer getPuntajeCoherente419() {
                return puntajeCoherente419;
        }

        public void setPuntajeCoherente419(Integer puntajeCoherente419) {
                this.puntajeCoherente419 = puntajeCoherente419;
        }

        public Integer getPuntajeCoherente4110() {
                return puntajeCoherente4110;
        }

        public void setPuntajeCoherente4110(Integer puntajeCoherente4110) {
                this.puntajeCoherente4110 = puntajeCoherente4110;
        }

        public Integer getPuntajeCoherente421() {
                return puntajeCoherente421;
        }

        public void setPuntajeCoherente421(Integer puntajeCoherente421) {
                this.puntajeCoherente421 = puntajeCoherente421;
        }

        public Integer getPuntajeCoherente4221() {
                return puntajeCoherente4221;
        }

        public void setPuntajeCoherente4221(Integer puntajeCoherente4221) {
                this.puntajeCoherente4221 = puntajeCoherente4221;
        }

        public Integer getPuntajeCoherente4222() {
                return puntajeCoherente4222;
        }

        public void setPuntajeCoherente4222(Integer puntajeCoherente4222) {
                this.puntajeCoherente4222 = puntajeCoherente4222;
        }

        public Integer getPuntajeNoCoherente4111() {
                return puntajeNoCoherente4111;
        }

        public void setPuntajeNoCoherente4111(Integer puntajeNoCoherente4111) {
                this.puntajeNoCoherente4111 = puntajeNoCoherente4111;
        }

        public Integer getPuntajeNoCoherente4112() {
                return puntajeNoCoherente4112;
        }

        public void setPuntajeNoCoherente4112(Integer puntajeNoCoherente4112) {
                this.puntajeNoCoherente4112 = puntajeNoCoherente4112;
        }

        public Integer getPuntajeNoCoherente412() {
                return puntajeNoCoherente412;
        }

        public void setPuntajeNoCoherente412(Integer puntajeNoCoherente412) {
                this.puntajeNoCoherente412 = puntajeNoCoherente412;
        }

        public Integer getPuntajeNoCoherente413() {
                return puntajeNoCoherente413;
        }

        public void setPuntajeNoCoherente413(Integer puntajeNoCoherente413) {
                this.puntajeNoCoherente413 = puntajeNoCoherente413;
        }

        public Integer getPuntajeNoCoherente414() {
                return puntajeNoCoherente414;
        }

        public void setPuntajeNoCoherente414(Integer puntajeNoCoherente414) {
                this.puntajeNoCoherente414 = puntajeNoCoherente414;
        }

        public Integer getPuntajeNoCoherente415() {
                return puntajeNoCoherente415;
        }

        public void setPuntajeNoCoherente415(Integer puntajeNoCoherente415) {
                this.puntajeNoCoherente415 = puntajeNoCoherente415;
        }

        public Integer getPuntajeNoCoherente416() {
                return puntajeNoCoherente416;
        }

        public void setPuntajeNoCoherente416(Integer puntajeNoCoherente416) {
                this.puntajeNoCoherente416 = puntajeNoCoherente416;
        }

        public Integer getPuntajeNoCoherente417() {
                return puntajeNoCoherente417;
        }

        public void setPuntajeNoCoherente417(Integer puntajeNoCoherente417) {
                this.puntajeNoCoherente417 = puntajeNoCoherente417;
        }

        public Integer getPuntajeNoCoherente418() {
                return puntajeNoCoherente418;
        }

        public void setPuntajeNoCoherente418(Integer puntajeNoCoherente418) {
                this.puntajeNoCoherente418 = puntajeNoCoherente418;
        }

        public Integer getPuntajeNoCoherente419() {
                return puntajeNoCoherente419;
        }

        public void setPuntajeNoCoherente419(Integer puntajeNoCoherente419) {
                this.puntajeNoCoherente419 = puntajeNoCoherente419;
        }

        public Integer getPuntajeNoCoherente4110() {
                return puntajeNoCoherente4110;
        }

        public void setPuntajeNoCoherente4110(Integer puntajeNoCoherente4110) {
                this.puntajeNoCoherente4110 = puntajeNoCoherente4110;
        }

        public Integer getPuntajeNoCoherente421() {
                return puntajeNoCoherente421;
        }

        public void setPuntajeNoCoherente421(Integer puntajeNoCoherente421) {
                this.puntajeNoCoherente421 = puntajeNoCoherente421;
        }

        public Integer getPuntajeNoCoherente4221() {
                return puntajeNoCoherente4221;
        }

        public void setPuntajeNoCoherente4221(Integer puntajeNoCoherente4221) {
                this.puntajeNoCoherente4221 = puntajeNoCoherente4221;
        }

        public Integer getPuntajeNoCoherente4222() {
                return puntajeNoCoherente4222;
        }

        public void setPuntajeNoCoherente4222(Integer puntajeNoCoherente4222) {
                this.puntajeNoCoherente4222 = puntajeNoCoherente4222;
        }

        public Integer getPuntajeNoConsigna4111() {
                return puntajeNoConsigna4111;
        }

        public void setPuntajeNoConsigna4111(Integer puntajeNoConsigna4111) {
                this.puntajeNoConsigna4111 = puntajeNoConsigna4111;
        }

        public Integer getPuntajeNoConsigna4112() {
                return puntajeNoConsigna4112;
        }

        public void setPuntajeNoConsigna4112(Integer puntajeNoConsigna4112) {
                this.puntajeNoConsigna4112 = puntajeNoConsigna4112;
        }

        public Integer getPuntajeNoConsigna412() {
                return puntajeNoConsigna412;
        }

        public void setPuntajeNoConsigna412(Integer puntajeNoConsigna412) {
                this.puntajeNoConsigna412 = puntajeNoConsigna412;
        }

        public Integer getPuntajeNoConsigna413() {
                return puntajeNoConsigna413;
        }

        public void setPuntajeNoConsigna413(Integer puntajeNoConsigna413) {
                this.puntajeNoConsigna413 = puntajeNoConsigna413;
        }

        public Integer getPuntajeNoConsigna414() {
                return puntajeNoConsigna414;
        }

        public void setPuntajeNoConsigna414(Integer puntajeNoConsigna414) {
                this.puntajeNoConsigna414 = puntajeNoConsigna414;
        }

        public Integer getPuntajeNoConsigna415() {
                return puntajeNoConsigna415;
        }

        public void setPuntajeNoConsigna415(Integer puntajeNoConsigna415) {
                this.puntajeNoConsigna415 = puntajeNoConsigna415;
        }

        public Integer getPuntajeNoConsigna416() {
                return puntajeNoConsigna416;
        }

        public void setPuntajeNoConsigna416(Integer puntajeNoConsigna416) {
                this.puntajeNoConsigna416 = puntajeNoConsigna416;
        }

        public Integer getPuntajeNoConsigna417() {
                return puntajeNoConsigna417;
        }

        public void setPuntajeNoConsigna417(Integer puntajeNoConsigna417) {
                this.puntajeNoConsigna417 = puntajeNoConsigna417;
        }

        public Integer getPuntajeNoConsigna418() {
                return puntajeNoConsigna418;
        }

        public void setPuntajeNoConsigna418(Integer puntajeNoConsigna418) {
                this.puntajeNoConsigna418 = puntajeNoConsigna418;
        }

        public Integer getPuntajeNoConsigna419() {
                return puntajeNoConsigna419;
        }

        public void setPuntajeNoConsigna419(Integer puntajeNoConsigna419) {
                this.puntajeNoConsigna419 = puntajeNoConsigna419;
        }

        public Integer getPuntajeNoConsigna4110() {
                return puntajeNoConsigna4110;
        }

        public void setPuntajeNoConsigna4110(Integer puntajeNoConsigna4110) {
                this.puntajeNoConsigna4110 = puntajeNoConsigna4110;
        }

        public Integer getPuntajeNoConsigna421() {
                return puntajeNoConsigna421;
        }

        public void setPuntajeNoConsigna421(Integer puntajeNoConsigna421) {
                this.puntajeNoConsigna421 = puntajeNoConsigna421;
        }

        public Integer getPuntajeNoConsigna4221() {
                return puntajeNoConsigna4221;
        }

        public void setPuntajeNoConsigna4221(Integer puntajeNoConsigna4221) {
                this.puntajeNoConsigna4221 = puntajeNoConsigna4221;
        }

        public Integer getPuntajeNoConsigna4222() {
                return puntajeNoConsigna4222;
        }

        public void setPuntajeNoConsigna4222(Integer puntajeNoConsigna4222) {
                this.puntajeNoConsigna4222 = puntajeNoConsigna4222;
        }

}
