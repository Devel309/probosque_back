package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class ResponseEntity<S> implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private boolean success;
	private String message;
	private Object data;
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}


    public void setInformacion(String s) {
    }
}
