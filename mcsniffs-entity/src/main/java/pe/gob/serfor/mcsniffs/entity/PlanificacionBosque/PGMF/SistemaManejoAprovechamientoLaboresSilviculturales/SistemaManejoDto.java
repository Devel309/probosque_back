package pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.SistemaManejoAprovechamientoLaboresSilviculturales;

import pe.gob.serfor.mcsniffs.entity.ActividadSilviculturalEntity;

public class SistemaManejoDto {
    private ActividadSilviculturalEntity aprovechamiento;
    private ActividadSilviculturalEntity divisionAdministrativa;
    private ActividadSilviculturalEntity laboresSilviculturales;
    
    public ActividadSilviculturalEntity getAprovechamiento() {
        return aprovechamiento;
    }
    public ActividadSilviculturalEntity getLaboresSilviculturales() {
        return laboresSilviculturales;
    }
    public void setLaboresSilviculturales(ActividadSilviculturalEntity laboresSilviculturales) {
        this.laboresSilviculturales = laboresSilviculturales;
    }
    public ActividadSilviculturalEntity getDivisionAdministrativa() {
        return divisionAdministrativa;
    }
    public void setDivisionAdministrativa(ActividadSilviculturalEntity divisionAdministrativa) {
        this.divisionAdministrativa = divisionAdministrativa;
    }
    public void setAprovechamiento(ActividadSilviculturalEntity aprovechamiento) {
        this.aprovechamiento = aprovechamiento;
    }
}
