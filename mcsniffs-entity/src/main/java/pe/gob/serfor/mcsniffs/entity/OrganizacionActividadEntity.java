package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class OrganizacionActividadEntity extends AuditoriaEntity implements Serializable {
    private Integer idOrgActividad;
    private PlanManejoEntity planManejo;
    private String actividad;
    private String organizacion;

    public Integer getIdOrgActividad() {
        return idOrgActividad;
    }

    public void setIdOrgActividad(Integer idOrgActividad) {
        this.idOrgActividad = idOrgActividad;
    }

    public PlanManejoEntity getPlanManejo() {
        return planManejo;
    }

    public void setPlanManejo(PlanManejoEntity planManejo) {
        this.planManejo = planManejo;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(String organizacion) {
        this.organizacion = organizacion;
    }
}
