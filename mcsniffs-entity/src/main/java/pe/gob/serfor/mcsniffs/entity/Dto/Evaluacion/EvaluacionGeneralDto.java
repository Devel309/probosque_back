package pe.gob.serfor.mcsniffs.entity.Dto.Evaluacion;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.Evaluacion.EvaluacionDetalleDto;
import pe.gob.serfor.mcsniffs.entity.ProteccionBosqueDetalleEntity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;


public class EvaluacionGeneralDto {

    private Integer idPlanManejo;
    private String codigoEvaluacion;
    private String codigoEvaluacionDetSub;
    private String codigoEvaluacionDetPost;
    private String conforme;
    private String observacion;
    private Integer idArchivo;
    private List<EvaluacionDetalleDto> listAcordion;

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public String getCodigoEvaluacion() {
        return codigoEvaluacion;
    }

    public void setCodigoEvaluacion(String codigoEvaluacion) {
        this.codigoEvaluacion = codigoEvaluacion;
    }

    public String getCodigoEvaluacionDetSub() {
        return codigoEvaluacionDetSub;
    }

    public void setCodigoEvaluacionDetSub(String codigoEvaluacionDetSub) {
        this.codigoEvaluacionDetSub = codigoEvaluacionDetSub;
    }

    public String getCodigoEvaluacionDetPost() {
        return codigoEvaluacionDetPost;
    }

    public void setCodigoEvaluacionDetPost(String codigoEvaluacionDetPost) {
        this.codigoEvaluacionDetPost = codigoEvaluacionDetPost;
    }

    public String getConforme() {
        return conforme;
    }

    public void setConforme(String conforme) {
        this.conforme = conforme;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }

    public List<EvaluacionDetalleDto> getListAcordion() {
        return listAcordion;
    }

    public void setListAcordion(List<EvaluacionDetalleDto> listAcordion) {
        this.listAcordion = listAcordion;
    }
}
