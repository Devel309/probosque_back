package pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Getter
@Setter
public class ResponsableExperienciaLaboralDto extends AuditoriaEntity implements Serializable {

    private Integer idResponsableExperienciaLaboral;
    private Integer idSolicitudConcesionResponsable;
    private String entidad;
    private String cargo;
    private Integer tiempo;
    private String descripcion;
    private String telefonoContacto;

    private String estadoResponsableExperienciaLaboral;

}
