package pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlanManejoEvaluacionIterDto extends AuditoriaEntity implements Serializable {

    private Integer idPlanManejoEvaluacionIter;
    private Integer idPlanManejo;
    private Integer idTitularRegente;
    private String titularRegente;
    private Date fechaPresentacion;
    private String estadoPlanManejoEvaluacion;
    private String descEstadoPlanManejoEvaluacion;

}
