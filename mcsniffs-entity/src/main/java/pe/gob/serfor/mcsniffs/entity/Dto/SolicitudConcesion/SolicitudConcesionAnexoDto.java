package pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionArchivoEntity;

@Getter
@Setter
public class SolicitudConcesionAnexoDto extends AuditoriaEntity implements Serializable {
    
    private Integer idSolicitudConcesionAnexo;
    private Integer idSolicitudConcesion;
    private String codigoAnexo;
    private Boolean conforme;
    private Date fechaFirma;
    private String codigoEstadoAnexo;
    private String descripcionCodigoEstado;
    private String lugar;

    private SolicitudConcesionArchivoEntity solicitudConcesionArchivo;
}
