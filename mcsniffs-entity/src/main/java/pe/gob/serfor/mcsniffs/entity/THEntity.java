package pe.gob.serfor.mcsniffs.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class THEntity extends AuditoriaEntity implements Serializable {
    private String codigoTH;
    private String nombreTH;
    private String paternoTH;

    private String maternoTH;
    private String tipoTH;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaInicio;
    private String ubigeo;
    private Integer nroGestion;
    private String tipoDocumentoRepresentante;
    private String nroDocumentoRepresentante;
    private String nombreRepresentante;
    private String paternoRepresentante;
    private String maternoRepresentante;
    private String numeroTH;
    private BigDecimal areaTotal;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaFin;
    private Integer vigencia;
    private String modalidadTH;
    private String  mecanismo;
    private String  mecanismoTexto;
    private String  nombrePlan;
    private String  tipoPlan;
}
