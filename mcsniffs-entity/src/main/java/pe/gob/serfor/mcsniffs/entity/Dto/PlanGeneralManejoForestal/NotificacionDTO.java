package pe.gob.serfor.mcsniffs.entity.Dto.PlanGeneralManejoForestal;

import java.io.Serializable;

public class NotificacionDTO implements Serializable {

    private Integer idPlanManejo;
    private Integer idTitularRegente;
    private String nombreTitularRegente;
    private String correoTitularRegente;
    private Integer nroObservaciones;

    private Integer idMincul;
    private Integer idAna;
    private Integer idSernap;
    private Integer idEvaluacionCampo;


    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public Integer getIdTitularRegente() {
        return idTitularRegente;
    }

    public void setIdTitularRegente(Integer idTitularRegente) {
        this.idTitularRegente = idTitularRegente;
    }

    public String getNombreTitularRegente() {
        return nombreTitularRegente;
    }

    public void setNombreTitularRegente(String nombreTitularRegente) {
        this.nombreTitularRegente = nombreTitularRegente;
    }

    public String getCorreoTitularRegente() {
        return correoTitularRegente;
    }

    public void setCorreoTitularRegente(String correoTitularRegente) {
        this.correoTitularRegente = correoTitularRegente;
    }

    public Integer getNroObservaciones() {
        return nroObservaciones;
    }

    public void setNroObservaciones(Integer nroObservaciones) {
        this.nroObservaciones = nroObservaciones;
    }

    public Integer getIdMincul() {
        return idMincul;
    }

    public void setIdMincul(Integer idMincul) {
        this.idMincul = idMincul;
    }

    public Integer getIdAna() {
        return idAna;
    }

    public void setIdAna(Integer idAna) {
        this.idAna = idAna;
    }

    public Integer getIdSernap() {
        return idSernap;
    }

    public void setIdSernap(Integer idSernap) {
        this.idSernap = idSernap;
    }

    public Integer getIdEvaluacionCampo() {
        return idEvaluacionCampo;
    }

    public void setIdEvaluacionCampo(Integer idEvaluacionCampo) {
        this.idEvaluacionCampo = idEvaluacionCampo;
    }
}
