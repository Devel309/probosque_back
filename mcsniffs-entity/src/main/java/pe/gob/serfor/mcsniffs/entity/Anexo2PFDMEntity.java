package pe.gob.serfor.mcsniffs.entity;

import java.util.List;

public class Anexo2PFDMEntity extends FirmaAnexosPFDMEntity {
    private Integer IdPostulacionPFDM;
    private Integer IdUsuarioPostulante;
    private String profeApellidosNombres;
    private String profeDNIRUC;
    private String profeDomicilio;
    private String profeSector;
    private String profeIdDistrito;
    private String profeIdProvincia;
    private String profeIdDepartamento;
    private String profeTelefono;
    private String profeCelular;
    private String profeCorreo;
    private String profeNombreRSSolicitante;
    private String formaProfesion;
    private String formaNroRegistroColegioProfesional;
    private List<FormacionAcademicaEntity> formacionAcademica;
    private String idAdjuntosFormacion;
    private List<ExperienciaLaboralEntity> experienciaLaboral;
    private String idAdjuntosExperiencia;

    
    
    public String getIdAdjuntosFormacion() {
        return idAdjuntosFormacion;
    }
    public void setIdAdjuntosFormacion(String idAdjuntosFormacion) {
        this.idAdjuntosFormacion = idAdjuntosFormacion;
    }
    public String getIdAdjuntosExperiencia() {
        return idAdjuntosExperiencia;
    }
    public void setIdAdjuntosExperiencia(String idAdjuntosExperiencia) {
        this.idAdjuntosExperiencia = idAdjuntosExperiencia;
    }
    public Integer getIdPostulacionPFDM() {
        return IdPostulacionPFDM;
    }
    public void setIdPostulacionPFDM(Integer idPostulacionPFDM) {
        IdPostulacionPFDM = idPostulacionPFDM;
    }
    public Integer getIdUsuarioPostulante() {
        return IdUsuarioPostulante;
    }
    public void setIdUsuarioPostulante(Integer idUsuarioPostulante) {
        IdUsuarioPostulante = idUsuarioPostulante;
    }
   
    public String getProfeApellidosNombres() {
        return profeApellidosNombres;
    }
    public void setProfeApellidosNombres(String profeApellidosNombres) {
        this.profeApellidosNombres = profeApellidosNombres;
    }
    public String getProfeDNIRUC() {
        return profeDNIRUC;
    }
    public void setProfeDNIRUC(String profeDNIRUC) {
        this.profeDNIRUC = profeDNIRUC;
    }
    public String getProfeDomicilio() {
        return profeDomicilio;
    }
    public void setProfeDomicilio(String profeDomicilio) {
        this.profeDomicilio = profeDomicilio;
    }
    public String getProfeSector() {
        return profeSector;
    }
    public void setProfeSector(String profeSector) {
        this.profeSector = profeSector;
    }
    public String getProfeIdDistrito() {
        return profeIdDistrito;
    }
    public void setProfeIdDistrito(String profeIdDistrito) {
        this.profeIdDistrito = profeIdDistrito;
    }
    public String getProfeIdProvincia() {
        return profeIdProvincia;
    }
    public void setProfeIdProvincia(String profeIdProvincia) {
        this.profeIdProvincia = profeIdProvincia;
    }
    public String getProfeIdDepartamento() {
        return profeIdDepartamento;
    }
    public void setProfeIdDepartamento(String profeIdDepartamento) {
        this.profeIdDepartamento = profeIdDepartamento;
    }
    public String getProfeTelefono() {
        return profeTelefono;
    }
    public void setProfeTelefono(String profeTelefono) {
        this.profeTelefono = profeTelefono;
    }
    public String getProfeCelular() {
        return profeCelular;
    }
    public void setProfeCelular(String profeCelular) {
        this.profeCelular = profeCelular;
    }
    public String getProfeCorreo() {
        return profeCorreo;
    }
    public void setProfeCorreo(String profeCorreo) {
        this.profeCorreo = profeCorreo;
    }
    public String getProfeNombreRSSolicitante() {
        return profeNombreRSSolicitante;
    }
    public void setProfeNombreRSSolicitante(String profeNombreRSSolicitante) {
        this.profeNombreRSSolicitante = profeNombreRSSolicitante;
    }
    public String getFormaProfesion() {
        return formaProfesion;
    }
    public void setFormaProfesion(String formaProfesion) {
        this.formaProfesion = formaProfesion;
    }
    public String getFormaNroRegistroColegioProfesional() {
        return formaNroRegistroColegioProfesional;
    }
    public void setFormaNroRegistroColegioProfesional(String formaNroRegistroColegioProfesional) {
        this.formaNroRegistroColegioProfesional = formaNroRegistroColegioProfesional;
    }
    public List<FormacionAcademicaEntity> getFormacionAcademica() {
        return formacionAcademica;
    }
    public void setFormacionAcademica(List<FormacionAcademicaEntity> formacionAcademica) {
        this.formacionAcademica = formacionAcademica;
    }
    public List<ExperienciaLaboralEntity> getExperienciaLaboral() {
        return experienciaLaboral;
    }
    public void setExperienciaLaboral(List<ExperienciaLaboralEntity> experienciaLaboral) {
        this.experienciaLaboral = experienciaLaboral;
    }
    
    


}
