package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class TipoBosqueManejoEntity extends AuditoriaEntity implements Serializable {
    private Integer idTipoBosqueManejo;
    private String anexoSector;
    private Integer subParcela;
    private String descripcion;
    private Double area;
    private PlanManejoEntity planManejo;

    public Integer getIdTipoBosqueManejo() {
        return idTipoBosqueManejo;
    }

    public void setIdTipoBosqueManejo(Integer idTipoBosqueManejo) {
        this.idTipoBosqueManejo = idTipoBosqueManejo;
    }

    public String getAnexoSector() {
        return anexoSector;
    }

    public void setAnexoSector(String anexoSector) {
        this.anexoSector = anexoSector;
    }

    public Integer getSubParcela() {
        return subParcela;
    }

    public void setSubParcela(Integer subParcela) {
        this.subParcela = subParcela;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public PlanManejoEntity getPlanManejo() {
        return planManejo;
    }

    public void setPlanManejo(PlanManejoEntity planManejo) {
        this.planManejo = planManejo;
    }
}
