package pe.gob.serfor.mcsniffs.entity;

public class FirmaAnexosPFDMEntity {
    private String lugarFirma;
    private String fechaFirma;
    
    public String getLugarFirma() {
        return lugarFirma;
    }
    public void setLugarFirma(String lugarFirma) {
        this.lugarFirma = lugarFirma;
    }
    public String getFechaFirma() {
        return fechaFirma;
    }
    public void setFechaFirma(String fechaFirma) {
        this.fechaFirma = fechaFirma;
    }

}
