package pe.gob.serfor.mcsniffs.entity.Dto.centroTansformacion;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Getter
@Setter
public class CentroTransformacionDto extends AuditoriaEntity implements Serializable{
    private Integer idCentroTransformacion;
    private String razonSocial;
    private String  ruc;
    private String direccion;
    private String autorizacion;
    private String repLegal;
    private String depositos;
    private String centroComercializacion;
    private String centroAcopio;
}
