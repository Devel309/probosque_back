package pe.gob.serfor.mcsniffs.entity.Dto.SolPlantacionForestal;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;

@Data
public class SolPlantacionForestalEvaluacionDetalleDto extends AuditoriaEntity implements Serializable {

    private Integer idSolPlantacionForestalEvaluacionDetalle;
    private Integer idSolPlantacionForestalEvaluacion;
    private Integer idSolPlantacionForestal;
    private String codigoSeccion;
    private String codigoSubSeccion;
    private Boolean lineamiento;
    private String codigoLineamiento;
    private Boolean conforme;
    private String estado;
    private String observacion;
    private String codigoMotivDenegacion;
}
