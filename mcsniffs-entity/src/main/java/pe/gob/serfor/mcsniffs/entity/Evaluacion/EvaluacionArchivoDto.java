package pe.gob.serfor.mcsniffs.entity.Evaluacion;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Getter
@Setter
public class EvaluacionArchivoDto extends AuditoriaEntity implements Serializable{
    private Integer idEvalArchivo;
    private Integer idEvaluacion;
    private Integer idArchivo;
    private Integer idTipoDocumento;
    private String codigoEvalArchivo;
    private String observacion;
    private String detalle;
    private String descripcion;
    private String nombreArchivo;
    private String nombreDocumento;
}
