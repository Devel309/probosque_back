package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListarProcesoPosEntity extends AuditoriaEntity implements Serializable {
    private Integer idUsuarioPostulacion;
    private Integer idStatus;
    private Integer idProcesoPostulacion;
    private Integer idProcesoOferta;
    private String perfil;

     
}
