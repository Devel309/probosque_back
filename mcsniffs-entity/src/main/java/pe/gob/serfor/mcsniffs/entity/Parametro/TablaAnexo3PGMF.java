package pe.gob.serfor.mcsniffs.entity.Parametro;
import java.util.List;
public class TablaAnexo3PGMF {
    String bloquequinquenal;
    String idTipoBosque;
    String tipoBosque;
    String areaBloqueQuinquenal;
    String areaTipoBosque;
    List<ResultadosAnexo3PGMF> resultadosAnexo3PGMFList;
    public String getBloquequinquenal() {
        return bloquequinquenal;
    }

    public void setBloquequinquenal(String bloquequinquenal) {
        this.bloquequinquenal = bloquequinquenal;
    }

    public String getIdTipoBosque() {
        return idTipoBosque;
    }

    public void setIdTipoBosque(String idTipoBosque) {
        this.idTipoBosque = idTipoBosque;
    }

    public String getTipoBosque() {
        return tipoBosque;
    }

    public void setTipoBosque(String tipoBosque) {
        this.tipoBosque = tipoBosque;
    }

    public String getAreaBloqueQuinquenal() {
        return areaBloqueQuinquenal;
    }

    public void setAreaBloqueQuinquenal(String areaBloqueQuinquenal) {
        this.areaBloqueQuinquenal = areaBloqueQuinquenal;
    }

    public String getAreaTipoBosque() {
        return areaTipoBosque;
    }

    public void setAreaTipoBosque(String areaTipoBosque) {
        this.areaTipoBosque = areaTipoBosque;
    }

    public List<ResultadosAnexo3PGMF> getResultadosAnexo3PGMFList() {
        return resultadosAnexo3PGMFList;
    }

    public void setResultadosAnexo3PGMFList(List<ResultadosAnexo3PGMF> resultadosAnexo3PGMFList) {
        this.resultadosAnexo3PGMFList = resultadosAnexo3PGMFList;
    }
}
