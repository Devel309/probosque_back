package pe.gob.serfor.mcsniffs.entity;
import java.io.Serializable;
import java.util.List;

public class ProteccionBosqueDetalleEntity extends AuditoriaEntity implements Serializable {

    private Integer idProBosqueDet;
    private String codPlanGeneralDet;
    private String subCodPlanGeneralDet;
    private String tipoMarcacion;
    private Boolean nuAccion;
    private String implementacion;
    private String factorAmbiental;
    private String impacto;
    private Boolean nuCenso;
    private Boolean nuDemarcacionLineal;
    private Boolean nuConstruccionCampamento;
    private Boolean nuConstruccionCamino;
    private Boolean nuTala;
    private Boolean nuArrastre;
    private Boolean nuOtra;
    private String descOtra;
    private Integer tipoPrograma;
    private String actividad;
    private String mitigacionAmbiental;
    private String impactoGestion;

    private List<ProteccionBosqueDetalleActividadEntity> listProteccionBosqueActividad;

    public List<ProteccionBosqueDetalleActividadEntity> getListProteccionBosqueActividad() {
        return listProteccionBosqueActividad;
    }

    public void setListProteccionBosqueActividad(List<ProteccionBosqueDetalleActividadEntity> listProteccionBosqueActividad) {
        this.listProteccionBosqueActividad = listProteccionBosqueActividad;
    }

    public String getCodPlanGeneralDet() {
        return codPlanGeneralDet;
    }

    public void setCodPlanGeneralDet(String codPlanGeneralDet) {
        this.codPlanGeneralDet = codPlanGeneralDet;
    }

    public String getSubCodPlanGeneralDet() {
        return subCodPlanGeneralDet;
    }

    public void setSubCodPlanGeneralDet(String subCodPlanGeneralDet) {
        this.subCodPlanGeneralDet = subCodPlanGeneralDet;
    }

    public Integer getIdProBosqueDet() {
        return idProBosqueDet;
    }

    public void setIdProBosqueDet(Integer idProBosqueDet) {
        this.idProBosqueDet = idProBosqueDet;
    }


    public String getTipoMarcacion() {
        return tipoMarcacion;
    }

    public void setTipoMarcacion(String tipoMarcacion) {
        this.tipoMarcacion = tipoMarcacion;
    }

    public Boolean getNuAccion() {
        return nuAccion;
    }

    public void setNuAccion(Boolean nuAccion) {
        this.nuAccion = nuAccion;
    }

    public String getImplementacion() {
        return implementacion;
    }

    public void setImplementacion(String implementacion) {
        this.implementacion = implementacion;
    }

    public String getFactorAmbiental() {
        return factorAmbiental;
    }

    public void setFactorAmbiental(String factorAmbiental) {
        this.factorAmbiental = factorAmbiental;
    }

    public String getImpacto() {
        return impacto;
    }

    public void setImpacto(String impacto) {
        this.impacto = impacto;
    }

    public Boolean getNuCenso() {
        return nuCenso;
    }

    public void setNuCenso(Boolean nuCenso) {
        this.nuCenso = nuCenso;
    }

    public Boolean getNuDemarcacionLineal() {
        return nuDemarcacionLineal;
    }

    public void setNuDemarcacionLineal(Boolean nuDemarcacionLineal) {
        this.nuDemarcacionLineal = nuDemarcacionLineal;
    }

    public Boolean getNuConstruccionCampamento() {
        return nuConstruccionCampamento;
    }

    public void setNuConstruccionCampamento(Boolean nuConstruccionCampamento) {
        this.nuConstruccionCampamento = nuConstruccionCampamento;
    }

    public Boolean getNuConstruccionCamino() {
        return nuConstruccionCamino;
    }

    public void setNuConstruccionCamino(Boolean nuConstruccionCamino) {
        this.nuConstruccionCamino = nuConstruccionCamino;
    }

    public Boolean getNuTala() {
        return nuTala;
    }

    public void setNuTala(Boolean nuTala) {
        this.nuTala = nuTala;
    }

    public Boolean getNuArrastre() {
        return nuArrastre;
    }

    public void setNuArrastre(Boolean nuArrastre) {
        this.nuArrastre = nuArrastre;
    }

    public Boolean getNuOtra() {
        return nuOtra;
    }

    public void setNuOtra(Boolean nuOtra) {
        this.nuOtra = nuOtra;
    }

    public String getDescOtra() {
        return descOtra;
    }

    public void setDescOtra(String descOtra) {
        this.descOtra = descOtra;
    }

    public Integer getTipoPrograma() {
        return tipoPrograma;
    }

    public void setTipoPrograma(Integer tipoPrograma) {
        this.tipoPrograma = tipoPrograma;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getMitigacionAmbiental() {
        return mitigacionAmbiental;
    }

    public void setMitigacionAmbiental(String mitigacionAmbiental) {
        this.mitigacionAmbiental = mitigacionAmbiental;
    }

    public String getImpactoGestion() {
        return impactoGestion;
    }

    public void setImpactoGestion(String impactoGestion) {
        this.impactoGestion = impactoGestion;
    }
}
