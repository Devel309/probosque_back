package pe.gob.serfor.mcsniffs.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class PagoGuiaEspecieEntity extends AuditoriaEntity implements Serializable {
    private Integer idPagoGuiaEspecie;
    private String codPagoGuiaEspecie;
    private Integer idEspecie;
    private String nombreCientifico;
    private String nombreComun;
    private BigDecimal volumen;
    private BigDecimal valorEstadoNatural;
    private BigDecimal monto;
    private BigDecimal descuento;
    private BigDecimal montoFinal;
    private BigDecimal descuentoTotal;
    private BigDecimal montoTotal;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaComprobante;
    private String comentarios;
    private String observacion;
    private String detalle;
    private String descripcion;
    private Integer idPagoGuia;
    private String perfil;
    private String categoria;
}
