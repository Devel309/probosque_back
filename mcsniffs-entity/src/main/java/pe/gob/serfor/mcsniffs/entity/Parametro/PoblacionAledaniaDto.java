package pe.gob.serfor.mcsniffs.entity.Parametro;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;

public class PoblacionAledaniaDto extends AuditoriaEntity implements Serializable {
    private Integer idPoblacion;
    private String codigo;
    private String poblacionAledania;
    private String actividades;

    public Integer getIdPoblacion() {
        return idPoblacion;
    }

    public PoblacionAledaniaDto setIdPoblacion(Integer idPoblacion) {
        this.idPoblacion = idPoblacion;
        return this;
    }

    public String getCodigo() {
        return codigo;
    }

    public PoblacionAledaniaDto setCodigo(String codigo) {
        this.codigo = codigo;
        return this;
    }

    public String getPoblacionAledania() {
        return poblacionAledania;
    }

    public PoblacionAledaniaDto setPoblacionAledania(String poblacionAledania) {
        this.poblacionAledania = poblacionAledania;
        return this;
    }

    public String getActividades() {
        return actividades;
    }

    public PoblacionAledaniaDto setActividades(String actividades) {
        this.actividades = actividades;
        return this;
    }
}
