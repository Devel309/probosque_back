package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class TipoConcesionEntity implements Serializable {

    private Integer tipoConcesion;
    private String descripcion;

    public Integer getTipoConcesion() {
        return tipoConcesion;
    }
    public void setTipoConcesion(Integer tipoConcesion) {
        this.tipoConcesion = tipoConcesion;
    }

    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
