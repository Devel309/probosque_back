package pe.gob.serfor.mcsniffs.entity;

public class ParticipacionComunalParamEntity {
    private Integer idPlanManejo;
    private String codTipoPartComunal;
    private String codTipoPartComunalDet;
    private Integer IdPartComunal;
    private Integer IdPartComunalDet;
    private Integer IdUsuarioElimina;
    
    
    public Integer getIdUsuarioElimina() {
        return IdUsuarioElimina;
    }
    public void setIdUsuarioElimina(Integer idUsuarioElimina) {
        IdUsuarioElimina = idUsuarioElimina;
    }
    public Integer getIdPartComunalDet() {
        return IdPartComunalDet;
    }
    public void setIdPartComunalDet(Integer idPartComunalDet) {
        IdPartComunalDet = idPartComunalDet;
    }
    public Integer getIdPartComunal() {
        return IdPartComunal;
    }
    public void setIdPartComunal(Integer idPartComunal) {
        IdPartComunal = idPartComunal;
    }
    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }
    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }
    public String getCodTipoPartComunal() {
        return codTipoPartComunal;
    }
    public void setCodTipoPartComunal(String codTipoPartComunal) {
        this.codTipoPartComunal = codTipoPartComunal;
    }
    public String getCodTipoPartComunalDet() {
        return codTipoPartComunalDet;
    }
    public void setCodTipoPartComunalDet(String codTipoPartComunalDet) {
        this.codTipoPartComunalDet = codTipoPartComunalDet;
    }

    
}
