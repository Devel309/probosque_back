package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalDto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
@Data
public class ManejoBosqueDetalleEntity extends AuditoriaEntity implements Serializable {
    private Integer idManejoBosqueDet;
    private Integer  idManejoBosque ;
    private String codtipoManejoDet ;
    private String subCodtipoManejoDet;
            //ACTIVIDAD
    private String catOrdenamiento ;
    private String ordenamiento ;
    private String actividad ;
    private String codOpcion ;
    private String descripcionOrd ;
            //APROVECHAMIENTO
//1
    private String idtipoMetodoAprov ;
    private String tipoMetodoAprov ;
            //2.1
    private String idtipoTransMaderable ;
    private String tipoTransMaderable ;
            //2.2
    private String idtipoTransDestino ;
    private String tipoTransDestino ;
            //2.3
    private BigDecimal nuCaminoAcceso ;
    private BigDecimal nuCaminoPrincipal ;
    private String descripcionCamino ;
    private String descripcionOtro ;
            //3
    private Integer nuIdOperaciones ;
    private String manoObra ;
    private String maquina ;
    private String descripcionManoObra ;

            //ESPECIES
    private Integer nuIdEspecie ;
    private String especie ;
    private String justificacionEspecie ;
            //SILVICULTURAL
    private String tratamientoSilvicultural ;
    private String descripcionTratamiento ;
    private String Reforestacion ;


    private String infraestructura;
    private String caracteristicasTecnicas;
    private String metodoConstruccion;

    private String descripcionOtros;
    private Integer idOperacion;
    private String lineaProduccion;
    private BigDecimal nuDiametro;
    private Boolean accion;
    private String tipoEspecie;

    //MANEJO FORESTAL 311
    private BigDecimal nuTotalNroArboles;
    private BigDecimal nuTotalVcp;
    private BigDecimal nuValorVcpProd;
    private BigDecimal nuValorVcap;
    private Integer idArchivo;
    private List<ManejoBosqueAniosEntity> listManejoBosqueAnios;
    //VERTICES
    private String puntoVertice;
    private BigDecimal coordenadaEsteIni;
    private BigDecimal coordenadaNorteIni;
    private String referencia;

    // 6.1
    private String equipo;
    private String insumo;
    private String personal;
    private String observacion;

}
