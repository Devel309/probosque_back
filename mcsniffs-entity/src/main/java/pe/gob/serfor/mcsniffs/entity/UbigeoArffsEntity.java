package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.Date;


public class UbigeoArffsEntity extends AuditoriaEntity implements Serializable {
    private short id;
    private String ubigeo;
    private String codDepartamento;
    private String nombreDepartamento;
    private String codProvincia;
    private String nombreProvincia;
    private String codDistrito;
    private String nombreDistrito;
    private String region;
    private Double latitud;
    private Double longitud;
    private Integer idAutoridadForestal;
    private String nombreAutoridadForestal;
    private Integer idContrato;
    

    public Integer getIdContrato() {
        return idContrato;
    }
    public void setIdContrato(Integer idContrato) {
        this.idContrato = idContrato;
    }
    public short getId() { return id; }
    public void setId(short id) { this.id = id; }
    public String getUbigeo() { return ubigeo; }
    public void setUbigeo(String ubigeo) { this.ubigeo = ubigeo; }
    public String getCodDepartamento() { return codDepartamento; }
    public void setCodDepartamento(String codDepartamento) { this.codDepartamento = codDepartamento; }
    public String getNombreDepartamento() { return nombreDepartamento; }
    public void setNombreDepartamento(String nombreDepartamento) { this.nombreDepartamento = nombreDepartamento; }
    public String getCodProvincia() { return codProvincia; }
    public void setCodProvincia(String codProvincia) { this.codProvincia = codProvincia; }
    public String getNombreProvincia() { return nombreProvincia; }
    public void setNombreProvincia(String nombreProvincia) { this.nombreProvincia = nombreProvincia; }
    public String getCodDistrito() { return codDistrito; }
    public void setCodDistrito(String codDistrito) { this.codDistrito = codDistrito; }
    public String getNombreDistrito() { return nombreDistrito; }
    public void setNombreDistrito(String nombreDistrito) { this.nombreDistrito = nombreDistrito; }
    public String getRegion() { return region; }
    public void setRegion(String region) { this.region = region; }
    public Double getLatitud() { return latitud; }
    public void setLatitud(Double latitud) { this.latitud = latitud; }
    public Double getLongitud() { return longitud; }
    public void setLongitud(Double longitud) { this.longitud = longitud; }
    public Integer getIdAutoridadForestal() { return idAutoridadForestal; }
    public void setIdAutoridadForestal(Integer idAutoridadForestal) { this.idAutoridadForestal = idAutoridadForestal; }
    public String getNombreAutoridadForestal() { return nombreAutoridadForestal; }
    public void setNombreAutoridadForestal(String nombreAutoridadForestal) { this.nombreAutoridadForestal = nombreAutoridadForestal; }
}