package pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal;

import lombok.Data;

@Data
public class SistemaPlantacionEspeciesEntity {

    private Integer idEspecie;
    private String nombreComun;
    private String nombreCientifico;



    //                        ISNULL(ed.NU_ID_SOLPLANTFOREST_DETALLE,0) idSolPlanForestDetalle
//                                ,ISNULL(ed.[NU_ID_SOLPLANTFOREST],0) idSolPlanForest
//                                ,e.idEspecie
//                                ,ISNULL(ed.TX_ESPCS_NOM_COMUN,e.nombreComun) nombreComun
//                                ,ISNULL(ed.[TX_ESPCS_NOM_CIENTIFICO],e.nombreCientifico) nombreCientifico
//                                ,ISNULL([NU_TOTAL_ARBOL_MATAS_ESPCS_EXISTENTES],0)[NU_TOTAL_ARBOL_MATAS_ESPCS_EXISTENTES]
//				   ,ISNULL([NU_PRODUCCION_ESTIMADA],0)[NU_PRODUCCION_ESTIMADA]
//				   ,ISNULL([TX_PRODUCION_ESTIMADA_UND_MEDIDA],0)[TX_PRODUCION_ESTIMADA_UND_MEDIDA]
//				   ,ISNULL([TX_COORD_ESTE],0)[TX_COORD_ESTE]
//				   ,ISNULL([TX_COORD_NORTE],0)[TX_COORD_NORTE]
}
