package pe.gob.serfor.mcsniffs.entity.Dto.VolumenComercialPromedio;

import lombok.Data;

@Data
public class VolumenComercialPromedioDetalleDto {
    String bloquequinquenal;
    String tipoBosque;
    Double areaHa;
    Integer bloque;
}
