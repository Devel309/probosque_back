package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class FichaEvaluacionEntitty implements Serializable{
    private String codigoProceso;
    private String titular;
    private String asunto;
    private String elaborador;
    private Integer idPlanManejo;
    private String tipoRequisito;
    private String observacion;
    private String evaluacion;
    private String conforme;
    private String subtitulo;
    private String detalle;
    private String codigoEvaluacionDet;
    private String observacionDet;
    
    public String getObservacionDet() {
        return observacionDet;
    }
    public void setObservacionDet(String observacionDet) {
        this.observacionDet = observacionDet;
    }
    public String getCodigoEvaluacionDet() {
        return codigoEvaluacionDet;
    }
    public void setCodigoEvaluacionDet(String codigoEvaluacionDet) {
        this.codigoEvaluacionDet = codigoEvaluacionDet;
    }
    public String getEvaluacion() {
        return evaluacion;
    }
    public void setEvaluacion(String evaluacion) {
        this.evaluacion = evaluacion;
    }
    public String getCodigoProceso() {
        return codigoProceso;
    }
    public void setCodigoProceso(String codigoProceso) {
        this.codigoProceso = codigoProceso;
    }
    public String getTitular() {
        return titular;
    }
    public void setTitular(String titular) {
        this.titular = titular;
    }
    public String getAsunto() {
        return asunto;
    }
    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }
    public String getElaborador() {
        return elaborador;
    }
    public void setElaborador(String elaborador) {
        this.elaborador = elaborador;
    }
    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }
    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }
    public String getTipoRequisito() {
        return tipoRequisito;
    }
    public void setTipoRequisito(String tipoRequisito) {
        this.tipoRequisito = tipoRequisito;
    }
    public String getObservacion() {
        return observacion;
    }
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    public String getConforme() {
        return conforme;
    }
    public void setConforme(String conforme) {
        this.conforme = conforme;
    }
    public String getSubtitulo() {
        return subtitulo;
    }
    public void setSubtitulo(String subtitulo) {
        this.subtitulo = subtitulo;
    }
    public String getDetalle() {
        return detalle;
    }
    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }
    
}
