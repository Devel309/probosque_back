package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class AspectoFisiograficoEntity extends AuditoriaEntity implements Serializable {
    private Integer idAspectoFisiografico;
    private String descripcion;
    private String especificacion;
    private Double area;
    private UnidadFisiograficaEntity unidadFisiografica;
    private PlanManejoEntity planManejo;
}
