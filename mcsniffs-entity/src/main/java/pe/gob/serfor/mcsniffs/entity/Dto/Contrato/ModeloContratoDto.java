package pe.gob.serfor.mcsniffs.entity.Dto.Contrato;


import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.Setter; 

@Getter
@Setter
public class ModeloContratoDto {
    
 
    private Integer idContrato;
    private String departamentoTitular;
    private String provinciaTitular;
    private String distritoTitular;
    private String departamentoRle;
    private String provinciaRle;
    private String distritoRle;
    private String estadoContrato;
    private String tipoContrato;
    private String tituloHabilitante;
    private String codigoPartidaReg;
    private String numeroContratoConcesion;
    private Date fechaIniContrato;
    private Date fechaFinContrato;
    private BigDecimal areaTotal;
    private String unidadAprovechamiento;
    private String region;
    private String tipoPersonaTitularTH;
    private String tipoPersonaTitularTHDesc;
    private String tipoDocumentoTitularTH;
    private String tipoDocumentoTitularTHDesc;
    private String numeroDocumentoTitularTH;
    private String nombreTitularTH;
    private String apellidoPaternoTitularTH;
    private String apellidoMaternoTitularTH; 
    private String razonSocialTitularTH;
    private String emailTitularTH;
    private Integer idUsuarioTitularTH;
    private String direccionTitularTH;
    private String tipoPersonaRepLegal;
    private String tipoPersonaRepLegalDesc;
    private String tipoDocumentoRepLegal;
    private String tipoDocumentoRepLegalDesc;
    private String numeroDocumentoRepLegal;
    private String nombreRepLegal;
    private String apellidoPaternoRepLegal;
    private String apellidoMaternoRepLegal;
    private String emailRepLegal;
    private String direccionAu;
    private String departamentoAu;
    private String provinciaAu;
    private String distritoAu;
    private String numeroDocumentoAu;
    private String direccionRepLegal;


    private String diaLetra;
    private String mesLetra;
    private String anioLetra;
    
}
