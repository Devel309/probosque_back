package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class OposicionAdjuntosEntity implements Serializable {
    private Integer idOposicionadjunto;
    private Integer idArchivo;
    private OposicionEntity oposicion;
    private byte[] documento;
    private String nombreDocumento;
    private Integer idOposicionRespuesta;


    public Integer getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }

    public Integer getIdOposicionadjunto() {
        return idOposicionadjunto;
    }

    public void setIdOposicionadjunto(Integer idOposicionadjunto) {
        this.idOposicionadjunto = idOposicionadjunto;
    }

    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }

    public Integer getIdOposicionRespuesta() {
        return idOposicionRespuesta;
    }

    public void setIdOposicionRespuesta(Integer idOposicionRespuesta) {
        this.idOposicionRespuesta = idOposicionRespuesta;
    }

    public OposicionEntity getOposicion() {
        return oposicion;
    }

    public void setOposicion(OposicionEntity oposicion) {
        this.oposicion = oposicion;
    }

    public byte[] getDocumento() {
        return documento;
    }

    public void setDocumento(byte[] documento) {
        this.documento = documento;
    }
}
