package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class EnviarConsultaEntity extends AuditoriaEntity implements Serializable {
    private Integer IdEnviarConsulta;
    private Integer IdUnidadAprovechamiento;
    private Integer IdConsultaEntidad;
    private String Asunto;
    private String Descripcion;
    private Integer IdUsuarioRemite;

    public Integer getIdEnviarConsulta() {
        return IdEnviarConsulta;
    }

    public void setIdEnviarConsulta(Integer idEnviarConsulta) {
        IdEnviarConsulta = idEnviarConsulta;
    }

    public Integer getIdUnidadAprovechamiento() {
        return IdUnidadAprovechamiento;
    }

    public void setIdUnidadAprovechamiento(Integer idUnidadAprovechamiento) {
        IdUnidadAprovechamiento = idUnidadAprovechamiento;
    }

    public Integer getIdConsultaEntidad() {
        return IdConsultaEntidad;
    }

    public void setIdConsultaEntidad(Integer idConsultaEntidad) {
        IdConsultaEntidad = idConsultaEntidad;
    }

    public String getAsunto() {
        return Asunto;
    }

    public void setAsunto(String asunto) {
        Asunto = asunto;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public Integer getIdUsuarioRemite() {
        return IdUsuarioRemite;
    }

    public void setIdUsuarioRemite(Integer idUsuarioRemite) {
        IdUsuarioRemite = idUsuarioRemite;
    }
}
