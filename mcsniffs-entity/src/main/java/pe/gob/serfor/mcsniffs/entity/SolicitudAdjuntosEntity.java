package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.Dto.Solicitud.SolicitudDto;

import java.io.Serializable;
import java.util.List;
@Data
public class SolicitudAdjuntosEntity implements Serializable {
    private Integer idSolicitudAdjunto;
    private Integer idSolicitud;
    private byte[] documento;
    private String nombredocumento;
    private Integer idTipoDocumento;
    private Integer idDocumentoAdjunto;
    private String nombreGenerado;
    private String nombre;
    private Boolean conforme;
    private String observacion;
    byte[] file;
}
