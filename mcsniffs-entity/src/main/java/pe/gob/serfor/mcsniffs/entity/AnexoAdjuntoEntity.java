package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class AnexoAdjuntoEntity  extends AuditoriaEntity  implements Serializable {
    private Integer idAnexoAdjunto;
    private Integer idAnexo;
    private String  codigoTab;
    private Boolean adjuntaAnexo;
    private Integer IdPlanManejo;

    public Integer getIdPlanManejo() { return IdPlanManejo; }

    public void setIdPlanManejo(Integer idPlanManejo) { IdPlanManejo = idPlanManejo; }

    public Integer getIdAnexoAdjunto() {
        return idAnexoAdjunto;
    }

    public void setIdAnexoAdjunto(Integer idAnexoAdjunto) {
        this.idAnexoAdjunto = idAnexoAdjunto;
    }

    public Integer getIdAnexo() {
        return idAnexo;
    }

    public void setIdAnexo(Integer idAnexo) {
        this.idAnexo = idAnexo;
    }

    public String getCodigoTab() {
        return codigoTab;
    }

    public void setCodigoTab(String codigoTab) {
        this.codigoTab = codigoTab;
    }

    public Boolean getAdjuntaAnexo() {  return adjuntaAnexo;  }

    public void setAdjuntaAnexo(Boolean adjuntaAnexo) { this.adjuntaAnexo = adjuntaAnexo; }
}
