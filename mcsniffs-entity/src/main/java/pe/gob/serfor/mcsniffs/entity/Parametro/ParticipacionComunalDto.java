package pe.gob.serfor.mcsniffs.entity.Parametro;

import pe.gob.serfor.mcsniffs.entity.ParticipacionComunalDetalleEntity;

import java.util.List;

public class ParticipacionComunalDto {
    private Integer idParticipacionComunal;
    private String codTipoPartComunal;
    private String actividad;
    private String responsable;
    private String seguimientoActividad;
    private Integer idPlanManejo;
    private List<ParticipacionComunalDetalleEntity> listParticipacionComunal;

    public Integer getIdParticipacionComunal() {
        return idParticipacionComunal;
    }

    public void setIdParticipacionComunal(Integer idParticipacionComunal) {
        this.idParticipacionComunal = idParticipacionComunal;
    }

    public String getCodTipoPartComunal() {
        return codTipoPartComunal;
    }

    public void setCodTipoPartComunal(String codTipoPartComunal) {
        this.codTipoPartComunal = codTipoPartComunal;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getSeguimientoActividad() {
        return seguimientoActividad;
    }

    public void setSeguimientoActividad(String seguimientoActividad) {
        this.seguimientoActividad = seguimientoActividad;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public List<ParticipacionComunalDetalleEntity> getListParticipacionComunal() {
        return listParticipacionComunal;
    }

    public void setListParticipacionComunal(List<ParticipacionComunalDetalleEntity> listParticipacionComunal) {
        this.listParticipacionComunal = listParticipacionComunal;
    }
}
