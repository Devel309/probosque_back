package pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
public class SolicitudPlantacionForestalDetalleEntity extends AuditoriaEntity implements Serializable {

    private Integer idSolPlantForestDetalle;
    private Integer idSolPlantForest;

    private Integer idEspecie;
    private String especieNombreComun;
    private String especieNombreCientifico;

    private Integer totalArbolesExistentes;
    private BigDecimal produccionEstimada;

    private String unidadMedida;

    private String coordenadaEste;
    private String coordenadaNorte;
    private Integer numVertice;
}
