package pe.gob.serfor.mcsniffs.entity;

import java.math.BigDecimal;
import java.util.List;

public class ActividadAprovechamientoDetEntity extends AuditoriaEntity{
    private Integer idActvAproveDet;
    private Integer idActvAprove;
    private String codActvAproveDet;
    private String codSubActvAproveDet;
    private String tipoDetalle;
    private Integer numeracion;
    private String nombreComun;
    private String nombreCientifica;
    private String familia;
    private String lineaProduccion;
	private BigDecimal dcm;
    private String sector;
    private BigDecimal area;
    private Integer totalIndividuos;
    private String individuosHA;
    private String volumen;
    private String operaciones;
    private String unidad;
    private String carcteristicaTec;
    private String personal;
    private String maquinariaMaterial;
	private String descripcion;
    private String observacion;
    private String detalle;
    private String gc;
    private List<ActividadAprovechamientoDetSubEntity> lstactividadDetSub;


   
    public List<ActividadAprovechamientoDetSubEntity> getLstactividadDetSub() {
        return lstactividadDetSub;
    }
    public void setLstactividadDetSub(List<ActividadAprovechamientoDetSubEntity> lstactividadDetSub) {
        this.lstactividadDetSub = lstactividadDetSub;
    }
    public Integer getIdActvAproveDet() {
        return idActvAproveDet;
    }
    public void setIdActvAproveDet(Integer idActvAproveDet) {
        this.idActvAproveDet = idActvAproveDet;
    }
    public Integer getIdActvAprove() {
        return idActvAprove;
    }
    public void setIdActvAprove(Integer idActvAprove) {
        this.idActvAprove = idActvAprove;
    }
    public String getCodActvAproveDet() {
        return codActvAproveDet;
    }
    public void setCodActvAproveDet(String codActvAproveDet) {
        this.codActvAproveDet = codActvAproveDet;
    }
    public String getCodSubActvAproveDet() {
        return codSubActvAproveDet;
    }
    public void setCodSubActvAproveDet(String codSubActvAproveDet) {
        this.codSubActvAproveDet = codSubActvAproveDet;
    }
    public String getTipoDetalle() {
        return tipoDetalle;
    }
    public void setTipoDetalle(String tipoDetalle) {
        this.tipoDetalle = tipoDetalle;
    }
    public Integer getNumeracion() {
        return numeracion;
    }
    public void setNumeracion(Integer numeracion) {
        this.numeracion = numeracion;
    }
    public String getNombreComun() {
        return nombreComun;
    }
    public void setNombreComun(String nombreComun) {
        this.nombreComun = nombreComun;
    }
    public String getNombreCientifica() {
        return nombreCientifica;
    }
    public void setNombreCientifica(String nombreCientifica) {
        this.nombreCientifica = nombreCientifica;
    }
    public String getFamilia() {
        return familia;
    }
    public void setFamilia(String familia) {
        this.familia = familia;
    }
    public String getLineaProduccion() {
        return lineaProduccion;
    }
    public void setLineaProduccion(String lineaProduccion) {
        this.lineaProduccion = lineaProduccion;
    }
    public BigDecimal getDcm() {
        return dcm;
    }
    public void setDcm(BigDecimal dcm) {
        this.dcm = dcm;
    }
    public String getSector() {
        return sector;
    }
    public void setSector(String sector) {
        this.sector = sector;
    }
    public BigDecimal getArea() {
        return area;
    }
    public void setArea(BigDecimal area) {
        this.area = area;
    }
    public Integer getTotalIndividuos() {
        return totalIndividuos;
    }
    public void setTotalIndividuos(Integer totalIndividuos) {
        this.totalIndividuos = totalIndividuos;
    }
    public String getIndividuosHA() {
        return individuosHA;
    }
    public void setIndividuosHA(String individuosHA) {
        this.individuosHA = individuosHA;
    }
    public String getVolumen() {
        return volumen;
    }
    public void setVolumen(String volumen) {
        this.volumen = volumen;
    }
    public String getOperaciones() {
        return operaciones;
    }
    public void setOperaciones(String operaciones) {
        this.operaciones = operaciones;
    }
    public String getUnidad() {
        return unidad;
    }
    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }
    public String getCarcteristicaTec() {
        return carcteristicaTec;
    }
    public void setCarcteristicaTec(String carcteristicaTec) {
        this.carcteristicaTec = carcteristicaTec;
    }
    public String getPersonal() {
        return personal;
    }
    public void setPersonal(String personal) {
        this.personal = personal;
    }
    public String getMaquinariaMaterial() {
        return maquinariaMaterial;
    }
    public void setMaquinariaMaterial(String maquinariaMaterial) {
        this.maquinariaMaterial = maquinariaMaterial;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String getObservacion() {
        return observacion;
    }
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    public String getDetalle() {
        return detalle;
    }
    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }
    public String getGc() {
        return gc;
    }
    public void setGc(String gc) {
        this.gc = gc;
    }
    
}
