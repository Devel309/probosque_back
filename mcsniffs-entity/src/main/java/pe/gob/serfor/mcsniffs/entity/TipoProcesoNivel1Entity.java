package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.List;

public class TipoProcesoNivel1Entity implements Serializable {
    private Integer IdTipoProceso;
    private String Descripcion;
    private Integer Nivel;
    private String Codigo;
    private String CodigoPadre;
    private List<TipoProcesoNivel2Entity> nivel2;

    public Integer getNivel() {
        return Nivel;
    }

    public void setNivel(Integer nivel) {
        Nivel = nivel;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getCodigoPadre() {
        return CodigoPadre;
    }

    public void setCodigoPadre(String codigoPadre) {
        CodigoPadre = codigoPadre;
    }

    public Integer getIdTipoProceso() {
        return IdTipoProceso;
    }

    public void setIdTipoProceso(Integer idTipoProceso) {
        IdTipoProceso = idTipoProceso;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public List<TipoProcesoNivel2Entity> getNivel2() {
        return nivel2;
    }

    public void setNivel2(List<TipoProcesoNivel2Entity> nivel2) {
        this.nivel2 = nivel2;
    }
}
