package pe.gob.serfor.mcsniffs.entity.Dto.VolumenComercialPromedio;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class VolumenComercialPromedioEspecieDto {
    Integer idTipoBosque;
    String nombreComun;
    String nombreCientifico;
    Integer DMC;
    Integer cantidadArb;
    Integer totalArb;
    Double porcentajeArb;
    BigDecimal volumenComercialPromedio;
    BigDecimal volumenComercialPromedioTotal;
    BigDecimal volumenComercialPromedioPonderado;
    BigDecimal volumenCortaAnualPermisible;
}
