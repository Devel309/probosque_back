package pe.gob.serfor.mcsniffs.entity;
import java.io.Serializable;

public class PGMFAbreviadoDetalleEntity extends AuditoriaEntity implements Serializable {
    private Integer idPGMFDetalle;
    private Integer idOrganizacionManejo;
    private Integer idPlanManejo;
    
    public PGMFAbreviadoDetalleEntity(){}

    public Integer getIdPGMFDetalle() {
        return idPGMFDetalle;
    }

    public void setIdPGMFDetalle(Integer idPGMFDetalle) {
        this.idPGMFDetalle = idPGMFDetalle;
    }

    public Integer getIdOrganizacionManejo() { return idOrganizacionManejo; }

    public void setIdOrganizacionManejo(Integer idOrganizacionManejo) {
        this.idOrganizacionManejo = idOrganizacionManejo;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }
     
}