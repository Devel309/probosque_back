package pe.gob.serfor.mcsniffs.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
public class ManejoBosqueAniosEntity extends AuditoriaEntity implements Serializable {
    private Integer  idManejoBosqueAnios ;
    private Integer idManejoBosqueDet;
    private String codigo ;
    private String descripcion;
    private String detalle;
    private String observacion;
}
