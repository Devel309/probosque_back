package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class HidrografiaEntity extends AuditoriaEntity implements Serializable {
    private Integer idHidrografia;
    private String tipoHidrografia;
    private String nombre;
    private String descripcion;
    private String desembocadura;
    private String region;

    public String getDesembocadura() {
        return desembocadura;
    }

    public void setDesembocadura(String desembocadura) {
        this.desembocadura = desembocadura;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Integer getIdHidrografia() {
        return idHidrografia;
    }

    public void setIdHidrografia(Integer idHidrografia) {
        this.idHidrografia = idHidrografia;
    }

    public String getTipoHidrografia() {
        return tipoHidrografia;
    }

    public void setTipoHidrografia(String tipoHidrografia) {
        this.tipoHidrografia = tipoHidrografia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
