package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.List;

public class ResumenActividadPoEntity extends AuditoriaEntity implements Serializable {
    private Integer idResumenAct;
    private String codTipoResumen;
    private String codigoResumen;
    private String codigoSubResumen;
    private Integer nroResolucion;
    private Integer nroPcs;
    private Double areaHa;
    private Integer idPlanManejo;
    private List<ResumenActividadPoDetalleEntity> listResumenEvaluacion;
    private List<ResumenActividadPoDetalleEntity> listResumenActividadDetalle;

    public List<ResumenActividadPoDetalleEntity> getListResumenActividadDetalle() {
        return listResumenActividadDetalle;
    }

    public void setListResumenActividadDetalle(List<ResumenActividadPoDetalleEntity> listResumenActividadDetalle) {
        this.listResumenActividadDetalle = listResumenActividadDetalle;
    }

    public String getCodigoResumen() {
        return codigoResumen;
    }

    public void setCodigoResumen(String codigoResumen) {
        this.codigoResumen = codigoResumen;
    }

    public String getCodigoSubResumen() {
        return codigoSubResumen;
    }

    public void setCodigoSubResumen(String codigoSubResumen) {
        this.codigoSubResumen = codigoSubResumen;
    }

    public Integer getIdResumenAct() {
        return idResumenAct;
    }

    public ResumenActividadPoEntity setIdResumenAct(Integer idResumenAct) {
        this.idResumenAct = idResumenAct;
        return this;
    }

    public String getCodTipoResumen() {
        return codTipoResumen;
    }

    public ResumenActividadPoEntity setCodTipoResumen(String codTipoResumen) {
        this.codTipoResumen = codTipoResumen;
        return this;
    }

    public Integer getNroResolucion() {
        return nroResolucion;
    }

    public ResumenActividadPoEntity setNroResolucion(Integer nroResolucion) {
        this.nroResolucion = nroResolucion;
        return this;
    }

    public Integer getNroPcs() {
        return nroPcs;
    }

    public ResumenActividadPoEntity setNroPcs(Integer nroPcs) {
        this.nroPcs = nroPcs;
        return this;
    }

    public Double getAreaHa() {
        return areaHa;
    }

    public ResumenActividadPoEntity setAreaHa(Double areaHa) {
        this.areaHa = areaHa;
        return this;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public ResumenActividadPoEntity setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
        return this;
    }

    public List<ResumenActividadPoDetalleEntity> getListResumenEvaluacion() {
        return listResumenEvaluacion;
    }

    public ResumenActividadPoEntity setListResumenEvaluacion(List<ResumenActividadPoDetalleEntity> listResumenEvaluacion) {
        this.listResumenEvaluacion = listResumenEvaluacion;
        return this;
    }
}
