package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class SolicitudConcesionArchivoEntity extends AuditoriaEntity implements Serializable{

    private Integer idSolicitudConcesionArchivo;
    private Integer idSolicitudConcesion;
    private String codigoArchivo;
    private String codigoSubArchivo;
    private String codigoSeccion;
    private String codigoSubSeccion;
    private String descripcion;
    private String detalle;
    private String observacion;
    private Integer idArchivo;
    private String tipoDocumento;
    private String nombreArchivo;
    private String nombreGeneradoArchivo;
    private String extensionArchivo;
    private String rutaArchivo;
    private byte[] documento;
    private Boolean conforme;
    private String descripcionSeccion;
    private String tipoArchivo;
}
