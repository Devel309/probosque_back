package pe.gob.serfor.mcsniffs.entity.Dto.LibroOperaciones;

import java.io.Serializable;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
public class LibroOperacionesDto extends AuditoriaEntity implements Serializable {
    private Integer idLibroOperaciones;
    private String nroRegistro;
    private Integer idArchivoEvidencia;
    private String estadoLO;
    private String descripcionEstadoLO;
    private Boolean permisoOtorgado;

    private String nombreSolicitanteUnico;
    private String numeroDocumento;
    private String tipoDocumento;
    private String tipoPersona;
    private String disposicionNroRegistro;

    private String fechaSolicitud;

    private Integer idLibroOperacionesPlanManejo;
    private Integer idPlanManejo;
    private Boolean vigente;
    private Integer idResolucion;
    private Integer codigoTH;

}
