package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class SolicitudConcesionAreaEntity extends AuditoriaEntity implements Serializable {

    private Integer idSolicitudConcesionArea;
    private Integer idSolicitudConcesion;
    private Double superficieHa;
    private String codigoUbigeo;
    private Integer zonaUTM;
    private String observacion;
    private String justificacion;
    private String analisisMapa;
    private String delimitacion;
    private String observacionTamanio;
    private String fuenteBibliografica;
    private Integer idDistritoArea;
    private Integer idProvinciaArea;
    private Integer idDepartamentoArea;
    private String nombreDistritoArea;
    private String nombreProvinciaArea;
    private String nombreDepartamentoArea;

    private String caractFisica;
    private String caractBiologica;
    private String caractSocioeconomica;
    private String factorAmbientalBiologico;
    private String factorSocioEconocultural;
    private String manejoConcesion;
    private String padre;
}
