package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class RentabilidadManejoForestalDetalleEntity  extends AuditoriaEntity implements Serializable {
    private Integer idRentManejoForestalDet;
    private Integer anio;
    private Double monto;
    private RentabilidadManejoForestalEntity rentabilidadManejoForestal;
    private Integer mes;

    public Integer getIdRentManejoForestalDet() {
        return idRentManejoForestalDet;
    }

    public void setIdRentManejoForestalDet(Integer idRentManejoForestalDet) {
        this.idRentManejoForestalDet = idRentManejoForestalDet;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public RentabilidadManejoForestalEntity getRentabilidadManejoForestal() {
        return rentabilidadManejoForestal;
    }

    public void setRentabilidadManejoForestal(RentabilidadManejoForestalEntity rentabilidadManejoForestal) {
        this.rentabilidadManejoForestal = rentabilidadManejoForestal;
    }

    public Integer getMes() { return mes; }
    public void setMes(Integer mes) { this.mes = mes; }
}
