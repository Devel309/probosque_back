package pe.gob.serfor.mcsniffs.entity.Parametro;

public class TablaAnexo6Dto {
    String nombreComun;
    String nTotalPorTipoBosque;
    String nTotalPorHa;
    String cTotalPorTipoBosque;
    String cTotalPorHa;
    String nombreBosque;
    String variable;

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public String getNombreBosque() {
        return nombreBosque;
    }

    public void setNombreBosque(String nombreBosque) {
        this.nombreBosque = nombreBosque;
    }

    public String getNombreComun() {
        return nombreComun;
    }

    public void setNombreComun(String nombreComun) {
        this.nombreComun = nombreComun;
    }

    public String getnTotalPorTipoBosque() {
        return nTotalPorTipoBosque;
    }

    public void setnTotalPorTipoBosque(String nTotalPorTipoBosque) {
        this.nTotalPorTipoBosque = nTotalPorTipoBosque;
    }

    public String getnTotalPorHa() {
        return nTotalPorHa;
    }

    public void setnTotalPorHa(String nTotalPorHa) {
        this.nTotalPorHa = nTotalPorHa;
    }

    public String getcTotalPorTipoBosque() {
        return cTotalPorTipoBosque;
    }

    public void setcTotalPorTipoBosque(String cTotalPorTipoBosque) {
        this.cTotalPorTipoBosque = cTotalPorTipoBosque;
    }

    public String getcTotalPorHa() {
        return cTotalPorHa;
    }

    public void setcTotalPorHa(String cTotalPorHa) {
        this.cTotalPorHa = cTotalPorHa;
    }
}
