package pe.gob.serfor.mcsniffs.entity;

public class Anexo5PFDMEntity extends FirmaAnexosPFDMEntity{
    private Integer IdPostulacionPFDM;
    private Integer IdUsuarioPostulante;
    private String motivoSolicitud;
    private String soliNombre;
    private String soliVigencia;
    private String soliSuperficie;
    private String soliUbicacion;
    private String soliIdDepartamento;
    private String soliIdProvincia;
    private String soliIdDistrito;
    private String soliSector;
    private String idAreaSolicitadaAdjunto;
    private String idVerticesAdjunto;
    private String objetivoConcesion;
    private String descripcionProyecto;
    private String fechaFinEntregaPropuesta;
    private String direccionEntregaPropuesta;


    public Integer getIdPostulacionPFDM() {
        return IdPostulacionPFDM;
    }
    public void setIdPostulacionPFDM(Integer idPostulacionPFDM) {
        IdPostulacionPFDM = idPostulacionPFDM;
    }
    public Integer getIdUsuarioPostulante() {
        return IdUsuarioPostulante;
    }
    public void setIdUsuarioPostulante(Integer idUsuarioPostulante) {
        IdUsuarioPostulante = idUsuarioPostulante;
    }
    public String getMotivoSolicitud() {
        return motivoSolicitud;
    }
    public void setMotivoSolicitud(String motivoSolicitud) {
        this.motivoSolicitud = motivoSolicitud;
    }
    public String getSoliNombre() {
        return soliNombre;
    }
    public void setSoliNombre(String soliNombre) {
        this.soliNombre = soliNombre;
    }
    public String getSoliVigencia() {
        return soliVigencia;
    }
    public void setSoliVigencia(String soliVigencia) {
        this.soliVigencia = soliVigencia;
    }
    public String getSoliSuperficie() {
        return soliSuperficie;
    }
    public void setSoliSuperficie(String soliSuperficie) {
        this.soliSuperficie = soliSuperficie;
    }
    public String getSoliUbicacion() {
        return soliUbicacion;
    }
    public void setSoliUbicacion(String soliUbicacion) {
        this.soliUbicacion = soliUbicacion;
    }
    public String getSoliIdDepartamento() {
        return soliIdDepartamento;
    }
    public void setSoliIdDepartamento(String soliIdDepartamento) {
        this.soliIdDepartamento = soliIdDepartamento;
    }
    public String getSoliIdProvincia() {
        return soliIdProvincia;
    }
    public void setSoliIdProvincia(String soliIdProvincia) {
        this.soliIdProvincia = soliIdProvincia;
    }
    public String getSoliIdDistrito() {
        return soliIdDistrito;
    }
    public void setSoliIdDistrito(String soliIdDistrito) {
        this.soliIdDistrito = soliIdDistrito;
    }
    public String getSoliSector() {
        return soliSector;
    }
    public void setSoliSector(String soliSector) {
        this.soliSector = soliSector;
    }
    public String getIdAreaSolicitadaAdjunto() {
        return idAreaSolicitadaAdjunto;
    }
    public void setIdAreaSolicitadaAdjunto(String idAreaSolicitadaAdjunto) {
        this.idAreaSolicitadaAdjunto = idAreaSolicitadaAdjunto;
    }
    public String getIdVerticesAdjunto() {
        return idVerticesAdjunto;
    }
    public void setIdVerticesAdjunto(String idVerticesAdjunto) {
        this.idVerticesAdjunto = idVerticesAdjunto;
    }
    public String getObjetivoConcesion() {
        return objetivoConcesion;
    }
    public void setObjetivoConcesion(String objetivoConcesion) {
        this.objetivoConcesion = objetivoConcesion;
    }
    public String getDescripcionProyecto() {
        return descripcionProyecto;
    }
    public void setDescripcionProyecto(String descripcionProyecto) {
        this.descripcionProyecto = descripcionProyecto;
    }
    public String getFechaFinEntregaPropuesta() {
        return fechaFinEntregaPropuesta;
    }
    public void setFechaFinEntregaPropuesta(String fechaFinEntregaPropuesta) {
        this.fechaFinEntregaPropuesta = fechaFinEntregaPropuesta;
    }
    public String getDireccionEntregaPropuesta() {
        return direccionEntregaPropuesta;
    }
    public void setDireccionEntregaPropuesta(String direccionEntregaPropuesta) {
        this.direccionEntregaPropuesta = direccionEntregaPropuesta;
    }

    
}
