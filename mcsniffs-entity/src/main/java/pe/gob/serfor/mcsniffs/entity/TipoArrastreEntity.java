package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.Date;

public class TipoArrastreEntity extends AuditoriaEntity implements Serializable {
    private Short IdTipoArrastre;
    private String Descripcion;

    public Short getIdTipoArrastre() {
        return IdTipoArrastre;
    }

    public void setIdTipoArrastre(Short idTipoArrastre) {
        IdTipoArrastre = idTipoArrastre;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }
}
