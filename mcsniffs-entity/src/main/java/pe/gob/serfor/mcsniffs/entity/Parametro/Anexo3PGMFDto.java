package pe.gob.serfor.mcsniffs.entity.Parametro;

public class Anexo3PGMFDto {
    String nombreComun;
    String arbHaDap10a19;
    String arbHaDap20a29;
    String arbHaTotalTipoBosque;
    String arbHaTotalPorArea;
    String abHaDap10a19;
    String abHaDap20a29;
    String abHaTotalTipoBosque;
    String abHaTotalPorArea;
    Integer bloque;
    String areaBosque;
    String nombreBosque;
    public Integer getBloque() {
        return bloque;
    }

    public void setBloque(Integer bloque) {
        this.bloque = bloque;
    }

    public String getNombreComun() {
        return nombreComun;
    }

    public void setNombreComun(String nombreComun) {
        this.nombreComun = nombreComun;
    }

    public String getArbHaDap10a19() {
        return arbHaDap10a19;
    }

    public void setArbHaDap10a19(String arbHaDap10a19) {
        this.arbHaDap10a19 = arbHaDap10a19;
    }

    public String getArbHaDap20a29() {
        return arbHaDap20a29;
    }

    public void setArbHaDap20a29(String arbHaDap20a29) {
        this.arbHaDap20a29 = arbHaDap20a29;
    }

    public String getArbHaTotalTipoBosque() {
        return arbHaTotalTipoBosque;
    }

    public void setArbHaTotalTipoBosque(String arbHaTotalTipoBosque) {
        this.arbHaTotalTipoBosque = arbHaTotalTipoBosque;
    }

    public String getArbHaTotalPorArea() {
        return arbHaTotalPorArea;
    }

    public void setArbHaTotalPorArea(String arbHaTotalPorArea) {
        this.arbHaTotalPorArea = arbHaTotalPorArea;
    }

    public String getAbHaDap10a19() {
        return abHaDap10a19;
    }

    public void setAbHaDap10a19(String abHaDap10a19) {
        this.abHaDap10a19 = abHaDap10a19;
    }

    public String getAbHaDap20a29() {
        return abHaDap20a29;
    }

    public void setAbHaDap20a29(String abHaDap20a29) {
        this.abHaDap20a29 = abHaDap20a29;
    }

    public String getAbHaTotalTipoBosque() {
        return abHaTotalTipoBosque;
    }

    public void setAbHaTotalTipoBosque(String abHaTotalTipoBosque) {
        this.abHaTotalTipoBosque = abHaTotalTipoBosque;
    }

    public String getAbHaTotalPorArea() {
        return abHaTotalPorArea;
    }

    public void setAbHaTotalPorArea(String abHaTotalPorArea) {
        this.abHaTotalPorArea = abHaTotalPorArea;
    }
    public String getAreaBosque() {
        return areaBosque;
    }

    public void setAreaBosque(String areaBosque) {
        this.areaBosque = areaBosque;
    }
    public String getNombreBosque() {
        return nombreBosque;
    }

    public void setNombreBosque(String nombreBosque) {
        this.nombreBosque = nombreBosque;
    }
}
