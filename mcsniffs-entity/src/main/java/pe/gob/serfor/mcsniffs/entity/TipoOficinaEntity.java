package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.security.cert.Extension;
import java.util.Date;
public class TipoOficinaEntity  extends  AuditoriaEntity implements Serializable{
    private Integer idTipoOficina;
    private String descripcion;

    public Integer getIdTipoOficina() { return idTipoOficina; }
    public void setIdTipoOficina(Integer idTipoOficina) { this.idTipoOficina = idTipoOficina; }
    public String getDescripcion() { return descripcion; }
    public void setDescripcion(String descripcion) { this.descripcion = descripcion; }
}