package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class LogAuditoriaEntity extends AuditoriaEntity implements Serializable {

    private Integer idLogAuditoria;
    private String nombreTabla;
    private String tipoAccion;
    private String descripcionAccion;
    private String estado;
    private Integer idUsuarioRegistro;


    public LogAuditoriaEntity(){

    }

    public LogAuditoriaEntity(String nombreTabla, String tipoAccion, String descripcionAccion, Integer idUsuarioRegistro) {
        this.nombreTabla = nombreTabla;
        this.tipoAccion = tipoAccion;
        this.descripcionAccion = descripcionAccion;
        this.idUsuarioRegistro = idUsuarioRegistro;
    }
}
