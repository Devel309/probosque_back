package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ValidacionRequisitoEntity extends AuditoriaEntity implements Serializable {
    private int idSolicitudValidacion;
    private int idSolicitud;
    private String tipoSeccion;
    private String tipovalidacion;
    private String tieneObservaciones;
    private String observacion;
    private String nombrePersonaSolicitud;
    private String apellidoPersonaSolicitud;
    private String emailPersonaSolicitud;
    private String estadoSolicitud;
    private Boolean flagObservado;

    public int getIdSolicitudValidacion() {
        return idSolicitudValidacion;
    }

    public void setIdSolicitudValidacion(int idSolicitudValidacion) {
        this.idSolicitudValidacion = idSolicitudValidacion;
    }

    public int getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(int idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public String getTipoSeccion() {
        return tipoSeccion;
    }

    public void setTipoSeccion(String tipoSeccion) {
        this.tipoSeccion = tipoSeccion;
    }

    public String getTieneObservaciones() {
        return tieneObservaciones;
    }

    public void setTieneObservaciones(String tieneObservaciones) {
        this.tieneObservaciones = tieneObservaciones;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getNombrePersonaSolicitud() {
        return nombrePersonaSolicitud;
    }

    public void setNombrePersonaSolicitud(String nombrePersonaSolicitud) {
        this.nombrePersonaSolicitud = nombrePersonaSolicitud;
    }

    public String getApellidoPersonaSolicitud() {
        return apellidoPersonaSolicitud;
    }

    public void setApellidoPersonaSolicitud(String apellidoPersonaSolicitud) {
        this.apellidoPersonaSolicitud = apellidoPersonaSolicitud;
    }

    public String getEmailPersonaSolicitud() {
        return emailPersonaSolicitud;
    }

    public void setEmailPersonaSolicitud(String emailPersonaSolicitud) {
        this.emailPersonaSolicitud = emailPersonaSolicitud;
    }

    public String getEstadoSolicitud() {
        return estadoSolicitud;
    }

    public void setEstadoSolicitud(String estadoSolicitud) {
        this.estadoSolicitud = estadoSolicitud;
    }

    public String getTipovalidacion() {
        return tipovalidacion;
    }

    public void setTipovalidacion(String tipovalidacion) {
        this.tipovalidacion = tipovalidacion;
    }

    public Boolean getFlagObservado() {
        return flagObservado;
    }

    public void setFlagObservado(Boolean flagObservado) {
        this.flagObservado = flagObservado;
    }
}
