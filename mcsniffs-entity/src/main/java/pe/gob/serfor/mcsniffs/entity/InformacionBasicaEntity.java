package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class InformacionBasicaEntity extends AuditoriaEntity implements Serializable {
    private Integer idInfBasica;
    private String  codInfBasica;
    private String  codSubInfBasica;
    private String  codNombreInfBasica;
    private String  departamento;
    private String  provincia;
    private String  distrito;
    private String  cuenca;
    private String  anexo;
    private Integer idPlanManejo;
    private Integer numeroPc;
    private BigDecimal areaTotal;
    private String nroHojaCatastral;
    private String nombreHojaCatastral;
    private Integer idHidrografia;
    private String frenteCorta;
    private String comunidad;
    private String codCabecera1;
    private String codCabecera2;
    private String codCabecera3;
    private String subCuenca;
    private String nroPc;

    public String getComunidad() {
        return comunidad;
    }

    public void setComunidad(String comunidad) {
        this.comunidad = comunidad;
    }

    private List<InformacionBasicaDetalleEntity> listInformacionBasicaDet;
    private List<InformacionBasicaUbigeoEntity> listInformacionBasicaDistrito;
    
    public Integer getIdInfBasica() {
        return idInfBasica;
    }

    public InformacionBasicaEntity setIdInfBasica(Integer idInfBasica) {
        this.idInfBasica = idInfBasica;
        return this;
    }

    public String getCodInfBasica() {
        return codInfBasica;
    }

    public InformacionBasicaEntity setCodInfBasica(String codInfBasica) {
        this.codInfBasica = codInfBasica;
        return this;
    }

    public String getCodSubInfBasica() {
        return codSubInfBasica;
    }

    public InformacionBasicaEntity setCodSubInfBasica(String codSubInfBasica) {
        this.codSubInfBasica = codSubInfBasica;
        return this;
    }

    public String getCodNombreInfBasica() {
        return codNombreInfBasica;
    }

    public InformacionBasicaEntity setCodNombreInfBasica(String codNombreInfBasica) {
        this.codNombreInfBasica = codNombreInfBasica;
        return this;
    }

    public String getDepartamento() {
        return departamento;
    }

    public InformacionBasicaEntity setDepartamento(String departamento) {
        this.departamento = departamento;
        return this;
    }

    public String getProvincia() {
        return provincia;
    }

    public InformacionBasicaEntity setProvincia(String provincia) {
        this.provincia = provincia;
        return this;
    }

    public String getDistrito() {
        return distrito;
    }

    public InformacionBasicaEntity setDistrito(String distrito) {
        this.distrito = distrito;
        return this;
    }

    public String getCuenca() {
        return cuenca;
    }

    public InformacionBasicaEntity setCuenca(String cuenca) {
        this.cuenca = cuenca;
        return this;
    }

    public String getAnexo() {
        return anexo;
    }

    public InformacionBasicaEntity setAnexo(String anexo) {
        this.anexo = anexo;
        return this;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public InformacionBasicaEntity setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
        return this;
    }

    public List<InformacionBasicaDetalleEntity> getListInformacionBasicaDet() {
        return listInformacionBasicaDet;
    }

    public InformacionBasicaEntity setListInformacionBasicaDet(List<InformacionBasicaDetalleEntity> listInformacionBasicaDet) {
        this.listInformacionBasicaDet = listInformacionBasicaDet;
        return this;
    }

    public Integer getNumeroPc() {
        return numeroPc;
    }

    public void setNumeroPc(Integer numeroPc) {
        this.numeroPc = numeroPc;
    }

    public BigDecimal getAreaTotal() {
        return areaTotal;
    }

    public void setAreaTotal(BigDecimal areaTotal) {
        this.areaTotal = areaTotal;
    }

    public String getNroHojaCatastral() {
        return nroHojaCatastral;
    }

    public void setNroHojaCatastral(String nroHojaCatastral) {
        this.nroHojaCatastral = nroHojaCatastral;
    }

    public String getNombreHojaCatastral() {
        return nombreHojaCatastral;
    }

    public void setNombreHojaCatastral(String nombreHojaCatastral) {
        this.nombreHojaCatastral = nombreHojaCatastral;
    }

    public Integer getIdHidrografia() {
        return idHidrografia;
    }

    public void setIdHidrografia(Integer idHidrografia) {
        this.idHidrografia = idHidrografia;
    }

    public String getFrenteCorta() {
        return frenteCorta;
    }

    public void setFrenteCorta(String frenteCorta) {
        this.frenteCorta = frenteCorta;
    }

    public List<InformacionBasicaUbigeoEntity> getListInformacionBasicaDistrito() {
        return listInformacionBasicaDistrito;
    }

    public void setListInformacionBasicaDistrito(List<InformacionBasicaUbigeoEntity> listInformacionBasicaDistrito) {
        this.listInformacionBasicaDistrito = listInformacionBasicaDistrito;
    }

    public String getCodCabecera1() {
        return codCabecera1;
    }

    public void setCodCabecera1(String codCabecera1) {
        this.codCabecera1 = codCabecera1;
    }

    public String getCodCabecera2() {
        return codCabecera2;
    }

    public void setCodCabecera2(String codCabecera2) {
        this.codCabecera2 = codCabecera2;
    }

    public String getCodCabecera3() {
        return codCabecera3;
    }

    public void setCodCabecera3(String codCabecera3) {
        this.codCabecera3 = codCabecera3;
    }

    public String getSubCuenca() { return subCuenca; }
    public void setSubCuenca(String subCuenca) { this.subCuenca = subCuenca; }

    public String getNroPc() { return nroPc; }
    public void setNroPc(String nroPc) { this.nroPc = nroPc; }
}

