package pe.gob.serfor.mcsniffs.entity.AutorizacionPublicacionSolicitud;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class AutorizacionPublicacionSolicitudEntity extends AuditoriaEntity implements Serializable {
    private Integer idAutorizacionPublicacionSolicitud;
    private Integer idProcesoPostulacion;
    private Integer numeroDocumento;
    private Date fechaAutorizacion;

}