package pe.gob.serfor.mcsniffs.entity.Impugnacion;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImpugnacionEntity extends AuditoriaEntity implements Serializable {
    @Id
    Integer idImpugnacion ;
    Integer idSolicitud ;
    Integer  idTipoSolicitud; 
    String  descripcion ;
    Date fechaPresentacion; 
    Boolean calificacionImpugnacion; 
    String asunto ;
    Date fechaInicio;  
    Date fechaFin ;
    Boolean  fundadoInfundado; 
    String  idTipoEvaluacion; 
  
    String  region; 

  
}
