package pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.InformacionBasica;

import pe.gob.serfor.mcsniffs.entity.InformacionBasicaEntity;
import pe.gob.serfor.mcsniffs.entity.UnidadFisiograficaEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;

public class UnidadFisiograficaUMFEntity extends UnidadFisiograficaEntity {
    private Double area;
    private Double porcentaje;
    private Boolean accion;
    private Boolean file;
    private InformacionBasicaEntity informacionBasica;
    private ResultArchivoEntity archivo;
    public Double getArea() {
        return area;
    }
    public void setArea(Double area) {
        this.area = area;
    }
    public Double getPorcentaje() {
        return porcentaje;
    }
    public void setPorcentaje(Double porcentaje) {
        this.porcentaje = porcentaje;
    }
    public Boolean getAccion() {
        return accion;
    }
    public void setAccion(Boolean accion) {
        this.accion = accion;
    }
    public Boolean getFile() {
        return file;
    }
    public void setFile(Boolean file) {
        this.file = file;
    }
    public InformacionBasicaEntity getInformacionBasica() {
        return informacionBasica;
    }
    public void setInformacionBasica(InformacionBasicaEntity informacionBasica) {
        this.informacionBasica = informacionBasica;
    }
	public ResultArchivoEntity getArchivo() {
		return archivo;
	}
	public void setArchivo(ResultArchivoEntity archivo) {
		this.archivo = archivo;
	}
    
}
