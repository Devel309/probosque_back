package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;



public class CronogramaActividadDetalle2Entity extends AuditoriaEntity implements Serializable {
    //NU_ID_PLAN_MANEJO, TX_ACTIVIDAD, TX_ESTADO
    
    private Integer idPlanManejo;
    private String actividad;    
    private String[] cabDetalle;

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String[] getCabDetalle() {
        return cabDetalle;
    }

    public void setCabDetalle(String[] cabDetalle) {
        this.cabDetalle = cabDetalle;
    }   
}