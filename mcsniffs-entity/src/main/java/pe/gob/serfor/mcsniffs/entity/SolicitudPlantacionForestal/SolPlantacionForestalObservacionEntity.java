package pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
public class SolPlantacionForestalObservacionEntity extends AuditoriaEntity{

Integer id_solplantacionforest_observacion ;
Integer id_solplantforest ;
Integer id_detalleseccion ;
Integer evaluacion ;
Boolean alerta ;
String observacion ;
public Integer getId_solplantacionforest_observacion() {
    return id_solplantacionforest_observacion;
}
public void setId_solplantacionforest_observacion(Integer id_solplantacionforest_observacion) {
    this.id_solplantacionforest_observacion = id_solplantacionforest_observacion;
}
public Integer getId_solplantforest() {
    return id_solplantforest;
}
public void setId_solplantforest(Integer id_solplantforest) {
    this.id_solplantforest = id_solplantforest;
}
public Integer getId_detalleseccion() {
    return id_detalleseccion;
}
public void setId_detalleseccion(Integer id_detalleseccion) {
    this.id_detalleseccion = id_detalleseccion;
}
public Integer getEvaluacion() {
    return evaluacion;
}
public void setEvaluacion(Integer evaluacion) {
    this.evaluacion = evaluacion;
}
public Boolean getAlerta() {
    return alerta;
}
public void setAlerta(Boolean alerta) {
    this.alerta = alerta;
}
public String getObservacion() {
    return observacion;
}
public void setObservacion(String observacion) {
    this.observacion = observacion;
}


}
