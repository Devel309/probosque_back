package pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Getter
@Setter
public class SolicitudBosqueLocalFlujoEstadoDto extends AuditoriaEntity implements Serializable {
    private Integer idFlujoEstado;
    private Integer idSolBosqueLocal;
    private String estadoSolicitud; 
 
}
