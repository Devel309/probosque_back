package pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima;

import java.util.Date;
import java.util.List;

public class CondicionMinimaDto {

    private Integer idCondicionMinima;
    private String tipoDocumento;
    private String nroDocumento;
    private String descripcion;
    private String resultadoEvaluacion;
    private Date fechaEvaluacion;
    private String estado;
    private Integer idUsuarioRegistro;

    private List<CondicionMinimaDetalleDto> condicionMinimaDetalle;

    public Integer getIdCondicionMinima() {
        return idCondicionMinima;
    }

    public void setIdCondicionMinima(Integer idCondicionMinima) {
        this.idCondicionMinima = idCondicionMinima;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getResultadoEvaluacion() {
        return resultadoEvaluacion;
    }

    public void setResultadoEvaluacion(String resultadoEvaluacion) {
        this.resultadoEvaluacion = resultadoEvaluacion;
    }

    public Date getFechaEvaluacion() {
        return fechaEvaluacion;
    }

    public void setFechaEvaluacion(Date fechaEvaluacion) {
        this.fechaEvaluacion = fechaEvaluacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Integer getIdUsuarioRegistro() {
        return idUsuarioRegistro;
    }

    public void setIdUsuarioRegistro(Integer idUsuarioRegistro) {
        this.idUsuarioRegistro = idUsuarioRegistro;
    }

    public List<CondicionMinimaDetalleDto> getCondicionMinimaDetalle() {
        return condicionMinimaDetalle;
    }

    public void setCondicionMinimaDetalle(List<CondicionMinimaDetalleDto> condicionMinimaDetalle) {
        this.condicionMinimaDetalle = condicionMinimaDetalle;
    }
}
