package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class AntecedenteUsoEntity extends AuditoriaEntity implements Serializable {
    private Integer idAntecedenteUso;
    private String descripcion;

    public Integer getIdAntecedenteUso() {
        return idAntecedenteUso;
    }

    public void setIdAntecedenteUso(Integer idAntecedenteUso) {
        this.idAntecedenteUso = idAntecedenteUso;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
