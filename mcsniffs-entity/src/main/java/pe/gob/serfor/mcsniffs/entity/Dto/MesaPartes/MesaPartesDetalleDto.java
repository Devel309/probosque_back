package pe.gob.serfor.mcsniffs.entity.Dto.MesaPartes;

import java.io.Serializable;
import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Getter
@Setter
public class MesaPartesDetalleDto extends AuditoriaEntity implements Serializable {
    
    private Integer idMesaPartesDet;
    private String codigo;
    private String conforme;
    private Integer idArchivo;
    private String requisito;
    private Integer idMesaPartes;
    private String observacion;
    private String detalle;
    private String descripcion;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Timestamp fecha;
    private String tipoRequisito;
    private String comentario;
    private String archivo;
    private String justificacion;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Timestamp fechaDocumento;

}
