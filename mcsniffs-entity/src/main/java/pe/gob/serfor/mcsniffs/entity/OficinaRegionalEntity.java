package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class OficinaRegionalEntity extends AuditoriaEntity implements Serializable {
    private Integer IdOficinaReginal;
    private Integer IdTipoOficina;
    private String NombreOficina;
    private String Direccion;
    private String NombrePoblado;
    private String CodUbigeo;
    private Short ZonaUtm;
    private Short IdOrigen;
    private Short IdAutoridadForestal;
    private BigDecimal CoordenadaNo;
    private BigDecimal CoordenadaEs;
    private Integer TipoUbicacion;
    private String Fuente;
    private String DocumentoRegistro;
    private String Observacion;

    public Integer getIdOficinaReginal() {
        return IdOficinaReginal;
    }

    public void setIdOficinaReginal(Integer idOficinaReginal) {
        IdOficinaReginal = idOficinaReginal;
    }

    public Integer getIdTipoOficina() {
        return IdTipoOficina;
    }

    public void setIdTipoOficina(Integer idTipoOficina) {
        IdTipoOficina = idTipoOficina;
    }

    public String getNombreOficina() {
        return NombreOficina;
    }

    public void setNombreOficina(String nombreOficina) {
        NombreOficina = nombreOficina;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public String getNombrePoblado() {
        return NombrePoblado;
    }

    public void setNombrePoblado(String nombrePoblado) {
        NombrePoblado = nombrePoblado;
    }

    public String getCodUbigeo() {
        return CodUbigeo;
    }

    public void setCodUbigeo(String codUbigeo) {
        CodUbigeo = codUbigeo;
    }

    public Short getZonaUtm() {
        return ZonaUtm;
    }

    public void setZonaUtm(Short zonaUtm) {
        ZonaUtm = zonaUtm;
    }

    public Short getIdOrigen() {
        return IdOrigen;
    }

    public void setIdOrigen(Short idOrigen) {
        IdOrigen = idOrigen;
    }

    public Short getIdAutoridadForestal() {
        return IdAutoridadForestal;
    }

    public void setIdAutoridadForestal(Short idAutoridadForestal) {
        IdAutoridadForestal = idAutoridadForestal;
    }

    public BigDecimal getCoordenadaNo() {
        return CoordenadaNo;
    }

    public void setCoordenadaNo(BigDecimal coordenadaNo) {
        CoordenadaNo = coordenadaNo;
    }

    public BigDecimal getCoordenadaEs() {
        return CoordenadaEs;
    }

    public void setCoordenadaEs(BigDecimal coordenadaEs) {
        CoordenadaEs = coordenadaEs;
    }

    public Integer getTipoUbicacion() {
        return TipoUbicacion;
    }

    public void setTipoUbicacion(Integer tipoUbicacion) {
        TipoUbicacion = tipoUbicacion;
    }

    public String getFuente() {
        return Fuente;
    }

    public void setFuente(String fuente) {
        Fuente = fuente;
    }

    public String getDocumentoRegistro() {
        return DocumentoRegistro;
    }

    public void setDocumentoRegistro(String documentoRegistro) {
        DocumentoRegistro = documentoRegistro;
    }

    public String getObservacion() {
        return Observacion;
    }

    public void setObservacion(String observacion) {
        Observacion = observacion;
    }
}
