package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import java.io.Serializable;
@Data
public class CoordenadaUtmEntity extends AuditoriaEntity implements Serializable {

	private Integer idCoordUtm;
	private Integer idUnidadManejo;
	private String anexoSector;
	private String referencia;
	private Integer tipo;
	private Integer vertice;
	private Double este;
	private Double norte;


}
