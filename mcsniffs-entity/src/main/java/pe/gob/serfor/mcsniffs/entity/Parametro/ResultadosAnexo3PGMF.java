package pe.gob.serfor.mcsniffs.entity.Parametro;

public class ResultadosAnexo3PGMF {
    String especie;
    String nDap10a15;
    String nDap15a20;
    String nDap20a30;
    String nDap30a40;
    String nDap40a50;
    String nDap50a60;
    String nDap60a70;
    String nDap70aMas;
    String nDapTotalTipoBosque;
    String nDapTotalTipoBosquePorHa;
    String abDap10a15;
    String abDap15a20;
    String abDap20a30;
    String abDap30a40;
    String abDap40a50;
    String abDap50a60;
    String abDap60a70;
    String abDap70aMas;
    String abDapTotalTipoBosque;
    String abDapTotalTipoBosquePorHa;
    String volDap10a15;
    String volDap15a20;
    String volDap20a30;
    String volDap30a40;
    String volDap40a50;
    String volDap50a60;
    String volDap60a70;
    String volDap70aMas;
    String volDapTotalTipoBosque;
    String volDapTotalTipoBosquePorHa;


    public String getnDap50a60() {
        return nDap50a60;
    }

    public void setnDap50a60(String nDap50a60) {
        this.nDap50a60 = nDap50a60;
    }

    public String getAbDap50a60() {
        return abDap50a60;
    }

    public void setAbDap50a60(String abDap50a60) {
        this.abDap50a60 = abDap50a60;
    }

    public String getVolDap50a60() {
        return volDap50a60;
    }

    public void setVolDap50a60(String volDap50a60) {
        this.volDap50a60 = volDap50a60;
    }

    public String getEspecie() {
        return especie;
    }

    public String getnDap10a15() {
        return nDap10a15;
    }

    public void setnDap10a15(String nDap10a15) {
        this.nDap10a15 = nDap10a15;
    }

    public String getnDap15a20() {
        return nDap15a20;
    }

    public void setnDap15a20(String nDap15a20) {
        this.nDap15a20 = nDap15a20;
    }

    public String getnDap20a30() {
        return nDap20a30;
    }

    public void setnDap20a30(String nDap20a30) {
        this.nDap20a30 = nDap20a30;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getnDap30a40() {
        return nDap30a40;
    }

    public void setnDap30a40(String nDap30a40) {
        this.nDap30a40 = nDap30a40;
    }

    public String getnDap40a50() {
        return nDap40a50;
    }

    public void setnDap40a50(String nDap40a50) {
        this.nDap40a50 = nDap40a50;
    }

    public String getnDap60a70() {
        return nDap60a70;
    }

    public void setnDap60a70(String nDap60a70) {
        this.nDap60a70 = nDap60a70;
    }

    public String getnDap70aMas() {
        return nDap70aMas;
    }

    public void setnDap70aMas(String nDap70aMas) {
        this.nDap70aMas = nDap70aMas;
    }

    public String getnDapTotalTipoBosque() {
        return nDapTotalTipoBosque;
    }

    public void setnDapTotalTipoBosque(String nDapTotalTipoBosque) {
        this.nDapTotalTipoBosque = nDapTotalTipoBosque;
    }

    public String getnDapTotalTipoBosquePorHa() {
        return nDapTotalTipoBosquePorHa;
    }

    public void setnDapTotalTipoBosquePorHa(String nDapTotalTipoBosquePorHa) {
        this.nDapTotalTipoBosquePorHa = nDapTotalTipoBosquePorHa;
    }

    public String getAbDap10a15() {
        return abDap10a15;
    }

    public void setAbDap10a15(String abDap10a15) {
        this.abDap10a15 = abDap10a15;
    }

    public String getAbDap15a20() {
        return abDap15a20;
    }

    public void setAbDap15a20(String abDap15a20) {
        this.abDap15a20 = abDap15a20;
    }

    public String getAbDap20a30() {
        return abDap20a30;
    }

    public void setAbDap20a30(String abDap20a30) {
        this.abDap20a30 = abDap20a30;
    }

    public String getAbDap30a40() {
        return abDap30a40;
    }

    public void setAbDap30a40(String abDap30a40) {
        this.abDap30a40 = abDap30a40;
    }

    public String getAbDap40a50() {
        return abDap40a50;
    }

    public void setAbDap40a50(String abDap40a50) {
        this.abDap40a50 = abDap40a50;
    }

    public String getAbDap60a70() {
        return abDap60a70;
    }

    public void setAbDap60a70(String abDap60a70) {
        this.abDap60a70 = abDap60a70;
    }

    public String getAbDap70aMas() {
        return abDap70aMas;
    }

    public void setAbDap70aMas(String abDap70aMas) {
        this.abDap70aMas = abDap70aMas;
    }

    public String getAbDapTotalTipoBosque() {
        return abDapTotalTipoBosque;
    }

    public void setAbDapTotalTipoBosque(String abDapTotalTipoBosque) {
        this.abDapTotalTipoBosque = abDapTotalTipoBosque;
    }

    public String getAbDapTotalTipoBosquePorHa() {
        return abDapTotalTipoBosquePorHa;
    }

    public void setAbDapTotalTipoBosquePorHa(String abDapTotalTipoBosquePorHa) {
        this.abDapTotalTipoBosquePorHa = abDapTotalTipoBosquePorHa;
    }

    public String getVolDap10a15() {
        return volDap10a15;
    }

    public void setVolDap10a15(String volDap10a15) {
        this.volDap10a15 = volDap10a15;
    }

    public String getVolDap15a20() {
        return volDap15a20;
    }

    public void setVolDap15a20(String volDap15a20) {
        this.volDap15a20 = volDap15a20;
    }

    public String getVolDap20a30() {
        return volDap20a30;
    }

    public void setVolDap20a30(String volDap20a30) {
        this.volDap20a30 = volDap20a30;
    }

    public String getVolDap30a40() {
        return volDap30a40;
    }

    public void setVolDap30a40(String volDap30a40) {
        this.volDap30a40 = volDap30a40;
    }

    public String getVolDap40a50() {
        return volDap40a50;
    }

    public void setVolDap40a50(String volDap40a50) {
        this.volDap40a50 = volDap40a50;
    }

    public String getVolDap60a70() {
        return volDap60a70;
    }

    public void setVolDap60a70(String volDap60a70) {
        this.volDap60a70 = volDap60a70;
    }

    public String getVolDap70aMas() {
        return volDap70aMas;
    }

    public void setVolDap70aMas(String volDap70aMas) {
        this.volDap70aMas = volDap70aMas;
    }

    public String getVolDapTotalTipoBosque() {
        return volDapTotalTipoBosque;
    }

    public void setVolDapTotalTipoBosque(String volDapTotalTipoBosque) {
        this.volDapTotalTipoBosque = volDapTotalTipoBosque;
    }

    public String getVolDapTotalTipoBosquePorHa() {
        return volDapTotalTipoBosquePorHa;
    }

    public void setVolDapTotalTipoBosquePorHa(String volDapTotalTipoBosquePorHa) {
        this.volDapTotalTipoBosquePorHa = volDapTotalTipoBosquePorHa;
    }
}
