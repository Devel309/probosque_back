package pe.gob.serfor.mcsniffs.entity.Dto.ImpactoAmbientalPmfi;

import java.io.Serializable;

public class MedidasPmfiDto implements Serializable {

    private Integer idImpactoAmbiental;
    private String codigoImpactoAmbiental;
    private Integer idPlanManejo;
    private String codigoImpactoAmbientalDet;

    public Integer getIdImpactoAmbiental() {
        return idImpactoAmbiental;
    }

    public void setIdImpactoAmbiental(Integer idImpactoAmbiental) {
        this.idImpactoAmbiental = idImpactoAmbiental;
    }

    public String getCodigoImpactoAmbiental() {
        return codigoImpactoAmbiental;
    }

    public void setCodigoImpactoAmbiental(String codigoImpactoAmbiental) {
        this.codigoImpactoAmbiental = codigoImpactoAmbiental;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public String getCodigoImpactoAmbientalDet() {
        return codigoImpactoAmbientalDet;
    }

    public void setCodigoImpactoAmbientalDet(String codigoImpactoAmbientalDet) {
        this.codigoImpactoAmbientalDet = codigoImpactoAmbientalDet;
    }
}
