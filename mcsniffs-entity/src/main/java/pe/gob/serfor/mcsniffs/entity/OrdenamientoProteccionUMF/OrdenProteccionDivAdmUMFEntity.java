package pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionUMF;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
public class OrdenProteccionDivAdmUMFEntity extends AuditoriaEntity{
   Integer id_ordenprotec_divadm_umf ;
   Integer id_plan_manejo ;
    
   Integer numero ;
   Integer anios ;
    
   Integer tipobloque_seccion ;	
    
    Integer numerobloque ;
    String bloque ;
    Integer  tipobosque ;
    String  area ;
    Integer  porcentaje ;
    public Integer getId_ordenprotec_divadm_umf() {
        return id_ordenprotec_divadm_umf;
    }
    public void setId_ordenprotec_divadm_umf(Integer id_ordenprotec_divadm_umf) {
        this.id_ordenprotec_divadm_umf = id_ordenprotec_divadm_umf;
    }
    public Integer getId_plan_manejo() {
        return id_plan_manejo;
    }
    public void setId_plan_manejo(Integer id_plan_manejo) {
        this.id_plan_manejo = id_plan_manejo;
    }
    public Integer getNumero() {
        return numero;
    }
    public void setNumero(Integer numero) {
        this.numero = numero;
    }
    public Integer getAnios() {
        return anios;
    }
    public void setAnios(Integer anios) {
        this.anios = anios;
    }
    public Integer getTipobloque_seccion() {
        return tipobloque_seccion;
    }
    public void setTipobloque_seccion(Integer tipobloque_seccion) {
        this.tipobloque_seccion = tipobloque_seccion;
    }
    public Integer getNumerobloque() {
        return numerobloque;
    }
    public void setNumerobloque(Integer numerobloque) {
        this.numerobloque = numerobloque;
    }
    public String getBloque() {
        return bloque;
    }
    public void setBloque(String bloque) {
        this.bloque = bloque;
    }
    public Integer getTipobosque() {
        return tipobosque;
    }
    public void setTipobosque(Integer tipobosque) {
        this.tipobosque = tipobosque;
    }
    public String getArea() {
        return area;
    }
    public void setArea(String area) {
        this.area = area;
    }
    public Integer getPorcentaje() {
        return porcentaje;
    }
    public void setPorcentaje(Integer porcentaje) {
        this.porcentaje = porcentaje;
    }
    

}
