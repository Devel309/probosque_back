package pe.gob.serfor.mcsniffs.entity.Dto.DescargarDeclaracionManejoProductosForestales;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Getter
@Setter
public class DescargarArchivoDeclaracionManejoProductosForestalesDto extends  AuditoriaEntity implements Serializable {
    private Integer idPlanManejo;

    private String codigo;
    private String subCodigoGeneral;
    private String codigoProceso;
    private String codCabecera;
}
