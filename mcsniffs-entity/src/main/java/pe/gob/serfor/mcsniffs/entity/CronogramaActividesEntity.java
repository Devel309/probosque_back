package pe.gob.serfor.mcsniffs.entity;


import lombok.Data;



@Data
// @AllArgsConstructor
//@NoArgsConstructor
//@Entity
//@Table(name = "T_MVD_CRONOGRAMAACTIVIDAD", schema = "dbo")
public class CronogramaActividesEntity extends AuditoriaEntity {//implements Identifiable<Integer>, Serializable {

    //@Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    //@Column(name = "ID_CRON_ACTIVIDAD", updatable = false, nullable = false)
    Integer ID_CRON_ACTIVIDAD;

    Integer ID_PLAN_MANEJO;
    String ACTIVIDAD;
    Integer ANO;
    Double IMPORTE;
    String ACTIVIDAD_ACTUAL;
    String[] ANIO;



    public Integer getID_CRON_ACTIVIDAD() {
        return ID_CRON_ACTIVIDAD;
    }
    public void setID_CRON_ACTIVIDAD(Integer iD_CRON_ACTIVIDAD) {
        ID_CRON_ACTIVIDAD = iD_CRON_ACTIVIDAD;
    }
    public Integer getID_PLAN_MANEJO() {
        return ID_PLAN_MANEJO;
    }
    public void setID_PLAN_MANEJO(Integer iD_PLAN_MANEJO) {
        ID_PLAN_MANEJO = iD_PLAN_MANEJO;
    }
    public String getACTIVIDAD() {
        return ACTIVIDAD;
    }
    public void setACTIVIDAD(String aCTIVIDAD) {
        ACTIVIDAD = aCTIVIDAD;
    }
    public Integer getANO() {
        return ANO;
    }
    public void setANO(Integer aNO) {
        ANO = aNO;
    }
    public Double getIMPORTE() {
        return IMPORTE;
    }
    public void setIMPORTE(Double iMPORTE) {
        IMPORTE = iMPORTE;
    }
    public String getACTIVIDAD_ACTUAL() {
        return ACTIVIDAD_ACTUAL;
    }
    public void setACTIVIDAD_ACTUAL(String aCTIVIDAD_ACTUAL) {
        ACTIVIDAD_ACTUAL = aCTIVIDAD_ACTUAL;
    }
    public String[] getANIO() {
        return ANIO;
    }
    public void setANIO(String[] aNIO) {
        ANIO = aNIO;
    }
    
      

 
    

}
