package pe.gob.serfor.mcsniffs.entity.Parametro;

import java.util.List;

public class ResultadosVolumenDeCortaTabla {
    String nombreComun;
    String nombreCientifico;
     List<ParcelaCorteDto> parcelaCorteDtos;
    String numArbolesTotales;
    String volumenTotal;

    public String getNombreComun() {
        return nombreComun;
    }

    public void setNombreComun(String nombreComun) {
        this.nombreComun = nombreComun;
    }

    public String getNombreCientifico() {
        return nombreCientifico;
    }

    public void setNombreCientifico(String nombreCientifico) {
        this.nombreCientifico = nombreCientifico;
    }

    public List<ParcelaCorteDto> getParcelaCorteDtos() {
        return parcelaCorteDtos;
    }

    public void setParcelaCorteDtos(List<ParcelaCorteDto> parcelaCorteDtos) {
        this.parcelaCorteDtos = parcelaCorteDtos;
    }

    public String getNumArbolesTotales() {
        return numArbolesTotales;
    }

    public void setNumArbolesTotales(String numArbolesTotales) {
        this.numArbolesTotales = numArbolesTotales;
    }

    public String getVolumenTotal() {
        return volumenTotal;
    }

    public void setVolumenTotal(String volumenTotal) {
        this.volumenTotal = volumenTotal;
    }


}
