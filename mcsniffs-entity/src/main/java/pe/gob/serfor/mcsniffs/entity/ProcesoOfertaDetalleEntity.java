package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ProcesoOfertaDetalleEntity extends AuditoriaEntity implements Serializable {
    private Integer IdProcesoOferta;
    private Integer IdUnidadAprovechamiento;

    public Integer getIdProcesoOferta() {
        return IdProcesoOferta;
    }

    public void setIdProcesoOferta(Integer idProcesoOferta) {
        IdProcesoOferta = idProcesoOferta;
    }

    public Integer getIdUnidadAprovechamiento() {
        return IdUnidadAprovechamiento;
    }

    public void setIdUnidadAprovechamiento(Integer idUnidadAprovechamiento) {
        IdUnidadAprovechamiento = idUnidadAprovechamiento;
    }
}
