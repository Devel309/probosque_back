package pe.gob.serfor.mcsniffs.entity;

public class Anexo4PFDMEntity extends FirmaAnexosPFDMEntity {
    private Integer IdPostulacionPFDM;
    private Integer IdUsuarioPostulante;
    private String soliNombreRS;
    private String soliSuperficie;
    private String soliPlazo;
    private String soliUbicacionPolitica;
    private String soliSector;
    private String soliIdDistrito;
    private String soliIdProvincia;
    private String soliIdDepartamento;
    private String tamaConectividad;
    private String tamaAmbito;
    private String tamaProteccion;
    private String tamaCuenca;
    private String tamaOtros;
    private String justificacion;
    private String idMapaAdjunto;
    private String delimitacion;
    private String observaciones;
    private String fuentesBibliograficas;
    private String idShapeFileFirmado;
    private String idImagenesConsilidado;

    
    public String getSoliIdProvincia() {
        return soliIdProvincia;
    }
    public void setSoliIdProvincia(String soliIdProvincia) {
        this.soliIdProvincia = soliIdProvincia;
    }
    public Integer getIdPostulacionPFDM() {
        return IdPostulacionPFDM;
    }
    public void setIdPostulacionPFDM(Integer idPostulacionPFDM) {
        IdPostulacionPFDM = idPostulacionPFDM;
    }
    public Integer getIdUsuarioPostulante() {
        return IdUsuarioPostulante;
    }
    public void setIdUsuarioPostulante(Integer idUsuarioPostulante) {
        IdUsuarioPostulante = idUsuarioPostulante;
    }
    public String getSoliNombreRS() {
        return soliNombreRS;
    }
    public void setSoliNombreRS(String soliNombreRS) {
        this.soliNombreRS = soliNombreRS;
    }
    public String getSoliSuperficie() {
        return soliSuperficie;
    }
    public void setSoliSuperficie(String soliSuperficie) {
        this.soliSuperficie = soliSuperficie;
    }
    public String getSoliPlazo() {
        return soliPlazo;
    }
    public void setSoliPlazo(String soliPlazo) {
        this.soliPlazo = soliPlazo;
    }
    public String getSoliUbicacionPolitica() {
        return soliUbicacionPolitica;
    }
    public void setSoliUbicacionPolitica(String soliUbicacionPolitica) {
        this.soliUbicacionPolitica = soliUbicacionPolitica;
    }
    public String getSoliSector() {
        return soliSector;
    }
    public void setSoliSector(String soliSector) {
        this.soliSector = soliSector;
    }
    public String getSoliIdDistrito() {
        return soliIdDistrito;
    }
    public void setSoliIdDistrito(String soliIdDistrito) {
        this.soliIdDistrito = soliIdDistrito;
    }
    public String getSoliIdDepartamento() {
        return soliIdDepartamento;
    }
    public void setSoliIdDepartamento(String soliIdDepartamento) {
        this.soliIdDepartamento = soliIdDepartamento;
    }
    public String getTamaConectividad() {
        return tamaConectividad;
    }
    public void setTamaConectividad(String tamaConectividad) {
        this.tamaConectividad = tamaConectividad;
    }
    public String getTamaAmbito() {
        return tamaAmbito;
    }
    public void setTamaAmbito(String tamaAmbito) {
        this.tamaAmbito = tamaAmbito;
    }
    public String getTamaProteccion() {
        return tamaProteccion;
    }
    public void setTamaProteccion(String tamaProteccion) {
        this.tamaProteccion = tamaProteccion;
    }
    public String getTamaCuenca() {
        return tamaCuenca;
    }
    public void setTamaCuenca(String tamaCuenca) {
        this.tamaCuenca = tamaCuenca;
    }
    public String getTamaOtros() {
        return tamaOtros;
    }
    public void setTamaOtros(String tamaOtros) {
        this.tamaOtros = tamaOtros;
    }
    public String getJustificacion() {
        return justificacion;
    }
    public void setJustificacion(String justificacion) {
        this.justificacion = justificacion;
    }
    public String getIdMapaAdjunto() {
        return idMapaAdjunto;
    }
    public void setIdMapaAdjunto(String idMapaAdjunto) {
        this.idMapaAdjunto = idMapaAdjunto;
    }
    public String getDelimitacion() {
        return delimitacion;
    }
    public void setDelimitacion(String delimitacion) {
        this.delimitacion = delimitacion;
    }
    public String getObservaciones() {
        return observaciones;
    }
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
    public String getFuentesBibliograficas() {
        return fuentesBibliograficas;
    }
    public void setFuentesBibliograficas(String fuentesBibliograficas) {
        this.fuentesBibliograficas = fuentesBibliograficas;
    }
    public String getIdShapeFileFirmado() {
        return idShapeFileFirmado;
    }
    public void setIdShapeFileFirmado(String idShapeFileFirmado) {
        this.idShapeFileFirmado = idShapeFileFirmado;
    }
    public String getIdImagenesConsilidado() {
        return idImagenesConsilidado;
    }
    public void setIdImagenesConsilidado(String idImagenesConsilidado) {
        this.idImagenesConsilidado = idImagenesConsilidado;
    }
    


}
