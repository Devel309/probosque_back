package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class TipoDocGestionErgtfEntity implements Serializable {
    private short idDocumentoGestion;
    private String descripcionCorta;
    private String descripcion;
    private String tipoDocGestion;
    private String estado;

    public short getIdDocumentoGestion() { return idDocumentoGestion; }
    public void setIdDocumentoGestion(short idDocumentoGestion) { this.idDocumentoGestion = idDocumentoGestion; }
    public String getDescripcionCorta() { return descripcionCorta; }
    public void setDescripcionCorta(String descripcionCorta) { this.descripcionCorta = descripcionCorta; }
    public String getDescripcion() { return descripcion; }
    public void setDescripcion(String descripcion) { this.descripcion = descripcion; }
    public String getTipoDocGestion() { return tipoDocGestion; }
    public void setTipoDocGestion(String tipoDocGestion) { this.tipoDocGestion = tipoDocGestion; }
    public String getEstado() { return estado; }
    public void setEstado(String estado) { this.estado = estado; }


}
