package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class MonitoreoDetalleEntity extends AuditoriaEntity implements Serializable {
    private Integer idMonitoreoDetalle;
    private String descripcion;
    private String responsable;
    private Integer idMonitoreo;
    private String actividad;
    private String observacion;
    private String operacion;
    private String subOperacion;
    private String codigoMonotoreoDet;


    public String getSubOperacion() {
        return subOperacion;
    }

    public void setSubOperacion(String subOperacion) {
        this.subOperacion = subOperacion;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public Integer getIdMonitoreoDetalle() {
        return idMonitoreoDetalle;
    }
    public void setIdMonitoreoDetalle(Integer idMonitoreoDetalle) {
        this.idMonitoreoDetalle = idMonitoreoDetalle;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String getResponsable() {
        return responsable;
    }
    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }
    public Integer getIdMonitoreo() {
        return idMonitoreo;
    }
    public void setIdMonitoreo(Integer idMonitoreo) {
        this.idMonitoreo = idMonitoreo;
    }
    public String getActividad() {
        return actividad;
    }
    public void setActividad(String actividad) {
        this.actividad = actividad;
    }
    public String getObservacion() {
        return observacion;
    }
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    public String getCodigoMonotoreoDet() {
        return codigoMonotoreoDet;
    }
    public void setCodigoMonotoreoDet(String codigoMonotoreoDet) {
        this.codigoMonotoreoDet = codigoMonotoreoDet;
    }
    
}
