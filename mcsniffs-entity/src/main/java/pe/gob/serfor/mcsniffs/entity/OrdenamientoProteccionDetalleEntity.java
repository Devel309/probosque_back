package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class OrdenamientoProteccionDetalleEntity extends AuditoriaEntity implements Serializable {
    private Integer idOrdenamientoProteccionDet;
    private String codigoTipoOrdenamientoDet;
    private String categoria;
    private String actividad;
    private String meta;
    private BigDecimal areaHA;
    private BigDecimal areaHAPorcentaje;
    private String descripcion;
    private String observacion;
    private String verticeBloque;
    private BigDecimal coordenadaEste;
    private BigDecimal coordenadaNorte;
    private String zonaUTM;
    private String observacionDetalle;
    private String usoPotencial;
    private String actividadesRealizar;
    private Integer idOrdenamientoProtecccion;
    private Boolean accion;

    private String tipoBosque;
    private String bloqueQuinquenal;
    private String parcelaCorta;
    private String actRealizar;
    private Integer idArchivo;

    public Integer getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }

    public Integer getIdOrdenamientoProteccionDet() {
        return idOrdenamientoProteccionDet;
    }

    public void setIdOrdenamientoProteccionDet(Integer idOrdenamientoProteccionDet) {
        this.idOrdenamientoProteccionDet = idOrdenamientoProteccionDet;
    }


    public String getCodigoTipoOrdenamientoDet() {
        return codigoTipoOrdenamientoDet;
    }

    public void setCodigoTipoOrdenamientoDet(String codigoTipoOrdenamientoDet) {
        this.codigoTipoOrdenamientoDet = codigoTipoOrdenamientoDet;
    }

    public String getActRealizar() {
        return actRealizar;
    }

    public void setActRealizar(String actRealizar) {
        this.actRealizar = actRealizar;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

  

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getVerticeBloque() {
        return verticeBloque;
    }

    public void setVerticeBloque(String verticeBloque) {
        this.verticeBloque = verticeBloque;
    }
 

    public String getZonaUTM() {
        return zonaUTM;
    }

    public void setZonaUTM(String zonaUTM) {
        this.zonaUTM = zonaUTM;
    }

    public String getObservacionDetalle() {
        return observacionDetalle;
    }

    public void setObservacionDetalle(String observacionDetalle) {
        this.observacionDetalle = observacionDetalle;
    }

    public String getUsoPotencial() {
        return usoPotencial;
    }

    public void setUsoPotencial(String usoPotencial) {
        this.usoPotencial = usoPotencial;
    }

    public String getActividadesRealizar() {
        return actividadesRealizar;
    }

    public Boolean getAccion() {
        return accion;
    }

    public void setAccion(Boolean accion) {
        this.accion = accion;
    }

    public void setActividadesRealizar(String actividadesRealizar) {
        this.actividadesRealizar = actividadesRealizar;
    }

    public Integer getIdOrdenamientoProtecccion() {
        return idOrdenamientoProtecccion;
    }

    public void setIdOrdenamientoProtecccion(Integer idOrdenamientoProtecccion) {
        this.idOrdenamientoProtecccion = idOrdenamientoProtecccion;
    }

    

    public BigDecimal getAreaHA() {
        return areaHA;
    }

    public void setAreaHA(BigDecimal areaHA) {
        this.areaHA = areaHA;
    }

    public BigDecimal getAreaHAPorcentaje() {
        return areaHAPorcentaje;
    }

    public void setAreaHAPorcentaje(BigDecimal areaHAPorcentaje) {
        this.areaHAPorcentaje = areaHAPorcentaje;
    }

    public BigDecimal getCoordenadaEste() {
        return coordenadaEste;
    }

    public void setCoordenadaEste(BigDecimal coordenadaEste) {
        this.coordenadaEste = coordenadaEste;
    }

    public BigDecimal getCoordenadaNorte() {
        return coordenadaNorte;
    }

    public void setCoordenadaNorte(BigDecimal coordenadaNorte) {
        this.coordenadaNorte = coordenadaNorte;
    }

    public OrdenamientoProteccionDetalleEntity(){

    }
    public OrdenamientoProteccionDetalleEntity(Integer idOrdenamientoProteccionDet ) {
        this.idOrdenamientoProteccionDet = idOrdenamientoProteccionDet;

    }

    public String getTipoBosque() {
        return tipoBosque;
    }

    public void setTipoBosque(String tipoBosque) {
        this.tipoBosque = tipoBosque;
    }

    public String getBloqueQuinquenal() {
        return bloqueQuinquenal;
    }

    public void setBloqueQuinquenal(String bloqueQuinquenal) {
        this.bloqueQuinquenal = bloqueQuinquenal;
    }

    public String getParcelaCorta() {
        return parcelaCorta;
    }

    public void setParcelaCorta(String parcelaCorta) {
        this.parcelaCorta = parcelaCorta;
    }

    
}
