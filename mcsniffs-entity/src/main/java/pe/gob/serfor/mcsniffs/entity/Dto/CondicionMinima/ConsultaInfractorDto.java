package pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima;

import lombok.Data;

@Data
public class ConsultaInfractorDto {

    private String tipoDocumento;
    private String numDocumento;
 
}