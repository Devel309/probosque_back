package pe.gob.serfor.mcsniffs.entity.Dto.MedidaEvaluacion;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Getter
@Setter
public class MedidaDetalleDto extends AuditoriaEntity implements Serializable{
    
    private Integer idMedidaDet;
    private String autoridadSancionadora;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaResolucion;
    private String numeroResolucion;
    private Integer idArchivo;
    private Integer proceso;
    private String plazo;
    private Integer idMedida;
    private String detalle;
    private String observacion;
    private String descripcion;
    private String contrato;
}
