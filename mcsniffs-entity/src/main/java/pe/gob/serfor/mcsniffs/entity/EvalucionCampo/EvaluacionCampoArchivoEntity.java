package pe.gob.serfor.mcsniffs.entity.EvalucionCampo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EvaluacionCampoArchivoEntity  extends AuditoriaEntity  implements Serializable {
    
    Integer idEvaluacionCampoArchivo ;
    Integer idEvaluacionCampo ;
    Integer idArchivo ;
    String idTipoDocumento ;
    Integer idTipoSeccionArchivo ;
    String nombre;
    String tipoDocumento;
}
