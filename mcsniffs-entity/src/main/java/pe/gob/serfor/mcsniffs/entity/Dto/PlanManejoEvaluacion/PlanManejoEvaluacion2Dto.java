package pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlanManejoEvaluacion2Dto extends AuditoriaEntity implements Serializable {

    private Integer idPlanManejoEvaluacion;
    private Integer idPlanManejo;
    private Integer idTitularRegente;
    private Date fechaPresentacion;
    private String estadoPlanManejoEvaluacion;
    private Date fechaEstadoEvaluacion;
    private String descEstadoPlanManejoEvaluacion;

}
