package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SolicitudBosqueLocalFlujEstadoEntity extends AuditoriaEntity implements Serializable {

    private Integer idSolBosqueLocal;
    private String numeroDocumento;
    private String nombreArchivo;
    private String rutaArchivo;
    private Integer idDepartamento;
    private Integer idProvincia;
    private Integer idDistrito;
    private String departamento;
    private String Perfil;
    private Integer idUsuario;
    private String persona;
    private String telefono;
    private String Email;
    private Integer idPersona;
}
