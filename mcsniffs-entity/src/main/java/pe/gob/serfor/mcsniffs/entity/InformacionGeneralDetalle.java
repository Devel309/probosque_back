package pe.gob.serfor.mcsniffs.entity;

import java.math.BigDecimal;

public class InformacionGeneralDetalle extends AuditoriaEntity{
    private Integer idInforGeneDetalle;
    private Integer idInforGeneral;
    private String codigo;
    private String descripcion;
    private String detalle;
    private String observacion;

    private String codigoInformacionDet;
    private String tipoInformacionDet;
    private Integer vigenciaDet;
    private Integer vigenciaFinalDet;
    private BigDecimal area;

    public String getCodigoInformacionDet() {
        return codigoInformacionDet;
    }

    public void setCodigoInformacionDet(String codigoInformacionDet) {
        this.codigoInformacionDet = codigoInformacionDet;
    }

    public String getTipoInformacionDet() {
        return tipoInformacionDet;
    }

    public void setTipoInformacionDet(String tipoInformacionDet) {
        this.tipoInformacionDet = tipoInformacionDet;
    }

    public Integer getVigenciaDet() {
        return vigenciaDet;
    }

    public void setVigenciaDet(Integer vigenciaDet) {
        this.vigenciaDet = vigenciaDet;
    }

    public Integer getVigenciaFinalDet() {
        return vigenciaFinalDet;
    }

    public void setVigenciaFinalDet(Integer vigenciaFinalDet) {
        this.vigenciaFinalDet = vigenciaFinalDet;
    }

    public BigDecimal getArea() {
        return area;
    }

    public void setArea(BigDecimal area) {
        this.area = area;
    }

    public Integer getIdInforGeneDetalle() {
        return idInforGeneDetalle;
    }
    public void setIdInforGeneDetalle(Integer idInforGeneDetalle) {
        this.idInforGeneDetalle = idInforGeneDetalle;
    }
    public Integer getIdInforGeneral() {
        return idInforGeneral;
    }
    public void setIdInforGeneral(Integer idInforGeneral) {
        this.idInforGeneral = idInforGeneral;
    }
    public String getCodigo() {
        return codigo;
    }
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String getDetalle() {
        return detalle;
    }
    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }
    public String getObservacion() {
        return observacion;
    }
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    
}
