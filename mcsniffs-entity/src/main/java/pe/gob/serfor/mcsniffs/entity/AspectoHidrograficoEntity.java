package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;


public class AspectoHidrograficoEntity extends AuditoriaEntity implements Serializable {
    private Integer idAspectoHidrografico;
    private String descripcion;
    private Integer idPlanManejo;

    public Integer getIdAspectoHidrografico() {
        return idAspectoHidrografico;
    }

    public void setIdAspectoHidrografico(Integer idAspectoHidrografico) {
        this.idAspectoHidrografico = idAspectoHidrografico;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }
}
