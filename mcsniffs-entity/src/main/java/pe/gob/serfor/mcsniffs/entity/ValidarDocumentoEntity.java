package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.Date;

public class ValidarDocumentoEntity extends AuditoriaEntity implements Serializable {
    private Integer IdValidadAnexo;
    private String Descripcion;
    private Integer IdDocumentoAdjunto;
    private Boolean Devolucion;
    private Integer IdValidarDetalle;
    private String Observacion;
    private Date FechaInicioPlazo;
    private Date FechaFinPlazo;
    private Integer Sla;
    private Integer IdStatusAnexo;
    private String DescripcionStatus;
    private Integer IdAnexoDetalleStatus;
    private String CodigoAnexo;
    private Integer IdUsuarioPostulacion;
    private Integer IdProcesoPostulacion;
    private Integer IdPostulacionPFDM;
    private Boolean Titulo;

    
    public Integer getIdPostulacionPFDM() {
        return IdPostulacionPFDM;
    }

    public void setIdPostulacionPFDM(Integer idPostulacionPFDM) {
        IdPostulacionPFDM = idPostulacionPFDM;
    }

    public Integer getIdProcesoPostulacion() {
        return IdProcesoPostulacion;
    }

    public void setIdProcesoPostulacion(Integer idProcesoPostulacion) {
        IdProcesoPostulacion = idProcesoPostulacion;
    }

    public String getCodigoAnexo() {
        return CodigoAnexo;
    }

    public void setCodigoAnexo(String codigoAnexo) {
        CodigoAnexo = codigoAnexo;
    }

    public Integer getIdUsuarioPostulacion() {
        return IdUsuarioPostulacion;
    }

    public void setIdUsuarioPostulacion(Integer idUsuarioPostulacion) {
        IdUsuarioPostulacion = idUsuarioPostulacion;
    }


    public Integer getIdAnexoDetalleStatus() {
        return IdAnexoDetalleStatus;
    }

    public void setIdAnexoDetalleStatus(Integer idAnexoDetalleStatus) {
        IdAnexoDetalleStatus = idAnexoDetalleStatus;
    }

    public Integer getIdStatusAnexo() {
        return IdStatusAnexo;
    }

    public void setIdStatusAnexo(Integer idStatusAnexo) {
        IdStatusAnexo = idStatusAnexo;
    }

    public String getDescripcionStatus() {
        return DescripcionStatus;
    }

    public void setDescripcionStatus(String descripcionStatus) {
        DescripcionStatus = descripcionStatus;
    }

    public Integer getIdValidarDetalle() {
        return IdValidarDetalle;
    }

    public void setIdValidarDetalle(Integer idValidarDetalle) {
        IdValidarDetalle = idValidarDetalle;
    }

    public Date getFechaInicioPlazo() {
        return FechaInicioPlazo;
    }

    public void setFechaInicioPlazo(Date fechaInicioPlazo) {
        FechaInicioPlazo = fechaInicioPlazo;
    }

    public String getObservacion() {
        return Observacion;
    }

    public void setObservacion(String observacion) {
        Observacion = observacion;
    }

    public Date getFechaFinPlazo() {
        return FechaFinPlazo;
    }

    public void setFechaFinPlazo(Date fechaFinPlazo) {
        FechaFinPlazo = fechaFinPlazo;
    }

    public Integer getSla() {
        return Sla;
    }

    public void setSla(Integer sla) {
        Sla = sla;
    }

    public Integer getIdValidadAnexo() {
        return IdValidadAnexo;
    }

    public void setIdValidadAnexo(Integer idValidadAnexo) {
        IdValidadAnexo = idValidadAnexo;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public Integer getIdDocumentoAdjunto() {
        return IdDocumentoAdjunto;
    }

    public void setIdDocumentoAdjunto(Integer idDocumentoAdjunto) {
        IdDocumentoAdjunto = idDocumentoAdjunto;
    }

    public Boolean getDevolucion() {
        return Devolucion;
    }

    public void setDevolucion(Boolean devolucion) {
        Devolucion = devolucion;
    }

    public Boolean getTitulo() {
        return Titulo;
    }

    public void setTitulo(Boolean titulo) {
        Titulo = titulo;
    }
}
