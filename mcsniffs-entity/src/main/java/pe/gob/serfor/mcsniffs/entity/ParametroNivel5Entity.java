package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class ParametroNivel5Entity implements Serializable {
    Integer idParametro;
    String codigo;
    String valorPrimario;
    String valorSecundario;
    String valorTerciario;
    String codigoSniffs;
    String codigoSniffsDescripcion;
    String prefijo;
    Integer idPlanManejo;
    String Observacion;
    Integer idEvaluacionDet;

}
