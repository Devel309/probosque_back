package pe.gob.serfor.mcsniffs.entity.EvalucionCampo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EvaluacionCampoDetalleEntity extends AuditoriaEntity  implements Serializable{
   
    Integer idEvaluacionCampoDetalle ;
    Integer idEvaluacionCampo ;
    Integer idObservancia ;
    String observacia ;
}
