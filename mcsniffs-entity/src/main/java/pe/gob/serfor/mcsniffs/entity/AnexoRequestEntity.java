package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class AnexoRequestEntity implements Serializable {
    private Integer IdProcesoPostulacion;
    private Integer IdUsuarioPostulacion;
    private String CodigoAnexo;
    private Integer IdStatusAnexo;
    private Integer IdUsuarioModificacion;
    private Integer IdAnexoDetalle;
    private Integer IdDocumentoAdjunto;
    private Integer IdUsuarioCreacion;
    private Integer IdTipoDocumento;
    private Integer IdPostulacionPFDM;
    

    
    public Integer getIdPostulacionPFDM() {
        return IdPostulacionPFDM;
    }

    public void setIdPostulacionPFDM(Integer idPostulacionPFDM) {
        IdPostulacionPFDM = idPostulacionPFDM;
    }

    public Integer getIdTipoDocumento() {
    return IdTipoDocumento;
}

public void setIdTipoDocumento(Integer idTipoDocumento) {
    IdTipoDocumento = idTipoDocumento;
}

    public Integer getIdUsuarioCreacion() {
    return IdUsuarioCreacion;
}

public void setIdUsuarioCreacion(Integer idUsuarioCreacion) {
    IdUsuarioCreacion = idUsuarioCreacion;
}

    public Integer getIdDocumentoAdjunto() {
    return IdDocumentoAdjunto;
}

public void setIdDocumentoAdjunto(Integer idDocumentoAdjunto) {
    IdDocumentoAdjunto = idDocumentoAdjunto;
}

    public Integer getIdAnexoDetalle() {
        return IdAnexoDetalle;
    }

    public void setIdAnexoDetalle(Integer idAnexoDetalle) {
        IdAnexoDetalle = idAnexoDetalle;
    }

    public Integer getIdUsuarioModificacion() {
        return IdUsuarioModificacion;
    }

    public void setIdUsuarioModificacion(Integer idUsuarioModificacion) {
        IdUsuarioModificacion = idUsuarioModificacion;
    }

    public Integer getIdStatusAnexo() {
        return IdStatusAnexo;
    }

    public void setIdStatusAnexo(Integer idStatusAnexo) {
        IdStatusAnexo = idStatusAnexo;
    }

    public Integer getIdProcesoPostulacion() {
        return IdProcesoPostulacion;
    }

    public void setIdProcesoPostulacion(Integer idProcesoPostulacion) {
        IdProcesoPostulacion = idProcesoPostulacion;
    }

    public Integer getIdUsuarioPostulacion() {
        return IdUsuarioPostulacion;
    }

    public void setIdUsuarioPostulacion(Integer idUsuarioPostulacion) {
        IdUsuarioPostulacion = idUsuarioPostulacion;
    }

    public String getCodigoAnexo() {
        return CodigoAnexo;
    }

    public void setCodigoAnexo(String codigoAnexo) {
        CodigoAnexo = codigoAnexo;
    }
}
