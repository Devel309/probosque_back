package pe.gob.serfor.mcsniffs.entity.Parametro;

public class Anexo2PGMFDto {
    String nombreComun;
    String arbHaDap20a29;
    String arbHaDap30a39;
    String arbHaDap40a49;
    String arbHaDap50a59;
    String arbHaDap60a69;
    String arbHaDap70a79;
    String arbHaDap80a89;
    String arbHaDap90aMas;
    String arbHaDap70aMas;
    String arbHaTotalTipoBosque;
    String arbHaTotalPorArea;
    String abHaDap20a29;
    String abHaDap30a39;
    String abHaDap40a49;
    String abHaDap50a59;
    String abHaDap60a69;
    String abHaDap70a79;
    String abHaDap80a89;
    String abHaDap90aMas;
    String abHaDap70aMas;
    String abHaTotalTipoBosque;
    String abHaTotalPorArea;
    String volHaDap20a29;
    String volHaDap30a39;
    String volHaDap40a49;
    String volHaDap50a59;
    String volHaDap60a69;
    String volHaDap70a79;
    String volHaDap80a89;
    String volHaDap90aMas;
    String volHaDap70aMas;

    String volHaTotalTipoBosque;
    String volHaTotalPorArea;
    Integer bloque;
    String areaBosque;
    String nombreBosque;

    public Integer getBloque() {
        return bloque;
    }

    public void setBloque(Integer bloque) {
        this.bloque = bloque;
    }

    public String getAbHaDap20a29() {
        return abHaDap20a29;
    }

    public void setAbHaDap20a29(String abHaDap20a29) {
        this.abHaDap20a29 = abHaDap20a29;
    }

    public String getAbHaDap70aMas() {
        return abHaDap70aMas;
    }

    public void setAbHaDap70aMas(String abHaDap70aMas) {
        this.abHaDap70aMas = abHaDap70aMas;
    }

    public String getVolHaDap20a29() {
        return volHaDap20a29;
    }

    public void setVolHaDap20a29(String volHaDap20a29) {
        this.volHaDap20a29 = volHaDap20a29;
    }

    public String getVolHaDap70aMas() {
        return volHaDap70aMas;
    }

    public void setVolHaDap70aMas(String volHaDap70aMas) {
        this.volHaDap70aMas = volHaDap70aMas;
    }

    public String getArbHaDap20a29() {
        return arbHaDap20a29;
    }

    public void setArbHaDap20a29(String arbHaDap20a29) {
        this.arbHaDap20a29 = arbHaDap20a29;
    }

    public String getArbHaDap70aMas() {
        return arbHaDap70aMas;
    }

    public void setArbHaDap70aMas(String arbHaDap70aMas) {
        this.arbHaDap70aMas = arbHaDap70aMas;
    }

    public String getNombreComun() {
        return nombreComun;
    }

    public void setNombreComun(String nombreComun) {
        this.nombreComun = nombreComun;
    }

    public String getArbHaDap30a39() {
        return arbHaDap30a39;
    }

    public void setArbHaDap30a39(String arbHaDap30a39) {
        this.arbHaDap30a39 = arbHaDap30a39;
    }

    public String getArbHaDap40a49() {
        return arbHaDap40a49;
    }

    public void setArbHaDap40a49(String arbHaDap40a49) {
        this.arbHaDap40a49 = arbHaDap40a49;
    }

    public String getArbHaDap50a59() {
        return arbHaDap50a59;
    }

    public void setArbHaDap50a59(String arbHaDap50a59) {
        this.arbHaDap50a59 = arbHaDap50a59;
    }

    public String getArbHaDap60a69() {
        return arbHaDap60a69;
    }

    public void setArbHaDap60a69(String arbHaDap60a69) {
        this.arbHaDap60a69 = arbHaDap60a69;
    }

    public String getArbHaDap70a79() {
        return arbHaDap70a79;
    }

    public void setArbHaDap70a79(String arbHaDap70a79) {
        this.arbHaDap70a79 = arbHaDap70a79;
    }

    public String getArbHaDap80a89() {
        return arbHaDap80a89;
    }

    public void setArbHaDap80a89(String arbHaDap80a89) {
        this.arbHaDap80a89 = arbHaDap80a89;
    }

    public String getArbHaDap90aMas() {
        return arbHaDap90aMas;
    }

    public void setArbHaDap90aMas(String arbHaDap90aMas) {
        this.arbHaDap90aMas = arbHaDap90aMas;
    }

    public String getArbHaTotalTipoBosque() {
        return arbHaTotalTipoBosque;
    }

    public void setArbHaTotalTipoBosque(String arbHaTotalTipoBosque) {
        this.arbHaTotalTipoBosque = arbHaTotalTipoBosque;
    }

    public String getArbHaTotalPorArea() {
        return arbHaTotalPorArea;
    }

    public void setArbHaTotalPorArea(String arbHaTotalPorArea) {
        this.arbHaTotalPorArea = arbHaTotalPorArea;
    }

    public String getAbHaDap30a39() {
        return abHaDap30a39;
    }

    public void setAbHaDap30a39(String abHaDap30a39) {
        this.abHaDap30a39 = abHaDap30a39;
    }

    public String getAbHaDap40a49() {
        return abHaDap40a49;
    }

    public void setAbHaDap40a49(String abHaDap40a49) {
        this.abHaDap40a49 = abHaDap40a49;
    }

    public String getAbHaDap50a59() {
        return abHaDap50a59;
    }

    public void setAbHaDap50a59(String abHaDap50a59) {
        this.abHaDap50a59 = abHaDap50a59;
    }

    public String getAbHaDap60a69() {
        return abHaDap60a69;
    }

    public void setAbHaDap60a69(String abHaDap60a69) {
        this.abHaDap60a69 = abHaDap60a69;
    }

    public String getAbHaDap70a79() {
        return abHaDap70a79;
    }

    public void setAbHaDap70a79(String abHaDap70a79) {
        this.abHaDap70a79 = abHaDap70a79;
    }

    public String getAbHaDap80a89() {
        return abHaDap80a89;
    }

    public void setAbHaDap80a89(String abHaDap80a89) {
        this.abHaDap80a89 = abHaDap80a89;
    }

    public String getAbHaDap90aMas() {
        return abHaDap90aMas;
    }

    public void setAbHaDap90aMas(String abHaDap90aMas) {
        this.abHaDap90aMas = abHaDap90aMas;
    }

    public String getAbHaTotalTipoBosque() {
        return abHaTotalTipoBosque;
    }

    public void setAbHaTotalTipoBosque(String abHaTotalTipoBosque) {
        this.abHaTotalTipoBosque = abHaTotalTipoBosque;
    }

    public String getAbHaTotalPorArea() {
        return abHaTotalPorArea;
    }

    public void setAbHaTotalPorArea(String abHaTotalPorArea) {
        this.abHaTotalPorArea = abHaTotalPorArea;
    }

    public String getVolHaDap30a39() {
        return volHaDap30a39;
    }

    public void setVolHaDap30a39(String volHaDap30a39) {
        this.volHaDap30a39 = volHaDap30a39;
    }

    public String getVolHaDap40a49() {
        return volHaDap40a49;
    }

    public void setVolHaDap40a49(String volHaDap40a49) {
        this.volHaDap40a49 = volHaDap40a49;
    }

    public String getVolHaDap50a59() {
        return volHaDap50a59;
    }

    public void setVolHaDap50a59(String volHaDap50a59) {
        this.volHaDap50a59 = volHaDap50a59;
    }

    public String getVolHaDap60a69() {
        return volHaDap60a69;
    }

    public void setVolHaDap60a69(String volHaDap60a69) {
        this.volHaDap60a69 = volHaDap60a69;
    }

    public String getVolHaDap70a79() {
        return volHaDap70a79;
    }

    public void setVolHaDap70a79(String volHaDap70a79) {
        this.volHaDap70a79 = volHaDap70a79;
    }

    public String getVolHaDap80a89() {
        return volHaDap80a89;
    }

    public void setVolHaDap80a89(String volHaDap80a89) {
        this.volHaDap80a89 = volHaDap80a89;
    }

    public String getVolHaDap90aMas() {
        return volHaDap90aMas;
    }

    public void setVolHaDap90aMas(String volHaDap90aMas) {
        this.volHaDap90aMas = volHaDap90aMas;
    }

    public String getVolHaTotalTipoBosque() {
        return volHaTotalTipoBosque;
    }

    public void setVolHaTotalTipoBosque(String volHaTotalTipoBosque) {
        this.volHaTotalTipoBosque = volHaTotalTipoBosque;
    }

    public String getVolHaTotalPorArea() {
        return volHaTotalPorArea;
    }

    public void setVolHaTotalPorArea(String volHaTotalPorArea) {
        this.volHaTotalPorArea = volHaTotalPorArea;
    }
    public String getAreaBosque() {
        return areaBosque;
    }

    public void setAreaBosque(String areaBosque) {
        this.areaBosque = areaBosque;
    }
    public String getNombreBosque() {
        return nombreBosque;
    }

    public void setNombreBosque(String nombreBosque) {
        this.nombreBosque = nombreBosque;
    }
}
