package pe.gob.serfor.mcsniffs.entity;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
@Getter
@Setter
public class CuencaEntity implements Serializable {
    private Integer idCuenca;
    private String codCuenca;
    private String cuenca;
    private short idSubCuenca;
    private String codSubCuenca;
    private String subCuenca;

    private String codigo;
    private String valor;
}
