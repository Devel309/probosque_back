package pe.gob.serfor.mcsniffs.entity;
import java.io.Serializable;

public class ProteccionBosqueDetalleActividadEntity  {

    private Integer idProBosqueActividad;
    private Integer idProBosqueDet;
    private String codigo;
    private String descripcion;
    private boolean accion;

    public Integer getIdProBosqueActividad() {
        return idProBosqueActividad;
    }

    public void setIdProBosqueActividad(Integer idProBosqueActividad) {
        this.idProBosqueActividad = idProBosqueActividad;
    }

    public Integer getIdProBosqueDet() {
        return idProBosqueDet;
    }

    public void setIdProBosqueDet(Integer idProBosqueDet) {
        this.idProBosqueDet = idProBosqueDet;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isAccion() {
        return accion;
    }

    public void setAccion(boolean accion) {
        this.accion = accion;
    }
}
