package pe.gob.serfor.mcsniffs.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ParticipacionComunalDetEntity {
    private Integer idPartComunal;
    private Integer idPartComunalDet;
    private String codigoPartComunal;
    private String codPartComunalDet;
    private String participacion;
    private String metodologia;
    private String mecanismos;
    private String actividad;
    private String lugar;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
    private Date fecha;
    private String descripcion;
    private String observacion;
    private String detalle;
    private Integer idUsuarioRegistro;
    
    public Integer getIdPartComunal() {
        return idPartComunal;
    }
    public void setIdPartComunal(Integer idPartComunal) {
        this.idPartComunal = idPartComunal;
    }
    public Integer getIdPartComunalDet() {
        return idPartComunalDet;
    }
    public void setIdPartComunalDet(Integer idPartComunalDet) {
        this.idPartComunalDet = idPartComunalDet;
    }
    public String getCodigoPartComunal() {
        return codigoPartComunal;
    }
    public void setCodigoPartComunal(String codigoPartComunal) {
        this.codigoPartComunal = codigoPartComunal;
    }
    public String getCodPartComunalDet() {
        return codPartComunalDet;
    }
    public void setCodPartComunalDet(String codPartComunalDet) {
        this.codPartComunalDet = codPartComunalDet;
    }
    public String getParticipacion() {
        return participacion;
    }
    public void setParticipacion(String participacion) {
        this.participacion = participacion;
    }
    public String getMetodologia() {
        return metodologia;
    }
    public void setMetodologia(String metodologia) {
        this.metodologia = metodologia;
    }
    public String getMecanismos() {
        return mecanismos;
    }
    public void setMecanismos(String mecanismos) {
        this.mecanismos = mecanismos;
    }
    public String getActividad() {
        return actividad;
    }
    public void setActividad(String actividad) {
        this.actividad = actividad;
    }
    public String getLugar() {
        return lugar;
    }
    public void setLugar(String lugar) {
        this.lugar = lugar;
    }
    public Date getFecha() {
        return fecha;
    }
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String getObservacion() {
        return observacion;
    }
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    public String getDetalle() {
        return detalle;
    }
    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }
    public Integer getIdUsuarioRegistro() {
        return idUsuarioRegistro;
    }
    public void setIdUsuarioRegistro(Integer idUsuarioRegistro) {
        this.idUsuarioRegistro = idUsuarioRegistro;
    }
    
   
}
