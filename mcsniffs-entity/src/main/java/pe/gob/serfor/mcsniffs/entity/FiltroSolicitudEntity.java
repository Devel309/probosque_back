package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class FiltroSolicitudEntity implements Serializable {
    private Integer IdTipoSolicitud;
    private Integer IdProcesoPostulacion;
    private Boolean Aprobado;
    private Integer IdSolicitud;
    private Integer IdProcesoOferta;
    

    public Integer getIdSolicitud() {
        return IdSolicitud;
    }

    public void setIdSolicitud(Integer idSolicitud) {
        IdSolicitud = idSolicitud;
    }

    public Integer getIdProcesoOferta() {
        return IdProcesoOferta;
    }

    public void setIdProcesoOferta(Integer idProcesoOferta) {
        IdProcesoOferta = idProcesoOferta;
    }

    public Integer getIdTipoSolicitud() {
        return IdTipoSolicitud;
    }

    public void setIdTipoSolicitud(Integer idTipoSolicitud) {
        IdTipoSolicitud = idTipoSolicitud;
    }

    public Integer getIdProcesoPostulacion() {
        return IdProcesoPostulacion;
    }

    public void setIdProcesoPostulacion(Integer idProcesoPostulacion) {
        IdProcesoPostulacion = idProcesoPostulacion;
    }

    public Boolean getAprobado() {
        return Aprobado;
    }

    public void setAprobado(Boolean aprobado) {
        Aprobado = aprobado;
    }
}
