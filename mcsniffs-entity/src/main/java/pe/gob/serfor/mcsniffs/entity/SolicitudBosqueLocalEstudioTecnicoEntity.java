package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import java.io.Serializable;
import java.sql.Date;

@Data
public class SolicitudBosqueLocalEstudioTecnicoEntity extends AuditoriaEntity implements Serializable {

    private Integer idSolBosqueLocal;
    private Integer idSolBosqueLocalEstudioTecnico;
    private Integer idRegente;
    private String numeroDocumentoRegente;
    private String nombreRegente;
    private String licenciaRegente;
    private String procedimientoProyectado;
    private String procedimientoEmision;
    private String procedimientoCobro;
    private String actividadProteccion;
    private Integer idUsuarioRegistro;
    private Integer idUsuarioModificacion;
    private Boolean envioNotificacion;
    private String notificacionTexto;


}
