package pe.gob.serfor.mcsniffs.entity;

import java.math.BigDecimal;

public class VolumenCortaEntity {
    private Integer idActvAprove;
    private Integer idPlanManejo;
    private String codActvAprove;
    private String codSubActvAprove;
    private BigDecimal total;

    public Integer getIdActvAprove() {
        return idActvAprove;
    }

    public void setIdActvAprove(Integer idActvAprove) {
        this.idActvAprove = idActvAprove;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public String getCodActvAprove() {
        return codActvAprove;
    }

    public void setCodActvAprove(String codActvAprove) {
        this.codActvAprove = codActvAprove;
    }

    public String getCodSubActvAprove() {
        return codSubActvAprove;
    }

    public void setCodSubActvAprove(String codSubActvAprove) {
        this.codSubActvAprove = codSubActvAprove;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }
}
