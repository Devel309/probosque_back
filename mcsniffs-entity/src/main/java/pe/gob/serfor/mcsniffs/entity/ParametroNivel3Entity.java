package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ParametroNivel3Entity implements Serializable {
    Integer idParametro;
    String codigo;
    String valorPrimario;
    String valorSecundario;
    String valorTerciario;
    String codigoSniffs;
    String codigoSniffsDescripcion;
    String prefijo;
    Integer idPlanManejo;
    String Observacion;
    Integer idEvaluacionDet;
    private List<ParametroNivel4Entity> listNivel4;
}
