package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ProcesoPostulacionResquestEntity implements Serializable {
    private Integer IdProcesoPostulacion;
    private ProcesoPostulacionEntity procesopostulacion;
    private Integer IdUsuarioPostulacion;
    private Anexo1Entity anexo1;
    private Anexo2Entity anexo2;
    private Anexo3Entity anexo3;

    public Integer getIdProcesoPostulacion() {
        return IdProcesoPostulacion;
    }

    public void setIdProcesoPostulacion(Integer idProcesoPostulacion) {
        IdProcesoPostulacion = idProcesoPostulacion;
    }

    public ProcesoPostulacionEntity getProcesopostulacion() {
        return procesopostulacion;
    }

    public void setProcesopostulacion(ProcesoPostulacionEntity procesopostulacion) {
        this.procesopostulacion = procesopostulacion;
    }

    public Integer getIdUsuarioPostulacion() {
        return IdUsuarioPostulacion;
    }

    public void setIdUsuarioPostulacion(Integer idUsuarioPostulacion) {
        IdUsuarioPostulacion = idUsuarioPostulacion;
    }

    public Anexo1Entity getAnexo1() {
        return anexo1;
    }

    public void setAnexo1(Anexo1Entity anexo1) {
        this.anexo1 = anexo1;
    }

    public Anexo2Entity getAnexo2() {
        return anexo2;
    }

    public void setAnexo2(Anexo2Entity anexo2) {
        this.anexo2 = anexo2;
    }

    public Anexo3Entity getAnexo3() {
        return anexo3;
    }

    public void setAnexo3(Anexo3Entity anexo3) {
        this.anexo3 = anexo3;
    }
}
