package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
@Data
public class ManejoBosqueEntity extends AuditoriaEntity implements Serializable {
    private Integer idManejoBosque;
    private String tipo;
    private String codigoManejo;
    private String subCodigoManejo;
    private String descripcionCab;
    private Integer idPlanManejo;
    private String txDetalle;
    private String txObservacion;
    private List<ManejoBosqueDetalleEntity> listManejoBosqueDetalle;
    private String codigoEscala;
    private Integer anio;
}
