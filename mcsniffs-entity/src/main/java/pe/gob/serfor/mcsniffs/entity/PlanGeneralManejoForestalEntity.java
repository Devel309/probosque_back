package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

@Data
public class PlanGeneralManejoForestalEntity extends AuditoriaEntity{
    private Integer idPlanManejo;
    private String descripcion;
    private Integer idSolicitud;
    private Integer idTipoProceso;
    private Integer idTipoPlan;
    private Short tipoEscala;
    private String aspectoComplementario;
    private Integer idPlanManejoPadre;
    

    
}
