package pe.gob.serfor.mcsniffs.entity;

import java.io.File;
import java.io.Serializable;

public class ResultadoPPAdjuntoEntity implements Serializable {
    private ResultadoPPEntity resultado;
    private byte[] documento;
    private String nombredocumento;
    private File documentoFile;

    public File getDocumentoFile() {
        return documentoFile;
    }

    public void setDocumentoFile(File documentoFile) {
        this.documentoFile = documentoFile;
    }

    public ResultadoPPEntity getResultado() {
        return resultado;
    }

    public void setResultado(ResultadoPPEntity resultado) {
        this.resultado = resultado;
    }

    public byte[] getDocumento() {
        return documento;
    }

    public void setDocumento(byte[] documento) {
        this.documento = documento;
    }

    public String getNombredocumento() {
        return nombredocumento;
    }

    public void setNombredocumento(String nombredocumento) {
        this.nombredocumento = nombredocumento;
    }
}
