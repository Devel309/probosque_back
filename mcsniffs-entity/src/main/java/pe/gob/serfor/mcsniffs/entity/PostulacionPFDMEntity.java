package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.Date;

public class PostulacionPFDMEntity extends AuditoriaEntity implements Serializable{
    private Integer IdPostulacionPFDM;
    private Integer IdUsuarioPostulacion;
    private Integer IdEstatusProceso;
    private Date FechaPostulacion;
    private String NumeroTramite;
    private Boolean RecepcionDocumentos;
    private Date FechaRecepcionDocumentos;

    public Integer getIdPostulacionPFDM() {
        return IdPostulacionPFDM;
    }
    public void setIdPostulacionPFDM(Integer idPostulacionPFDM) {
        IdPostulacionPFDM = idPostulacionPFDM;
    }
    public Integer getIdUsuarioPostulacion() {
        return IdUsuarioPostulacion;
    }
    public void setIdUsuarioPostulacion(Integer idUsuarioPostulacion) {
        IdUsuarioPostulacion = idUsuarioPostulacion;
    }
    public Integer getIdEstatusProceso() {
        return IdEstatusProceso;
    }
    public void setIdEstatusProceso(Integer idEstatusProceso) {
        IdEstatusProceso = idEstatusProceso;
    }
    public Date getFechaPostulacion() {
        return FechaPostulacion;
    }
    public void setFechaPostulacion(Date fechaPostulacion) {
        FechaPostulacion = fechaPostulacion;
    }
    public String getNumeroTramite() {
        return NumeroTramite;
    }
    public void setNumeroTramite(String numeroTramite) {
        NumeroTramite = numeroTramite;
    }
    public Boolean getRecepcionDocumentos() {
        return RecepcionDocumentos;
    }
    public void setRecepcionDocumentos(Boolean recepcionDocumentos) {
        RecepcionDocumentos = recepcionDocumentos;
    }
    public Date getFechaRecepcionDocumentos() {
        return FechaRecepcionDocumentos;
    }
    public void setFechaRecepcionDocumentos(Date fechaRecepcionDocumentos) {
        FechaRecepcionDocumentos = fechaRecepcionDocumentos;
    }
  
    
}
