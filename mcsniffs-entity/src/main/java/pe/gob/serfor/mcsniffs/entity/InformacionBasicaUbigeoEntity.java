package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class InformacionBasicaUbigeoEntity extends AuditoriaEntity implements Serializable {
    
    private Integer idInfBasicaUbigeo;
    private String codigoUbigeo;
    private boolean principal;
    private Integer idInfBasica;
    private Integer idUsuarioRegistro;
    private String codigoDepartamento;
    private String codigoProvincia;
    private String codigoDistrito;
    private String cuenca;
    private String nombreDepartamento;
    private String nombreProvincia;
    private String nombreDistrito;

    
    public Integer getIdInfBasicaUbigeo() {
        return idInfBasicaUbigeo;
    }
    public void setIdInfBasicaUbigeo(Integer idInfBasicaUbigeo) {
        this.idInfBasicaUbigeo = idInfBasicaUbigeo;
    }
    public String getCodigoUbigeo() {
        return codigoUbigeo;
    }
    public void setCodigoUbigeo(String codigoUbigeo) {
        this.codigoUbigeo = codigoUbigeo;
    }
    public boolean isPrincipal() {
        return principal;
    }
    public void setPrincipal(boolean principal) {
        this.principal = principal;
    }
    public Integer getIdInfBasica() {
        return idInfBasica;
    }
    public void setIdInfBasica(Integer idInfBasica) {
        this.idInfBasica = idInfBasica;
    }
    public Integer getIdUsuarioRegistro() {
        return idUsuarioRegistro;
    }
    public void setIdUsuarioRegistro(Integer idUsuarioRegistro) {
        this.idUsuarioRegistro = idUsuarioRegistro;
    }
    public String getCodigoDepartamento() {
        return codigoDepartamento;
    }
    public void setCodigoDepartamento(String codigoDepartamento) {
        this.codigoDepartamento = codigoDepartamento;
    }
    public String getCodigoProvincia() {
        return codigoProvincia;
    }
    public void setCodigoProvincia(String codigoProvincia) {
        this.codigoProvincia = codigoProvincia;
    }
    public String getCodigoDistrito() {
        return codigoDistrito;
    }
    public void setCodigoDistrito(String codigoDistrito) {
        this.codigoDistrito = codigoDistrito;
    }
    public String getCuenca() {
        return cuenca;
    }
    public void setCuenca(String cuenca) {
        this.cuenca = cuenca;
    }
    public String getNombreDepartamento() {
        return nombreDepartamento;
    }
    public void setNombreDepartamento(String nombreDepartamento) {
        this.nombreDepartamento = nombreDepartamento;
    }
    public String getNombreProvincia() {
        return nombreProvincia;
    }
    public void setNombreProvincia(String nombreProvincia) {
        this.nombreProvincia = nombreProvincia;
    }
    public String getNombreDistrito() {
        return nombreDistrito;
    }
    public void setNombreDistrito(String nombreDistrito) {
        this.nombreDistrito = nombreDistrito;
    }
    
    

   


    
     
}

