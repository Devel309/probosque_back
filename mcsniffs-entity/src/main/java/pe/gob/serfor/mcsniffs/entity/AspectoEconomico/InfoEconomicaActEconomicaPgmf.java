package pe.gob.serfor.mcsniffs.entity.AspectoEconomico;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
public class InfoEconomicaActEconomicaPgmf extends AuditoriaEntity{
Integer IdInfoeconomicaActiEconomica ;
Integer IdInformacionSocioEconomica;
String idTipoactividadEconomica ;
String tipoActividadEconomica;
String descripcion;
Boolean activo;
}
