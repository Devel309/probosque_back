package pe.gob.serfor.mcsniffs.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
public class OtorgamientoEntity extends AuditoriaEntity implements Serializable {
    private Integer idOtorgamiento;
    private Integer idPermisoForestal;
    private String codPermiso;
    private String codOtorgamiento;
    private String codTipo;
    private String estadoSolicitud;
    private String nroRuc;
    private String nroTituloPropiedad;
    private String nroPartidaRegistral;
    private String nroActaAsambleaRrll;
    private String nroOperacionComprobantePago;
    private String nroActaAcuerdoAsamblea;
    private String codTipoAprovechamiento;
    private String nroContratoTercero;
    private String nroActaAsamblea;
    private String escalaManejo;
    private String descripcion;
    private String detalle;
    private String observacion;
    private BigDecimal areaComunidad;
    private RegenteEntity regente;
    private InformacionGeneralPermisoForestalEntity informacionGeneral;
}
