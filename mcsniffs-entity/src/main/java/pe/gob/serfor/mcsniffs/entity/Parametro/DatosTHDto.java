package pe.gob.serfor.mcsniffs.entity.Parametro;

import java.util.Date;
import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ParametroValorEntity;

public class DatosTHDto {
    private String area;
    private String areaParcela;
    private Date fechaInicio;
    private Date fechaFin;
    private List<ParametroValorEntity> listaTipoBosque;
   // private EspecieNombreDto listaEspecies;
    private List<PoligonosDto> listaPoligonos;
    
    public String getArea() {
        return area;
    }
    public List<ParametroValorEntity> getListaTipoBosque() {
        return listaTipoBosque;
    }
    public void setListaTipoBosque(List<ParametroValorEntity> listaTipoBosque) {
        this.listaTipoBosque = listaTipoBosque;
    }
    public Date getFechaFin() {
        return fechaFin;
    }
    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }
    public Date getFechaInicio() {
        return fechaInicio;
    }
    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }
    public String getAreaParcela() {
        return areaParcela;
    }
    public void setAreaParcela(String areaParcela) {
        this.areaParcela = areaParcela;
    }
    public void setArea(String area) {
        this.area = area;
    }
    public List<PoligonosDto> getListaPoligonos() {
        return listaPoligonos;
    }

    public void setListaPoligonos(List<PoligonosDto> listaPoligonos) {
        this.listaPoligonos = listaPoligonos;
    }

}
