package pe.gob.serfor.mcsniffs.entity.AspectoEconomico;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
public class InfoEconomicaActividadesPgmf extends AuditoriaEntity {
    
Integer idInfoeconomicaActividades ;
Integer idInformacionSocioEconomica;
Integer idTipoActividad;
String tipoActividad ;
String especiesExtraidas;
String observacion ;
Boolean activo;
}
