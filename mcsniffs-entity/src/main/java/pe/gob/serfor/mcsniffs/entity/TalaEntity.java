package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.sql.Date;

public class TalaEntity  extends AuditoriaEntity implements Serializable {
    private	int idTala;
    private	int idCensoForestalDetalle;
    private	Date fechaTala;
    private	int cantidadRama;
    private	String Observaciones;
    private	String estadoTala;
    private	int numeroTrozado;
    private	String fechaTrozado;
    private	String observacionTrozado;
    
    public int getIdTala() {
        return idTala;
    }
    public void setIdTala(int idTala) {
        this.idTala = idTala;
    }
    public int getIdCensoForestalDetalle() {
        return idCensoForestalDetalle;
    }
    public void setIdCensoForestalDetalle(int idCensoForestalDetalle) {
        this.idCensoForestalDetalle = idCensoForestalDetalle;
    }
    public Date getFechaTala() {
        return fechaTala;
    }
    public void setFechaTala(Date fechaTala) {
        this.fechaTala = fechaTala;
    }
    public int getCantidadRama() {
        return cantidadRama;
    }
    public void setCantidadRama(int cantidadRama) {
        this.cantidadRama = cantidadRama;
    }
    public String getObservaciones() {
        return Observaciones;
    }
    public void setObservaciones(String observaciones) {
        Observaciones = observaciones;
    }
    public String getEstadoTala() {
        return estadoTala;
    }
    public void setEstadoTala(String estadoTala) {
        this.estadoTala = estadoTala;
    }
    public int getNumeroTrozado() {
        return numeroTrozado;
    }
    public void setNumeroTrozado(int numeroTrozado) {
        this.numeroTrozado = numeroTrozado;
    }
    public String getFechaTrozado() {
        return fechaTrozado;
    }
    public void setFechaTrozado(String fechaTrozado) {
        this.fechaTrozado = fechaTrozado;
    }
    public String getObservacionTrozado() {
        return observacionTrozado;
    }
    public void setObservacionTrozado(String observacionTrozado) {
        this.observacionTrozado = observacionTrozado;
    }

    
}
