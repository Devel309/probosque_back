package pe.gob.serfor.mcsniffs.entity.Dto.AreaSistemaPlantacion;

import java.io.Serializable;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
public class EspecieForestalSistemaPlantacionEspecieDto extends AuditoriaEntity implements Serializable  {
    private Integer idAreaSistemaPlantacion;
    private Integer idSistemaPlantacionEspecie;
    private Integer idEspecie;
    private String nombreComun;
    private String nombreCientifico;
    private String autor;
    private String familia;
}
