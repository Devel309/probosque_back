package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

import pe.gob.serfor.mcsniffs.entity.CoreCentral.DepartamentoEntity;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.DistritoEntity;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.ProvinciaEntity;

public class Anexo1Entity implements Serializable {
    private String Solicitante;
    private String RepresentanteLegal;
    private String SuperficieSolicitada;
    private Integer PlazoConcesion;
    private String SectorAnexoCaserio;
    private DistritoEntity IdDistrito;
    private ProvinciaEntity IdProvincia;
    private DepartamentoEntity IdDepartamento;
    private String RutaMapaUa;
    private String RutaPlantilla;
    private String Objetivo;
    private String CodigoAnexo;
    private Integer IdUsuarioRegistro;
    private String AutoridadForestal;
    private Integer IdStatusAnexo;
    

    public Integer getIdStatusAnexo() {
        return IdStatusAnexo;
    }

    public void setIdStatusAnexo(Integer idStatusAnexo) {
        IdStatusAnexo = idStatusAnexo;
    }

    public DistritoEntity getIdDistrito() {
        return IdDistrito;
    }

    public void setIdDistrito(DistritoEntity idDistrito) {
        IdDistrito = idDistrito;
    }

    public ProvinciaEntity getIdProvincia() {
        return IdProvincia;
    }

    public void setIdProvincia(ProvinciaEntity idProvincia) {
        IdProvincia = idProvincia;
    }

    public DepartamentoEntity getIdDepartamento() {
        return IdDepartamento;
    }

    public void setIdDepartamento(DepartamentoEntity idDepartamento) {
        IdDepartamento = idDepartamento;
    }

    public String getAutoridadForestal() {
        return AutoridadForestal;
    }

    public void setAutoridadForestal(String autoridadForestal) {
        AutoridadForestal = autoridadForestal;
    }

    public int getColumnCount() {
        return getClass().getDeclaredFields().length;
    }

    public String getCodigoAnexo() {
        return CodigoAnexo;
    }

    public void setCodigoAnexo(String codigoAnexo) {
        CodigoAnexo = codigoAnexo;
    }

    public Integer getIdUsuarioRegistro() {
        return IdUsuarioRegistro;
    }

    public void setIdUsuarioRegistro(Integer idUsuarioRegistro) {
        IdUsuarioRegistro = idUsuarioRegistro;
    }

    public String getObjetivo() {
        return Objetivo;
    }

    public void setObjetivo(String objetivo) {
        Objetivo = objetivo;
    }

    public String getRutaMapaUa() {
        return RutaMapaUa;
    }

    public void setRutaMapaUa(String rutaMapaUa) {
        RutaMapaUa = rutaMapaUa;
    }

    public String getRutaPlantilla() {
        return RutaPlantilla;
    }

    public void setRutaPlantilla(String rutaPlantilla) {
        RutaPlantilla = rutaPlantilla;
    }

    public String getSolicitante() {
        return Solicitante;
    }

    public void setSolicitante(String solicitante) {
        Solicitante = solicitante;
    }

    public String getRepresentanteLegal() {
        return RepresentanteLegal;
    }

    public void setRepresentanteLegal(String representanteLegal) {
        RepresentanteLegal = representanteLegal;
    }

    public String getSuperficieSolicitada() {
        return SuperficieSolicitada;
    }

    public void setSuperficieSolicitada(String superficieSolicitada) {
        SuperficieSolicitada = superficieSolicitada;
    }

    public Integer getPlazoConcesion() {
        return PlazoConcesion;
    }

    public void setPlazoConcesion(Integer plazoConcesion) {
        PlazoConcesion = plazoConcesion;
    }

    public String getSectorAnexoCaserio() {
        return SectorAnexoCaserio;
    }

    public void setSectorAnexoCaserio(String sectorAnexoCaserio) {
        SectorAnexoCaserio = sectorAnexoCaserio;
    }


}
