package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ConsultUAParametrosEntity implements Serializable {
    private String arffs;
    private Integer estado;
    private String lote;
    private String objectid;

    public String getArffs() {
        return arffs;
    }

    public void setArffs(String arffs) {
        this.arffs = arffs;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public String getObjectid() {
        return objectid;
    }

    public void setObjectid(String objectid) {
        this.objectid = objectid;
    }
}
