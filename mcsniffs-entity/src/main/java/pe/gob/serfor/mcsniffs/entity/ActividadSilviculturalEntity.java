package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.List;

public class ActividadSilviculturalEntity extends AuditoriaEntity implements Serializable {
    private Integer idActSilvicultural;
    private String codigoTipoActSilvicultural;
    private String actividad;
    private String monitoreo;
    private String anexo;
    private String descripcion;
    private String observacion;
    private Integer idPlanManejo;
    private Integer idTipoTratamiento;
    private List<ActividadSilviculturalDetalleEntity> listActividadSilvicultural;


    public Integer getIdActSilvicultural() {
        return idActSilvicultural;
    }

    public ActividadSilviculturalEntity setIdActSilvicultural(Integer idActSilvicultural) {
        this.idActSilvicultural = idActSilvicultural;
        return this;
    }

    public String getCodigoTipoActSilvicultural() {
        return codigoTipoActSilvicultural;
    }

    public ActividadSilviculturalEntity setCodigoTipoActSilvicultural(String codigoTipoActSilvicultural) {
        this.codigoTipoActSilvicultural = codigoTipoActSilvicultural;
        return this;
    }

    public String getActividad() {
        return actividad;
    }

    public ActividadSilviculturalEntity setActividad(String actividad) {
        this.actividad = actividad;
        return this;
    }

    public String getMonitoreo() {
        return monitoreo;
    }

    public ActividadSilviculturalEntity setMonitoreo(String monitoreo) {
        this.monitoreo = monitoreo;
        return this;
    }

    public String getAnexo() {
        return anexo;
    }

    public ActividadSilviculturalEntity setAnexo(String anexo) {
        this.anexo = anexo;
        return this;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public ActividadSilviculturalEntity setDescripcion(String descripcion) {
        this.descripcion = descripcion;
        return this;
    }

    public String getObservacion() {
        return observacion;
    }

    public ActividadSilviculturalEntity setObservacion(String observacion) {
        this.observacion = observacion;
        return this;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public ActividadSilviculturalEntity setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
        return this;
    }

    public Integer getIdTipoTratamiento() {
        return idTipoTratamiento;
    }

    public ActividadSilviculturalEntity setIdTipoTratamiento(Integer idTipoTratamiento) {
        this.idTipoTratamiento = idTipoTratamiento;
        return this;
    }

    public List<ActividadSilviculturalDetalleEntity> getListActividadSilvicultural() {
        return listActividadSilvicultural;
    }

    public ActividadSilviculturalEntity setListActividadSilvicultural(List<ActividadSilviculturalDetalleEntity> listActividadSilvicultural) {
        this.listActividadSilvicultural = listActividadSilvicultural;
        return this;
    }
}
