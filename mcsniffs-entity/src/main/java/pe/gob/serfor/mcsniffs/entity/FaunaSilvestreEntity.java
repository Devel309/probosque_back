package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;


public class FaunaSilvestreEntity extends AuditoriaEntity implements Serializable {
    private Integer idFaunaSilvestre;
    private FaunaEntity fauna;
    private PlanManejoEntity planManejo;
}
