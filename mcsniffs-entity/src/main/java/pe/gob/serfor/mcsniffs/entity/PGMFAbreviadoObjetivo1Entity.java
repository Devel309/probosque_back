package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


public class PGMFAbreviadoObjetivo1Entity extends AuditoriaEntity implements Serializable {
    private Number IdPgmf; 

    private String nombreTitular;
    private String nombreReprecentanteLegal;
    private String dni;
    private String domicilioLegal;
    private String distrito;
    private String nroContratoConcesion;
    private String departamento;
    private String provincia;
    
    private Date fechaPrecentacionPGMF;
    private Number duracionPGMF;
    private Date fechaInicioPGMF;
    private Date fechaFinPGMF;
    private Number areaTotalConsecion;
    private Number areaBosqueProduccionForestal;
    private Number areaProteccion;
    private String numeroBlouesQuinquenales;
    private BigDecimal potencialMaderable;
    private Number volumenCortaAnualPermisible;

    private Number idRegenteForestal; 
    private String nombreRegenteForestal; 
    private String domicilioLegalRegente; 
    private String contratoSuscritoTitularTituloHabilitante; 
    private String certificadoHabilitacionProfecionalRegenteForestal;
    private Number numeroInsccripcion;

    private String apellidoPaternoPersona;
    private String apellidoMaternoPersona;
    private String apellidoPaternoRepLegal;
    private String apellidoMaternoRepLegal;    

    public  PGMFAbreviadoObjetivo1Entity() {}

    
    public Number getIdRegenteForestal() {
        return idRegenteForestal;
    }
    public void setIdRegenteForestal(Number idRegenteForestal) {
        this.idRegenteForestal = idRegenteForestal;
    }
    public Number getIdPgmf() {
        return IdPgmf;
    }

    public void setIdPgmf(Number idPgmf) {
        IdPgmf = idPgmf;
    }

    public String getNombreTitular() {
        return nombreTitular;
    }

    public void setNombreTitular(String nombreTitular) {
        this.nombreTitular = nombreTitular;
    }

    public String getNombreReprecentanteLegal() {
        return nombreReprecentanteLegal;
    }

    public void setNombreReprecentanteLegal(String nombreReprecentanteLegal) {
        this.nombreReprecentanteLegal = nombreReprecentanteLegal;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getDomicilioLegal() {
        return domicilioLegal;
    }

    public void setDomicilioLegal(String domicilioLegal) {
        this.domicilioLegal = domicilioLegal;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getNroContratoConcesion() {
        return nroContratoConcesion;
    }

    public void setNroContratoConcesion(String nroContratoConcesion) {
        this.nroContratoConcesion = nroContratoConcesion;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public Date getFechaPrecentacionPGMF() {
        return fechaPrecentacionPGMF;
    }

    public void setFechaPrecentacionPGMF(Date fechaPrecentacionPGMF) {
        this.fechaPrecentacionPGMF = fechaPrecentacionPGMF;
    }

    public Number getDuracionPGMF() {
        return duracionPGMF;
    }

    public void setDuracionPGMF(Number duracionPGMF) {
        this.duracionPGMF = duracionPGMF;
    }

    public Date getFechaInicioPGMF() {
        return fechaInicioPGMF;
    }

    public void setFechaInicioPGMF(Date fechaInicioPGMF) {
        this.fechaInicioPGMF = fechaInicioPGMF;
    }

    public Date getFechaFinPGMF() {
        return fechaFinPGMF;
    }

    public void setFechaFinPGMF(Date fechaFinPGMF) {
        this.fechaFinPGMF = fechaFinPGMF;
    }

    public Number getAreaTotalConsecion() {
        return areaTotalConsecion;
    }

    public void setAreaTotalConsecion(Number areaTotalConsecion) {
        this.areaTotalConsecion = areaTotalConsecion;
    }

    public Number getAreaBosqueProduccionForestal() {
        return areaBosqueProduccionForestal;
    }

    public void setAreaBosqueProduccionForestal(Number areaBosqueProduccionForestal) {
        this.areaBosqueProduccionForestal = areaBosqueProduccionForestal;
    }

    public Number getAreaProteccion() {
        return areaProteccion;
    }

    public void setAreaProteccion(Number areaProteccion) {
        this.areaProteccion = areaProteccion;
    }

    public String getNumeroBlouesQuinquenales() {
        return numeroBlouesQuinquenales;
    }

    public void setNumeroBlouesQuinquenales(String numeroBlouesQuinquenales) {
        this.numeroBlouesQuinquenales = numeroBlouesQuinquenales;
    }

    public BigDecimal getPotencialMaderable() {
        return potencialMaderable;
    }

    public void setPotencialMaderable(BigDecimal potencialMaderable) {
        this.potencialMaderable = potencialMaderable;
    }

    public Number getVolumenCortaAnualPermisible() {
        return volumenCortaAnualPermisible;
    }

    public void setVolumenCortaAnualPermisible(Number volumenCortaAnualPermisible) {
        this.volumenCortaAnualPermisible = volumenCortaAnualPermisible;
    }

    public String getNombreRegenteForestal() {
        return nombreRegenteForestal;
    }

    public void setNombreRegenteForestal(String nombreRegenteForestal) {
        this.nombreRegenteForestal = nombreRegenteForestal;
    }

    public String getDomicilioLegalRegente() {
        return domicilioLegalRegente;
    }

    public void setDomicilioLegalRegente(String domicilioLegalRegente) {
        this.domicilioLegalRegente = domicilioLegalRegente;
    }

    public String getContratoSuscritoTitularTituloHabilitante() {
        return contratoSuscritoTitularTituloHabilitante;
    }

    public void setContratoSuscritoTitularTituloHabilitante(String contratoSuscritoTitularTituloHabilitante) {
        this.contratoSuscritoTitularTituloHabilitante = contratoSuscritoTitularTituloHabilitante;
    }

    public String getCertificadoHabilitacionProfecionalRegenteForestal() {
        return certificadoHabilitacionProfecionalRegenteForestal;
    }

    public void setCertificadoHabilitacionProfecionalRegenteForestal(
            String certificadoHabilitacionProfecionalRegenteForestal) {
        this.certificadoHabilitacionProfecionalRegenteForestal = certificadoHabilitacionProfecionalRegenteForestal;
    }

    public Number getNumeroInsccripcion() {
        return numeroInsccripcion;
    }

    public void setNumeroInsccripcion(Number numeroInsccripcion) {
        this.numeroInsccripcion = numeroInsccripcion;
    }

    public String getApellidoPaternoPersona() {
        return apellidoPaternoPersona;
    }


    public void setApellidoPaternoPersona(String apellidoPaternoPersona) {
        this.apellidoPaternoPersona = apellidoPaternoPersona;
    }


    public String getApellidoMaternoPersona() {
        return apellidoMaternoPersona;
    }


    public void setApellidoMaternoPersona(String apellidoMaternoPersona) {
        this.apellidoMaternoPersona = apellidoMaternoPersona;
    }


    public String getApellidoPaternoRepLegal() {
        return apellidoPaternoRepLegal;
    }

    public void setApellidoPaternoRepLegal(String apellidoPaternoRepLegal) {
        this.apellidoPaternoRepLegal = apellidoPaternoRepLegal;
    }

    public String getApellidoMaternoRepLegal() {
        return apellidoMaternoRepLegal;
    }

    public void setApellidoMaternoRepLegal(String apellidoMaternoRepLegal) {
        this.apellidoMaternoRepLegal = apellidoMaternoRepLegal;
    }    
}