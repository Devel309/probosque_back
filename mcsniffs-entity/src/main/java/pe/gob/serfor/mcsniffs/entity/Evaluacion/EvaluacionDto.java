package pe.gob.serfor.mcsniffs.entity.Evaluacion;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Getter
@Setter
public class EvaluacionDto extends AuditoriaEntity implements Serializable{
    private Integer idEvaluacion;
    private Integer idPlanManejo;
    private String codigoEvaluacion;
    private String tipoEvaluacion;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Timestamp fechaEvaluacionInicial;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Timestamp fechaEvaluacionFinal;
    private String observacion;
    private String detalle;
    private String descripcion;
    private String estadoEvaluacion;
    private String responsable;
    private List<EvaluacionDetalleDto> listarEvaluacionDetalle;
    private String codigoEvaluacionDet;
    private String codigoEvaluacionDetSub;
    private String codigoEvaluacionDetPost;
    

}
