package pe.gob.serfor.mcsniffs.entity;

public class ManejoBosqueAprovechamientoArchivoEntity {
    private Integer idManBosqueAproveArchivo;
    private Integer idArchivo;
    private ManejoBosqueAprovechamientoEntity manejoBosqueAprovechamiento;

    public Integer getIdManBosqueAproveArchivo() {
        return idManBosqueAproveArchivo;
    }

    public void setIdManBosqueAproveArchivo(Integer idManBosqueAproveArchivo) {
        this.idManBosqueAproveArchivo = idManBosqueAproveArchivo;
    }

    public Integer getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }

    public ManejoBosqueAprovechamientoEntity getManejoBosqueAprovechamiento() {
        return manejoBosqueAprovechamiento;
    }

    public void setManejoBosqueAprovechamiento(ManejoBosqueAprovechamientoEntity manejoBosqueAprovechamiento) {
        this.manejoBosqueAprovechamiento = manejoBosqueAprovechamiento;
    }
}
