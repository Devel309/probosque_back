package pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import lombok.Data;

@Data
public class PlanManejoEvaluacionSubDetalleDto extends AuditoriaEntity implements Serializable {

    private Integer idPlanManejoEvalSubDet;
    private Integer idEspecie;
    private String nombreCientifico;
    private String nombreComun;
    private Boolean conforme;
    private String observacion;
    private String consideracion;
    private String consideracionDescripcion;
    private Integer idPlanManejoEvalDet;


}
