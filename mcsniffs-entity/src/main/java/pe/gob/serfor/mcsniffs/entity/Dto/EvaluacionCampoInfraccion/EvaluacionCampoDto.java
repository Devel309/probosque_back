package pe.gob.serfor.mcsniffs.entity.Dto.EvaluacionCampoInfraccion;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EvaluacionCampoDto extends AuditoriaEntity implements Serializable {

    Integer idEvaluacionCampo ;
    Integer  idEvaluacionCampoEstado;
    Integer  idPlanManejo ;
    Boolean  cites ;
    Date fechaInicioEvaluacion;
    Date fechaFinEvaluacion ;
    Date fechaInicioImpedimentos ;
    Date fechaFinImpedimentos;
    Boolean  comunidadNatidadCampesina ;
    Boolean  resultadoFavorable;
    Boolean  notificacion;
    Integer  idEvaluacionCampoVersion ;
    String  EvaluacionCampoEstado;
    String  tipoDocumentoGestion ;
    String   titular;
    String   apellidoTitular;
    String   numThActoAdmin;
    Integer documentoGestion;
    String  tipoDocumentoGestionTexto;
}
