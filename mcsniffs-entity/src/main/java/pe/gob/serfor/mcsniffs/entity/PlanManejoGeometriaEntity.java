package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class PlanManejoGeometriaEntity extends AuditoriaEntity implements Serializable {
    private Integer idPlanManejoGeometria;
    private Integer idPlanManejo;
    private Integer idArchivo;
    private String tipoGeometria;
    private String geometry_wkt;
    private Integer srid ;
    private String nombreCapa;
    private Double area;
    private String codigoGeometria;
    private String codigoSeccion;
    private String codigoSubSeccion;
    private String colorCapa;
    private String descripcion;
    private String propiedad;
    private String observacion;
    private Boolean conforme;
    private String detalle;
    private String asunto;
    private String acapite;
    private String idsCatastro;

    public Integer getIdPlanManejoGeometria() {
        return idPlanManejoGeometria;
    }
    public void setIdPlanManejoGeometria(Integer idPlanManejoGeometria) {
        this.idPlanManejoGeometria = idPlanManejoGeometria;
    }
    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }
    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }
    public Integer getIdArchivo() {
        return idArchivo;
    }
    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }
    public String getTipoGeometria() {
        return tipoGeometria;
    }
    public void setTipoGeometria(String tipoGeometria) {
        this.tipoGeometria = tipoGeometria;
    }
    public String getGeometry_wkt() {
        return geometry_wkt;
    }
    public void setGeometry_wkt(String geometry_wkt) {
        this.geometry_wkt = geometry_wkt;
    }
    public Integer getSrid() {
        return srid;
    }
    public void setSrid(Integer srid) {
        this.srid = srid;
    }
    public String getNombreCapa() {
        return nombreCapa;
    }
    public void setNombreCapa(String nombreCapa) {
        this.nombreCapa = nombreCapa;
    }
    public Double getArea() {
        return area;
    }
    public void setArea(Double area) {
        this.area = area;
    }
    public String getCodigoGeometria() {
        return codigoGeometria;
    }
    public void setCodigoGeometria(String codigoGeometria) {
        this.codigoGeometria = codigoGeometria;
    }
    public String getCodigoSeccion() {
        return codigoSeccion;
    }
    public void setCodigoSeccion(String codigoSeccion) {
        this.codigoSeccion = codigoSeccion;
    }
    public String getCodigoSubSeccion() {
        return codigoSubSeccion;
    }
    public void setCodigoSubSeccion(String codigoSubSeccion) {
        this.codigoSubSeccion = codigoSubSeccion;
    }
    public String getColorCapa() {
        return colorCapa;
    }
    public void setColorCapa(String colorCapa) {
        this.colorCapa = colorCapa;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String getPropiedad() {
        return propiedad;
    }
    public void setPropiedad(String propiedad) {
        this.propiedad = propiedad;
    }

    public String getObservacion() { return observacion; }
    public void setObservacion(String observacion) { this.observacion = observacion; }
    public Boolean getConforme() { return conforme; }
    public void setConforme(Boolean conforme) { this.conforme = conforme; }
    public String getDetalle() { return detalle; }
    public void setDetalle(String detalle) { this.detalle = detalle; }
    public String getAsunto() { return asunto; }
    public void setAsunto(String asunto) { this.asunto = asunto; }
    public String getAcapite() { return acapite; }
    public void setAcapite(String acapite) { this.acapite = acapite; }
    
    public String getIdsCatastro() {
        return idsCatastro;
    }
    public void setIdsCatastro(String idsCatastro) {
        this.idsCatastro = idsCatastro;
    }

    
}
