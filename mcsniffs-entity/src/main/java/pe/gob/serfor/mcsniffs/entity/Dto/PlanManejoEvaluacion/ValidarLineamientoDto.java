package pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;

@Data
public class ValidarLineamientoDto {
 
    private Boolean completado;
    private Boolean conforme;
}
