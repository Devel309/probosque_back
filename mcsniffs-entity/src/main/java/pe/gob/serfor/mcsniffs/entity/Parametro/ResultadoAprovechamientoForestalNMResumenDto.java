package pe.gob.serfor.mcsniffs.entity.Parametro;
import java.util.List;

public class ResultadoAprovechamientoForestalNMResumenDto {
  String unidadMedidaResumen;
    String nombreEspecie;
    List<Anexo2Dto>  listaAnexo2Dto;
    public String getUnidadMedidaResumen() {
        return unidadMedidaResumen;
    }

    public void setUnidadMedidaResumen(String unidadMedidaResumen) {
        this.unidadMedidaResumen = unidadMedidaResumen;
    }

    public List<Anexo2Dto> getListaAnexo2Dto() {
        return listaAnexo2Dto;
    }

    public void setListaAnexo2Dto(List<Anexo2Dto> listaAnexo2Dto) {
        this.listaAnexo2Dto = listaAnexo2Dto;
    }
    public String getNombreEspecie() {
        return nombreEspecie;
    }

    public void setNombreEspecie(String nombreEspecie) {
        this.nombreEspecie = nombreEspecie;
    }
}
