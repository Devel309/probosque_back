package pe.gob.serfor.mcsniffs.entity.Dto.DerechoAprovechamiento;

import java.io.Serializable;
import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Getter
@Setter
public class DerechoAprovechamientoDto extends AuditoriaEntity implements Serializable {
    
    private Integer idDerechoAprovechamiento;
    private Integer idPlanManejo;
    private Integer idContrato;
    private String mecanismoPago;
    private  String codigoEstado;
    
    private String codigoTituloH;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Timestamp fechaInicioContrato;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Timestamp fechaFinContrato;
    private String nombreTitular;
    private String dniTitular;
    
    private String descMecanismoPago;



}
