package pe.gob.serfor.mcsniffs.entity.Dto.DescargarSolicitudPlantacionForestal;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;


@Getter
@Setter
public class DescargarSolicitudPlantacionForestalDto extends  AuditoriaEntity implements Serializable {
    private Integer idSolicitudPlantacionForestal;
}
