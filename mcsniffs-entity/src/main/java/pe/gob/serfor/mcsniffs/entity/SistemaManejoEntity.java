package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class SistemaManejoEntity extends AuditoriaEntity implements Serializable {
    private Integer idSistemaManejo;
    private Boolean generacion;
    private Boolean natural;
    private Boolean otro;
    private String descripcion;
    private PlanManejoEntity planManejo;

    public Integer getIdSistemaManejo() {
        return idSistemaManejo;
    }

    public void setIdSistemaManejo(Integer idSistemaManejo) {
        this.idSistemaManejo = idSistemaManejo;
    }

    public Boolean getGeneracion() {
        return generacion;
    }

    public void setGeneracion(Boolean generacion) {
        this.generacion = generacion;
    }

    public Boolean getNatural() {
        return natural;
    }

    public void setNatural(Boolean natural) {
        this.natural = natural;
    }

    public Boolean getOtro() {
        return otro;
    }

    public void setOtro(Boolean otro) {
        this.otro = otro;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public PlanManejoEntity getPlanManejo() {
        return planManejo;
    }

    public void setPlanManejo(PlanManejoEntity planManejo) {
        this.planManejo = planManejo;
    }
}
