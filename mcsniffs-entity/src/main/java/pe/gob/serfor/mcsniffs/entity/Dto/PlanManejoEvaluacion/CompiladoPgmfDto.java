package pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion;

import com.fasterxml.jackson.annotation.JsonFormat;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


public class CompiladoPgmfDto  implements Serializable {

    private Integer idPlanManejo;
    private Integer idPlanManejoEvaluacion;
    private String tipoDocumento;
    private String nombreUsuarioArffs;
    private String codigoProceso;
    private List<CompiladoArchivoDto> archivos;

    
    public String getCodigoProceso() {
        return codigoProceso;
    }

    public void setCodigoProceso(String codigoProceso) {
        this.codigoProceso = codigoProceso;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public Integer getIdPlanManejoEvaluacion() {
        return idPlanManejoEvaluacion;
    }

    public void setIdPlanManejoEvaluacion(Integer idPlanManejoEvaluacion) {
        this.idPlanManejoEvaluacion = idPlanManejoEvaluacion;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNombreUsuarioArffs() {
        return nombreUsuarioArffs;
    }

    public void setNombreUsuarioArffs(String nombreUsuarioArffs) {
        this.nombreUsuarioArffs = nombreUsuarioArffs;
    }

    public List<CompiladoArchivoDto> getArchivos() {
        return archivos;
    }

    public void setArchivos(List<CompiladoArchivoDto> archivos) {
        this.archivos = archivos;
    }
}
