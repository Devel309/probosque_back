package pe.gob.serfor.mcsniffs.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

public class RequisitoEvaluacionDetalleEntity extends AuditoriaEntity implements Serializable{
    private Integer idRequisito;
    private String codigoRequisitoDet;
    private String medida;
    private String conformidad;
    private String descripcion;
    private String observacion;
    private String detalle;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaIni;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaFin;
    private Integer idRequisitoDet;
    
    public Integer getIdRequisito() {
        return idRequisito;
    }
    public void setIdRequisito(Integer idRequisito) {
        this.idRequisito = idRequisito;
    }
    public String getCodigoRequisitoDet() {
        return codigoRequisitoDet;
    }
    public void setCodigoRequisitoDet(String codigoRequisitoDet) {
        this.codigoRequisitoDet = codigoRequisitoDet;
    }
    public String getMedida() {
        return medida;
    }
    public void setMedida(String medida) {
        this.medida = medida;
    }
    public String getConformidad() {
        return conformidad;
    }
    public void setConformidad(String conformidad) {
        this.conformidad = conformidad;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String getObservacion() {
        return observacion;
    }
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    public String getDetalle() {
        return detalle;
    }
    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }
    public Integer getIdRequisitoDet() {
        return idRequisitoDet;
    }
    public void setIdRequisitoDet(Integer idRequisitoDet) {
        this.idRequisitoDet = idRequisitoDet;
    }

    public Date getFechaIni() {
        return fechaIni;
    }

    public RequisitoEvaluacionDetalleEntity setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
        return this;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public RequisitoEvaluacionDetalleEntity setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
        return this;
    }
}
