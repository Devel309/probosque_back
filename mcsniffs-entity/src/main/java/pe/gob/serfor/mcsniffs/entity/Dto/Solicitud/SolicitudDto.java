package pe.gob.serfor.mcsniffs.entity.Dto.Solicitud;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.util.Date;


@Data
public class SolicitudDto extends AuditoriaEntity implements Serializable {
    private Integer idSolicitud;
    private Integer idProcesoPostulacion;
    private Integer idTipoSolicitud;
    private Date fechaInicioSolicitud;
    private Date fechaFinSolicitud;
    private String motivoSolicitud;
    private Integer idUsuarioSolicitante;
    private String tipoSolicitudDesc;
    private String observacion;
    private Integer idDocumentoAdjunto;
    private Date fechaRecepcionDocsFisicos;
    private String emailSolicitante;
    private Integer idProcesoOferta;
    private Integer idUsuarioPostulacion;
    private Integer idProcesoPostulacionGanador;


    private String nroPropiedad;
    private String nroPartidaRegistral;
    private String nroActaAsamblea;
    private String regenteForestal;
    private String nombreRegenteForestal;
    private String nroContratoRegente;
    private String escalaManejo;
    private String nroComprobantePago;

    private String descSolicitudReconocimiento;
    private String descSolicitudAmpliacion;
    private String descSolicitudTitulacion;
    private String descDocumentoResidencia;

    private String nroActaAsambleaAcuerdo;
    private String nroRucEmpresa;
    private String tipoAprovechamiento;
    private Integer idSolicitudAcceso;
    private Integer idPersonaRepresentante;
    private String nroContratoTercero;
    private String nroActaAprovechamiento;


    private Boolean aprobado;
    private String asunto;
    private Boolean flagAutorizacion;
    private Integer diaAmpliacion;

    private String perfil;
    private Integer idEstado;
    private Boolean vigencia;
}