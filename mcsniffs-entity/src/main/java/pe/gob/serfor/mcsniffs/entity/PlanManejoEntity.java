package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
@Data
public class PlanManejoEntity extends AuditoriaEntity implements Serializable {
    private Integer idPlanManejo;
    private Integer idContrato;
    private String descripcion;
    private Integer idSolicitud;
    private Integer idTipoProceso;
    private Integer idTipoPlan;
    private short idTipoEscala;
    private Integer perEmpadronada;
    private Integer perTrabajan;
    private String aspectoComplementario;
    private String codigoParametro;
    private Integer idPlanManejoPadre;
    private Integer nroParcela;
    private Integer nroAnio;
    private SolicitudEntity solicitud;
    private TipoProcesoEntity tipoProceso;
    private TipoPlanEntity tipoPlan;
    private TipoEscalaEntity tipoEscala;
    private List<PlanManejoContratoEntity> listaPlanManejoContrato;
    private String descripcionContrato;
    private String codigoEstado;
    private String codEstado;
    private Integer idPermisoForestal;
    private String codigoUnico;
    private String fecha;
    private InformacionGeneralPlanificacionBosqueEntity infoGeneral;
    private String tipoPlanDesripcion;
    private Integer idLibroOperacionesPlanManejo;
    private Integer idLibroOperaciones;
    private String codigoTipoPlan;
    private Boolean remitido;
    private Integer vigencia;
    private String modalidad;
    private String modalidadTexto;
}