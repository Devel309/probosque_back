package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

import lombok.Data;


public class UnidadManejoArchivoEntity extends ArchivoEntity implements Serializable {

	private Integer idUnidadArchivoManejo;
	private Integer idUnidadManejo;
	private Integer idTipo;

	public Integer getIdUnidadArchivoManejo() {
		return idUnidadArchivoManejo;
	}

	public void setIdUnidadArchivoManejo(Integer idUnidadArchivoManejo) {
		this.idUnidadArchivoManejo = idUnidadArchivoManejo;
	}

	public Integer getIdUnidadManejo() {
		return idUnidadManejo;
	}

	public void setIdUnidadManejo(Integer idUnidadManejo) {
		this.idUnidadManejo = idUnidadManejo;
	}

	public Integer getIdTipo() {
		return idTipo;
	}

	public void setIdTipo(Integer idTipo) {
		this.idTipo = idTipo;
	}
}
