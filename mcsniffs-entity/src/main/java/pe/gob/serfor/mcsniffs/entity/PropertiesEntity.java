package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class PropertiesEntity implements Serializable {
    private Integer OBJECTID;
    private String FUENTE;
    private String DOCREG;
    private String FECREG;
    private String OBSERV;
    private String ZONUTM;
    private String ORIGEN;
    private String LOTE;
    private String NOMDEP;
    private String CONTRA;
    private String NOMTIT;
    private String NOMREL;
    private Integer CONCUR;
    private String MODOTO;
    private String FECOTO;
    private BigDecimal SUPSIG;
    private BigDecimal SUPAPR;
    private String DOCLEG;
    private String FECLEG;
    private String ESTUA;
    private String FECEUA;
    private String AUTFOR;
    private String geometry_wkt;
    private Integer srid ;
    private Integer grupo ;

     public Integer getGrupo() {
        return grupo;
    }

    public void setGeometry_wkt(Integer grupo) {
        this.grupo = grupo;
    }
    public String getGeometry_wkt() {
        return geometry_wkt;
    }

    public void setGeometry_wkt(String geometry_wkt) {
        this.geometry_wkt = geometry_wkt;
    }

    public Integer getSrid() {
        return srid;
    }

    public void setSrid(Integer srid) {
        this.srid = srid;
    }

    public Integer getOBJECTID() {
        return OBJECTID;
    }

    public void setOBJECTID(Integer OBJECTID) {
        this.OBJECTID = OBJECTID;
    }

    public String getFUENTE() {
        return FUENTE;
    }

    public void setFUENTE(String FUENTE) {
        this.FUENTE = FUENTE;
    }

    public String getDOCREG() {
        return DOCREG;
    }

    public void setDOCREG(String DOCREG) {
        this.DOCREG = DOCREG;
    }

    public String getFECREG() {
        return FECREG;
    }

    public void setFECREG(String FECREG) {
        this.FECREG = FECREG;
    }

    public String getOBSERV() {
        return OBSERV;
    }

    public void setOBSERV(String OBSERV) {
        this.OBSERV = OBSERV;
    }

    public String getZONUTM() {
        return ZONUTM;
    }

    public void setZONUTM(String ZONUTM) {
        this.ZONUTM = ZONUTM;
    }

    public String getORIGEN() {
        return ORIGEN;
    }

    public void setORIGEN(String ORIGEN) {
        this.ORIGEN = ORIGEN;
    }

    public String getLOTE() {
        return LOTE;
    }

    public void setLOTE(String LOTE) {
        this.LOTE = LOTE;
    }

    public String getNOMDEP() {
        return NOMDEP;
    }

    public void setNOMDEP(String NOMDEP) {
        this.NOMDEP = NOMDEP;
    }

    public String getCONTRA() {
        return CONTRA;
    }

    public void setCONTRA(String CONTRA) {
        this.CONTRA = CONTRA;
    }

    public String getNOMTIT() {
        return NOMTIT;
    }

    public void setNOMTIT(String NOMTIT) {
        this.NOMTIT = NOMTIT;
    }

    public String getNOMREL() {
        return NOMREL;
    }

    public void setNOMREL(String NOMREL) {
        this.NOMREL = NOMREL;
    }

    public Integer getCONCUR() {
        return CONCUR;
    }

    public void setCONCUR(Integer CONCUR) {
        this.CONCUR = CONCUR;
    }

    public String getMODOTO() {
        return MODOTO;
    }

    public void setMODOTO(String MODOTO) {
        this.MODOTO = MODOTO;
    }

    public String getFECOTO() {
        return FECOTO;
    }

    public void setFECOTO(String FECOTO) {
        this.FECOTO = FECOTO;
    }

    public BigDecimal getSUPSIG() {
        return SUPSIG;
    }

    public void setSUPSIG(BigDecimal SUPSIG) {
        this.SUPSIG = SUPSIG;
    }

    public BigDecimal getSUPAPR() {
        return SUPAPR;
    }

    public void setSUPAPR(BigDecimal SUPAPR) {
        this.SUPAPR = SUPAPR;
    }

    public String getDOCLEG() {
        return DOCLEG;
    }

    public void setDOCLEG(String DOCLEG) {
        this.DOCLEG = DOCLEG;
    }

    public String getFECLEG() {
        return FECLEG;
    }

    public void setFECLEG(String FECLEG) {
        this.FECLEG = FECLEG;
    }

    public String getESTUA() {
        return ESTUA;
    }

    public void setESTUA(String ESTUA) {
        this.ESTUA = ESTUA;
    }

    public String getFECEUA() {
        return FECEUA;
    }

    public void setFECEUA(String FECEUA) {
        this.FECEUA = FECEUA;
    }

    public String getAUTFOR() {
        return AUTFOR;
    }

    public void setAUTFOR(String AUTFOR) {
        this.AUTFOR = AUTFOR;
    }
}
