package pe.gob.serfor.mcsniffs.entity;

public class FormacionAcademicaEntity {
    //para obtener el anexo, orden : value1,value2,value3,value4
    private String gradoTitulo, especialidad, nombreUniversidadInstituto, anioEmisionTitulo;

    public String getGradoTitulo() {
        return gradoTitulo;
    }

    public void setGradoTitulo(String gradoTitulo) {
        this.gradoTitulo = gradoTitulo;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getNombreUniversidadInstituto() {
        return nombreUniversidadInstituto;
    }

    public void setNombreUniversidadInstituto(String nombreUniversidadInstituto) {
        this.nombreUniversidadInstituto = nombreUniversidadInstituto;
    }

    public String getAnioEmisionTitulo() {
        return anioEmisionTitulo;
    }

    public void setAnioEmisionTitulo(String anioEmisionTitulo) {
        this.anioEmisionTitulo = anioEmisionTitulo;
    }

    
}
