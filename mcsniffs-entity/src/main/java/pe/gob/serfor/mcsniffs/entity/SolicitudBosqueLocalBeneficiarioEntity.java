package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;

import java.sql.Date;

@Data
public class SolicitudBosqueLocalBeneficiarioEntity  extends AuditoriaEntity implements Serializable {

    private Integer idSolBosqueLocal;
    private Integer idBosqueLocalBeneficiario;
    private String tipoDocumento;
    private String numeroDocumento;
    private String nombreCompleto;
    private String residencia;
    private String condicion;
    private String telefono;
    private String email;
    private String cargaFamiliar;
    private Integer declaracionJurada;
    private String estado;
    private Integer idCertificado;
    private Boolean flagListaFinal;
    private String observaciones;
    private Boolean flagActualizarResidencia;
    private String ubigeoTexto	;
    private Boolean tieneOposicion;
    private Integer idAdjuntoOposicion;
    private Integer flagTieneOtrasSolicitudes;
    private String condicionDescripcion;





}
