package pe.gob.serfor.mcsniffs.entity.Dto.Contrato;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity; 

@Getter
@Setter
public class GenerarCodContratoDto extends  AuditoriaEntity implements Serializable {
    
    private Integer idUsuario;
    private String derecho;
    private String recurso;
    private String codigo;
    private Integer idDepartamento;
}
