package pe.gob.serfor.mcsniffs.entity.Evaluacion;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
public class EvaluacionPermisoForestalDto extends AuditoriaEntity implements Serializable{
    private Integer idEvaluacionPermiso;
    private Integer idPermisoForestal;
    private String codigoEvaluacion;
    private String tipoEvaluacion;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Timestamp fechaEvaluacionInicial;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Timestamp fechaEvaluacionFinal;
    private String observacion;
    private String detalle;
    private String descripcion;
    private String estadoEvaluacion;
    private String responsable;
    private List<EvaluacionPermisoForestalDetalleDto> listarEvaluacionPermisoDetalle;
    private String codigoEvaluacionDet;
    private String codigoEvaluacionDetSub;
    private String codigoEvaluacionDetPost;
    

}
