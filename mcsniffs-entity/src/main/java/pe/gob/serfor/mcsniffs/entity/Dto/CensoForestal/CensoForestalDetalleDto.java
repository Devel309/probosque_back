package pe.gob.serfor.mcsniffs.entity.Dto.CensoForestal;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.CensoForestalEntity;

import java.io.Serializable;


public class CensoForestalDetalleDto extends AuditoriaEntity implements Serializable {

    private Integer idCensoForestalDetalle;
    private Integer parcelaCorte;
    private Integer bloque;
    private Integer faja;
    private String idTipoRecurso;
    private Integer idTipoBosque;
    private Integer idTipoEvaluacion;
    private String correlativo; //Lenght = 100
    private Integer idCodigoEspecie;
    private String idNombreComun;
    private String idNombreCientifico;
    private String descripcionOtros; //Lenght = 150
    private Integer idEspecieCodigo;
    private String idTipoArbol;
    private Double dap;
    private Double alturaComercial;
    private Double alturaTotal;
    private String codigoArbolCalidad;
    private Double volumen;
    private Float volumenComercial;
    private Double factorForma;
    private Double categoriaDiametrica;
    private String  este;
    private String  norte;
    private String coorEste;
    private String  coorNorte;
    private Integer zona;
    private String nota;
    private Double cantidad;
    private String codigoUnidadMedida;
    private String productoTipo;
    private Integer idCensoForestal;
    private CensoForestalEntity censoForestalEntity;
    private Double superficieComercial;
    private String nombreComun;
    private String nombreCientifico;
    private String nombreNativo;

    public String getNombreNativo() {
        return nombreNativo;
    }

    public void setNombreNativo(String nombreNativo) {
        this.nombreNativo = nombreNativo;
    }

    public String getNombreComun() {
        return nombreComun;
    }

    public void setNombreComun(String nombreComun) {
        this.nombreComun = nombreComun;
    }

    public String getNombreCientifico() {
        return nombreCientifico;
    }

    public void setNombreCientifico(String nombreCientifico) {
        this.nombreCientifico = nombreCientifico;
    }

    public Double getSuperficieComercial() {
        return superficieComercial;
    }

    public Float getVolumenComercial() {
        return volumenComercial;
    }

    public void setVolumenComercial(Float volumenComercial) {
        this.volumenComercial = volumenComercial;
    }

    public void setSuperficieComercial(Double superficieComercial) {
        this.superficieComercial = superficieComercial;
    }

    public Integer getIdCensoForestalDetalle() {
        return idCensoForestalDetalle;
    }

    public Integer getIdCensoForestal() {
        return idCensoForestal;
    }

    public void setIdCensoForestal(Integer idCensoForestal) {
        this.idCensoForestal = idCensoForestal;
    }

    public void setIdCensoForestalDetalle(Integer idCensoForestalDetalle) {
        this.idCensoForestalDetalle = idCensoForestalDetalle;
    }

    public Integer getParcelaCorte() {
        return parcelaCorte;
    }

    public void setParcelaCorte(Integer parcelaCorte) {
        this.parcelaCorte = parcelaCorte;
    }

    public Integer getBloque() {
        return bloque;
    }

    public void setBloque(Integer bloque) {
        this.bloque = bloque;
    }

    public Integer getFaja() {
        return faja;
    }

    public void setFaja(Integer faja) {
        this.faja = faja;
    }

    public String getIdTipoRecurso() {
        return idTipoRecurso;
    }

    public void setIdTipoRecurso(String idTipoRecurso) {
        this.idTipoRecurso = idTipoRecurso;
    }

    public Integer getIdTipoBosque() {
        return idTipoBosque;
    }

    public void setIdTipoBosque(Integer idTipoBosque) {
        this.idTipoBosque = idTipoBosque;
    }

    public Integer getIdTipoEvaluacion() {
        return idTipoEvaluacion;
    }

    public void setIdTipoEvaluacion(Integer idTipoEvaluacion) {
        this.idTipoEvaluacion = idTipoEvaluacion;
    }

    public String getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(String correlativo) {
        this.correlativo = correlativo;
    }

    public Integer getIdCodigoEspecie() {
        return idCodigoEspecie;
    }

    public void setIdCodigoEspecie(Integer idCodigoEspecie) {
        this.idCodigoEspecie = idCodigoEspecie;
    }

    public String getIdNombreComun() {
        return idNombreComun;
    }

    public void setIdNombreComun(String idNombreComun) {
        this.idNombreComun = idNombreComun;
    }

    public String getIdNombreCientifico() {
        return idNombreCientifico;
    }

    public void setIdNombreCientifico(String idNombreCientifico) {
        this.idNombreCientifico = idNombreCientifico;
    }

    public String getDescripcionOtros() {
        return descripcionOtros;
    }

    public void setDescripcionOtros(String descripcionOtros) {
        this.descripcionOtros = descripcionOtros;
    }

    public Integer getIdEspecieCodigo() {
        return idEspecieCodigo;
    }

    public void setIdEspecieCodigo(Integer idEspecieCodigo) {
        this.idEspecieCodigo = idEspecieCodigo;
    }

    public String getIdTipoArbol() {
        return idTipoArbol;
    }

    public void setIdTipoArbol(String idTipoArbol) {
        this.idTipoArbol = idTipoArbol;
    }

    public Double getDap() {
        return dap;
    }

    public void setDap(Double dap) {
        this.dap = dap;
    }

    public Double getAlturaComercial() {
        return alturaComercial;
    }

    public void setAlturaComercial(Double alturaComercial) {
        this.alturaComercial = alturaComercial;
    }

    public Double getAlturaTotal() {
        return alturaTotal;
    }

    public void setAlturaTotal(Double alturaTotal) {
        this.alturaTotal = alturaTotal;
    }

    public String getCodigoArbolCalidad() {
        return codigoArbolCalidad;
    }

    public void setCodigoArbolCalidad(String codigoArbolCalidad) {
        this.codigoArbolCalidad = codigoArbolCalidad;
    }

    public Double getVolumen() {
        return volumen;
    }

    public void setVolumen(Double volumen) {
        this.volumen = volumen;
    }

    public Double getFactorForma() {
        return factorForma;
    }

    public void setFactorForma(Double factorForma) {
        this.factorForma = factorForma;
    }

    public Double getCategoriaDiametrica() {
        return categoriaDiametrica;
    }

    public void setCategoriaDiametrica(Double categoriaDiametrica) {
        this.categoriaDiametrica = categoriaDiametrica;
    }

    public Integer getZona() {
        return zona;
    }

    public void setZona(Integer zona) {
        this.zona = zona;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public String getCodigoUnidadMedida() {
        return codigoUnidadMedida;
    }

    public void setCodigoUnidadMedida(String codigoUnidadMedida) {
        this.codigoUnidadMedida = codigoUnidadMedida;
    }

    public String getProductoTipo() {
        return productoTipo;
    }

    public void setProductoTipo(String productoTipo) {
        this.productoTipo = productoTipo;
    }

    public CensoForestalEntity getCensoForestalEntity() {
        return censoForestalEntity;
    }

    public void setCensoForestalEntity(CensoForestalEntity censoForestalEntity) {
        this.censoForestalEntity = censoForestalEntity;
    }

    public String getEste() {
        return este;
    }

    public void setEste(String este) {
        this.este = este;
    }

    public String getNorte() {
        return norte;
    }

    public void setNorte(String norte) {
        this.norte = norte;
    }

    public String getCoorEste() {
        return coorEste;
    }

    public void setCoorEste(String coorEste) {
        this.coorEste = coorEste;
    }

    public String getCoorNorte() {
        return coorNorte;
    }

    public void setCoorNorte(String coorNorte) {
        this.coorNorte = coorNorte;
    }
}

