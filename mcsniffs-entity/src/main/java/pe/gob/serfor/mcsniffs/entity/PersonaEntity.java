package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;


@Data
public class PersonaEntity extends AuditoriaEntity implements Serializable {
    private Integer idPersona;
    private String nombres;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private ComunidadEntity comunidad;
    private Integer idTipoPersona;
    private String correo;
    private String telefono;
    private String celular;
    private String ruc;
    private String tipoCNCC;
    private String tipoActor;
    private Integer personaQueRepresenta;
    String direccion ;
    String direccionNumero ;
    String sector ;
    private String codInrena;
    private Integer idSolicitudAcceso;
    private Integer idUsuario;
            String tipoPersona;

           String codigo_tipo_persona ;
           String codigo_tipo_documento;
             
           Integer id_tipo_documento;
           String tipo_documento;
    private Integer idDistrito;
    private Integer idProvincia;
    private Integer idDepartamento;
    private String numeroDocumento;
         
           String es_repr_legal;
           Integer id_persona_empresa_repr_legal ;
           String persona_empresa_repr_legal ;
           String numero_documento_empresa ;
           String codigo_eval_empresa ;
           String razonSocialEmpresa;
           String direccion_empresa ;
           String direccion_numero_empresa ;
           String sector_empresa ;
           String telefono_empresa ;
           String celular_empresa ;
           String email_empresa  ;    
           
           Integer id_distrito_empresa;
           Integer id_provincia_empresa;
           Integer id_departamento_empresa;

    private String nombreDepartamento;
    private String nombreProvincia;
    private String nombreDistrito;

        public String getnombreDepartamento(){
            return nombreDepartamento;
        }
        public String getnombreProvincia(){
            return nombreProvincia;
        }
        public String getnombreDistrito(){
            return nombreDistrito;
        }

        public Integer getIdPersona() {
            return idPersona;
        }
        public void setIdPersona(Integer idPersona) {
            this.idPersona = idPersona;
        }
        public String getNombres() {
            return nombres;
        }
        public void setNombres(String nombres) {
            this.nombres = nombres;
        }
        public String getApellidoPaterno() {
            return apellidoPaterno;
        }
        public void setApellidoPaterno(String apellidoPaterno) {
            this.apellidoPaterno = apellidoPaterno;
        }
        public String getApellidoMaterno() {
            return apellidoMaterno;
        }
        public void setApellidoMaterno(String apellidoMaterno) {
            this.apellidoMaterno = apellidoMaterno;
        }
        public ComunidadEntity getComunidad() {
            return comunidad;
        }
        public void setComunidad(ComunidadEntity comunidad) {
            this.comunidad = comunidad;
        }
        public Integer getIdTipoPersona() {
            return idTipoPersona;
        }
        public void setIdTipoPersona(Integer idTipoPersona) {
            this.idTipoPersona = idTipoPersona;
        }
        public String getCorreo() {
            return correo;
        }
        public void setCorreo(String correo) {
            this.correo = correo;
        }
        public String getTelefono() {
            return telefono;
        }
        public void setTelefono(String telefono) {
            this.telefono = telefono;
        }
        public String getCelular() {
            return celular;
        }
        public void setCelular(String celular) {
            this.celular = celular;
        }
        public String getRuc() {
            return ruc;
        }
        public void setRuc(String ruc) {
            this.ruc = ruc;
        }
        public String getDireccion() {
            return direccion;
        }
        public void setDireccion(String direccion) {
            this.direccion = direccion;
        }
        public String getDireccionNumero() {
            return direccionNumero;
        }
        public void setDireccionNumero(String direccion_numero) {
            this.direccionNumero = direccion_numero;
        }
        public String getSector() {
            return sector;
        }
        public void setSector(String sector) {
            this.sector = sector;
        }
        public String getCodInrena() {
            return codInrena;
        }
        public void setCodInrena(String codInrena) {
            this.codInrena = codInrena;
        }
        public Integer getIdSolicitudAcceso() {
            return idSolicitudAcceso;
        }
        public void setIdSolicitudAcceso(Integer idSolicitudAcceso) {
            this.idSolicitudAcceso = idSolicitudAcceso;
        }
        public Integer getIdUsuario() {
            return idUsuario;
        }
        public void setIdUsuario(Integer idUsuario) {
            this.idUsuario = idUsuario;
        }
        public String getTipoPersona() {
            return tipoPersona;
        }
        public void setTipoPersona(String tipoPersona) {
            this.tipoPersona = tipoPersona;
        }
        public String getCodigo_tipo_persona() {
            return codigo_tipo_persona;
        }
        public void setCodigo_tipo_persona(String codigo_tipo_persona) {
            this.codigo_tipo_persona = codigo_tipo_persona;
        }
        public String getCodigo_tipo_documento() {
            return codigo_tipo_documento;
        }
        public void setCodigo_tipo_documento(String codigo_tipo_documento) {
            this.codigo_tipo_documento = codigo_tipo_documento;
        }
        public Integer getId_tipo_documento() {
            return id_tipo_documento;
        }
        public void setId_tipo_documento(Integer id_tipo_documento) {
            this.id_tipo_documento = id_tipo_documento;
        }
        public String getTipo_documento() {
            return tipo_documento;
        }
        public void setTipo_documento(String tipo_documento) {
            this.tipo_documento = tipo_documento;
        }
        public Integer getIdDistrito() {
            return idDistrito;
        }
        public void setIdDistrito(Integer id_distrito) {
            this.idDistrito = id_distrito;
        }
        public Integer getIdProvincia() {
            return idProvincia;
        }
        public void setIdProvincia(Integer id_provincia) {
            this.idProvincia = id_provincia;
        }
        public Integer getIdDepartamento() {
            return idDepartamento;
        }
        public void setIdDepartamento(Integer id_departamento) {
            this.idDepartamento = id_departamento;
        }
        public String getNumeroDocumento() {
            return numeroDocumento;
        }
        public void setNumeroDocumento(String numero_documento) {
            this.numeroDocumento = numero_documento;
        }
        public String getEs_repr_legal() {
            return es_repr_legal;
        }
        public void setEs_repr_legal(String es_repr_legal) {
            this.es_repr_legal = es_repr_legal;
        }
        public Integer getId_persona_empresa_repr_legal() {
            return id_persona_empresa_repr_legal;
        }
        public void setId_persona_empresa_repr_legal(Integer id_persona_empresa_repr_legal) {
            this.id_persona_empresa_repr_legal = id_persona_empresa_repr_legal;
        }
        public String getPersona_empresa_repr_legal() {
            return persona_empresa_repr_legal;
        }
        public void setPersona_empresa_repr_legal(String persona_empresa_repr_legal) {
            this.persona_empresa_repr_legal = persona_empresa_repr_legal;
        }
        public String getNumero_documento_empresa() {
            return numero_documento_empresa;
        }
        public void setNumero_documento_empresa(String numero_documento_empresa) {
            this.numero_documento_empresa = numero_documento_empresa;
        }
        public String getCodigo_eval_empresa() {
            return codigo_eval_empresa;
        }
        public void setCodigo_eval_empresa(String codigo_eval_empresa) {
            this.codigo_eval_empresa = codigo_eval_empresa;
        }
        public String getRazonSocialEmpresa() {
            return razonSocialEmpresa;
        }
        public void setRazonSocialEmpresa(String razon_social_empresa) {
            this.razonSocialEmpresa = razon_social_empresa;
        }
        public String getDireccion_empresa() {
            return direccion_empresa;
        }
        public void setDireccion_empresa(String direccion_empresa) {
            this.direccion_empresa = direccion_empresa;
        }
        public String getDireccion_numero_empresa() {
            return direccion_numero_empresa;
        }
        public void setDireccion_numero_empresa(String direccion_numero_empresa) {
            this.direccion_numero_empresa = direccion_numero_empresa;
        }
        public String getSector_empresa() {
            return sector_empresa;
        }
        public void setSector_empresa(String sector_empresa) {
            this.sector_empresa = sector_empresa;
        }
        public String getTelefono_empresa() {
            return telefono_empresa;
        }
        public void setTelefono_empresa(String telefono_empresa) {
            this.telefono_empresa = telefono_empresa;
        }
        public String getCelular_empresa() {
            return celular_empresa;
        }
        public void setCelular_empresa(String celular_empresa) {
            this.celular_empresa = celular_empresa;
        }
        public String getEmail_empresa() {
            return email_empresa;
        }
        public void setEmail_empresa(String email_empresa) {
            this.email_empresa = email_empresa;
        }
        public Integer getId_distrito_empresa() {
            return id_distrito_empresa;
        }
        public void setId_distrito_empresa(Integer id_distrito_empresa) {
            this.id_distrito_empresa = id_distrito_empresa;
        }
        public Integer getId_provincia_empresa() {
            return id_provincia_empresa;
        }
        public void setId_provincia_empresa(Integer id_provincia_empresa) {
            this.id_provincia_empresa = id_provincia_empresa;
        }
        public Integer getId_departamento_empresa() {
            return id_departamento_empresa;
        }
        public void setId_departamento_empresa(Integer id_departamento_empresa) {
            this.id_departamento_empresa = id_departamento_empresa;
        }

      


   
    
    
}