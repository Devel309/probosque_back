package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.List;

public class RequisitoEvaluacionEntity extends AuditoriaEntity implements Serializable{
    private Integer idPlanManejo;
    private String codigoRequisito;
    private String tipoRequisito;
    private String descripcion;
    private String observacion;
    private String detalle;
    private String titular;
    private String regente;
    private String titularHabilitado;
    private String regenteHabilitado;
    private String evaluacion;
    private String comentarios;
    private String comunidad;
    private String titularConsecion;
    private String tituloHabilitante;
    private String ruc;
    private String partidaRegistral;
    private String area;
    private Integer idRequisito;
    private String numeroDocumentoRegente;
    private String numeroDocumentoTitular;
    private String numeroDocumentoComunidad;
    private String numeroDocumento;
    private List<RequisitoEvaluacionDetalleEntity> evaluacionDetalle;

   
    public String getNumeroDocumentoRegente() {
        return numeroDocumentoRegente;
    }
    public void setNumeroDocumentoRegente(String numeroDocumentoRegente) {
        this.numeroDocumentoRegente = numeroDocumentoRegente;
    }
    public String getNumeroDocumentoTitular() {
        return numeroDocumentoTitular;
    }
    public void setNumeroDocumentoTitular(String numeroDocumentoTitular) {
        this.numeroDocumentoTitular = numeroDocumentoTitular;
    }
    public String getNumeroDocumentoComunidad() {
        return numeroDocumentoComunidad;
    }
    public void setNumeroDocumentoComunidad(String numeroDocumentoComunidad) {
        this.numeroDocumentoComunidad = numeroDocumentoComunidad;
    }
    public String getNumeroDocumento() {
        return numeroDocumento;
    }
    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }
    public List<RequisitoEvaluacionDetalleEntity> getEvaluacionDetalle() {
        return evaluacionDetalle;
    }
    public void setEvaluacionDetalle(List<RequisitoEvaluacionDetalleEntity> evaluacionDetalle) {
        this.evaluacionDetalle = evaluacionDetalle;
    }
    public String getEvaluacion() {
        return evaluacion;
    }
    public void setEvaluacion(String evaluacion) {
        this.evaluacion = evaluacion;
    }
    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }
    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }
    public String getCodigoRequisito() {
        return codigoRequisito;
    }
    public void setCodigoRequisito(String codigoRequisito) {
        this.codigoRequisito = codigoRequisito;
    }
    public String getTipoRequisito() {
        return tipoRequisito;
    }
    public void setTipoRequisito(String tipoRequisito) {
        this.tipoRequisito = tipoRequisito;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String getObservacion() {
        return observacion;
    }
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    public String getDetalle() {
        return detalle;
    }
    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }
    public String getTitular() {
        return titular;
    }
    public void setTitular(String titular) {
        this.titular = titular;
    }
    public String getRegente() {
        return regente;
    }
    public void setRegente(String regente) {
        this.regente = regente;
    }
    public String getTitularHabilitado() {
        return titularHabilitado;
    }
    public void setTitularHabilitado(String titularHabilitado) {
        this.titularHabilitado = titularHabilitado;
    }
    public String getRegenteHabilitado() {
        return regenteHabilitado;
    }
    public void setRegenteHabilitado(String regenteHabilitado) {
        this.regenteHabilitado = regenteHabilitado;
    }
  
    public String getComentarios() {
        return comentarios;
    }
    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }
    public String getComunidad() {
        return comunidad;
    }
    public void setComunidad(String comunidad) {
        this.comunidad = comunidad;
    }
    public String getTitularConsecion() {
        return titularConsecion;
    }
    public void setTitularConsecion(String titularConsecion) {
        this.titularConsecion = titularConsecion;
    }
    public String getTituloHabilitante() {
        return tituloHabilitante;
    }
    public void setTituloHabilitante(String tituloHabilitante) {
        this.tituloHabilitante = tituloHabilitante;
    }
    public String getRuc() {
        return ruc;
    }
    public void setRuc(String ruc) {
        this.ruc = ruc;
    }
    public String getPartidaRegistral() {
        return partidaRegistral;
    }
    public void setPartidaRegistral(String partidaRegistral) {
        this.partidaRegistral = partidaRegistral;
    }
    public String getArea() {
        return area;
    }
    public void setArea(String area) {
        this.area = area;
    }
    public Integer getIdRequisito() {
        return idRequisito;
    }
    public void setIdRequisito(Integer idRequisito) {
        this.idRequisito = idRequisito;
    }


}
