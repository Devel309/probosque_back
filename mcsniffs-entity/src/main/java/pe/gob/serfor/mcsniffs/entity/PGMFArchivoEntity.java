package pe.gob.serfor.mcsniffs.entity;

public class PGMFArchivoEntity extends AuditoriaEntity {
    private Integer idPGMFArchivo;
    private String codigoTipoPGMF;
    private String codigoSubTipoPGMF;
    private String descripcion;
    private String observacion;
    private Integer idPlanManejo;
    private Integer idArchivo;
    private String nombre;
    private String extension;
    private String tipoDocumento;
    private String nombreArchivo;
    private String extensionArchivo;
    private Integer idTipoDocumento;
    private byte[] documento;
    private String asunto;
    private String acapite;
    private Boolean conforme;
    private String detalle;

    public byte[] getDocumento() {
        return documento;
    }
    public void setDocumento(byte[] documento) {
        this.documento = documento;
    }
    public String getNombreArchivo() {
        return nombreArchivo;
    }
    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }
    public String getExtensionArchivo() {
        return extensionArchivo;
    }
    public void setExtensionArchivo(String extensionArchivo) {
        this.extensionArchivo = extensionArchivo;
    }
    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }
    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }
    public String getTipoDocumento() {
        return tipoDocumento;
    }
    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Integer getIdPGMFArchivo() {
        return idPGMFArchivo;
    }

    public void setIdPGMFArchivo(Integer idPGMFArchivo) {
        this.idPGMFArchivo = idPGMFArchivo;
    }

    public String getCodigoTipoPGMF() {
        return codigoTipoPGMF;
    }

    public void setCodigoTipoPGMF(String codigoTipoPGMF) {
        this.codigoTipoPGMF = codigoTipoPGMF;
    }

    public String getCodigoSubTipoPGMF() {
        return codigoSubTipoPGMF;
    }

    public void setCodigoSubTipoPGMF(String codigoSubTipoPGMF) {
        this.codigoSubTipoPGMF = codigoSubTipoPGMF;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public Integer getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getAsunto() { return asunto; }

    public void setAsunto(String asunto) { this.asunto = asunto; }

    public String getAcapite() { return acapite; }

    public void setAcapite(String acapite) { this.acapite = acapite; }

    public Boolean getConforme() { return conforme; }
    public void setConforme(Boolean conforme) { this.conforme = conforme; }
    public String getDetalle() { return detalle; }
    public void setDetalle(String detalle) { this.detalle = detalle; }
}
