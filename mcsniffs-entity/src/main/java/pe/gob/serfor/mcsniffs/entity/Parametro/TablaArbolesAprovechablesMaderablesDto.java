package pe.gob.serfor.mcsniffs.entity.Parametro;

import java.util.List;

public class TablaArbolesAprovechablesMaderablesDto {
   private  String especie;
    private  List<DatosTipoBosqueDto>  tipoBosqueDtos;
    private String nArbolesTotal;
    private  String volumenTotal;
    private  String areaBasalTotal;
    private  String unidadCantidadTotal;
    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public List<DatosTipoBosqueDto> getTipoBosqueDtos() {
        return tipoBosqueDtos;
    }

    public void setTipoBosqueDtos(List<DatosTipoBosqueDto> tipoBosqueDtos) {
        this.tipoBosqueDtos = tipoBosqueDtos;
    }

    public String getnArbolesTotal() {
        return nArbolesTotal;
    }

    public void setnArbolesTotal(String nArbolesTotal) {
        this.nArbolesTotal = nArbolesTotal;
    }

    public String getVolumenTotal() {
        return volumenTotal;
    }

    public void setVolumenTotal(String volumenTotal) {
        this.volumenTotal = volumenTotal;
    }
    public String getAreaBasalTotal() {
        return areaBasalTotal;
    }

    public void setAreaBasalTotal(String areaBasalTotal) {
        this.areaBasalTotal = areaBasalTotal;
    }
    public String getUnidadCantidadTotal() {
        return unidadCantidadTotal;
    }

    public void setUnidadCantidadTotal(String unidadCantidadTotal) {
        this.unidadCantidadTotal = unidadCantidadTotal;
    }
}
