package pe.gob.serfor.mcsniffs.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class PermisoForestalPlanManejoEntity extends AuditoriaEntity implements Serializable {
    private Integer idPermisoForestal;
    private Integer idPlanManejo;
    private String descripcion;
    private String detalle;
    private String observacion;
    private String codTipoPlan;
    private Boolean remitido;
    private String codigoEstado;
    private String estado;
}
