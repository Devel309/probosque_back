package pe.gob.serfor.mcsniffs.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class TituloHabilitanteEntity extends AuditoriaEntity implements Serializable {
    private Integer idTituloHabilitante;
    private String codigoUnico;
    private String codigoResumido;
    private String procedencia;
    private String estadoTH;
    private String modalidad;
    private Integer idPermisoForestal;
}
