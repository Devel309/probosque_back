package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ProteccionBosqueGestionAmbientalEntity extends AuditoriaEntity implements Serializable {
    private Integer idProBosqueGesAmb;
    private PlanManejoEntity planManejo;
    private Integer idTipoPrograma;
    private String actividad;
    private String impacto;
    private String mitigacionAmbiental;

    public Integer getIdProBosqueGesAmb() {
        return idProBosqueGesAmb;
    }

    public void setIdProBosqueGesAmb(Integer idProBosqueGesAmb) {
        this.idProBosqueGesAmb = idProBosqueGesAmb;
    }

    public PlanManejoEntity getPlanManejo() {
        return planManejo;
    }

    public void setPlanManejo(PlanManejoEntity planManejo) {
        this.planManejo = planManejo;
    }

    public Integer getIdTipoPrograma() {
        return idTipoPrograma;
    }

    public void setIdTipoPrograma(Integer idTipoPrograma) {
        this.idTipoPrograma = idTipoPrograma;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getImpacto() {
        return impacto;
    }

    public void setImpacto(String impacto) {
        this.impacto = impacto;
    }

    public String getMitigacionAmbiental() {
        return mitigacionAmbiental;
    }

    public void setMitigacionAmbiental(String mitigacionAmbiental) {
        this.mitigacionAmbiental = mitigacionAmbiental;
    }
}
