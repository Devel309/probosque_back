package pe.gob.serfor.mcsniffs.entity.Parametro;

import java.util.List;

public class VolumenCortaDto {
    String areaTotalPc;
    Integer contadorPc;
    List<ResultadosVolumenDeCortaTabla> resultadosVolumenDeCortaTablas;
    public String getAreaTotalPc() {
        return areaTotalPc;
    }

    public void setAreaTotalPc(String areaTotalPc) {
        this.areaTotalPc = areaTotalPc;
    }

    public List<ResultadosVolumenDeCortaTabla> getResultadosVolumenDeCortaTablas() {
        return resultadosVolumenDeCortaTablas;
    }

    public void setResultadosVolumenDeCortaTablas(List<ResultadosVolumenDeCortaTabla> resultadosVolumenDeCortaTablas) {
        this.resultadosVolumenDeCortaTablas = resultadosVolumenDeCortaTablas;
    }

    public Integer getContadorPc() {
        return contadorPc;
    }

    public void setContadorPc(Integer contadorPc) {
        this.contadorPc = contadorPc;
    }

}
