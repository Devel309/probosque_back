package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ProcesoOfertaRequestEntity extends AuditoriaEntity implements Serializable {
    private Integer IdProcesoOferta;
    private Integer IdTipoProceso;
    private String IdStatusProceso;
    private Boolean Agrupada;
    private Integer idUsuarioConsulta;

    private Integer  IdUsuario;

    public Integer getIdUsuarioConsulta() {
        return idUsuarioConsulta;
    }

    public void setIdUsuarioConsulta(Integer idUsuarioConsulta) {
        this.idUsuarioConsulta = idUsuarioConsulta;
    }

    public Integer getIdProcesoOferta() {
        return IdProcesoOferta;
    }

    public void setIdProcesoOferta(Integer idProcesoOferta) {
        IdProcesoOferta = idProcesoOferta;
    }

    public Integer getIdTipoProceso() {
        return IdTipoProceso;
    }

    public void setIdTipoProceso(Integer idTipoProceso) {
        IdTipoProceso = idTipoProceso;
    }

    public String getIdStatusProceso() {
        return IdStatusProceso;
    }

    public void setIdStatusProceso(String idStatusProceso) {
        IdStatusProceso = idStatusProceso;
    }

    public Boolean getAgrupada() {
        return Agrupada;
    }

    public void setAgrupada(Boolean agrupada) {
        Agrupada = agrupada;
    }

    public Integer getIdUsuario() {
        return IdUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        IdUsuario = idUsuario;
    }

}
