package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ActividadEconomicaEntity extends AuditoriaEntity implements Serializable {
    private Integer idActiEconomica;
    private String descripcion;

    public Integer getIdActiEconomica() {
        return idActiEconomica;
    }

    public void setIdActiEconomica(Integer idActiEconomica) {
        this.idActiEconomica = idActiEconomica;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
