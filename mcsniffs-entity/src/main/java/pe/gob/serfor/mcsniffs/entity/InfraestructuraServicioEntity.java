package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class InfraestructuraServicioEntity extends AuditoriaEntity implements Serializable {
    private Integer idInfraServicio;
    private String descripcion;

    public Integer getIdInfraServicio() {
        return idInfraServicio;
    }

    public void setIdInfraServicio(Integer idInfraServicio) {
        this.idInfraServicio = idInfraServicio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
