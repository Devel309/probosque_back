package pe.gob.serfor.mcsniffs.entity;

import com.microsoft.sqlserver.jdbc.Geometry;

import java.io.Serializable;

public class AreaManejoEntity extends AuditoriaEntity implements Serializable {
    private Integer idAreaManejo;
    private String codDistrito;
    private String cuenca;
    private String subCuenca;
    private Integer idPlanManejo;
    private Geometry geometry;
    private Double area;

    public Integer getIdAreaManejo() {
        return idAreaManejo;
    }

    public void setIdAreaManejo(Integer idAreaManejo) {
        this.idAreaManejo = idAreaManejo;
    }

    public String getCodDistrito() {
        return codDistrito;
    }

    public void setCodDistrito(String codDistrito) {
        this.codDistrito = codDistrito;
    }

    public String getCuenca() {
        return cuenca;
    }

    public void setCuenca(String cuenca) {
        this.cuenca = cuenca;
    }

    public String getSubCuenca() {
        return subCuenca;
    }

    public void setSubCuenca(String subCuenca) {
        this.subCuenca = subCuenca;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }
}
