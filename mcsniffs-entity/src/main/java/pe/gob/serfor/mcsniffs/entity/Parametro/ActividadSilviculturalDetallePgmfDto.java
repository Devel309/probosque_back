package pe.gob.serfor.mcsniffs.entity.Parametro;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.util.List;

public class ActividadSilviculturalDetallePgmfDto extends AuditoriaEntity implements Serializable {

    private Integer idPlanManejo;
    private Integer idActividadSilvicultural;
    private Integer idActividadSilviculturalDet;
    private Boolean accion;
    private String descripcionDetalle;
    private String tratamiento;
    private String justificacion;
    private String idTipo;
    private String idTipoTratamiento;
    private String tipoTratamiento;

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public Integer getIdActividadSilvicultural() {
        return idActividadSilvicultural;
    }

    public void setIdActividadSilvicultural(Integer idActividadSilvicultural) {
        this.idActividadSilvicultural = idActividadSilvicultural;
    }

    public Integer getIdActividadSilviculturalDet() {
        return idActividadSilviculturalDet;
    }

    public void setIdActividadSilviculturalDet(Integer idActividadSilviculturalDet) {
        this.idActividadSilviculturalDet = idActividadSilviculturalDet;
    }

    public Boolean getAccion() {
        return accion;
    }

    public void setAccion(Boolean accion) {
        this.accion = accion;
    }

    public String getDescripcionDetalle() {
        return descripcionDetalle;
    }

    public void setDescripcionDetalle(String descripcionDetalle) {
        this.descripcionDetalle = descripcionDetalle;
    }

    public String getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }

    public String getJustificacion() {
        return justificacion;
    }

    public void setJustificacion(String justificacion) {
        this.justificacion = justificacion;
    }

    public String getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(String idTipo) {
        this.idTipo = idTipo;
    }

    public String getIdTipoTratamiento() {
        return idTipoTratamiento;
    }

    public void setIdTipoTratamiento(String idTipoTratamiento) {
        this.idTipoTratamiento = idTipoTratamiento;
    }

    public String getTipoTratamiento() {
        return tipoTratamiento;
    }

    public void setTipoTratamiento(String tipoTratamiento) {
        this.tipoTratamiento = tipoTratamiento;
    }

    public ActividadSilviculturalDetallePgmfDto() {
        super();
    }

}
