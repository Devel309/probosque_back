package pe.gob.serfor.mcsniffs.entity.Dto.ProyeccionCosecha;

import lombok.Data;

import java.util.List;
@Data
public class ProyeccionCosechaCabeceraDto {

    String claseDiametrica;
    Double valor;
    List<ProyeccionCosechaDetalleDto> detalle;
}
