package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ObjetivoEspecificoManejoEntity extends AuditoriaEntity implements Serializable {
    private Integer idObjEspecificoManejo;
    private ObjetivoManejoEntity objetivoManejo;
    private String  desObjEspecifico;
    private String detalleOtro;
    private String idTipoObjEspecifico;

    private String codTipo;
    private String tipoObjetivo;
    private String descripcion;
    private String detalle;
    private String observacion;
    private String activo;
    private Integer idObjetivo;


    public String getIdTipoObjEspecifico() {
        return idTipoObjEspecifico;
    }

    public void setIdTipoObjEspecifico(String idTipoObjEspecifico) {
        this.idTipoObjEspecifico = idTipoObjEspecifico;
    }

    public Integer getIdObjEspecificoManejo() {
        return idObjEspecificoManejo;
    }

    public void setIdObjEspecificoManejo(Integer idObjEspecificoManejo) {
        this.idObjEspecificoManejo = idObjEspecificoManejo;
    }

    public ObjetivoManejoEntity getObjetivoManejo() {
        return objetivoManejo;
    }

    public void setObjetivoManejo(ObjetivoManejoEntity objetivoManejo) {
        this.objetivoManejo = objetivoManejo;
    }

    public String getDesObjEspecifico() {
        return desObjEspecifico;
    }

    public void setDesObjEspecifico(String desObjEspecifico) {
        this.desObjEspecifico = desObjEspecifico;
    }

    public String getDetalleOtro() {
        return detalleOtro;
    }

    public void setDetalleOtro(String detalleOtro) {
        this.detalleOtro = detalleOtro;
    }

    public String getCodTipo() {
        return codTipo;
    }

    public ObjetivoEspecificoManejoEntity setCodTipo(String codTipo) {
        this.codTipo = codTipo;
        return this;
    }

    public String getTipoObjetivo() {
        return tipoObjetivo;
    }

    public ObjetivoEspecificoManejoEntity setTipoObjetivo(String tipoObjetivo) {
        this.tipoObjetivo = tipoObjetivo;
        return this;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public ObjetivoEspecificoManejoEntity setDescripcion(String descripcion) {
        this.descripcion = descripcion;
        return this;
    }

    public String getDetalle() {
        return detalle;
    }

    public ObjetivoEspecificoManejoEntity setDetalle(String detalle) {
        this.detalle = detalle;
        return this;
    }

    public String getObservacion() {
        return observacion;
    }

    public ObjetivoEspecificoManejoEntity setObservacion(String observacion) {
        this.observacion = observacion;
        return this;
    }

    public String getActivo() {
        return activo;
    }

    public ObjetivoEspecificoManejoEntity setActivo(String activo) {
        this.activo = activo;
        return this;
    }

    public Integer getIdObjetivo() {
        return idObjetivo;
    }

    public ObjetivoEspecificoManejoEntity setIdObjetivo(Integer idObjetivo) {
        this.idObjetivo = idObjetivo;
        return this;
    }
}
