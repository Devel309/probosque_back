package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import java.io.Serializable;
@Data
public class AdjuntoRequestEntity implements Serializable {
    private Integer IdProcesoPostulacion;
    private Integer IdUsuarioAdjunta;
    private String CodigoAnexo;
    private Integer IdDocumentoAdjunto;
    private Integer IdTipoDocumento;
    private Integer IdPostulacionPFDM;
    private Integer NombreGenerado;

}
