package pe.gob.serfor.mcsniffs.entity;

import java.util.Date;

public class FiscalizacionResponseEntity  extends AuditoriaEntity{
    private Integer IdProcesoOferta;
    private Integer IdProcesoPostulacion;
    private Integer IdUsuarioPostulacion;
    private String CodigoTituloH;
    private Integer IdResultadoPP;
    private String NombresRepresentanteLegal;
    private Integer IdContrato;
    private Date FechaInicioContrato;
    private Date FechaFinContrato;
    private String NombresApellidosRegente;
    private String ObjectIdUa;
    private Integer IdFiscalizacion;
    private String Observacion;
    private Boolean ConObservacion;
    private Date FechaFinContratoFiscalizacion;
    private Integer IdStatusContrato;
    private String StatusContrato;
    private String LoteUa;
    private String codigoEstadoContrato;
    
    
    
    public String getCodigoEstadoContrato() {
        return codigoEstadoContrato;
    }
    public void setCodigoEstadoContrato(String codigoEstadoContrato) {
        this.codigoEstadoContrato = codigoEstadoContrato;
    }
    public String getLoteUa() {
        return LoteUa;
    }
    public void setLoteUa(String loteUa) {
        LoteUa = loteUa;
    }
    public Integer getIdStatusContrato() {
        return IdStatusContrato;
    }
    public void setIdStatusContrato(Integer idStatusContrato) {
        IdStatusContrato = idStatusContrato;
    }
    public String getStatusContrato() {
        return StatusContrato;
    }
    public void setStatusContrato(String statusContrato) {
        StatusContrato = statusContrato;
    }
    public Integer getIdProcesoOferta() {
        return IdProcesoOferta;
    }
    public void setIdProcesoOferta(Integer idProcesoOferta) {
        IdProcesoOferta = idProcesoOferta;
    }
    public Integer getIdProcesoPostulacion() {
        return IdProcesoPostulacion;
    }
    public void setIdProcesoPostulacion(Integer idProcesoPostulacion) {
        IdProcesoPostulacion = idProcesoPostulacion;
    }
    public Integer getIdUsuarioPostulacion() {
        return IdUsuarioPostulacion;
    }
    public void setIdUsuarioPostulacion(Integer idUsuarioPostulacion) {
        IdUsuarioPostulacion = idUsuarioPostulacion;
    }
    public String getCodigoTituloH() {
        return CodigoTituloH;
    }
    public void setCodigoTituloH(String codigoTituloH) {
        CodigoTituloH = codigoTituloH;
    }
    public Integer getIdResultadoPP() {
        return IdResultadoPP;
    }
    public void setIdResultadoPP(Integer idResultadoPP) {
        IdResultadoPP = idResultadoPP;
    }
    public String getNombresRepresentanteLegal() {
        return NombresRepresentanteLegal;
    }
    public void setNombresRepresentanteLegal(String nombresRepresentanteLegal) {
        NombresRepresentanteLegal = nombresRepresentanteLegal;
    }
    public Integer getIdContrato() {
        return IdContrato;
    }
    public void setIdContrato(Integer idContrato) {
        IdContrato = idContrato;
    }
    public Date getFechaInicioContrato() {
        return FechaInicioContrato;
    }
    public void setFechaInicioContrato(Date fechaInicioContrato) {
        FechaInicioContrato = fechaInicioContrato;
    }
    public Date getFechaFinContrato() {
        return FechaFinContrato;
    }
    public void setFechaFinContrato(Date fechaFinContrato) {
        FechaFinContrato = fechaFinContrato;
    }
    public String getNombresApellidosRegente() {
        return NombresApellidosRegente;
    }
    public void setNombresApellidosRegente(String nombresApellidosRegente) {
        NombresApellidosRegente = nombresApellidosRegente;
    }
    public String getObjectIdUa() {
        return ObjectIdUa;
    }
    public void setObjectIdUa(String objectIdUa) {
        ObjectIdUa = objectIdUa;
    }
    public Integer getIdFiscalizacion() {
        return IdFiscalizacion;
    }
    public void setIdFiscalizacion(Integer idFiscalizacion) {
        IdFiscalizacion = idFiscalizacion;
    }
    public String getObservacion() {
        return Observacion;
    }
    public void setObservacion(String observacion) {
        Observacion = observacion;
    }
    public Boolean getConObservacion() {
        return ConObservacion;
    }
    public void setConObservacion(Boolean conObservacion) {
        ConObservacion = conObservacion;
    }
    public Date getFechaFinContratoFiscalizacion() {
        return FechaFinContratoFiscalizacion;
    }
    public void setFechaFinContratoFiscalizacion(Date fechaFinContratoFiscalizacion) {
        FechaFinContratoFiscalizacion = fechaFinContratoFiscalizacion;
    }
    
}
