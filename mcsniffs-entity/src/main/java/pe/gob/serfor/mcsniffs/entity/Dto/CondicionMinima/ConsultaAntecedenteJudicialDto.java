package pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima;

import lombok.Data;

@Data
public class ConsultaAntecedenteJudicialDto{

    private String apellidoPaterno;
    private String apellidoMaterno;
    private String nombres;
 
}