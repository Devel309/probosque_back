package pe.gob.serfor.mcsniffs.entity;
import java.io.Serializable;
import java.sql.Timestamp;

public class RecursoForestalDetalleEntity extends AuditoriaEntity implements Serializable {
    private Integer idRecursoForestalDetalle;
    private String codigoCabecera;

    private String nombreComun;
    private String nombreCientifico;
    private String nombreNativo;
    private String coordenadaEste;
    private String coordenadaNorte;
    private Integer numeroArbolesAprovechables;
    private String volumenComercial;
    private Integer numeroArbolesSemilleros;
    private Integer numeroArbolesTotal;


    private Integer numParcelaCorte;
    private String productoTipo;
    private String unidadMedida;

    private Integer cantidad;



    public Integer getIdRecursoForestalDetalle() {
        return idRecursoForestalDetalle;
    }

    public void setIdRecursoForestalDetalle(Integer idRecursoForestalDetalle) {
        this.idRecursoForestalDetalle = idRecursoForestalDetalle;
    }

    public String getCodigoCabecera() {
        return codigoCabecera;
    }

    public void setCodigoCabecera(String codigoCabecera) {
        this.codigoCabecera = codigoCabecera;
    }

    public String getNombreComun() {
        return nombreComun;
    }

    public void setNombreComun(String nombreComun) {
        this.nombreComun = nombreComun;
    }

    public String getNombreCientifico() {
        return nombreCientifico;
    }

    public void setNombreCientifico(String nombreCientifico) {
        this.nombreCientifico = nombreCientifico;
    }

    public String getNombreNativo() {
        return nombreNativo;
    }

    public void setNombreNativo(String nombreNativo) {
        this.nombreNativo = nombreNativo;
    }

    public String getCoordenadaEste() {
        return coordenadaEste;
    }

    public void setCoordenadaEste(String coordenadaEste) {
        this.coordenadaEste = coordenadaEste;
    }

    public String getCoordenadaNorte() {
        return coordenadaNorte;
    }

    public void setCoordenadaNorte(String coordenadaNorte) {
        this.coordenadaNorte = coordenadaNorte;
    }

    public Integer getNumeroArbolesAprovechables() {
        return numeroArbolesAprovechables;
    }

    public void setNumeroArbolesAprovechables(Integer numeroArbolesAprovechables) {
        this.numeroArbolesAprovechables = numeroArbolesAprovechables;
    }

    public String getVolumenComercial() {
        return volumenComercial;
    }

    public void setVolumenComercial(String volumenComercial) {
        this.volumenComercial = volumenComercial;
    }

    public Integer getNumeroArbolesSemilleros() {
        return numeroArbolesSemilleros;
    }

    public void setNumeroArbolesSemilleros(Integer numeroArbolesSemilleros) {
        this.numeroArbolesSemilleros = numeroArbolesSemilleros;
    }

    public Integer getNumeroArbolesTotal() {
        return numeroArbolesTotal;
    }

    public void setNumeroArbolesTotal(Integer numeroArbolesTotal) {
        this.numeroArbolesTotal = numeroArbolesTotal;
    }



    public Integer getNumParcelaCorte() {
        return numParcelaCorte;
    }

    public void setNumParcelaCorte(Integer numParcelaCorte) {
        this.numParcelaCorte = numParcelaCorte;
    }

    public String getProductoTipo() {
        return productoTipo;
    }

    public void setProductoTipo(String productoTipo) {
        this.productoTipo = productoTipo;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }
}
