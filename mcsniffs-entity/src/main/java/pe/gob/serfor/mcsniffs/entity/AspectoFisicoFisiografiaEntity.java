package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;


public class AspectoFisicoFisiografiaEntity extends AuditoriaEntity implements Serializable {

    private Integer idApectofisicofisiografia;
    private Integer idPlanManejo;
    private String idTipounidadfisiografica;
    private String tipounidadfisiografica;
    private String especificacion;
    private Double area;
    private Boolean accion;
    private String descripcion;

    public Integer getIdApectofisicofisiografia() {
        return idApectofisicofisiografia;
    }

    public void setIdApectofisicofisiografia(Integer idApectofisicofisiografia) {
        this.idApectofisicofisiografia = idApectofisicofisiografia;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public String getIdTipounidadfisiografica() {
        return idTipounidadfisiografica;
    }

    public void setIdTipounidadfisiografica(String idTipounidadfisiografica) {
        this.idTipounidadfisiografica = idTipounidadfisiografica;
    }

    public String getTipounidadfisiografica() {
        return tipounidadfisiografica;
    }

    public void setTipounidadfisiografica(String tipounidadfisiografica) {
        this.tipounidadfisiografica = tipounidadfisiografica;
    }

    public String getEspecificacion() {
        return especificacion;
    }

    public void setEspecificacion(String especificacion) {
        this.especificacion = especificacion;
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public Boolean getAccion() {
        return accion;
    }

    public void setAccion(Boolean accion) {
        this.accion = accion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
