package pe.gob.serfor.mcsniffs.entity.Dto.InformacionBasicaDetalle;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.math.BigDecimal;
@Data
public class InformacionBasicaDetalleDto extends AuditoriaEntity implements Serializable {
    private Integer idInfBasica;
    private String codInfBasica;
    private String codNombreInfBasica;

    private String departamento;
    private String provincia;
    private String distrito;
    private String comunidad;
    private String cuenca;
    private Integer idPlanManejo;
    private Integer idInfBasicaDet;
    private String codInfBasicaDet;
    private String codSubInfBasicaDet;//TAB_1

    private String puntoVertice;

    private String nombre;
    private String acceso;
    private Integer numeroFamilia;
    private String actividad;
    private Boolean subsistencia;
    private Boolean perenne;
    private Boolean ganaderia;
    private Boolean caza;
    private Boolean pesca;
    private Boolean madera;
    private Boolean otroProducto;
    private Boolean ampliarAnexo;
    private String justificacion;
    private String especieExtraida;
    private Boolean marcar;

    private String referencia;
    private BigDecimal distanciaKm;
    private BigDecimal tiempo;
    private String medioTransporte;
    private String epoca;
    private Integer idRios;
    private String descripcion;
    private BigDecimal areaHa;
    private BigDecimal areaHaPorcentaje;
    private String zonaVida;
    private Integer idFauna;
    private Integer idFlora;
    private String nombreRio;
    private String nombreLaguna;
    private String nombreQuebrada;

    private String  anexo;
    private Integer numeroPc;
    private BigDecimal areaTotal;
    private String nroHojaCatastral;
    private String nombreHojaCatastral;
    private Integer idTipoBosque;

    private String nombreComun;
    private String nombreCientifico;
    private String familia;

    private String observaciones;
    private String coordenadaUtm;

    private BigDecimal coordenadaEste;
    private BigDecimal coordenadaNorte;

    private BigDecimal coordenadaEsteFin;
    private BigDecimal coordenadaNorteFin;

    //
    private Short idEspecie;
    private String autor;
    private String amenaza;
}
