package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class PostulacionPFDMResquestEntity implements Serializable{
    private Integer IdPostulacionPFDM;
    private Integer IdUsuarioPostulacion;
    private PostulacionPFDMEntity postulacionPFDM;
    private Anexo1PFDMEntity anexo1;

    public Integer getIdPostulacionPFDM() {
        return IdPostulacionPFDM;
    }
    public void setIdPostulacionPFDM(Integer idPostulacionPFDM) {
        IdPostulacionPFDM = idPostulacionPFDM;
    }
    public Integer getIdUsuarioPostulacion() {
        return IdUsuarioPostulacion;
    }
    public void setIdUsuarioPostulacion(Integer idUsuarioPostulacion) {
        IdUsuarioPostulacion = idUsuarioPostulacion;
    }
    public PostulacionPFDMEntity getPostulacionPFDM() {
        return postulacionPFDM;
    }
    public void setPostulacionPFDM(PostulacionPFDMEntity postulacionPFDM) {
        this.postulacionPFDM = postulacionPFDM;
    }
    public Anexo1PFDMEntity getAnexo1() {
        return anexo1;
    }
    public void setAnexo1(Anexo1PFDMEntity anexo1) {
        this.anexo1 = anexo1;
    }

    
}
