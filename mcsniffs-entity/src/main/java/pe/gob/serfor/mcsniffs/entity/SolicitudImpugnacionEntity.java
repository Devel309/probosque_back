package pe.gob.serfor.mcsniffs.entity;
import java.util.Date;

public class SolicitudImpugnacionEntity  extends AuditoriaEntity {

    private Integer idSolicitudImpugnacion;
    private Integer idSolicitud;
    private String codigoTipoSolicitud;
    private String asunto;
    private String descripcion;
    private String observacion;
    //private ArchivoEntity archivo;
    // private Integer idArchivo;
    // private Integer idArchivoSan;
    // private Integer idArchivoInforme;
    private Integer idProcesoPostulacion;
    private Integer idTipoSolicitud;

    private Integer idArchivoRecursoImpugnacion; // 5
    private Integer idArchivoResultadoEvaluacion; //3
    private Integer idArchivoResolucion; //4
// -------------

    private String rutaArchivoSan;
    private String rutaArchivoInforme;    

    private Integer idUsuarioRegistro;
    private String rutaArchivo; 
    private Date fechaInicio;
    private Date fechaFin; 

    private Boolean habilitado; 
    private Boolean procede; 

    private String calificacionImpugnacion;
    private Date fechaPresentacion; 
    private Boolean fundadoInfundado;
    private Boolean remitidoARFFS; 
    private boolean remitidoResolucion;
    private String bandeja; 
    
    
    public String getBandeja() {
        return bandeja;
    }
    public void setBandeja(String bandeja) {
        this.bandeja = bandeja;
    }
    public Date getFechaInicio() {
        return fechaInicio;
    }
    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }
    public Integer getIdArchivoResolucion() {
        return idArchivoResolucion;
    }
    public void setIdArchivoResolucion(Integer idArchivoResolucion) {
        this.idArchivoResolucion = idArchivoResolucion;
    }
    public Integer getIdArchivoRecursoImpugnacion() {
        return idArchivoRecursoImpugnacion;
    }
    public void setIdArchivoRecursoImpugnacion(Integer idArchivoRecursoImpugnacion) {
        this.idArchivoRecursoImpugnacion = idArchivoRecursoImpugnacion;
    }
    public Integer getIdArchivoResultadoEvaluacion() {
        return idArchivoResultadoEvaluacion;
    }
    public void setIdArchivoResultadoEvaluacion(Integer idArchivoResultadoEvaluacion) {
        this.idArchivoResultadoEvaluacion = idArchivoResultadoEvaluacion;
    }

    public String getCalificacionImpugnacion() {
        return calificacionImpugnacion;
    }

    public void setCalificacionImpugnacion(String calificacionImpugnacion) {
        this.calificacionImpugnacion = calificacionImpugnacion;
    }

    public Date getFechaPresentacion() {
        return fechaPresentacion;
    }
    public void setFechaPresentacion(Date fechaPresentacion) {
        this.fechaPresentacion = fechaPresentacion;
    }
    public Boolean getFundadoInfundado() {
        return fundadoInfundado;
    }
    public void setFundadoInfundado(Boolean fundadoInfundado) {
        this.fundadoInfundado = fundadoInfundado;
    }
    public Boolean getRemitidoARFFS() {
        return remitidoARFFS;
    }
    public void setRemitidoARFFS(Boolean remitidoARFFS) {
        this.remitidoARFFS = remitidoARFFS;
    }
    public boolean getRemitidoResolucion() {
        return remitidoResolucion;
    }
    public void setRemitidoResolucion(boolean remitidoResolucion) {
        this.remitidoResolucion = remitidoResolucion;
    }

    public String getRutaArchivoSan() {
        return rutaArchivoSan;
    }
    public void setRutaArchivoSan(String rutaArchivoSan) {
        this.rutaArchivoSan = rutaArchivoSan;
    }
    public String getRutaArchivoInforme() {
        return rutaArchivoInforme;
    }
    public void setRutaArchivoInforme(String rutaArchivoInforme) {
        this.rutaArchivoInforme = rutaArchivoInforme;
    }
    // public Integer getIdArchivoSan() {
    //     return idArchivoSan;
    // }
    // public void setIdArchivoSan(Integer idArchivoSan) {
    //     this.idArchivoSan = idArchivoSan;
    // }
    // public Integer getIdArchivoInforme() {
    //     return idArchivoInforme;
    // }
    // public void setIdArchivoInforme(Integer idArchivoInforme) {
    //     this.idArchivoInforme = idArchivoInforme;
    // }
    public Boolean getHabilitado() {
        return habilitado;
    }
    public void setHabilitado(Boolean habilitado) {
        this.habilitado = habilitado;
    }
    public Boolean getProcede() {
        return procede;
    }
    public void setProcede(Boolean procede) {
        this.procede = procede;
    }
    public Date getFechaFin() {
        return fechaFin;
    }
    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }    
    public SolicitudImpugnacionEntity() {
    }

    public SolicitudImpugnacionEntity(Integer idSolicitudImpugnacion, String codigoTipoSolicitud, String asunto, 
        String descripcion, String observacion, Integer idUsuarioRegistro) {

        this.setIdSolicitudImpugnacion(idSolicitudImpugnacion == null ? 0 : idSolicitudImpugnacion);
        this.setCodigoTipoSolicitud(codigoTipoSolicitud);
        this.setAsunto(asunto);
        this.setDescripcion(descripcion);
        this.setObservacion(observacion);
        this.setIdUsuarioRegistro(idUsuarioRegistro);
        
    }

    public String getObservacion() {
        return observacion;
    }
    
    public String getRutaArchivo() {
        return rutaArchivo;
    }
    public void setRutaArchivo(String rutaArchivo) {
        this.rutaArchivo = rutaArchivo;
    }
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getIdSolicitud() {
        return idSolicitud;
    }

    // public ArchivoEntity getArchivo() {
    //     return archivo;
    // }

    // public void setArchivo(ArchivoEntity archivo) {
    //     this.archivo = archivo;
    // }

    public Integer getIdSolicitudImpugnacion() {
        return idSolicitudImpugnacion;
    }

    public void setIdSolicitudImpugnacion(Integer idSolicitudImpugnacion) {
        this.idSolicitudImpugnacion = idSolicitudImpugnacion;
    }

    public Integer getIdUsuarioRegistro() {
        return idUsuarioRegistro;
    }

    public void setIdUsuarioRegistro(Integer idUsuarioRegistro) {
        this.idUsuarioRegistro = idUsuarioRegistro;
    }

    // public Integer getIdDocumentoAdjunto() {
    //     return idArchivo;
    // }

    // public void setIdDocumentoAdjunto(Integer idArchivo) {
    //     this.idArchivo = idArchivo == null ? 0 : idArchivo;
    // }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getCodigoTipoSolicitud() {
        return codigoTipoSolicitud;
    }

    public void setCodigoTipoSolicitud(String codigoTipoSolicitud) {
        this.codigoTipoSolicitud = codigoTipoSolicitud;
    }

    public void setIdSolicitud(Integer idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public Integer getIdProcesoPostulacion() {
        return idProcesoPostulacion;
    }

    public void setIdProcesoPostulacion(Integer idProcesoPostulacion) {
        this.idProcesoPostulacion = idProcesoPostulacion;
    }

    public Integer getIdTipoSolicitud() {
        return idTipoSolicitud;
    }

    public void setIdTipoSolicitud(Integer idTipoSolicitud) {
        this.idTipoSolicitud = idTipoSolicitud;
    }
}
