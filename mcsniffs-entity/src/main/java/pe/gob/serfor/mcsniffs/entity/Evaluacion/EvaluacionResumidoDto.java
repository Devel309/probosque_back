package pe.gob.serfor.mcsniffs.entity.Evaluacion;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class EvaluacionResumidoDto  implements Serializable{

    private Integer idEvaluacion;
    private Integer idPlanManejo;
    private String codigoEvaluacion;

    private List<EvaluacionResumidoDetalleDto> listarEvaluacionDetalle;

}
