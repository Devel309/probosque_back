package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProdTerminadoEntity  extends AuditoriaEntity implements Serializable {
    private	Integer idProdTerminado;
    private	Integer idOrdProduccion;
    private	Integer trozas;
    private	BigDecimal volumen;
    private	Integer cantidad;
    private	BigDecimal volumentransformado;
    private	Integer tipoProducto;
    private	BigDecimal espesor;
    private	BigDecimal ancho;
    private	BigDecimal longitud;
    private	Date fechaProdTerminado;
}
