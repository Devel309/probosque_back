package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ManejoBosqEntity extends AuditoriaEntity implements Serializable {
    private Integer idManejoBosque;
    private String idTipo;
    private Boolean accion;
    private String descripcion;
    private PlanManejoEntity planManejo;


    public Integer getIdManejoBosque() {
        return idManejoBosque;
    }

    public void setIdManejoBosque(Integer idManejoBosque) {
        this.idManejoBosque = idManejoBosque;
    }

    public String getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(String idTipo) {
        this.idTipo = idTipo;
    }

    public Boolean getAccion() {
        return accion;
    }

    public void setAccion(Boolean accion) {
        this.accion = accion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public PlanManejoEntity getPlanManejo() {
        return planManejo;
    }

    public void setPlanManejo(PlanManejoEntity planManejo) {
        this.planManejo = planManejo;
    }
}
