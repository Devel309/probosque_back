package pe.gob.serfor.mcsniffs.entity;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;

public class CensoForestalDetalleEntity extends AuditoriaEntity implements Serializable {
    private String idTipoRecurso;
    private Integer idTipoBosque;
    private String idTipoArbol;
    private String codigoArbolCalidad;
    private String codigoUnidadMedida;
    private String productoTipo;
    private Integer parcelaCorte;
    private Integer bloque;
    private Integer faja;
    private Double alturaTotal;
    private Double dap;
    private String strDap;
    private Double alturaComercial;
    private String strAlturaComercial;
    private String  este;
    private Integer zona;
    private String  norte;
    private Double cantidad;
    private Double superficieComercial;//superficie
    private String contrato;
    private String codigoTipoCensoForestal;
    private String codigoUnico;
    private Integer idCodigoEspecie;//Lenght = 100
    private String nombreNativo;
    private Double volumen;
    private String strVolumen;
    private Double factorForma;
    private Double categoriaDiametrica;
    private String descripcionOtros;
    private String nombreComercial;
    private String nombreAlterno;
    private String familia;
    private Boolean mortanda;
    private String  codigoEstadoArbol;
    private String  estadoArbol;
    private String nombreComun;
    private String nombreCientifico;
    private String  codigoCondicionArbol;
    private String  condicionArbol;
    private Integer  numeroCorrelativoArbol;
    private String  bosqueSecundario;
    private Integer  idPlanManManejo;
    private Integer idCensoForestalCabecera;
    private Integer unidadTrabajo;
    private String codigoDeArbol;
    private String  imagen;
    private String  audio;
    //////////////////////////////////////////
    private Integer idTipoEvaluacion;
    private Integer idCensoForestalDetalle;
    private Integer idCensoForestal; 
    private CensoForestalEntity censoForestalEntity;
    private String correlativo;
    private String  idNombreComun;
    private String  idNombreCientifico;
    private Integer idEspecieCodigo;
    private String coorEste;
    private String  coorNorte;
    private String nota;

    ////////////////////////////////////
    private String  parcelaCorta;
    private String  bloqueQuinquenal;

    private String codigoTipoArbol;
    private String tipoArbol;

    private Integer idPlanManejo;
    private Integer nroIndividuo;
    private Double areaProductiva;
    private String productoExtraer;
    private String unidadMedida;
    private Double cantidadProducto;
    private Integer nroArbolesCensados;
    private Integer nroArboles;
    private Integer nroArbolesSemilleros;

    private String codigoArbol;

    private String  strIdPlanManejo;

    public Integer getNroArbolesCensados() {
        return nroArbolesCensados;
    }

    public void setNroArbolesCensados(Integer nroArbolesCensados) {
        this.nroArbolesCensados = nroArbolesCensados;
    }

    public Integer getNroArboles() {
        return nroArboles;
    }

    public void setNroArboles(Integer nroArboles) {
        this.nroArboles = nroArboles;
    }

    public Integer getNroArbolesSemilleros() {
        return nroArbolesSemilleros;
    }

    public void setNroArbolesSemilleros(Integer nroArbolesSemilleros) {
        this.nroArbolesSemilleros = nroArbolesSemilleros;
    }

    public Integer getNroIndividuo() {
        return nroIndividuo;
    }

    public void setNroIndividuo(Integer nroIndividuo) {
        this.nroIndividuo = nroIndividuo;
    }

    public Double getAreaProductiva() {
        return areaProductiva;
    }

    public void setAreaProductiva(Double areaProductiva) {
        this.areaProductiva = areaProductiva;
    }

    public String getProductoExtraer() {
        return productoExtraer;
    }

    public void setProductoExtraer(String productoExtraer) {
        this.productoExtraer = productoExtraer;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public Double getCantidadProducto() {
        return cantidadProducto;
    }

    public void setCantidadProducto(Double cantidadProducto) {
        this.cantidadProducto = cantidadProducto;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }



    public String getCodigoTipoArbol() {
        return codigoTipoArbol;
    }

    public void setCodigoTipoArbol(String codigoTipoArbol) {
        this.codigoTipoArbol = codigoTipoArbol;
    }

    public String getTipoArbol() {
        return tipoArbol;
    }

    public void setTipoArbol(String tipoArbol) {
        this.tipoArbol = tipoArbol;
    }

    public String getParcelaCorta() {

        parcelaCorta=llenaNulosString(parcelaCorta);
        return parcelaCorta;
    }

    public void setParcelaCorta(String parcelaCorta) {
        this.parcelaCorta = parcelaCorta;
    }

    public String getBloqueQuinquenal() {
        bloqueQuinquenal=llenaNulosString(bloqueQuinquenal);
        return bloqueQuinquenal;
    }

    public void setBloqueQuinquenal(String bloqueQuinquenal) {
        this.bloqueQuinquenal = bloqueQuinquenal;
    }

    public String getIdTipoRecurso() {
        idTipoRecurso=llenaNulosString(idTipoRecurso);
        return idTipoRecurso;
    }

    public void setIdTipoRecurso(String idTipoRecurso) {
        this.idTipoRecurso = idTipoRecurso;
    }

    public Integer getIdTipoBosque() {
        idTipoBosque=llenaNulosInteger(idTipoBosque);
        return idTipoBosque;
    }

    public void setIdTipoBosque(Integer idTipoBosque) {
        this.idTipoBosque = idTipoBosque;
    }

    public String getIdTipoArbol() {
        idTipoArbol=llenaNulosString(idTipoArbol);
        return idTipoArbol;
    }

    public void setIdTipoArbol(String idTipoArbol) {
        this.idTipoArbol = idTipoArbol;
    }

    public String getCodigoArbolCalidad() {
        codigoArbolCalidad=llenaNulosString(codigoArbolCalidad);
        return codigoArbolCalidad;
    }

    public void setCodigoArbolCalidad(String codigoArbolCalidad) {
        this.codigoArbolCalidad = codigoArbolCalidad;
    }

    public String getCodigoArbol() {
        codigoArbol=llenaNulosString(codigoArbol);
        return codigoArbol;
    }

    public void setCodigoArbol(String codigoArbol) {
        this.codigoArbol = codigoArbol;
    }

    public String getCodigoUnidadMedida() {
        codigoUnidadMedida=llenaNulosString(codigoUnidadMedida);
        return codigoUnidadMedida;
    }

    public void setCodigoUnidadMedida(String codigoUnidadMedida) {
        this.codigoUnidadMedida = codigoUnidadMedida;
    }

    public String getProductoTipo() {
        productoTipo=llenaNulosString(productoTipo);
        return productoTipo;
    }

    public void setProductoTipo(String productoTipo) {
        this.productoTipo = productoTipo;
    }

    public Integer getFaja() {
        faja=llenaNulosInteger(faja);
        return faja;
    }

    public void setFaja(Integer faja) {
        this.faja = faja;
    }

    public Double getAlturaTotal() {
        alturaTotal=llenaNulosDouble(alturaTotal);
        return alturaTotal;
    }

    public void setAlturaTotal(Double alturaTotal) {
        this.alturaTotal = alturaTotal;
    }

    public Double getDap() {
        dap=llenaNulosDouble(dap);
        return dap;
    }

    public void setDap(Double dap) {
        this.dap = dap;
    }

    public Double getAlturaComercial() {
        alturaComercial=llenaNulosDouble(alturaComercial);
        return alturaComercial;
    }

    public void setAlturaComercial(Double alturaComercial) {
        this.alturaComercial = alturaComercial;
    }

    public String getEste() {
        return este;
    }

    public void setEste(String este) {
        this.este = este;
    }

    public Integer getZona() {
        zona=llenaNulosInteger(zona);
        return zona;
    }

    public void setZona(Integer zona) {
        this.zona = zona;
    }

    public String getNorte() {
        return norte;
    }

    public void setNorte(String norte) {
        this.norte = norte;
    }

    public Double getCantidad() {
        cantidad=llenaNulosDouble(cantidad);
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public Double getSuperficieComercial() {
        superficieComercial=llenaNulosDouble(superficieComercial);
        return superficieComercial;
    }

    public void setSuperficieComercial(Double superficieComercial) {
        this.superficieComercial = superficieComercial;
    }

    public String getContrato() {
        contrato=llenaNulosString(contrato);
        return contrato;
    }

    public void setContrato(String contrato) {
        this.contrato = contrato;
    }

    public String getCodigoUnico() {
        codigoUnico=llenaNulosString(codigoUnico);
        return codigoUnico;
    }

    public void setCodigoUnico(String codigoUnico) {
        this.codigoUnico = codigoUnico;
    }

    public Integer getIdCodigoEspecie() {
        idCodigoEspecie=llenaNulosInteger(idCodigoEspecie);
        return idCodigoEspecie;
    }

    public void setIdCodigoEspecie(Integer idCodigoEspecie) {
        this.idCodigoEspecie = idCodigoEspecie;
    }

    public String getNombreNativo() {
        nombreNativo=llenaNulosString(nombreNativo);
        return nombreNativo;
    }

    public void setNombreNativo(String nombreNativo) {
        this.nombreNativo = nombreNativo;
    }

    public Double getVolumen() {
        volumen=llenaNulosDouble(volumen);
        return volumen;
    }

    public void setVolumen(Double volumen) {
        this.volumen = volumen;
    }

    public Double getFactorForma() {
        factorForma=llenaNulosDouble(factorForma);
        return factorForma;
    }

    public void setFactorForma(Double factorForma) {
        this.factorForma = factorForma;
    }

    public Double getCategoriaDiametrica() {
        categoriaDiametrica=llenaNulosDouble(categoriaDiametrica);
        return categoriaDiametrica;
    }

    public void setCategoriaDiametrica(Double categoriaDiametrica) {
        this.categoriaDiametrica = categoriaDiametrica;
    }

    public String getDescripcionOtros() {
        descripcionOtros=llenaNulosString(descripcionOtros);
        return descripcionOtros;
    }

    public void setDescripcionOtros(String descripcionOtros) {
        this.descripcionOtros = descripcionOtros;
    }

    public String getNombreComercial() {
        nombreComercial=llenaNulosString(nombreComercial);
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public String getNombreAlterno() {
        nombreAlterno=llenaNulosString(nombreAlterno);
        return nombreAlterno;
    }

    public void setNombreAlterno(String nombreAlterno) {
        this.nombreAlterno = nombreAlterno;
    }

    public String getFamilia() {
        familia=llenaNulosString(familia);
        return familia;
    }

    public void setFamilia(String familia) {
        this.familia = familia;
    }

    public Boolean getMortanda() {
        mortanda=llenaNulosBoolean(mortanda);
        return mortanda;
    }

    public void setMortanda(Boolean mortanda) {
        this.mortanda = mortanda;
    }

    public String getCodigoEstadoArbol() {
        codigoEstadoArbol = llenaNulosString(codigoEstadoArbol);
        return codigoEstadoArbol;
    }

    public void setCodigoEstadoArbol(String codigoEstadoArbol) {
        this.codigoEstadoArbol = codigoEstadoArbol;
    }

    public String getEstadoArbol() {
        estadoArbol=llenaNulosString(estadoArbol);
        return estadoArbol;
    }

    public void setEstadoArbol(String estadoArbol) {
        this.estadoArbol = estadoArbol;
    }

    public String getNombreComun() {
        nombreComun=llenaNulosString(nombreComun);
        return nombreComun;
    }

    public void setNombreComun(String nombreComun) {
        this.nombreComun = nombreComun;
    }

    public String getNombreCientifico() {
        nombreCientifico=llenaNulosString(nombreCientifico);
        return nombreCientifico;
    }

    public void setNombreCientifico(String nombreCientifico) {
        this.nombreCientifico = nombreCientifico;
    }

    public String getCodigoCondicionArbol() {
        codigoCondicionArbol=llenaNulosString(codigoCondicionArbol);
        return codigoCondicionArbol;
    }

    public void setCodigoCondicionArbol(String codigoCondicionArbol) {
        this.codigoCondicionArbol = codigoCondicionArbol;
    }

    public String getCondicionArbol() {
        condicionArbol=llenaNulosString(condicionArbol);
        return condicionArbol;
    }

    public void setCondicionArbol(String condicionArbol) {
        this.condicionArbol = condicionArbol;
    }

    public String getBosqueSecundario() {
        bosqueSecundario=llenaNulosString(bosqueSecundario);
        return bosqueSecundario;
    }

    public void setBosqueSecundario(String bosqueSecundario) {
        this.bosqueSecundario = bosqueSecundario;
    }

    public String getImagen() {
        imagen=llenaNulosString(imagen);
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getAudio() {
        audio=llenaNulosString(audio);
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public Integer getIdCensoForestalDetalle() {
        idCensoForestalDetalle=llenaNulosInteger(idCensoForestalDetalle);
        return idCensoForestalDetalle;
    }

    public void setIdCensoForestalDetalle(Integer idCensoForestalDetalle) {
        this.idCensoForestalDetalle = idCensoForestalDetalle;
    }

    public Integer getIdCensoForestal() {
        idCensoForestal=llenaNulosInteger(idCensoForestal);
        return idCensoForestal;
    }

    public void setIdCensoForestal(Integer idCensoForestal) {
        this.idCensoForestal = idCensoForestal;
    }

    public CensoForestalEntity getCensoForestalEntity() {
        return censoForestalEntity;
    }

    public void setCensoForestalEntity(CensoForestalEntity censoForestalEntity) {
        this.censoForestalEntity = censoForestalEntity;
    }

    public String getCorrelativo() {
        correlativo=llenaNulosString(correlativo);
        return correlativo;
    }

    public void setCorrelativo(String correlativo) {
        this.correlativo = correlativo;
    }


    public Integer getIdEspecieCodigo() {
        idEspecieCodigo=llenaNulosInteger(idEspecieCodigo);
        return idEspecieCodigo;
    }

    public void setIdEspecieCodigo(Integer idEspecieCodigo) {
        this.idEspecieCodigo = idEspecieCodigo;
    }

    public String getNota() {
        nota=llenaNulosString(nota);
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }
    public String getCoorEste() {
        coorEste=llenaNulosString(coorEste);
        return coorEste;
    }

    public void setCoorEste(String coorEste) {
        this.coorEste = coorEste;
    }

    public String getCoorNorte() {
        coorNorte=llenaNulosString(coorNorte);
        return coorNorte;
    }

    public void setCoorNorte(String coorNorte) {
        this.coorNorte = coorNorte;
    }

    public String getIdNombreComun() {
        idNombreComun=llenaNulosString(idNombreComun);
        return idNombreComun;
    }

    public void setIdNombreComun(String idNombreComun) {
        this.idNombreComun = idNombreComun;
    }

    public String getIdNombreCientifico() {
        idNombreCientifico=llenaNulosString(idNombreCientifico);
        return idNombreCientifico;
    }

    public void setIdNombreCientifico(String idNombreCientifico) {
        this.idNombreCientifico = idNombreCientifico;
    }
    public Integer getNumeroCorrelativoArbol() {
        numeroCorrelativoArbol=llenaNulosInteger(numeroCorrelativoArbol);
        return numeroCorrelativoArbol;
    }

    public void setNumeroCorrelativoArbol(Integer numeroCorrelativoArbol) {
        this.numeroCorrelativoArbol = numeroCorrelativoArbol;
    }

    public Integer getIdPlanManManejo() {
        idPlanManManejo=llenaNulosInteger(idPlanManManejo);
        return idPlanManManejo;
    }

    public void setIdPlanManManejo(Integer idPlanManManejo) {
        this.idPlanManManejo = idPlanManManejo;
    }

    public Integer getIdCensoForestalCabecera() {
        idCensoForestalCabecera=llenaNulosInteger(idCensoForestalCabecera);
        return idCensoForestalCabecera;
    }

    public void setIdCensoForestalCabecera(Integer idCensoForestalCabecera) {
        this.idCensoForestalCabecera = idCensoForestalCabecera;
    }

    public Integer getUnidadTrabajo() {
        unidadTrabajo=llenaNulosInteger(unidadTrabajo);
        return unidadTrabajo;
    }

    public void setUnidadTrabajo(Integer unidadTrabajo) {
        this.unidadTrabajo = unidadTrabajo;
    }

    public String getCodigoDeArbol() {
        codigoDeArbol=llenaNulosString(codigoDeArbol);
        return codigoDeArbol;
    }

    public void setCodigoDeArbol(String codigoDeArbol) {
        this.codigoDeArbol = codigoDeArbol;
    }

    public String getCodigoTipoCensoForestal() {
        codigoTipoCensoForestal=llenaNulosString(codigoTipoCensoForestal);
        return codigoTipoCensoForestal;
    }

    public void setCodigoTipoCensoForestal(String codigoTipoCensoForestal) {
        this.codigoTipoCensoForestal = codigoTipoCensoForestal;
    }

    public Integer getIdTipoEvaluacion() {
        idTipoEvaluacion=llenaNulosInteger(idTipoEvaluacion);
        return idTipoEvaluacion;
    }

    public void setIdTipoEvaluacion(Integer idTipoEvaluacion) {
        this.idTipoEvaluacion = idTipoEvaluacion;
    }

    public String  cadenaRetificadaEnteros(String cadenaExcel) {
        String  cadenaEntero=""+(int) Double.parseDouble(cadenaExcel);
        return cadenaEntero;
    }

    public Integer getParcelaCorte() {
        parcelaCorte=llenaNulosInteger(parcelaCorte);
        return parcelaCorte;
    }

    public void setParcelaCorte(Integer parcelaCorte) {
        this.parcelaCorte = parcelaCorte;
    }

    public Integer getBloque() {
        bloque=llenaNulosInteger(bloque);
        return bloque;
    }

    public void setBloque(Integer bloque) {
        this.bloque = bloque;
    }

    public String getStrDap() {
        return this.strDap;
    }

    public void setStrDap(String dap) {
        this.strDap = dap;
    }

    public String getStrAlturaComercial() {
        return this.strAlturaComercial;
    }

    public void setStrAlturaComercial(String alturaCpmercial) {
        this.strAlturaComercial = alturaCpmercial;
    }

    public String getStrVolumen() {
        return this.strVolumen;
    }

    public void setStrVolumen(String volumen) {
        this.strVolumen = volumen;
    }

    public String getStrIdPlanManejo() {
        return this.strIdPlanManejo;
    }

    public void setStrIdPlanManejo(String idPlanManejo) {
        this.strIdPlanManejo = idPlanManejo;
    }

    public CensoForestalDetalleEntity lenarCensoForestal(ArrayList obj) {
        CensoForestalDetalleEntity  censoForestalDetalleEntity = new CensoForestalDetalleEntity();
        censoForestalDetalleEntity.setIdTipoRecurso(""+obj.get(0));
        censoForestalDetalleEntity.setIdTipoBosque(Integer.parseInt(censoForestalDetalleEntity.cadenaRetificadaEnteros(""+obj.get(1))));
        censoForestalDetalleEntity.setIdTipoArbol(""+obj.get(2));
        censoForestalDetalleEntity.setCodigoArbolCalidad(""+obj.get(3));
        censoForestalDetalleEntity.setCodigoUnidadMedida(""+obj.get(4));
        censoForestalDetalleEntity.setProductoTipo(""+obj.get(5));
        censoForestalDetalleEntity.setParcelaCorta(""+obj.get(6));
        censoForestalDetalleEntity.setBloqueQuinquenal(""+obj.get(7));
        censoForestalDetalleEntity.setFaja(Integer.parseInt(censoForestalDetalleEntity.cadenaRetificadaEnteros(""+obj.get(8))));
        censoForestalDetalleEntity.setAlturaTotal(Double.parseDouble(""+obj.get(9)));
        censoForestalDetalleEntity.setDap(Double.parseDouble(""+obj.get(10)));
        censoForestalDetalleEntity.setAlturaComercial(Double.parseDouble(""+obj.get(11)));
        censoForestalDetalleEntity.setEste(""+obj.get(12));
        censoForestalDetalleEntity.setZona(Integer.parseInt(censoForestalDetalleEntity.cadenaRetificadaEnteros(""+obj.get(13))));
        censoForestalDetalleEntity.setNorte(""+obj.get(14));
        censoForestalDetalleEntity.setCantidad(Double.parseDouble(""+obj.get(15)));
        censoForestalDetalleEntity.setSuperficieComercial(Double.parseDouble(""+obj.get(16)));
        censoForestalDetalleEntity.setContrato(""+obj.get(17));
        censoForestalDetalleEntity.setCodigoTipoCensoForestal(""+obj.get(18));
        censoForestalDetalleEntity.setCodigoUnico(""+obj.get(19));
        censoForestalDetalleEntity.setIdCodigoEspecie(Integer.parseInt(censoForestalDetalleEntity.cadenaRetificadaEnteros(""+obj.get(20))));
        censoForestalDetalleEntity.setNombreNativo(""+obj.get(21));
        censoForestalDetalleEntity.setVolumen(Double.parseDouble(""+obj.get(22)));
        censoForestalDetalleEntity.setFactorForma(Double.parseDouble(""+obj.get(23)));
        censoForestalDetalleEntity.setCategoriaDiametrica(Double.parseDouble(""+obj.get(24)));
        censoForestalDetalleEntity.setDescripcionOtros(""+obj.get(25));
        censoForestalDetalleEntity.setNombreComercial(""+obj.get(26));
        censoForestalDetalleEntity.setNombreAlterno(""+obj.get(27));
        censoForestalDetalleEntity.setFamilia(""+obj.get(28));
        censoForestalDetalleEntity.setMortanda(("SI".equals(""+obj.get(29))) ?true:false);
        censoForestalDetalleEntity.setEstadoArbol(""+obj.get(30));
        censoForestalDetalleEntity.setNombreComun(""+obj.get(31));
        censoForestalDetalleEntity.setNombreCientifico(""+obj.get(32));
        censoForestalDetalleEntity.setCondicionArbol(""+obj.get(33));
        censoForestalDetalleEntity.setNumeroCorrelativoArbol(Integer.parseInt(censoForestalDetalleEntity.cadenaRetificadaEnteros(""+obj.get(34))));
        censoForestalDetalleEntity.setBosqueSecundario(""+obj.get(35));
        censoForestalDetalleEntity.setIdPlanManManejo(Integer.parseInt(censoForestalDetalleEntity.cadenaRetificadaEnteros(""+obj.get(36))));
      //  censoForestalDetalleEntity.setIdCensoForestalCabecera(Integer.parseInt(censoForestalDetalleEntity.cadenaRetificadaEnteros(""+obj.get(37))));
        censoForestalDetalleEntity.setUnidadTrabajo(Integer.parseInt(censoForestalDetalleEntity.cadenaRetificadaEnteros(""+obj.get(37))));
        censoForestalDetalleEntity.setCodigoDeArbol(""+obj.get(38));
        censoForestalDetalleEntity.setImagen("");
        censoForestalDetalleEntity.setAudio("");
        return  censoForestalDetalleEntity;
    }

    private String llenaNulosString(String entrada){
        return  (entrada!=null)  ? entrada : "";
    }

    private Integer llenaNulosInteger(Integer entrada){
        return  (entrada!=null)  ? entrada : 0;
    }

    private Double llenaNulosDouble(Double entrada){
        return  (entrada!=null)  ? entrada : 0.0;
    }

    private Boolean llenaNulosBoolean(Boolean entrada){
        return  (entrada!=null)  ? entrada : false;
    }
    @Override
    public String toString() {
        return "CensoForestalDetalleEntity{" +
                "idTipoRecurso='" + idTipoRecurso +"\n" +
                ", idTipoBosque=" + idTipoBosque +"\n" +
                ", idTipoArbol='" + idTipoArbol +"\n" +
                ", codigoArbolCalidad='" + codigoArbolCalidad +"\n" +
                ", codigoUnidadMedida='" + codigoUnidadMedida +"\n" +
                ", productoTipo='" + productoTipo +"\n" +
                ", faja=" + faja +"\n" +
                ", alturaTotal=" + alturaTotal +"\n" +
                ", dap=" + dap +"\n" +
                ", alturaComercial=" + alturaComercial +"\n" +
                ", este='" + este +"\n" +
                ", zona=" + zona+"\n" +
                ", norte='" + norte +"\n" +
                ", cantidad=" + cantidad+"\n" +
                ", superficieComercial=" + superficieComercial +"\n" +
                ", contrato='" + contrato +"\n" +
                ", codigoTipoCensoForestal='" + codigoTipoCensoForestal +"\n" +
                ", codigoUnico='" + codigoUnico +"\n" +
                ", idCodigoEspecie=" + idCodigoEspecie +
                ", nombreNativo='" + nombreNativo +"\n" +
                ", volumen=" + volumen +"\n" +
                ", factorForma=" + factorForma +"\n" +
                ", categoriaDiametrica=" + categoriaDiametrica +"\n" +
                ", descripcionOtros='" + descripcionOtros +"\n" +
                ", nombreComercial='" + nombreComercial +"\n" +
                ", nombreAlterno='" + nombreAlterno +"\n" +
                ", familia='" + familia +"\n" +
                ", mortanda=" + mortanda +"\n" +
                ", estadoArbol='" + estadoArbol +"\n" +
                ", nombreComun='" + nombreComun +"\n" +
                ", nombreCientifico='" + nombreCientifico +"\n" +
                ", condicionArbol='" + condicionArbol +"\n" +
                ", numeroCorrelativoArbol=" + numeroCorrelativoArbol +
                ", bosqueSecundario='" + bosqueSecundario +"\n" +
                ", idPlanManManejo=" + idPlanManManejo +"\n" +
                ", idCensoForestalCabecera=" + idCensoForestalCabecera +"\n" +
                ", unidadTrabajo=" + unidadTrabajo +"\n" +
                ", codigoDeArbol='" + codigoDeArbol +"\n" +
                ", imagen='" + imagen +"\n" +
                ", audio='" + audio +"\n" +
                ", idTipoEvaluacion=" + idTipoEvaluacion+"\n" +
                ", idCensoForestalDetalle=" + idCensoForestalDetalle +"\n" +
                ", idCensoForestal=" + idCensoForestal +"\n" +
                ", censoForestalEntity=" + censoForestalEntity+"\n" +
                ", correlativo='" + correlativo +"\n" +
                ", idNombreComun='" + idNombreComun +"\n" +
                ", idNombreCientifico='" + idNombreCientifico +"\n" +
                ", idEspecieCodigo=" + idEspecieCodigo +"\n" +
                ", coorEste='" + coorEste +"\n" +
                ", coorNorte='" + coorNorte +"\n" +
                ", nota='" + nota +"\n" +
                '}';
    }
}
