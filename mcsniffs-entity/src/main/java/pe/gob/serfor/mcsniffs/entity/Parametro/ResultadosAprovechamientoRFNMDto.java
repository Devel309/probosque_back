package pe.gob.serfor.mcsniffs.entity.Parametro;

public class ResultadosAprovechamientoRFNMDto {
    String sector;
    String area;
    String nTotalIndividuos;
    String individuosHa;
    String volumen;
    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getnTotalIndividuos() {
        return nTotalIndividuos;
    }

    public void setnTotalIndividuos(String nTotalIndividuos) {
        this.nTotalIndividuos = nTotalIndividuos;
    }

    public String getIndividuosHa() {
        return individuosHa;
    }

    public void setIndividuosHa(String individuosHa) {
        this.individuosHa = individuosHa;
    }

    public String getVolumen() {
        return volumen;
    }

    public void setVolumen(String volumen) {
        this.volumen = volumen;
    }
}
