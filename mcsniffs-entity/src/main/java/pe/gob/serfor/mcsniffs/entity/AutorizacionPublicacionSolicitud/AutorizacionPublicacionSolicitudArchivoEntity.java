package pe.gob.serfor.mcsniffs.entity.AutorizacionPublicacionSolicitud;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AutorizacionPublicacionSolicitudArchivoEntity extends AuditoriaEntity  implements Serializable {

    Integer idAutorizacionPublicacionSolicitudArchivo ;
    Integer idAutorizacionPublicacionSolicitud ;
    Integer idArchivo ;
    String idTipoDocumento ;
    Integer idTipoSeccionArchivo ;
    String nombre;
    String tipoDocumento;
}
