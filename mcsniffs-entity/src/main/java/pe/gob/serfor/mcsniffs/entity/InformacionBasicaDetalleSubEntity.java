package pe.gob.serfor.mcsniffs.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class InformacionBasicaDetalleSubEntity extends AuditoriaEntity implements Serializable {
    private Integer idInfBasicaDetSub;
    private Integer idInfBasicaDet;
    private String codTipoInfBasicaDet;
    private String codSubTipoInfBasicaDet;
    private String descripcion;
    private String detalle;
    private String observacion;
}
