package pe.gob.serfor.mcsniffs.entity.Dto.SolPlantacionForestal;


import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.util.Date;

@Data
public class SolPlantacionForestalDto  extends AuditoriaEntity {
    private Integer idSolicitudPlantacion;


    private String tipoModalidad;
    private String tipoModalidadTexto;
    private String tipoPersonaSolicitante;
    private String tipoPersonaSolicitanteTexto;
    private String tipoDocumentoSolicitante;
    private String tipoDocumentoSolicitanteTexto;

    private String nombreSolicitante;


    private String nombreSolicitanteNatural;
    private String nombreSolicitanteJuridico;

    private String numeroDocumentoSolicitante;

    private Integer idSolicitante;

    private String telefonoFijo;
    private String correoElectronico;

    private Integer idDepartamentoSolicitante;
    private Integer idProvinciaSolicitante;
    private Integer idDistritoSolicitante;
    private String nombreDepartamentoSolicitante;
    private String nombreProvinciaSolicitante;
    private String nombreDistritoSolicitante;
    private String direccionSolicitante;

    private String estadoSolicitud;
    private String estadoSolicitudTexto;

    private String numeroTH;
    private String fechaRegistroTexto;
    private String tipoEnvio;
    private String nroTramite;
    private Date fechaTramite;

    private Boolean remitir;
    private Boolean notificarSolicitante;

    private Boolean favorable;
    private Boolean notificarPasSolicitante;
    private Boolean notificarPasSerfor;
    private String observacion;

    private Integer idEvaluacionCampo;
    private Integer idSolicitudPlantacionHijo;
    private Boolean informacionOpcionalActualizado;
    private Boolean notificarActualizadoARFFS;
    private Boolean actualizarInformacionRNPF;
}