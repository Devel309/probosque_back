package pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoArchivo;

import com.fasterxml.jackson.annotation.JsonFormat;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.EvaluacionResultadoDetalleEntity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class PlanManejoArchivoDto extends AuditoriaEntity implements Serializable {
  private Integer idPlanManejoArchivo;
  private String codigoTipoPgmf;
  private String codigoSubTipoPgmf;
  private String nombre;
  private String extension;
  private String idTipoDocumento;
  private String tipoDocumento;
  private String descripcion;
  private String observacion;
  private Integer idPlanManejo;
  private Integer idArchivo;
  private Boolean conforme;
  private String descripcionDocumento;
  private byte[] documento;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
  private Date fechaPlan;
  private String codigoUnico;

  public String getCodigoUnico() {
    return codigoUnico;
  }

  public void setCodigoUnico(String codigoUnico) {
    this.codigoUnico = codigoUnico;
  }

  public Date getFechaPlan() {
    return fechaPlan;
  }

  public void setFechaPlan(Date fechaPlan) {
    this.fechaPlan = fechaPlan;
  }

  public byte[] getDocumento() {
    return documento;
  }

  public void setDocumento(byte[] documento) {
    this.documento = documento;
  }

  public String getDescripcionDocumento() {
    return descripcionDocumento;
  }

  public void setDescripcionDocumento(String descripcionDocumento) {
    this.descripcionDocumento = descripcionDocumento;
  }

  public Integer getIdPlanManejoArchivo() {
    return idPlanManejoArchivo;
  }

  public void setIdPlanManejoArchivo(Integer idPlanManejoArchivo) {
    this.idPlanManejoArchivo = idPlanManejoArchivo;
  }

  public String getCodigoTipoPgmf() {
    return codigoTipoPgmf;
  }

  public void setCodigoTipoPgmf(String codigoTipoPgmf) {
    this.codigoTipoPgmf = codigoTipoPgmf;
  }

  public String getCodigoSubTipoPgmf() {
    return codigoSubTipoPgmf;
  }

  public void setCodigoSubTipoPgmf(String codigoSubTipoPgmf) {
    this.codigoSubTipoPgmf = codigoSubTipoPgmf;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getExtension() {
    return extension;
  }

  public void setExtension(String extension) {
    this.extension = extension;
  }

  public String getIdTipoDocumento() {
    return idTipoDocumento;
  }

  public void setIdTipoDocumento(String idTipoDocumento) {
    this.idTipoDocumento = idTipoDocumento;
  }

  public String getTipoDocumento() {
    return tipoDocumento;
  }

  public void setTipoDocumento(String tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public String getObservacion() {
    return observacion;
  }

  public void setObservacion(String observacion) {
    this.observacion = observacion;
  }

  public Integer getIdPlanManejo() {
    return idPlanManejo;
  }

  public void setIdPlanManejo(Integer idPlanManejo) {
    this.idPlanManejo = idPlanManejo;
  }

  public Integer getIdArchivo() {
    return idArchivo;
  }

  public void setIdArchivo(Integer idArchivo) {
    this.idArchivo = idArchivo;
  }

  public Boolean getConforme() {
    return conforme;
  }

  public void setConforme(Boolean conforme) {
    this.conforme = conforme;
  }
}
