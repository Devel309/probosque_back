package pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionUMF;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;


@Data
public class OrdenProteccionCategoriaUMFEntity  extends AuditoriaEntity{
    

    
   Integer id_ordenprotec_categ_umf ;
   Integer id_plan_manejo ;
   Integer id_coberturaboscosa ;
   Integer id_tipocoberturaboscosa;
   String area ;
   Integer porcentaje ;
   Boolean especificarinformacion ;
   public Integer getId_ordenprotec_categ_umf() {
      return id_ordenprotec_categ_umf;
   }
   public void setId_ordenprotec_categ_umf(Integer id_ordenprotec_categ_umf) {
      this.id_ordenprotec_categ_umf = id_ordenprotec_categ_umf;
   }
   public Integer getId_plan_manejo() {
      return id_plan_manejo;
   }
   public void setId_plan_manejo(Integer id_plan_manejo) {
      this.id_plan_manejo = id_plan_manejo;
   }
   public Integer getId_coberturaboscosa() {
      return id_coberturaboscosa;
   }
   public void setId_coberturaboscosa(Integer id_coberturaboscosa) {
      this.id_coberturaboscosa = id_coberturaboscosa;
   }
   public Integer getId_tipocoberturaboscosa() {
      return id_tipocoberturaboscosa;
   }
   public void setId_tipocoberturaboscosa(Integer id_tipocoberturaboscosa) {
      this.id_tipocoberturaboscosa = id_tipocoberturaboscosa;
   }
   public String getArea() {
      return area;
   }
   public void setArea(String area) {
      this.area = area;
   }
   public Integer getPorcentaje() {
      return porcentaje;
   }
   public void setPorcentaje(Integer porcentaje) {
      this.porcentaje = porcentaje;
   }
   public Boolean getEspecificarinformacion() {
      return especificarinformacion;
   }
   public void setEspecificarinformacion(Boolean especificarinformacion) {
      this.especificarinformacion = especificarinformacion;
   }

   
}
