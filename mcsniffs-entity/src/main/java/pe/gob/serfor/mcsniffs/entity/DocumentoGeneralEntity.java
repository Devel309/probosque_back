package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class DocumentoGeneralEntity extends AuditoriaEntity implements Serializable {
    private Integer idDocumentoGeneral;
    private Integer idTipoDocumento;
    private Integer idTipoProceso;
    private String url;
    private Integer obligatorio;

    public Integer getIdDocumentoGeneral() {
        return idDocumentoGeneral;
    }

    public void setIdDocumentoGeneral(Integer idDocumentoGeneral) {
        this.idDocumentoGeneral = idDocumentoGeneral;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public Integer getIdTipoProceso() {
        return idTipoProceso;
    }

    public void setIdTipoProceso(Integer idTipoProceso) {
        this.idTipoProceso = idTipoProceso;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getObligatorio() {
        return obligatorio;
    }

    public void setObligatorio(Integer obligatorio) {
        this.obligatorio = obligatorio;
    }
}
