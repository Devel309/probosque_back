package pe.gob.serfor.mcsniffs.entity;

import javax.persistence.criteria.Predicate.BooleanOperator;
import javax.sql.rowset.serial.SerialArray;

public class ContratoValidacionesEntity extends AuditoriaEntity{
    private Integer IdContrato;
    private Integer IdEntidadConsulta;
    private String DescripcionEntidadConsulta;
    private String Observacion;
    private Boolean Validado;
    private Integer IdContratoValidaciones;

    
    
    public Integer getIdContratoValidaciones() {
        return IdContratoValidaciones;
    }
    public void setIdContratoValidaciones(Integer idContratoValidaciones) {
        IdContratoValidaciones = idContratoValidaciones;
    }
    public Integer getIdContrato() {
        return IdContrato;
    }
    public void setIdContrato(Integer idContrato) {
        IdContrato = idContrato;
    }
    public Integer getIdEntidadConsulta() {
        return IdEntidadConsulta;
    }
    public void setIdEntidadConsulta(Integer idEntidadConsulta) {
        IdEntidadConsulta = idEntidadConsulta;
    }
    public String getDescripcionEntidadConsulta() {
        return DescripcionEntidadConsulta;
    }
    public void setDescripcionEntidadConsulta(String descripcionEntidadConsulta) {
        DescripcionEntidadConsulta = descripcionEntidadConsulta;
    }
    public String getObservacion() {
        return Observacion;
    }
    public void setObservacion(String observacion) {
        Observacion = observacion;
    }
    public Boolean getValidado() {
        return Validado;
    }
    public void setValidado(Boolean validado) {
        Validado = validado;
    }
    
    
}
