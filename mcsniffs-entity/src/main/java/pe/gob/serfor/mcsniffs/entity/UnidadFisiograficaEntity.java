package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class UnidadFisiograficaEntity extends AuditoriaEntity implements Serializable {
    private Integer idUnidadFisiografica;
    private String descripcion;

    public Integer getIdUnidadFisiografica() {
        return idUnidadFisiografica;
    }

    public void setIdUnidadFisiografica(Integer idUnidadFisiografica) {
        this.idUnidadFisiografica = idUnidadFisiografica;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
