package pe.gob.serfor.mcsniffs.entity.Dto.VolumenComercialPromedio;

import lombok.Data;

import java.util.List;

@Data
public class VolumenComercialPromedioCabeceraDto {
    String bloquequinquenal;
    Double toTalAreaHa;
    Integer bloque;
    List<VolumenComercialPromedioDetalleDto> detalle;
    List<VolumenComercialPromedioEspecieDto> listEspecie;
}
