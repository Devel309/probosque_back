package pe.gob.serfor.mcsniffs.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class SolicitudConcesionActividadEntity extends AuditoriaEntity implements Serializable{

    private Integer idSolicitudConcesionActividad;
    private Integer idSolicitudConcesion;
    private String codigoActividad;
    private String nombre;
    private String descripcion;
    private String descripcionRecurso;
    private Double presupuesto;
    private Integer periodo;
    private Boolean complementario;
}
