package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class CapacitacionPGMFAbreviadoEntity extends AuditoriaEntity implements Serializable {

    private Number idPGMF;
    private Number id;
    private String tipo;
    private String subTipo;
    private String nombreSubTipo;
    private String descripcion;
    private String personalCapacitar;
    private String modalidadCapacitar;
    private String lugar;

    public Number getIdPGMF() {
        return idPGMF;
    }

    public void setIdPGMF(Number idPGMF) {
        this.idPGMF = idPGMF;
    }

    public Number getId() {
        return id;
    }

    public void setId(Number id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSubTipo() {
        return subTipo;
    }

    public void setSubTipo(String subTipo) {
        this.subTipo = subTipo;
    }

    public String getNombreSubTipo() {
        return nombreSubTipo;
    }

    public void setNombreSubTipo(String nombreSubTipo) {
        this.nombreSubTipo = nombreSubTipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPersonalCapacitar() {
        return personalCapacitar;
    }

    public void setPersonalCapacitar(String personalCapacitar) {
        this.personalCapacitar = personalCapacitar;
    }

    public String getModalidadCapacitar() {
        return modalidadCapacitar;
    }

    public void setModalidadCapacitar(String modalidadCapacitar) {
        this.modalidadCapacitar = modalidadCapacitar;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }
}
