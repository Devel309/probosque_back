package pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Getter
@Setter
public class SolicitudConcesionEvaluacionDetalleDto extends AuditoriaEntity implements Serializable {

    private Integer idSolicitudConcesionEvaluacionDetalle;
    private Integer idSolicitudConcesionEvaluacion;
    private String codigoSeccion;
    private String codigoSubSeccion;
    private Boolean lineamiento;
    private String codigoLineamiento;
    private Boolean conforme;
    private String observacion;
    private String codigoMotivDenegacion;
}
