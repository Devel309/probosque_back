package pe.gob.serfor.mcsniffs.entity.Parametro;

import java.math.BigDecimal;

public class TablaEspeciesMuestreo {
    String especies;
    String numeroArbolesTotalBosque;
    String areaBasalTotalBosque;
    String volumenTotalBosque;
    String numeroArbolesTotalHa;
    String areaBasalTotalHa;
    String volumenTotalHa;
    String numeroArbolesDap10a14;
    String numeroArbolesDap15a19;
    String areaBasalDap10a14;
    String areaBasalDap15a19;

    public String getEspecies() {
        return especies;
    }

    public void setEspecies(String especies) {
        this.especies = especies;
    }

    public String getNumeroArbolesTotalBosque() {
        return numeroArbolesTotalBosque;
    }

    public void setNumeroArbolesTotalBosque(String numeroArbolesTotalBosque) {
        this.numeroArbolesTotalBosque = numeroArbolesTotalBosque;
    }

    public String getAreaBasalTotalBosque() {
        return areaBasalTotalBosque;
    }

    public void setAreaBasalTotalBosque(String areaBasalTotalBosque) {
        this.areaBasalTotalBosque = areaBasalTotalBosque;
    }

    public String getVolumenTotalBosque() {
        return volumenTotalBosque;
    }

    public void setVolumenTotalBosque(String volumenTotalBosque) {
        this.volumenTotalBosque = volumenTotalBosque;
    }

    public String getNumeroArbolesTotalHa() {
        return numeroArbolesTotalHa;
    }

    public void setNumeroArbolesTotalHa(String numeroArbolesTotalHa) {
        this.numeroArbolesTotalHa = numeroArbolesTotalHa;
    }

    public String getAreaBasalTotalHa() {
        return areaBasalTotalHa;
    }

    public void setAreaBasalTotalHa(String areaBasalTotalHa) {
        this.areaBasalTotalHa = areaBasalTotalHa;
    }

    public String getVolumenTotalHa() {
        return volumenTotalHa;
    }

    public void setVolumenTotalHa(String volumenTotalHa) {
        this.volumenTotalHa = volumenTotalHa;
    }

    public String getNumeroArbolesDap10a14() { return numeroArbolesDap10a14; }

    public void setNumeroArbolesDap10a14(String numeroArbolesDap10a14) { this.numeroArbolesDap10a14 = numeroArbolesDap10a14; }

    public String getNumeroArbolesDap15a19() { return numeroArbolesDap15a19; }

    public void setNumeroArbolesDap15a19(String numeroArbolesDap15a19) { this.numeroArbolesDap15a19 = numeroArbolesDap15a19; }

    public String getAreaBasalDap10a14() { return areaBasalDap10a14; }

    public void setAreaBasalDap10a14(String areaBasalDap10a14) { this.areaBasalDap10a14 = areaBasalDap10a14; }

    public String getAreaBasalDap15a19() { return areaBasalDap15a19; }

    public void setAreaBasalDap15a19(String areaBasalDap15a19) { this.areaBasalDap15a19 = areaBasalDap15a19; }
}
