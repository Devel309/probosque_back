package pe.gob.serfor.mcsniffs.entity.Parametro;

import java.util.List;

public class ResultadosEspeciesMuestreoDto {
    String idTipoBosque;
    String nombreBosque;
    List<TablaEspeciesMuestreo>  listTablaEspeciesMuestreo;
    public String getIdTipoBosque() {
        return idTipoBosque;
    }

    public void setIdTipoBosque(String idTipoBosque) {
        this.idTipoBosque = idTipoBosque;
    }

    public String getNombreBosque() {
        return nombreBosque;
    }

    public void setNombreBosque(String nombreBosque) {
        this.nombreBosque = nombreBosque;
    }

    public List<TablaEspeciesMuestreo> getListTablaEspeciesMuestreo() {
        return listTablaEspeciesMuestreo;
    }

    public void setListTablaEspeciesMuestreo(List<TablaEspeciesMuestreo> listTablaEspeciesMuestreo) {
        this.listTablaEspeciesMuestreo = listTablaEspeciesMuestreo;
    }
}
