package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class PlanManejoEvaluacionEntity extends AuditoriaEntity implements Serializable {
   
    private Integer idPlanManejoEval;
    private Integer idPlanManejo;
    private Boolean conforme;
    private String codigoTipo;
    private String observacion;
    private String tituloHabilitante;
    private String estadoValidacion;
    private String codigoTramite;
    private Date fechaRecepcion;
    private String idTipoParametro;
    private String descripcion;
    private Integer idArchivo;

    private List<PlanManejoEvaluacionDetalleEntity> listaDetalle;



    public List<PlanManejoEvaluacionDetalleEntity> getListaDetalle() {
        return listaDetalle;
    }

    public void setListaDetalle(List<PlanManejoEvaluacionDetalleEntity> listaDetalle) {
        this.listaDetalle = listaDetalle;
    }

    public Integer getIdPlanManejoEval() {
        return idPlanManejoEval;
    }

    public void setIdPlanManejoEval(Integer idPlanManejoEval) {
        this.idPlanManejoEval = idPlanManejoEval;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public Boolean getConforme() {
        return conforme;
    }

    public void setConforme(Boolean conforme) {
        this.conforme = conforme;
    }

    public String getCodigoTipo() {
        return codigoTipo;
    }

    public void setCodigoTipo(String codigoTipo) {
        this.codigoTipo = codigoTipo;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getTituloHabilitante() {
        return tituloHabilitante;
    }

    public void setTituloHabilitante(String tituloHabilitante) {
        this.tituloHabilitante = tituloHabilitante;
    }

    public String getEstadoValidacion() {
        return estadoValidacion;
    }

    public void setEstadoValidacion(String estadoValidacion) {
        this.estadoValidacion = estadoValidacion;
    }

    public String getCodigoTramite() {
        return codigoTramite;
    }

    public void setCodigoTramite(String codigoTramite) {
        this.codigoTramite = codigoTramite;
    }

    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }

    public void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    public String getIdTipoParametro() {
        return idTipoParametro;
    }

    public void setIdTipoParametro(String idTipoParametro) {
        this.idTipoParametro = idTipoParametro;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }
 
    
}