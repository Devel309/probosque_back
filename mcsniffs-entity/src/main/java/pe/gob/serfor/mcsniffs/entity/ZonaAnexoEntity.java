package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ZonaAnexoEntity extends AuditoriaEntity implements Serializable {
    private int idZonaAnexo;
    private int idZona;
    private String nombre;
    private String valor;
    public int getIdZonaAnexo() {
        return idZonaAnexo;
    }
    public void setIdZonaAnexo(int idZonaAnexo) {
        this.idZonaAnexo = idZonaAnexo;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getValor() {
        return valor;
    }
    public void setValor(String valor) {
        this.valor = valor;
    }
	public int getIdZona() {
		return idZona;
	}
	public void setIdZona(int idZona) {
		this.idZona = idZona;
	}
    
}
