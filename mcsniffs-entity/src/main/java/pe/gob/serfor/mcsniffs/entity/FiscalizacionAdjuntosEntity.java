package pe.gob.serfor.mcsniffs.entity;

public class FiscalizacionAdjuntosEntity {
    private FiscalizacionResponseEntity fizcalizacion;
    private byte[] documento;
    private String nombredocumento;
    private Integer IdTipoDocumento;
    private String DescripcionDoc;

    
    
    public String getDescripcionDoc() {
        return DescripcionDoc;
    }
    public void setDescripcionDoc(String descripcionDoc) {
        DescripcionDoc = descripcionDoc;
    }
    public FiscalizacionResponseEntity getFizcalizacion() {
        return fizcalizacion;
    }
    public void setFizcalizacion(FiscalizacionResponseEntity fizcalizacion) {
        this.fizcalizacion = fizcalizacion;
    }
    public byte[] getDocumento() {
        return documento;
    }
    public void setDocumento(byte[] documento) {
        this.documento = documento;
    }
    public String getNombredocumento() {
        return nombredocumento;
    }
    public void setNombredocumento(String nombredocumento) {
        this.nombredocumento = nombredocumento;
    }
    public Integer getIdTipoDocumento() {
        return IdTipoDocumento;
    }
    public void setIdTipoDocumento(Integer idTipoDocumento) {
        IdTipoDocumento = idTipoDocumento;
    }
    
}
