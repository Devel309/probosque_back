package pe.gob.serfor.mcsniffs.entity.Parametro;

public class CensoForestalDto {
    private Integer idCensoForestal;
    private Integer idPlanManejo;

    public Integer getIdCensoForestal() {
        return idCensoForestal;
    }

    public void setIdCensoForestal(Integer idCensoForestal) {
        this.idCensoForestal = idCensoForestal;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }
}
