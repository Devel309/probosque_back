package pe.gob.serfor.mcsniffs.entity;
import java.io.Serializable;

public class ActividadSilviculturalDetalleEntity extends AuditoriaEntity implements Serializable {
    private Integer idActividadSilviculturalDet;
    private String idTipo;
    private String idTipoTratamiento;
    private String tipoTratamiento;
    private boolean accion;
    private String descripcionDetalle;
    private String tratamiento;
    private String justificacion;

    private String actividad;
    private String equipo;
    private String insumo;
    private String personal;
    private String observacionDetalle;
    private String detalle;

    private Integer idActSilvicultural;
    private boolean accionSilvicultural;

    public boolean isAccionSilvicultural() {
        return accionSilvicultural;
    }

    public void setAccionSilvicultural(boolean accionSilvicultural) {
        this.accionSilvicultural = accionSilvicultural;
    }

    public String getDescripcionDetalle() {
        return descripcionDetalle;
    }

    public ActividadSilviculturalDetalleEntity setDescripcionDetalle(String descripcionDetalle) {
        this.descripcionDetalle = descripcionDetalle;
        return this;
    }

    public Integer getIdActividadSilviculturalDet() {
        return idActividadSilviculturalDet;
    }

    public ActividadSilviculturalDetalleEntity setIdActividadSilviculturalDet(Integer idActividadSilviculturalDet) {
        this.idActividadSilviculturalDet = idActividadSilviculturalDet;
        return this;
    }

    public String getIdTipo() {
        return idTipo;
    }

    public ActividadSilviculturalDetalleEntity setIdTipo(String idTipo) {
        this.idTipo = idTipo;
        return this;
    }

    public String getIdTipoTratamiento() {
        return idTipoTratamiento;
    }

    public ActividadSilviculturalDetalleEntity setIdTipoTratamiento(String idTipoTratamiento) {
        this.idTipoTratamiento = idTipoTratamiento;
        return this;
    }

    public String getTipoTratamiento() {
        return tipoTratamiento;
    }

    public ActividadSilviculturalDetalleEntity setTipoTratamiento(String tipoTratamiento) {
        this.tipoTratamiento = tipoTratamiento;
        return this;
    }

    public boolean getAccion() {
        return accion;
    }

    public ActividadSilviculturalDetalleEntity setAccion(boolean accion) {
        this.accion = accion;
        return this;
    }

    public String getTratamiento() {
        return tratamiento;
    }

    public ActividadSilviculturalDetalleEntity setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
        return this;
    }

    public String getJustificacion() {
        return justificacion;
    }

    public ActividadSilviculturalDetalleEntity setJustificacion(String justificacion) {
        this.justificacion = justificacion;
        return this;
    }

    public Integer getIdActSilvicultural() {
        return idActSilvicultural;
    }

    public ActividadSilviculturalDetalleEntity setIdActSilvicultural(Integer idActSilvicultural) {
        this.idActSilvicultural = idActSilvicultural;
        return this;
    }

    public boolean isAccion() {
        return accion;
    }

    public String getActividad() {
        return actividad;
    }

    public ActividadSilviculturalDetalleEntity setActividad(String actividad) {
        this.actividad = actividad;
        return this;
    }

    public String getEquipo() {
        return equipo;
    }

    public ActividadSilviculturalDetalleEntity setEquipo(String equipo) {
        this.equipo = equipo;
        return this;
    }

    public String getInsumo() {
        return insumo;
    }

    public ActividadSilviculturalDetalleEntity setInsumo(String insumo) {
        this.insumo = insumo;
        return this;
    }

    public String getPersonal() {
        return personal;
    }

    public ActividadSilviculturalDetalleEntity setPersonal(String personal) {
        this.personal = personal;
        return this;
    }

    public String getObservacionDetalle() {
        return observacionDetalle;
    }

    public ActividadSilviculturalDetalleEntity setObservacionDetalle(String observacionDetalle) {
        this.observacionDetalle = observacionDetalle;
        return this;
    }

    public String getDetalle() {
        return this.detalle;
    }

    public ActividadSilviculturalDetalleEntity setDetalle(String detalle) {
        this.detalle = detalle;
        return this;
    }
}
