package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class ZonificacionEntity extends AuditoriaEntity implements Serializable {

    private Integer idZonificacion;
    private Integer idPlanManejo;
    private String codZonificacion;
    private Integer idZonificacionPadre;
    private String zona;
    private Double anexo1;
    private Double anexo2;
    private Double anexo3;

    private BigDecimal sector1;
    private BigDecimal sector2;
    private BigDecimal sector3;


    private Double total;
    private Double porcentaje;

    public ZonificacionEntity() {

    }

    public BigDecimal getSector1() {
        return sector1;
    }

    public void setSector1(BigDecimal sector1) {
        this.sector1 = sector1;
    }

    public BigDecimal getSector2() {
        return sector2;
    }

    public void setSector2(BigDecimal sector2) {
        this.sector2 = sector2;
    }

    public BigDecimal getSector3() {
        return sector3;
    }

    public void setSector3(BigDecimal sector3) {
        this.sector3 = sector3;
    }

    public Integer getIdZonificacionPadre() {
        return idZonificacionPadre;
    }

    public void setIdZonificacionPadre(Integer idZonificacionPadre) {
        this.idZonificacionPadre = idZonificacionPadre;
    }

    public Integer getIdZonificacion() {
        return idZonificacion;
    }

    public void setIdZonificacion(Integer idZonificacion) {
        this.idZonificacion = idZonificacion;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public String getCodZonificacion() {
        return codZonificacion;
    }

    public void setCodZonificacion(String codZonificacion) {
        this.codZonificacion = codZonificacion;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public Double getAnexo1() {
        return anexo1;
    }

    public void setAnexo1(Double anexo1) {
        this.anexo1 = anexo1;
    }

    public Double getAnexo2() {
        return anexo2;
    }

    public void setAnexo2(Double anexo2) {
        this.anexo2 = anexo2;
    }

    public Double getAnexo3() {
        return anexo3;
    }

    public void setAnexo3(Double anexo3) {
        this.anexo3 = anexo3;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(Double porcentaje) {
        this.porcentaje = porcentaje;
    }
}
