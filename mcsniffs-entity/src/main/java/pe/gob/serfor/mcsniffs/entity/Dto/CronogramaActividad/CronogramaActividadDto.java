package pe.gob.serfor.mcsniffs.entity.Dto.CronogramaActividad;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.CronogramaActividadDetalleEntity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
@Data
public class CronogramaActividadDto extends AuditoriaEntity implements Serializable {

    private Integer idCronogramaActividad;
    private Integer idPlanManejo;
    private String prefijo;
    private String codigoProceso;
    private String actividad;
    private String codigoActividad;
    private String grupo;
    private Integer accion;
    private Integer cantidadAnio;
    private List<CronogramaActividadDetalleEntity> detalle;
}
