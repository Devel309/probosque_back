package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ImpactoAmbientalEntity extends AuditoriaEntity implements Serializable {
    private Integer idImpactoambiental;
    private Integer idPlanManejo;
    private String codigoImpactoAmbiental;
    private String actividad;
    private String descripcion;

    public ImpactoAmbientalEntity(){

    }

    public Integer getIdImpactoambiental() {
        return idImpactoambiental;
    }

    public void setIdImpactoambiental(Integer idImpactoambiental) {
        this.idImpactoambiental = idImpactoambiental;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigoImpactoAmbiental() {
        return codigoImpactoAmbiental;
    }

    public void setCodigoImpactoAmbiental(String codigoImpactoAmbiental) {
        this.codigoImpactoAmbiental = codigoImpactoAmbiental;
    }
}
