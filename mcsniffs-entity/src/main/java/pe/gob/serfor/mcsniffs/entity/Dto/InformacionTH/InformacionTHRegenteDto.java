package pe.gob.serfor.mcsniffs.entity.Dto.InformacionTH;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.util.Date;

@Data
public class InformacionTHRegenteDto extends AuditoriaEntity implements Serializable {
    private String codigoTH;
    private Integer idPlanManejoRegente;
    private Integer idPlanManejo ;
    private String codigoProceso;
    private String codigoTipoRegente;
    private String nombres;
    private Integer idContrato;
    private Integer idPlanManejoContrato;
    
}
