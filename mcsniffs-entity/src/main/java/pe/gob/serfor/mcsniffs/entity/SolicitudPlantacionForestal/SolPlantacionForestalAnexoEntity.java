package pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pe.gob.serfor.mcsniffs.entity.ArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;



@Data
@AllArgsConstructor
@NoArgsConstructor
public class SolPlantacionForestalAnexoEntity extends ArchivoEntity {
Integer id_archivo_solplantacionforest;
Integer id_solplantforest ;
//Integer id_archivo ;
Integer id_tipodetalleseccion;
Integer id_tipodetalleanexo ;
String descripcion;

String des_tipodetalleanexo;
String usuarioCreacion;
public Integer getId_archivo_solplantacionforest() {
    return id_archivo_solplantacionforest;
}
public void setId_archivo_solplantacionforest(Integer id_archivo_solplantacionforest) {
    this.id_archivo_solplantacionforest = id_archivo_solplantacionforest;
}
public Integer getId_solplantforest() {
    return id_solplantforest;
}
public void setId_solplantforest(Integer id_solplantforest) {
    this.id_solplantforest = id_solplantforest;
}
public Integer getId_tipodetalleseccion() {
    return id_tipodetalleseccion;
}
public void setId_tipodetalleseccion(Integer id_tipodetalleseccion) {
    this.id_tipodetalleseccion = id_tipodetalleseccion;
}
public Integer getId_tipodetalleanexo() {
    return id_tipodetalleanexo;
}
public void setId_tipodetalleanexo(Integer id_tipodetalleanexo) {
    this.id_tipodetalleanexo = id_tipodetalleanexo;
}
public String getDescripcion() {
    return descripcion;
}
public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
}
public String getDes_tipodetalleanexo() {
    return des_tipodetalleanexo;
}
public void setDes_tipodetalleanexo(String des_tipodetalleanexo) {
    this.des_tipodetalleanexo = des_tipodetalleanexo;
}
public String getUsuarioCreacion() {
    return usuarioCreacion;
}
public void setUsuarioCreacion(String usuarioCreacion) {
    this.usuarioCreacion = usuarioCreacion;
}

}
