package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class DocumentoAdjuntoEntity extends AuditoriaEntity implements Serializable {
    private Integer IdDocumentoAdjunto;
    private Integer IdProcesoPostulacion;
    private Integer IdUsuarioAdjunta;
    private String Ruta;
    private String Descripcion;
    private Integer IdTipoDocumento;
    private String TipoDocumento;
    private String NombreDocumento;
    private byte[] file;
    private Integer IdPostulacionPFDM;
    private String NombreGenerado;
    private Boolean conforme;
    private String observacion;    
    private String codigoTipoDocumento;
    private String asunto;
    private String descripcionCodigoTipoDocumento;
}
