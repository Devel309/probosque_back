package pe.gob.serfor.mcsniffs.entity.Parametro;

public class OrdenamientoPoligonoDto {
    String idOrdenamientoDet;
    String areaHa;
    String bloqueQuinquenal;
    String parcelaCorta;
    String idPlanManejo;
    String idArchivo;
    public String getIdOrdenamientoDet() {
        return idOrdenamientoDet;
    }

    public void setIdOrdenamientoDet(String idOrdenamientoDet) {
        this.idOrdenamientoDet = idOrdenamientoDet;
    }

    public String getAreaHa() {
        return areaHa;
    }

    public void setAreaHa(String areaHa) {
        this.areaHa = areaHa;
    }

    public String getBloqueQuinquenal() {
        return bloqueQuinquenal;
    }

    public void setBloqueQuinquenal(String bloqueQuinquenal) {
        this.bloqueQuinquenal = bloqueQuinquenal;
    }

    public String getParcelaCorta() {
        return parcelaCorta;
    }

    public void setParcelaCorta(String parcelaCorta) {
        this.parcelaCorta = parcelaCorta;
    }

    public String getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(String idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public String getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(String idArchivo) {
        this.idArchivo = idArchivo;
    }
}
