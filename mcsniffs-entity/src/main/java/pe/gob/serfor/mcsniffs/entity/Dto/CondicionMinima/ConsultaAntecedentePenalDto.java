package pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima;

import lombok.Data;

 
public class ConsultaAntecedentePenalDto {
    
    private String xApellidoMaterno;
    private String xApellidoPaterno;
    private String xAudDireccionMAC;
    private String xAudIP;
    private String xAudNombrePC;
    private String xAudNombreUsuario;
    private String xDni;
    private String xDniPersonaConsultante;
    private String xIpPublica;
    private String xMotivoConsulta;
    private String xNombre1;
    private String xNombre2;
    private String xNombre3;
    private String xProcesoEntidadConsultante;
    private String xRucEntidadConsultante;
    
    public String getxApellidoMaterno() {
        return xApellidoMaterno;
    }
    public void setxApellidoMaterno(String xApellidoMaterno) {
        this.xApellidoMaterno = xApellidoMaterno;
    }
    public String getxApellidoPaterno() {
        return xApellidoPaterno;
    }
    public void setxApellidoPaterno(String xApellidoPaterno) {
        this.xApellidoPaterno = xApellidoPaterno;
    }
    public String getxAudDireccionMAC() {
        return xAudDireccionMAC;
    }
    public void setxAudDireccionMAC(String xAudDireccionMAC) {
        this.xAudDireccionMAC = xAudDireccionMAC;
    }
    public String getxAudIP() {
        return xAudIP;
    }
    public void setxAudIP(String xAudIP) {
        this.xAudIP = xAudIP;
    }
    public String getxAudNombrePC() {
        return xAudNombrePC;
    }
    public void setxAudNombrePC(String xAudNombrePC) {
        this.xAudNombrePC = xAudNombrePC;
    }
    public String getxAudNombreUsuario() {
        return xAudNombreUsuario;
    }
    public void setxAudNombreUsuario(String xAudNombreUsuario) {
        this.xAudNombreUsuario = xAudNombreUsuario;
    }
    public String getxDni() {
        return xDni;
    }
    public void setxDni(String xDni) {
        this.xDni = xDni;
    }
    public String getxDniPersonaConsultante() {
        return xDniPersonaConsultante;
    }
    public void setxDniPersonaConsultante(String xDniPersonaConsultante) {
        this.xDniPersonaConsultante = xDniPersonaConsultante;
    }
    public String getxIpPublica() {
        return xIpPublica;
    }
    public void setxIpPublica(String xIpPublica) {
        this.xIpPublica = xIpPublica;
    }
    public String getxMotivoConsulta() {
        return xMotivoConsulta;
    }
    public void setxMotivoConsulta(String xMotivoConsulta) {
        this.xMotivoConsulta = xMotivoConsulta;
    }
    public String getxNombre1() {
        return xNombre1;
    }
    public void setxNombre1(String xNombre1) {
        this.xNombre1 = xNombre1;
    }
    public String getxNombre2() {
        return xNombre2;
    }
    public void setxNombre2(String xNombre2) {
        this.xNombre2 = xNombre2;
    }
    public String getxNombre3() {
        return xNombre3;
    }
    public void setxNombre3(String xNombre3) {
        this.xNombre3 = xNombre3;
    }
    public String getxProcesoEntidadConsultante() {
        return xProcesoEntidadConsultante;
    }
    public void setxProcesoEntidadConsultante(String xProcesoEntidadConsultante) {
        this.xProcesoEntidadConsultante = xProcesoEntidadConsultante;
    }
    public String getxRucEntidadConsultante() {
        return xRucEntidadConsultante;
    }
    public void setxRucEntidadConsultante(String xRucEntidadConsultante) {
        this.xRucEntidadConsultante = xRucEntidadConsultante;
    }
    

        
}
