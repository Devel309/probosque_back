package pe.gob.serfor.mcsniffs.entity.Parametro;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.RentabilidadManejoForestalDetalleEntity;

import java.io.Serializable;
import java.util.List;

public class RentabilidadManejoForestalDto  extends AuditoriaEntity implements Serializable {
    private Integer idRentManejoForestal;
    private Integer idTipoRubro;
    private String rubro;
    private String descripcion;
    private Integer idPlanManejo;
    private String observacion;
    private String codigoRentabilidad;
    private List<RentabilidadManejoForestalDetalleEntity> listRentabilidadManejoForestalDetalle ;

    
    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getCodigoRentabilidad() {
        return codigoRentabilidad;
    }

    public void setCodigoRentabilidad(String codigoRentabilidad) {
        this.codigoRentabilidad = codigoRentabilidad;
    }

    public String getRubro() {
        return rubro;
    }

    public void setRubro(String rubro) {
        this.rubro = rubro;
    }

    public Integer getIdRentManejoForestal() {
        return idRentManejoForestal;
    }

    public void setIdRentManejoForestal(Integer idRentManejoForestal) {
        this.idRentManejoForestal = idRentManejoForestal;
    }

    public Integer getIdTipoRubro() {
        return idTipoRubro;
    }

    public void setIdTipoRubro(Integer idTipoRubro) {
        this.idTipoRubro = idTipoRubro;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public List<RentabilidadManejoForestalDetalleEntity> getListRentabilidadManejoForestalDetalle() {
        return listRentabilidadManejoForestalDetalle;
    }

    public void setListRentabilidadManejoForestalDetalle(List<RentabilidadManejoForestalDetalleEntity> listRentabilidadManejoForestalDetalle) {
        this.listRentabilidadManejoForestalDetalle = listRentabilidadManejoForestalDetalle;
    }
}
