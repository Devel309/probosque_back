package pe.gob.serfor.mcsniffs.entity.OrdenamientoProteccionUMF;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
public class OrdenProyecionVigilanciaUMFEntity  extends AuditoriaEntity {

    
Integer id_ordenprotec_vigilancia_umf ;
Integer id_plan_manejo;

Integer tipobloque_seccion;

Integer numerolugar ;
String lugar ;

Integer numerovertice ;
String vertice ;

String coord_este ;
String coord_oste ;
String coord_zonautm ;
String material ;
String observacion ;
public Integer getId_ordenprotec_vigilancia_umf() {
    return id_ordenprotec_vigilancia_umf;
}
public void setId_ordenprotec_vigilancia_umf(Integer id_ordenprotec_vigilancia_umf) {
    this.id_ordenprotec_vigilancia_umf = id_ordenprotec_vigilancia_umf;
}
public Integer getId_plan_manejo() {
    return id_plan_manejo;
}
public void setId_plan_manejo(Integer id_plan_manejo) {
    this.id_plan_manejo = id_plan_manejo;
}
public Integer getTipobloque_seccion() {
    return tipobloque_seccion;
}
public void setTipobloque_seccion(Integer tipobloque_seccion) {
    this.tipobloque_seccion = tipobloque_seccion;
}
public Integer getNumerolugar() {
    return numerolugar;
}
public void setNumerolugar(Integer numerolugar) {
    this.numerolugar = numerolugar;
}
public String getLugar() {
    return lugar;
}
public void setLugar(String lugar) {
    this.lugar = lugar;
}
public Integer getNumerovertice() {
    return numerovertice;
}
public void setNumerovertice(Integer numerovertice) {
    this.numerovertice = numerovertice;
}
public String getVertice() {
    return vertice;
}
public void setVertice(String vertice) {
    this.vertice = vertice;
}
public String getCoord_este() {
    return coord_este;
}
public void setCoord_este(String coord_este) {
    this.coord_este = coord_este;
}
public String getCoord_oste() {
    return coord_oste;
}
public void setCoord_oste(String coord_oste) {
    this.coord_oste = coord_oste;
}
public String getCoord_zonautm() {
    return coord_zonautm;
}
public void setCoord_zonautm(String coord_zonautm) {
    this.coord_zonautm = coord_zonautm;
}
public String getMaterial() {
    return material;
}
public void setMaterial(String material) {
    this.material = material;
}
public String getObservacion() {
    return observacion;
}
public void setObservacion(String observacion) {
    this.observacion = observacion;
}


}
