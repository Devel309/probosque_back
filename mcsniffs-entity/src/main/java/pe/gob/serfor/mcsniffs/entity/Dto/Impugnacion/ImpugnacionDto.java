package pe.gob.serfor.mcsniffs.entity.Dto.Impugnacion;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImpugnacionDto extends AuditoriaEntity implements Serializable {

    Integer idImpugnacion;
    Integer idResolucion;
    String estadoImpugnacion;
    String asunto;
    String descripcion;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    Date fechaImpugnacion;
    Integer idArchivoRecurso;
    Integer idArchivoSustento;
    String tipoImpugnacion;
    Integer idArchivoEvaluacion;
    Boolean esfundado;
    String mensaje;
    Integer nroResolucion;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    Date fechaSolicitud;
    String estadoSolicitud;
    Integer nroDocGestion;
    Integer region;
    String documentoGestion;
    String desEstadoImpugnacion;
    String nombreArchivoRecurso;
    String nombreArchivoSustento;
    String nombreArchivoEvaluacion;
    String nombreArchivoResolucion;
    Integer idArchivoResolucion;
    Integer idArchivoFirme;
    String nombreArchivoFirme;
    String perfil;
    String estadoResolucion;
    Boolean retrotraer;
    String tipoDocGestion;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    Date fechaRegistroFirme;
}