package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.Dto.Oposicion.OposicionArchivoDTO;

@Getter
@Setter
public class OposicionEntity extends AuditoriaEntity implements Serializable {
    private Integer idOposicion;
    private Integer idProcesoPostulacion;
    private String descripcion;
    private Integer idArchivoAdjunto;
    private Date fechaEnvio;
    private String fechaEnvioFormato;
    private Integer idUsuarioOpositor;
    private Boolean fundada;
    private Boolean idOposicionRespuesta;
    private PersonaEntity persona;
    private Integer idArchivo;
    private Boolean resolucion;
    private Boolean respuesta;
    private String descripcionRespuesta;
    private List<OposicionArchivoDTO> listaOposicionArchivo;
    private String correoPostulante;
    private String tipoDocumentoGestion;
    private Integer documentoGestion;
    private String descTipoDocumentoGestion;
    private String nombreDocumentoOposicion;
    private Integer numeroDocumentoOposicion;
    private String nombreDocumentoTH;
    private String tituloHabilitante;
    private Date fechaDocumentoTH;
    private Boolean fueraFecha;
}
