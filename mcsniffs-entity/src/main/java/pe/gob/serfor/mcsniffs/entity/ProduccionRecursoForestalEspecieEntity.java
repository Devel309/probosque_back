package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ProduccionRecursoForestalEspecieEntity  extends AuditoriaEntity implements Serializable {
    private Integer idProdRecursoForestalEsp;
    private String idEspecie;
    private String especie;
    private ProduccionRecursoForestalEntity produccionRecursoForestal;

    public Integer getIdProdRecursoForestalEsp() {
        return idProdRecursoForestalEsp;
    }

    public void setIdProdRecursoForestalEsp(Integer idProdRecursoForestalEsp) {
        this.idProdRecursoForestalEsp = idProdRecursoForestalEsp;
    }

    public String getIdEspecie() {
        return idEspecie;
    }

    public void setIdEspecie(String idEspecie) {
        this.idEspecie = idEspecie;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public ProduccionRecursoForestalEntity getProduccionRecursoForestal() {
        return produccionRecursoForestal;
    }

    public void setProduccionRecursoForestal(ProduccionRecursoForestalEntity produccionRecursoForestal) {
        this.produccionRecursoForestal = produccionRecursoForestal;
    }
}
