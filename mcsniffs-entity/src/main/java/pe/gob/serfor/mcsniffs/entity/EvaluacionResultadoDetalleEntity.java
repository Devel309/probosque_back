package pe.gob.serfor.mcsniffs.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public class EvaluacionResultadoDetalleEntity extends AuditoriaEntity implements Serializable {
    private Integer idEvalResultadoDet;
    private String codResultadoDet;
    private String tipoArchivo;
    private String tipoDocumento;
    private Integer nuDocumento;
    private String nombreFirmante;
    private String paternoFirmante;
    private String maternoFirmante;
    private String cargoFirmante;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Timestamp fechaNotificacion;
    private String asunto;
    private String perfil;
    private String observacion;
    private String detalle;
    private String descripcion;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Timestamp fechaResolucion;
    private Integer nuDocumentoFirmante;

    public Timestamp getFechaResolucion() {
        return fechaResolucion;
    }

    public void setFechaResolucion(Timestamp fechaResolucion) {
        this.fechaResolucion = fechaResolucion;
    }

    public Integer getNuDocumentoFirmante() {
        return nuDocumentoFirmante;
    }

    public void setNuDocumentoFirmante(Integer nuDocumentoFirmante) {
        this.nuDocumentoFirmante = nuDocumentoFirmante;
    }

    public String getTipoArchivo() {
        return tipoArchivo;
    }

    public void setTipoArchivo(String tipoArchivo) {
        this.tipoArchivo = tipoArchivo;
    }

    public Integer getIdEvalResultadoDet() {
        return idEvalResultadoDet;
    }

    public void setIdEvalResultadoDet(Integer idEvalResultadoDet) {
        this.idEvalResultadoDet = idEvalResultadoDet;
    }

    public String getCodResultadoDet() {
        return codResultadoDet;
    }

    public void setCodResultadoDet(String codResultadoDet) {
        this.codResultadoDet = codResultadoDet;
    }


    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Integer getNuDocumento() {
        return nuDocumento;
    }

    public void setNuDocumento(Integer nuDocumento) {
        this.nuDocumento = nuDocumento;
    }

    public String getNombreFirmante() {
        return nombreFirmante;
    }

    public void setNombreFirmante(String nombreFirmante) {
        this.nombreFirmante = nombreFirmante;
    }

    public String getPaternoFirmante() {
        return paternoFirmante;
    }

    public void setPaternoFirmante(String paternoFirmante) {
        this.paternoFirmante = paternoFirmante;
    }

    public String getMaternoFirmante() {
        return maternoFirmante;
    }

    public void setMaternoFirmante(String maternoFirmante) {
        this.maternoFirmante = maternoFirmante;
    }

    public String getCargoFirmante() {
        return cargoFirmante;
    }

    public void setCargoFirmante(String cargoFirmante) {
        this.cargoFirmante = cargoFirmante;
    }

    public Timestamp getFechaNotificacion() {
        return fechaNotificacion;
    }

    public void setFechaNotificacion(Timestamp fechaNotificacion) {
        this.fechaNotificacion = fechaNotificacion;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
