package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.Date;

public class InformacionGeneralEntity extends AuditoriaEntity implements Serializable {
    private Integer idInfGeneral;
    private PlanManejoEntity planManejo;
    private PersonaEntity persona;
    private Date fechaPresentacion;
    private Integer duracion;
    private Date fechaInicio;
    private Date fechaFin;
    private Double potencial;
    private String nombreArchivo;


    /* 3.1.2 */
    private String titular;
    private String representante;
    private Integer nroContratoConcesion;
    private String docIdentidad;
    private String ruc;
    private String distrito;
    private String departamento;
    private String provincia;
    private Integer objGeneral;
    private Integer nroPo;
    private Double areaProteccion;
    private Double areaBosque;
    private Double areaTotal;
    private Double volumenTotal;
    private Double volumen;
    private Integer cantidad;
    private Double peso;
    private String nombreRegente;
    private String domicilioLegal;
    private Integer nroRegistroCIP;
    private Integer contrato;
    private Integer nroInscripcion_Regente;
    /* Fin  3.1.2 */

    public Integer getIdInfGeneral() {
        return idInfGeneral;
    }

    public void setIdInfGeneral(Integer idInfGeneral) {
        this.idInfGeneral = idInfGeneral;
    }

    public PlanManejoEntity getPlanManejo() {
        return planManejo;
    }

    public void setPlanManejo(PlanManejoEntity planManejo) {
        this.planManejo = planManejo;
    }

    public PersonaEntity getPersona() {
        return persona;
    }

    public void setPersona(PersonaEntity persona) {
        this.persona = persona;
    }

    public Date getFechaPresentacion() {
        return fechaPresentacion;
    }

    public void setFechaPresentacion(Date fechaPresentacion) {
        this.fechaPresentacion = fechaPresentacion;
    }

    public Integer getDuracion() {
        return duracion;
    }

    public void setDuracion(Integer duracion) {
        this.duracion = duracion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Double getPotencial() {
        return potencial;
    }

    public void setPotencial(Double potencial) {
        this.potencial = potencial;
    }
    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }
    /* 3.1.2 */

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public String getRepresentante() {
        return representante;
    }

    public void setRepresentante(String representante) {
        this.representante = representante;
    }

    public Integer getNroContratoConcesion() {
        return nroContratoConcesion;
    }

    public void setNroContratoConcesion(Integer nroContratoConcesion) {
        this.nroContratoConcesion = nroContratoConcesion;
    }

    public String getDocIdentidad() {
        return docIdentidad;
    }

    public void setDocIdentidad(String docIdentidad) {
        this.docIdentidad = docIdentidad;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public Integer getObjGeneral() {
        return objGeneral;
    }

    public void setObjGeneral(Integer objGeneral) {
        this.objGeneral = objGeneral;
    }

    public Integer getNroPo() {
        return nroPo;
    }

    public void setNroPo(Integer nroPo) {
        this.nroPo = nroPo;
    }

    public Double getAreaProteccion() {
        return areaProteccion;
    }

    public void setAreaProteccion(Double areaProteccion) {
        this.areaProteccion = areaProteccion;
    }

    public Double getAreaBosque() {
        return areaBosque;
    }

    public void setAreaBosque(Double areaBosque) {
        this.areaBosque = areaBosque;
    }

    public Double getAreaTotal() {
        return areaTotal;
    }

    public void setAreaTotal(Double areaTotal) {
        this.areaTotal = areaTotal;
    }

    public Double getVolumenTotal() {
        return volumenTotal;
    }

    public void setVolumenTotal(Double volumenTotal) {
        this.volumenTotal = volumenTotal;
    }

    public Double getVolumen() {
        return volumen;
    }

    public void setVolumen(Double volumen) {
        this.volumen = volumen;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Double getPeso() {
        return peso;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }

    public String getNombreRegente() {
        return nombreRegente;
    }

    public void setNombreRegente(String nombreRegente) {
        this.nombreRegente = nombreRegente;
    }

    public String getDomicilioLegal() {
        return domicilioLegal;
    }

    public void setDomicilioLegal(String domicilioLegal) {
        this.domicilioLegal = domicilioLegal;
    }

    public Integer getNroRegistroCIP() {
        return nroRegistroCIP;
    }

    public void setNroRegistroCIP(Integer nroRegistroCIP) {
        this.nroRegistroCIP = nroRegistroCIP;
    }

    public Integer getContrato() {
        return contrato;
    }

    public void setContrato(Integer contrato) {
        this.contrato = contrato;
    }

    public Integer getNroInscripcion_Regente() {
        return nroInscripcion_Regente;
    }

    public void setNroInscripcion_Regente(Integer nroInscripcion_Regente) {
        this.nroInscripcion_Regente = nroInscripcion_Regente;
    }
}

