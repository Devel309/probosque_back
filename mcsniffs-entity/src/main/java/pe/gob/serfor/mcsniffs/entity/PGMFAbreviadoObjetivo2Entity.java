package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;


public class PGMFAbreviadoObjetivo2Entity extends AuditoriaEntity implements Serializable {
    private Integer idPgmf;
    private String objetivoGeneral;
    private Boolean objetivoEspecificoA;
    private Boolean objetivoEspecificoB;
    private Boolean objetivoEspecificoC;
    private Boolean objetivoEspecificoD;
    private Boolean objetivoEspecificoE;
    private Boolean objetivoEspecificoF;
    private Boolean objetivoEspecificoG;
    private Boolean objetivoEspecificoH;
    private Boolean objetivoEspecificoI;
    private String objetivoEspecificoJ;
    
    
    public  PGMFAbreviadoObjetivo2Entity() {}
    public Integer getIdPgmf() {
        return idPgmf;
    }
    public void setIdPgmf(Integer idPgmf) {
        this.idPgmf = idPgmf;
    }
    public String getObjetivoGeneral() {
        return objetivoGeneral;
    }
    public void setObjetivoGeneral(String objetivoGeneral) {
        this.objetivoGeneral = objetivoGeneral;
    }
    public Boolean getObjetivoEspecificoA() {
        return objetivoEspecificoA;
    }
    public void setObjetivoEspecificoA(Boolean objetivoEspecificoA) {
        this.objetivoEspecificoA = objetivoEspecificoA;
    }
    public Boolean getObjetivoEspecificoB() {
        return objetivoEspecificoB;
    }
    public void setObjetivoEspecificoB(Boolean objetivoEspecificoB) {
        this.objetivoEspecificoB = objetivoEspecificoB;
    }
    public Boolean getObjetivoEspecificoC() {
        return objetivoEspecificoC;
    }
    public void setObjetivoEspecificoC(Boolean objetivoEspecificoC) {
        this.objetivoEspecificoC = objetivoEspecificoC;
    }
    public Boolean getObjetivoEspecificoD() {
        return objetivoEspecificoD;
    }
    public void setObjetivoEspecificoD(Boolean objetivoEspecificoD) {
        this.objetivoEspecificoD = objetivoEspecificoD;
    }
    public Boolean getObjetivoEspecificoE() {
        return objetivoEspecificoE;
    }
    public void setObjetivoEspecificoE(Boolean objetivoEspecificoE) {
        this.objetivoEspecificoE = objetivoEspecificoE;
    }
    public Boolean getObjetivoEspecificoF() {
        return objetivoEspecificoF;
    }
    public void setObjetivoEspecificoF(Boolean objetivoEspecificoF) {
        this.objetivoEspecificoF = objetivoEspecificoF;
    }
    public Boolean getObjetivoEspecificoG() {
        return objetivoEspecificoG;
    }
    public void setObjetivoEspecificoG(Boolean objetivoEspecificoG) {
        this.objetivoEspecificoG = objetivoEspecificoG;
    }
    public Boolean getObjetivoEspecificoH() {
        return objetivoEspecificoH;
    }
    public void setObjetivoEspecificoH(Boolean objetivoEspecificoH) {
        this.objetivoEspecificoH = objetivoEspecificoH;
    }
    public Boolean getObjetivoEspecificoI() {
        return objetivoEspecificoI;
    }
    public void setObjetivoEspecificoI(Boolean objetivoEspecificoI) {
        this.objetivoEspecificoI = objetivoEspecificoI;
    }
    public String getObjetivoEspecificoJ() {
        return objetivoEspecificoJ;
    }
    public void setObjetivoEspecificoJ(String objetivoEspecificoJ) {
        this.objetivoEspecificoJ = objetivoEspecificoJ;
    }
    
    
}