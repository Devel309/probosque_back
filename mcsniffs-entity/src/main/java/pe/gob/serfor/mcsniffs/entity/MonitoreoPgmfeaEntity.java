package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class MonitoreoPgmfeaEntity extends AuditoriaEntity implements Serializable {
    private Integer idMonitoreoPgmfea;
    private MonitoreoEntity monitoreo;
    private String actividad;
    private String descripcion;
    private String responsable;

    public Integer getIdMonitoreoPgmfea() {
        return idMonitoreoPgmfea;
    }

    public void setIdMonitoreoPgmfea(Integer idMonitoreoPgmfea) {
        this.idMonitoreoPgmfea = idMonitoreoPgmfea;
    }

    public MonitoreoEntity getMonitoreo() {
        return monitoreo;
    }

    public void setMonitoreo(MonitoreoEntity monitoreo) {
        this.monitoreo = monitoreo;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }
}
