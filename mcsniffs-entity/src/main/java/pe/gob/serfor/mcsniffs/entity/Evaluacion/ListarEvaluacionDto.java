package pe.gob.serfor.mcsniffs.entity.Evaluacion;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Getter
@Setter
public class ListarEvaluacionDto extends AuditoriaEntity implements Serializable{
   
    private Integer idPlanManejo;
    private String codigoEvaluacion;   
    private String codigoEvaluacionDet;
    private String codigoEvaluacionDetSub;
    private String codigoEvaluacionDetPost;
    

}
