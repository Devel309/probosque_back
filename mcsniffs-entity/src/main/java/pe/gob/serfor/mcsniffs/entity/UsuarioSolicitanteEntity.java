package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class UsuarioSolicitanteEntity implements Serializable {
    private String Solicitante;
    private Integer idUsuario;
    private String SuperficieSolicitada;
    private String SectorAnexoCaserio;
    private Integer IdDistrito;
    private Integer IdProvincia;
    private Integer IdDepartamento;
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private Integer tipoDocumento;
    private String numeroDocumento;
    private String correoElectronico;

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getSolicitante() {
        return Solicitante;
    }

    public void setSolicitante(String solicitante) {
        Solicitante = solicitante;
    }

    public String getSuperficieSolicitada() {
        return SuperficieSolicitada;
    }

    public void setSuperficieSolicitada(String superficieSolicitada) {
        SuperficieSolicitada = superficieSolicitada;
    }

    public String getSectorAnexoCaserio() {
        return SectorAnexoCaserio;
    }

    public void setSectorAnexoCaserio(String sectorAnexoCaserio) {
        SectorAnexoCaserio = sectorAnexoCaserio;
    }

    public Integer getIdDistrito() {
        return IdDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        IdDistrito = idDistrito;
    }

    public Integer getIdProvincia() {
        return IdProvincia;
    }

    public void setIdProvincia(Integer idProvincia) {
        IdProvincia = idProvincia;
    }

    public Integer getIdDepartamento() {
        return IdDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        IdDepartamento = idDepartamento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }
 
    public Integer getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(Integer tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }
}
