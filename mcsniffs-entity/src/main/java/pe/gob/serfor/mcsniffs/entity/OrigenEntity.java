package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.Date;

public class OrigenEntity extends AuditoriaEntity implements Serializable {
    private Short IdOrigen;
    private String NombreOrigen;

    public Short getIdOrigen() {
        return IdOrigen;
    }

    public void setIdOrigen(Short idOrigen) {
        IdOrigen = idOrigen;
    }

    public String getNombreOrigen() {
        return NombreOrigen;
    }

    public void setNombreOrigen(String nombreOrigen) {
        NombreOrigen = nombreOrigen;
    }

}
