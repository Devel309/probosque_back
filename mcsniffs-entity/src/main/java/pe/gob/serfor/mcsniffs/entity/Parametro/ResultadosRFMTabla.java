package pe.gob.serfor.mcsniffs.entity.Parametro;

public class ResultadosRFMTabla {
    private  String nombreEspecie;
    private  String dap30a39;
    private  String dap40a49;
    private  String dap50a59;
    private  String dap60a69;
    private  String dap70a79;
    private  String dap80a89;
    private  String dap90aMas;
    private  String dapTotalPC;
    private  String dapTotalporHa;
    private  String vol30a39;
    private  String vol40a49;
    private  String vol50a59;
    private  String vol60a69;
    private  String vol70a79;
    private  String vol80a89;
    private  String vol90aMas;
    private  String volTotalPC;
    private  String volTotalporHa;
    private String unidadC;
    public String getNombreEspecie() {
        return nombreEspecie;
    }

    public void setNombreEspecie(String nombreEspecie) {
        this.nombreEspecie = nombreEspecie;
    }


    public String getDap30a39() {
        return dap30a39;
    }

    public void setDap30a39(String dap30a39) {
        this.dap30a39 = dap30a39;
    }

    public String getDap40a49() {
        return dap40a49;
    }

    public void setDap40a49(String dap40a49) {
        this.dap40a49 = dap40a49;
    }

    public String getDap50a59() {
        return dap50a59;
    }

    public void setDap50a59(String dap50a59) {
        this.dap50a59 = dap50a59;
    }

    public String getDap60a69() {
        return dap60a69;
    }

    public void setDap60a69(String dap60a69) {
        this.dap60a69 = dap60a69;
    }

    public String getDap70a79() {
        return dap70a79;
    }

    public void setDap70a79(String dap70a79) {
        this.dap70a79 = dap70a79;
    }

    public String getDap80a89() {
        return dap80a89;
    }

    public void setDap80a89(String dap80a89) {
        this.dap80a89 = dap80a89;
    }

    public String getDap90aMas() {
        return dap90aMas;
    }

    public void setDap90aMas(String dap90aMas) {
        this.dap90aMas = dap90aMas;
    }

    public String getVol30a39() {
        return vol30a39;
    }

    public void setVol30a39(String vol30a39) {
        this.vol30a39 = vol30a39;
    }

    public String getVol40a49() {
        return vol40a49;
    }

    public void setVol40a49(String vol40a49) {
        this.vol40a49 = vol40a49;
    }

    public String getVol50a59() {
        return vol50a59;
    }

    public void setVol50a59(String vol50a59) {
        this.vol50a59 = vol50a59;
    }

    public String getVol60a69() {
        return vol60a69;
    }

    public void setVol60a69(String vol60a69) {
        this.vol60a69 = vol60a69;
    }

    public String getVol70a79() {
        return vol70a79;
    }

    public void setVol70a79(String vol70a79) {
        this.vol70a79 = vol70a79;
    }

    public String getVol80a89() {
        return vol80a89;
    }

    public void setVol80a89(String vol80a89) {
        this.vol80a89 = vol80a89;
    }

    public String getVol90aMas() {
        return vol90aMas;
    }

    public void setVol90aMas(String vol90aMas) {
        this.vol90aMas = vol90aMas;
    }
    public String getDapTotalPC() {
        return dapTotalPC;
    }

    public void setDapTotalPC(String dapTotalPC) {
        this.dapTotalPC = dapTotalPC;
    }

    public String getDapTotalporHa() {
        return dapTotalporHa;
    }

    public void setDapTotalporHa(String dapTotalporHa) {
        this.dapTotalporHa = dapTotalporHa;
    }

    public String getVolTotalPC() {
        return volTotalPC;
    }

    public void setVolTotalPC(String volTotalPC) {
        this.volTotalPC = volTotalPC;
    }

    public String getVolTotalporHa() {
        return volTotalporHa;
    }

    public void setVolTotalporHa(String volTotalporHa) {
        this.volTotalporHa = volTotalporHa;
    }
    public String getUnidadC() {
        return unidadC;
    }

    public void setUnidadC(String unidadC) {
        this.unidadC = unidadC;
    }

}
