package pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class PotencialProduccionForestalDto extends AuditoriaEntity implements Serializable {
    private Integer idPotProdForestal;
    private String codigoTipoPotencialProdForestal;
    private String CodigoTipoPotProdForestal;
    private String codigoTipoPotencialProdForestalDet;
    private Integer idPotProdForestalDet;
    private Integer idEspecie;
    private String especie;
    private Integer idVariable;
    private String variable;
    private BigDecimal totalHa;
    private BigDecimal totalAreaCensada;
    private BigDecimal totalAreaCensadaPorcentaje;
    private Integer totalArboles;
    private BigDecimal totalArbolesPorcentaje;
    private BigDecimal totalVolumenComercial;
    private BigDecimal totalVolumenComercialPorcentaje;
    private Integer nroTotalFustales;
    private BigDecimal nroTotalFustalesPorcentaje;
    private BigDecimal nroTotalAreaBasalm2;
    private BigDecimal nroTotalAreaBasalPorcentaje;
    private BigDecimal nroTotalUnidadC;
    private BigDecimal nroTotalUnidadCPorcentaje;
    private String anexo;
    private String disenio;
    private BigDecimal diametroMinimoInventariadoCm;
    private BigDecimal tamanioParcela;
    private Integer nroParcela;
    private BigDecimal distanciaParcela;
    private BigDecimal totalAreaInventariada;
    private BigDecimal rangoDiametroCm;
    private BigDecimal areaMuestreadaHa;
    private String metodoMuestreo;
    private BigDecimal intensidadMuestreoPorcentaje;
    private BigDecimal errorMuestreoPorcentaje;
    private Integer idPlanManejo;
    private BigDecimal nuTotalTipoBosque;
    private boolean accion;
    private Integer idTipoBosque;
    private String tipoBosque;
    private String codigoSubTipoPotencialProdForestalDet;
    private String codigoSubTipoPotencialProdForestal;
    private String grupoComercial;
    private Integer nroArboles;
    private BigDecimal nroArbolesPorcentaje;
    private BigDecimal porcentaje;

    private List<PotencialProduccionForestalVariableEntity> listPotencialProduccionVariable;

    public String getCodigoTipoPotProdForestal() {
        return CodigoTipoPotProdForestal;
    }

    public void setCodigoTipoPotProdForestal(String codigoTipoPotProdForestal) {
        CodigoTipoPotProdForestal = codigoTipoPotProdForestal;
    }



    public List<PotencialProduccionForestalVariableEntity> getListPotencialProduccionVariable() {
        return listPotencialProduccionVariable;
    }

    public void setListPotencialProduccionVariable(List<PotencialProduccionForestalVariableEntity> listPotencialProduccionVariable) {
        this.listPotencialProduccionVariable = listPotencialProduccionVariable;
    }

    public String getCodigoSubTipoPotencialProdForestal() {
        return codigoSubTipoPotencialProdForestal;
    }

    public void setCodigoSubTipoPotencialProdForestal(String codigoSubTipoPotencialProdForestal) {
        this.codigoSubTipoPotencialProdForestal = codigoSubTipoPotencialProdForestal;
    }

    public String getCodigoSubTipoPotencialProdForestalDet() {
        return codigoSubTipoPotencialProdForestalDet;
    }

    public void setCodigoSubTipoPotencialProdForestalDet(String codigoSubTipoPotencialProdForestalDet) {
        this.codigoSubTipoPotencialProdForestalDet = codigoSubTipoPotencialProdForestalDet;
    }

    public Integer getIdTipoBosque() {
        return idTipoBosque;
    }

    public void setIdTipoBosque(Integer idTipoBosque) {
        this.idTipoBosque = idTipoBosque;
    }

    public Integer getIdEspecie() {
        return idEspecie;
    }

    public void setIdEspecie(Integer idEspecie) {
        this.idEspecie = idEspecie;
    }

    public Integer getIdVariable() {
        return idVariable;
    }

    public void setIdVariable(Integer idVariable) {
        this.idVariable = idVariable;
    }

    public boolean isAccion() {
        return accion;
    }

    public void setAccion(boolean accion) {
        this.accion = accion;
    }

    public String getCodigoTipoPotencialProdForestalDet() {
        return codigoTipoPotencialProdForestalDet;
    }

    public void setCodigoTipoPotencialProdForestalDet(String codigoTipoPotencialProdForestalDet) {
        this.codigoTipoPotencialProdForestalDet = codigoTipoPotencialProdForestalDet;
    }

    public Integer getIdPotProdForestalDet() {
        return idPotProdForestalDet;
    }

    public void setIdPotProdForestalDet(Integer idPotProdForestalDet) {
        this.idPotProdForestalDet = idPotProdForestalDet;
    }

    public String getTipoBosque() {
        return tipoBosque;
    }

    public void setTipoBosque(String tipoBosque) {
        this.tipoBosque = tipoBosque;
    }

    public BigDecimal getNuTotalTipoBosque() {
        return nuTotalTipoBosque;
    }

    public void setNuTotalTipoBosque(BigDecimal nuTotalTipoBosque) {
        this.nuTotalTipoBosque = nuTotalTipoBosque;
    }

    public Integer getIdPotProdForestal() {
        return idPotProdForestal;
    }

    public void setIdPotProdForestal(Integer idPotProdForestal) {
        this.idPotProdForestal = idPotProdForestal;
    }

    public String getCodigoTipoPotencialProdForestal() {
        return codigoTipoPotencialProdForestal;
    }

    public void setCodigoTipoPotencialProdForestal(String codigoTipoPotencialProdForestal) {
        this.codigoTipoPotencialProdForestal = codigoTipoPotencialProdForestal;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public BigDecimal getTotalHa() {
        return totalHa;
    }

    public void setTotalHa(BigDecimal totalHa) {
        this.totalHa = totalHa;
    }

    public BigDecimal getTotalAreaCensada() {
        return totalAreaCensada;
    }

    public void setTotalAreaCensada(BigDecimal totalAreaCensada) {
        this.totalAreaCensada = totalAreaCensada;
    }

    public BigDecimal getTotalAreaCensadaPorcentaje() {
        return totalAreaCensadaPorcentaje;
    }

    public void setTotalAreaCensadaPorcentaje(BigDecimal totalAreaCensadaPorcentaje) {
        this.totalAreaCensadaPorcentaje = totalAreaCensadaPorcentaje;
    }

    public Integer getTotalArboles() {
        return totalArboles;
    }

    public void setTotalArboles(Integer totalArboles) {
        this.totalArboles = totalArboles;
    }

    public BigDecimal getTotalArbolesPorcentaje() {
        return totalArbolesPorcentaje;
    }

    public void setTotalArbolesPorcentaje(BigDecimal totalArbolesPorcentaje) {
        this.totalArbolesPorcentaje = totalArbolesPorcentaje;
    }

    public BigDecimal getTotalVolumenComercial() {
        return totalVolumenComercial;
    }

    public void setTotalVolumenComercial(BigDecimal totalVolumenComercial) {
        this.totalVolumenComercial = totalVolumenComercial;
    }

    public BigDecimal getTotalVolumenComercialPorcentaje() {
        return totalVolumenComercialPorcentaje;
    }

    public void setTotalVolumenComercialPorcentaje(BigDecimal totalVolumenComercialPorcentaje) {
        this.totalVolumenComercialPorcentaje = totalVolumenComercialPorcentaje;
    }

    public Integer getNroTotalFustales() {
        return nroTotalFustales;
    }

    public void setNroTotalFustales(Integer nroTotalFustales) {
        this.nroTotalFustales = nroTotalFustales;
    }

    public BigDecimal getNroTotalFustalesPorcentaje() {
        return nroTotalFustalesPorcentaje;
    }

    public void setNroTotalFustalesPorcentaje(BigDecimal nroTotalFustalesPorcentaje) {
        this.nroTotalFustalesPorcentaje = nroTotalFustalesPorcentaje;
    }

    public BigDecimal getNroTotalAreaBasalm2() {
        return nroTotalAreaBasalm2;
    }

    public void setNroTotalAreaBasalm2(BigDecimal nroTotalAreaBasalm2) {
        this.nroTotalAreaBasalm2 = nroTotalAreaBasalm2;
    }

    public BigDecimal getNroTotalAreaBasalPorcentaje() {
        return nroTotalAreaBasalPorcentaje;
    }

    public void setNroTotalAreaBasalPorcentaje(BigDecimal nroTotalAreaBasalPorcentaje) {
        this.nroTotalAreaBasalPorcentaje = nroTotalAreaBasalPorcentaje;
    }

    public BigDecimal getNroTotalUnidadC() {
        return nroTotalUnidadC;
    }

    public void setNroTotalUnidadC(BigDecimal nroTotalUnidadC) {
        this.nroTotalUnidadC = nroTotalUnidadC;
    }

    public BigDecimal getNroTotalUnidadCPorcentaje() {
        return nroTotalUnidadCPorcentaje;
    }

    public void setNroTotalUnidadCPorcentaje(BigDecimal nroTotalUnidadCPorcentaje) {
        this.nroTotalUnidadCPorcentaje = nroTotalUnidadCPorcentaje;
    }

    public String getAnexo() {
        return anexo;
    }

    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    public String getDisenio() {
        return disenio;
    }

    public void setDisenio(String disenio) {
        this.disenio = disenio;
    }

    public BigDecimal getDiametroMinimoInventariadoCm() {
        return diametroMinimoInventariadoCm;
    }

    public void setDiametroMinimoInventariadoCm(BigDecimal diametroMinimoInventariadoCm) {
        this.diametroMinimoInventariadoCm = diametroMinimoInventariadoCm;
    }

    public BigDecimal getTamanioParcela() {
        return tamanioParcela;
    }

    public void setTamanioParcela(BigDecimal tamanioParcela) {
        this.tamanioParcela = tamanioParcela;
    }

    public Integer getNroParcela() {
        return nroParcela;
    }

    public void setNroParcela(Integer nroParcela) {
        this.nroParcela = nroParcela;
    }

    public BigDecimal getDistanciaParcela() {
        return distanciaParcela;
    }

    public void setDistanciaParcela(BigDecimal distanciaParcela) {
        this.distanciaParcela = distanciaParcela;
    }

    public BigDecimal getTotalAreaInventariada() {
        return totalAreaInventariada;
    }

    public void setTotalAreaInventariada(BigDecimal totalAreaInventariada) {
        this.totalAreaInventariada = totalAreaInventariada;
    }

    public BigDecimal getRangoDiametroCm() {
        return rangoDiametroCm;
    }

    public void setRangoDiametroCm(BigDecimal rangoDiametroCm) {
        this.rangoDiametroCm = rangoDiametroCm;
    }

    public BigDecimal getAreaMuestreadaHa() {
        return areaMuestreadaHa;
    }

    public void setAreaMuestreadaHa(BigDecimal areaMuestreadaHa) {
        this.areaMuestreadaHa = areaMuestreadaHa;
    }

    public String getMetodoMuestreo() {
        return metodoMuestreo;
    }

    public void setMetodoMuestreo(String metodoMuestreo) {
        this.metodoMuestreo = metodoMuestreo;
    }

    public BigDecimal getIntensidadMuestreoPorcentaje() {
        return intensidadMuestreoPorcentaje;
    }

    public void setIntensidadMuestreoPorcentaje(BigDecimal intensidadMuestreoPorcentaje) {
        this.intensidadMuestreoPorcentaje = intensidadMuestreoPorcentaje;
    }

    public BigDecimal getErrorMuestreoPorcentaje() {
        return errorMuestreoPorcentaje;
    }

    public void setErrorMuestreoPorcentaje(BigDecimal errorMuestreoPorcentaje) {
        this.errorMuestreoPorcentaje = errorMuestreoPorcentaje;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public String getGrupoComercial() { return grupoComercial; }

    public void setGrupoComercial(String grupoComercial) { this.grupoComercial = grupoComercial; }

    public Integer getNroArboles() { return nroArboles; }

    public void setNroArboles(Integer nroArboles) { this.nroArboles = nroArboles; }

    public BigDecimal getNroArbolesPorcentaje() { return nroArbolesPorcentaje; }

    public void setNroArbolesPorcentaje(BigDecimal nroArbolesPorcentaje) { this.nroArbolesPorcentaje = nroArbolesPorcentaje; }

    public BigDecimal getPorcentaje() { return porcentaje; }

    public void setPorcentaje(BigDecimal porcentaje) { this.porcentaje = porcentaje; }
}
