package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;

@Data
public class ParametroEntity  extends AuditoriaEntity implements Serializable {
    Integer idParametro;
    String codigo;
    String valorPrimario;
    String valorSecundario;
    Integer idTipoParametro;
    Integer idParametroPadre;
    String valorTerciario;
    String prefijo;


}
