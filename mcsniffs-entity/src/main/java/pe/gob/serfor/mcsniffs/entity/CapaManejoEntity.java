package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class CapaManejoEntity extends AuditoriaEntity implements Serializable {
    private Integer idCapaManejo;
    private CoordenadaUtmEntity coordenadaUtm;
    private AreaManejoEntity areaManejo;
    private DocumentoGeneralEntity documentoGeneral;

    public Integer getIdCapaManejo() {
        return idCapaManejo;
    }

    public void setIdCapaManejo(Integer idCapaManejo) {
        this.idCapaManejo = idCapaManejo;
    }

    public CoordenadaUtmEntity getCoordenadaUtm() {
        return coordenadaUtm;
    }

    public void setCoordenadaUtm(CoordenadaUtmEntity coordenadaUtm) {
        this.coordenadaUtm = coordenadaUtm;
    }

    public AreaManejoEntity getAreaManejo() {
        return areaManejo;
    }

    public void setAreaManejo(AreaManejoEntity areaManejo) {
        this.areaManejo = areaManejo;
    }

    public DocumentoGeneralEntity getDocumentoGeneral() {
        return documentoGeneral;
    }

    public void setDocumentoGeneral(DocumentoGeneralEntity documentoGeneral) {
        this.documentoGeneral = documentoGeneral;
    }
}
