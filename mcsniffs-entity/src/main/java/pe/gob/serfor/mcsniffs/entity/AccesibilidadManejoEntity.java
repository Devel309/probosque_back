package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;


public class AccesibilidadManejoEntity extends AuditoriaEntity implements Serializable {
    private Integer idAcceManejo;
    private Boolean tipoAccesibilidad;
    private Integer idPlanManejo;

    public Integer getIdAcceManejo() {
        return idAcceManejo;
    }

    public void setIdAcceManejo(Integer idAcceManejo) {
        this.idAcceManejo = idAcceManejo;
    }

    public Boolean getTipoAccesibilidad() {
        return tipoAccesibilidad;
    }

    public void setTipoAccesibilidad(Boolean tipoAccesibilidad) {
        this.tipoAccesibilidad = tipoAccesibilidad;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }


}
