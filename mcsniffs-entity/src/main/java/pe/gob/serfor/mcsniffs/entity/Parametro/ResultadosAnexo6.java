package pe.gob.serfor.mcsniffs.entity.Parametro;

import java.util.List;

public class ResultadosAnexo6 {
    String idTipoBosque;
    String nombreBosque;
    String  areaBosque;
    List<TablaAnexo6Dto> listaTablaAnexo6Dto;
    public String getIdTipoBosque() {
        return idTipoBosque;
    }

    public void setIdTipoBosque(String idTipoBosque) {
        this.idTipoBosque = idTipoBosque;
    }

    public String getNombreBosque() {
        return nombreBosque;
    }

    public void setNombreBosque(String nombreBosque) {
        this.nombreBosque = nombreBosque;
    }

    public List<TablaAnexo6Dto> getListaTablaAnexo6Dto() {
        return listaTablaAnexo6Dto;
    }

    public void setListaTablaAnexo6Dto(List<TablaAnexo6Dto> listaTablaAnexo6Dto) {
        this.listaTablaAnexo6Dto = listaTablaAnexo6Dto;
    }
    public String getAreaBosque() {
        return areaBosque;
    }

    public void setAreaBosque(String areaBosque) {
        this.areaBosque = areaBosque;
    }

}
