package pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DescargarResAdminFavDesfavDto {
    
    private Integer idSolicitudConcesion;
    private String codRadio;
    private List<DescargarSolicitudConcesionVerticesDto> vertices;
}
