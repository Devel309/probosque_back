package pe.gob.serfor.mcsniffs.entity.Dto.ImpactoAmbientalPmfi;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.ImpactoAmbientalDetalleEntity;

import java.io.Serializable;
import java.util.List;

public class ImpactoAmbientalPmfiDto extends AuditoriaEntity implements Serializable {

    private Integer idImpactoAmbiental;
    private String codigoImpactoAmbiental;
    private Integer idPlanManejo;
    private List<ImpactoAmbientalDetalleEntity> detalle;

    public Integer getIdImpactoAmbiental() {
        return idImpactoAmbiental;
    }

    public void setIdImpactoAmbiental(Integer idImpactoAmbiental) {
        this.idImpactoAmbiental = idImpactoAmbiental;
    }

    public String getCodigoImpactoAmbiental() {
        return codigoImpactoAmbiental;
    }

    public void setCodigoImpactoAmbiental(String codigoImpactoAmbiental) {
        this.codigoImpactoAmbiental = codigoImpactoAmbiental;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }


    public List<ImpactoAmbientalDetalleEntity> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<ImpactoAmbientalDetalleEntity> detalle) {
        this.detalle = detalle;
    }
}
