package pe.gob.serfor.mcsniffs.entity.Dto.DecargarPDFSolBosqueLocalBeneficiario;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Getter
@Setter
public class DescargarPDFSolBeneficiario extends AuditoriaEntity implements Serializable{
    private Integer idSolBosqueLocal;
}
