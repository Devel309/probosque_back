package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class GeneralAnexoEntity extends AuditoriaEntity implements Serializable {
    private Integer IdGeneralAnexos;
    private String CodigoAnexo;
    private String Descripcion;
    private String Value;
    private Integer IdProcesoPostulacion;
    private Integer IdUsuarioPostulacion;

    public Integer getIdProcesoPostulacion() {
        return IdProcesoPostulacion;
    }

    public void setIdProcesoPostulacion(Integer idProcesoPostulacion) {
        IdProcesoPostulacion = idProcesoPostulacion;
    }

    public Integer getIdUsuarioPostulacion() {
        return IdUsuarioPostulacion;
    }

    public void setIdUsuarioPostulacion(Integer idUsuarioPostulacion) {
        IdUsuarioPostulacion = idUsuarioPostulacion;
    }

    public Integer getIdGeneralAnexos() {
        return IdGeneralAnexos;
    }

    public void setIdGeneralAnexos(Integer idGeneralAnexos) {
        IdGeneralAnexos = idGeneralAnexos;
    }

    public String getCodigoAnexo() {
        return CodigoAnexo;
    }

    public void setCodigoAnexo(String codigoAnexo) {
        CodigoAnexo = codigoAnexo;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
