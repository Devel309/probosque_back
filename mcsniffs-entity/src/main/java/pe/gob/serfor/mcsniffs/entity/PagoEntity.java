package pe.gob.serfor.mcsniffs.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class PagoEntity extends AuditoriaEntity implements Serializable {
    private Integer idPago;
    private String codTipoPago;
    private String codigoTH;
    private String modalidadTH;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaInicio;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaFin;
    private Integer vigencia;
    private String tipoDocumento;
    private String nroDocumento;
    private String nombreTitular;
    private String paternoTitular;
    private String maternoTitular;
    private String ubigeo;
    private String mecanismoPago;
    private BigDecimal areHA;
    private BigDecimal montoPagoHA;
    private String tipoMoneda;
    private BigDecimal montoPagoAnual;
    private BigDecimal montoPagoAnualCalculado;
    private Boolean accion;
    private String observacion;
    private String detalle;
    private String descripcion;
    private Integer nrDocumentoGestion;
    private String tipoDocumentoGestion;
    private String codigoEstado;
    private String codigoEstadoText;
    private String tipoPersona;
    private String tipoDocumentoRepresentante;
    private String nroDocumentoRepresentante;
    private String nombreRepresentante;
    private String paternoRepresentante;
    private String maternoRepresentante;
    private String conforme;
    private String perfil;
    private List<PagoCronogramaEntity> listaCronograma;
}
