package pe.gob.serfor.mcsniffs.entity.Dto.PlanManejo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlanManejoObtenerContrato{
  private Integer idContrato;
  private String codigoTitulo;
}
