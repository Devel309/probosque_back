package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ComunidadEntity extends AuditoriaEntity implements Serializable {
        private Integer idComunidad;
        private String ruc;
        private TipoComunidadEntity tipoComunidad;
        private FederacionComunidadEntity federacionComunidad;
        private String nombre;
        private String credencial;

    public Integer getIdComunidad() {
        return idComunidad;
    }

    public void setIdComunidad(Integer idComunidad) {
        this.idComunidad = idComunidad;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public TipoComunidadEntity getTipoComunidad() {
        return tipoComunidad;
    }

    public void setTipoComunidad(TipoComunidadEntity tipoComunidad) {
        this.tipoComunidad = tipoComunidad;
    }

    public FederacionComunidadEntity getFederacionComunidad() {
        return federacionComunidad;
    }

    public void setFederacionComunidad(FederacionComunidadEntity federacionComunidad) {
        this.federacionComunidad = federacionComunidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCredencial() {
        return credencial;
    }

    public void setCredencial(String credencial) {
        this.credencial = credencial;
    }
}
