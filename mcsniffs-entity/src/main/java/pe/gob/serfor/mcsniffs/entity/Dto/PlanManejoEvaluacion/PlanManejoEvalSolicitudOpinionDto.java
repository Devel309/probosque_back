package pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;

public class PlanManejoEvalSolicitudOpinionDto  extends AuditoriaEntity implements Serializable {

    private Integer idPlanManejoEvaluacion;
    private Integer idSolicitudOpinion;
    private String  tipoSolicitud;

    public Integer getIdPlanManejoEvaluacion() {
        return idPlanManejoEvaluacion;
    }

    public void setIdPlanManejoEvaluacion(Integer idPlanManejoEvaluacion) {
        this.idPlanManejoEvaluacion = idPlanManejoEvaluacion;
    }

    public Integer getIdSolicitudOpinion() {
        return idSolicitudOpinion;
    }

    public void setIdSolicitudOpinion(Integer idSolicitudOpinion) {
        this.idSolicitudOpinion = idSolicitudOpinion;
    }

    public String getTipoSolicitud() {
        return tipoSolicitud;
    }

    public void setTipoSolicitud(String tipoSolicitud) {
        this.tipoSolicitud = tipoSolicitud;
    }
}
