package pe.gob.serfor.mcsniffs.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EvaluacionResultadoEntity extends AuditoriaEntity implements Serializable {
    private Integer idEvalResultado;
    private Integer idPlanManejo;
    private String codResultado;
    private String subCodResultado;
    private String descripcionCab;
    private String codInforme;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaConsentimiento;

    public Date getFechaConsentimiento() {
        return fechaConsentimiento;
    }

    public void setFechaConsentimiento(Date fechaConsentimiento) {
        this.fechaConsentimiento = fechaConsentimiento;
    }

    public String getCodInforme() {
        return codInforme;
    }

    public void setCodInforme(String codInforme) {
        this.codInforme = codInforme;
    }

    private List<EvaluacionResultadoDetalleEntity> listEvaluacionResultado;

    public Integer getIdEvalResultado() {
        return idEvalResultado;
    }

    public void setIdEvalResultado(Integer idEvalResultado) {
        this.idEvalResultado = idEvalResultado;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public String getCodResultado() {
        return codResultado;
    }

    public void setCodResultado(String codResultado) {
        this.codResultado = codResultado;
    }

    public String getSubCodResultado() {
        return subCodResultado;
    }

    public void setSubCodResultado(String subCodResultado) {
        this.subCodResultado = subCodResultado;
    }

    public String getDescripcionCab() {
        return descripcionCab;
    }

    public void setDescripcionCab(String descripcionCab) {
        this.descripcionCab = descripcionCab;
    }

    public List<EvaluacionResultadoDetalleEntity> getListEvaluacionResultado() {
        return listEvaluacionResultado;
    }

    public void setListEvaluacionResultado(List<EvaluacionResultadoDetalleEntity> listEvaluacionResultado) {
        this.listEvaluacionResultado = listEvaluacionResultado;
    }
}
