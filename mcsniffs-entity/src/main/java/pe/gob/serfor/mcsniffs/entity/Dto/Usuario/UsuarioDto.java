package pe.gob.serfor.mcsniffs.entity.Dto.Usuario;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.util.Date;
@Data
public class UsuarioDto  extends AuditoriaEntity implements Serializable {

   Integer idusuario;
   String  region ;
   Integer idPerfil;
   String  perfil;
   String codigoAplicacion;
}
