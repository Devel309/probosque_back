package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
@Data
public class OrdenProduccionEntity extends AuditoriaEntity implements Serializable {
    
private Integer idOrdenProduccion;
private Integer idOrdenDesktop;
private Date fechaConsumo ;
private Integer idResolucion;


   
}
