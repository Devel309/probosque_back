package pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima;

import java.util.Date;

public class CondicionMinimaDetalleDto {

    private Integer idCondicionMinimaDet;
    private String codigoTipoCondicionMinimaDet;
    private String resultado;
    private Date fechaIngreso;
    private Date fechaVigencia;
    private Integer idCondicionMinima;
    private Integer idUsuarioModificacion;
    private Date fechaModificacion;
    private String estado;
    private Integer idUsuarioRegistro;
    private Date fechaRegistro;

    public Integer getIdCondicionMinimaDet() {
        return idCondicionMinimaDet;
    }

    public void setIdCondicionMinimaDet(Integer idCondicionMinimaDet) {
        this.idCondicionMinimaDet = idCondicionMinimaDet;
    }

    public String getCodigoTipoCondicionMinimaDet() {
        return codigoTipoCondicionMinimaDet;
    }

    public void setCodigoTipoCondicionMinimaDet(String codigoTipoCondicionMinimaDet) {
        this.codigoTipoCondicionMinimaDet = codigoTipoCondicionMinimaDet;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Date getFechaVigencia() {
        return fechaVigencia;
    }

    public void setFechaVigencia(Date fechaVigencia) {
        this.fechaVigencia = fechaVigencia;
    }

    public Integer getIdCondicionMinima() {
        return idCondicionMinima;
    }

    public void setIdCondicionMinima(Integer idCondicionMinima) {
        this.idCondicionMinima = idCondicionMinima;
    }

    public Integer getIdUsuarioModificacion() {
        return idUsuarioModificacion;
    }

    public void setIdUsuarioModificacion(Integer idUsuarioModificacion) {
        this.idUsuarioModificacion = idUsuarioModificacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Integer getIdUsuarioRegistro() {
        return idUsuarioRegistro;
    }

    public void setIdUsuarioRegistro(Integer idUsuarioRegistro) {
        this.idUsuarioRegistro = idUsuarioRegistro;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
}
