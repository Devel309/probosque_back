package pe.gob.serfor.mcsniffs.entity.seguridad;

import java.util.Date;
import java.util.List;

public class UserLoginDto {
    Integer IDUsuario;
    List<PerfilDto> Perfiles ;
    String Nombres ;
    String ApellidoPaterno ;
    String ApellidoMaterno ;
    String NumeroDocumento ;
    String CodigoEmpleado ;
    String CodigoUsuario ;
    String Usuario ;
    String UsuarioDescripcion ;
    String Contraseña ;
    String CorreoElectronico ;
    String Token ;
    Integer IDEmpresa ;
    Integer IDEntidad ;
    Integer IDTipoDocumento ;
    String TipoDocumento ;
    String Estado ;
    String EstadoDescripcion ;
    Integer IDAutoridad ;
    String Region ;
    Integer SYSUsuario ;
    Date SYSFecha ;
    String DescripcionARFFS ;
    String CodigoDepartamentoARFFS ;
    String SIRPerfil ;
    
    public Integer getIDUsuario() {
        return IDUsuario;
    }
    public void setIDUsuario(Integer iDUsuario) {
        IDUsuario = iDUsuario;
    }
    public List<PerfilDto> getPerfiles() {
        return Perfiles;
    }
    public void setPerfiles(List<PerfilDto> perfiles) {
        Perfiles = perfiles;
    }
    public String getNombres() {
        return Nombres;
    }
    public void setNombres(String nombres) {
        Nombres = nombres;
    }
    public String getApellidoPaterno() {
        return ApellidoPaterno;
    }
    public void setApellidoPaterno(String apellidoPaterno) {
        ApellidoPaterno = apellidoPaterno;
    }
    public String getApellidoMaterno() {
        return ApellidoMaterno;
    }
    public void setApellidoMaterno(String apellidoMaterno) {
        ApellidoMaterno = apellidoMaterno;
    }
    public String getNumeroDocumento() {
        return NumeroDocumento;
    }
    public void setNumeroDocumento(String numeroDocumento) {
        NumeroDocumento = numeroDocumento;
    }
    public String getCodigoEmpleado() {
        return CodigoEmpleado;
    }
    public void setCodigoEmpleado(String codigoEmpleado) {
        CodigoEmpleado = codigoEmpleado;
    }
    public String getCodigoUsuario() {
        return CodigoUsuario;
    }
    public void setCodigoUsuario(String codigoUsuario) {
        CodigoUsuario = codigoUsuario;
    }
    public String getUsuario() {
        return Usuario;
    }
    public void setUsuario(String usuario) {
        Usuario = usuario;
    }
    public String getUsuarioDescripcion() {
        return UsuarioDescripcion;
    }
    public void setUsuarioDescripcion(String usuarioDescripcion) {
        UsuarioDescripcion = usuarioDescripcion;
    }
    public String getContraseña() {
        return Contraseña;
    }
    public void setContraseña(String contraseña) {
        Contraseña = contraseña;
    }
    public String getCorreoElectronico() {
        return CorreoElectronico;
    }
    public void setCorreoElectronico(String correoElectronico) {
        CorreoElectronico = correoElectronico;
    }
    public String getToken() {
        return Token;
    }
    public void setToken(String token) {
        Token = token;
    }
    public Integer getIDEmpresa() {
        return IDEmpresa;
    }
    public void setIDEmpresa(Integer iDEmpresa) {
        IDEmpresa = iDEmpresa;
    }
    public Integer getIDEntidad() {
        return IDEntidad;
    }
    public void setIDEntidad(Integer iDEntidad) {
        IDEntidad = iDEntidad;
    }
    public Integer getIDTipoDocumento() {
        return IDTipoDocumento;
    }
    public void setIDTipoDocumento(Integer iDTipoDocumento) {
        IDTipoDocumento = iDTipoDocumento;
    }
    public String getTipoDocumento() {
        return TipoDocumento;
    }
    public void setTipoDocumento(String tipoDocumento) {
        TipoDocumento = tipoDocumento;
    }
    public String getEstado() {
        return Estado;
    }
    public void setEstado(String estado) {
        Estado = estado;
    }
    public String getEstadoDescripcion() {
        return EstadoDescripcion;
    }
    public void setEstadoDescripcion(String estadoDescripcion) {
        EstadoDescripcion = estadoDescripcion;
    }
    public Integer getIDAutoridad() {
        return IDAutoridad;
    }
    public void setIDAutoridad(Integer iDAutoridad) {
        IDAutoridad = iDAutoridad;
    }
    public String getRegion() {
        return Region;
    }
    public void setRegion(String region) {
        Region = region;
    }
    public Integer getSYSUsuario() {
        return SYSUsuario;
    }
    public void setSYSUsuario(Integer sYSUsuario) {
        SYSUsuario = sYSUsuario;
    }
    public Date getSYSFecha() {
        return SYSFecha;
    }
    public void setSYSFecha(Date sYSFecha) {
        SYSFecha = sYSFecha;
    }
    public String getDescripcionARFFS() {
        return DescripcionARFFS;
    }
    public void setDescripcionARFFS(String descripcionARFFS) {
        DescripcionARFFS = descripcionARFFS;
    }
    public String getCodigoDepartamentoARFFS() {
        return CodigoDepartamentoARFFS;
    }
    public void setCodigoDepartamentoARFFS(String codigoDepartamentoARFFS) {
        CodigoDepartamentoARFFS = codigoDepartamentoARFFS;
    }
    public String getSIRPerfil() {
        return SIRPerfil;
    }
    public void setSIRPerfil(String sIRPerfil) {
        SIRPerfil = sIRPerfil;
    }

    
}
