package pe.gob.serfor.mcsniffs.entity.util.codigos;

public class CodigosPGMF {

    public static final String PGMF_TAB_4      =  "PGMFCFFMOPUMF";
    public static final String PGMF_TAB_4_1     = CodigosPGMF.PGMF_TAB_4 + "CO";//4.1. Categorías de Ordenamiento

    public static final String PGMF_TAB_4_2     = CodigosPGMF.PGMF_TAB_4 + "DAB";//4.2. División Administrativa del Bosque

    public static final String PGMF_TAB_4_2_1   = CodigosPGMF.PGMF_TAB_4_2 + "BQ";//4.2.1. Bloques Quinquenales(opcional)
    public static final String PGMF_TAB_4_2_2   = CodigosPGMF.PGMF_TAB_4_2 + "PC";//4.2.2. Parcelas de Corta (PC)
    public static final String PGMF_TAB_4_2_3   = CodigosPGMF.PGMF_TAB_4_2 + "FC";//4.2.3. Frentes de Corta

    public static final String PGMF_TAB_4_3     = CodigosPGMF.PGMF_TAB_4 + "PV";//4.3. Protección y Vigilancia
    public static final String PGMF_TAB_4_3_1   = CodigosPGMF.PGMF_TAB_4_3 + "UMV";//4.3.1. Ubicación y Marcado de Vértices
    public static final String PGMF_TAB_4_3_2   = CodigosPGMF.PGMF_TAB_4_3 + "S";//4.3.2. Señalización
    public static final String PGMF_TAB_4_3_3   = CodigosPGMF.PGMF_TAB_4_3 + "DML";//4.3.3. Demarcación y Mantenimiento de Linderos
    public static final String PGMF_TAB_4_3_4   = CodigosPGMF.PGMF_TAB_4_3 + "VUMF";//4.3.4. Vigilancia de la UMF

}
