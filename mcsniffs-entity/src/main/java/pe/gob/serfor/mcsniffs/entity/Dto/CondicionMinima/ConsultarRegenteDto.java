package pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data",
        "totalRegistro",
        "dataService",
        "message",
        "stackTrace",
        "codigo",
        "innerException",
        "success",
        "informacion",
        "messageExeption"
})
@Getter
@Setter
public class ConsultarRegenteDto {

    @JsonProperty("data")
    private String data;
    @JsonProperty("totalRegistro")
    private Integer totalRegistro;
    @JsonProperty("dataService")
    private List<Regente> dataService;
    @JsonProperty("message")
    private String message;
    @JsonProperty("stackTrace")
    private String stackTrace;
    @JsonProperty("codigo")
    private String codigo;
    @JsonProperty("innerException")
    private String innerException;
    @JsonProperty("success")
    private boolean success;
    @JsonProperty("informacion")
    private String informacion;
    @JsonProperty("messageExeption")
    private String messageExeption;

    @Getter
    @Setter
    public static class Regente{
        private Integer id;
        private Integer periodo;
        private String numeroLicencia;
        private String nombres;
        private String estado;
        private String apellidos;
        private String numeroDocumento;
    }

    
}