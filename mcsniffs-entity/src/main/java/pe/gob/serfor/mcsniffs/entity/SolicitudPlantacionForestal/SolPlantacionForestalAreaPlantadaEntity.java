package pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal;

import java.util.Date;


import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;


@Data
// @AllArgsConstructor
// @NoArgsConstructor
// @Entity
// @Table(name = "T_MVD_SOLPLANTACIONFOREST_AREAPLANTADA", schema = "dbo")
public class SolPlantacionForestalAreaPlantadaEntity extends AuditoriaEntity{
  // @id
    // @generatedvalue(strategy = generationtype.identity)
    // @column(name = "nu_id_solplantforest_areaplantada", updatable = false, nullable = false)   
    Integer   id_solplantforest_areaplantada ;
    // @column(name = "nu_id_solplantforest", updatable = false, nullable = false)   
    Integer   id_solplantforest ;
    // @column(name = "nu_area_total_plantacion", updatable = false, nullable = false)   
    Integer   area_total_plantacion;
    // @column(name = "fe_mes_anio_plantacion", updatable = true, nullable = true)   
    Date mes_anio_plantacion ;
    // @column(name = "nu_id_sistemaplantacion", updatable = true, nullable = true)   
    Integer    id_sistemaplantacion ;
    // @column(name = "tx_superficie_medida", updatable = true, nullable = true)   
    String superficie_medida ;
    // @column(name = "nu_superficie_cantidad", updatable = true, nullable = true)   
    Integer   superficie_cantidad;
    // @column(name = "nu_fines", updatable = true, nullable = true)   
    Integer fines ;

    String especies_establecidas;

  


}
