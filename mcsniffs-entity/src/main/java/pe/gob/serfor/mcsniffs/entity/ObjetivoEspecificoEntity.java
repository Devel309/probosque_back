package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ObjetivoEspecificoEntity extends AuditoriaEntity implements Serializable {
    private Integer idObjEspecifico;
    private Integer idPadre;
    private Integer idHijo;
    private String descripcion;

    public Integer getIdObjEspecifico() {
        return idObjEspecifico;
    }

    public void setIdObjEspecifico(Integer idObjEspecifico) {
        this.idObjEspecifico = idObjEspecifico;
    }

    public Integer getIdPadre() {
        return idPadre;
    }

    public void setIdPadre(Integer idPadre) {
        this.idPadre = idPadre;
    }

    public Integer getIdHijo() {
        return idHijo;
    }

    public void setIdHijo(Integer idHijo) {
        this.idHijo = idHijo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
