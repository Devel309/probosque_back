package pe.gob.serfor.mcsniffs.entity.Evaluacion;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;

@Getter
@Setter
public class EvaluacionResumidoDetalleDto  implements Serializable{

    private Integer idEvaluacionDet;
    private Integer idEvaluacion;
    private String codigoEvaluacionDet;
    private String codigoEvaluacionDetSub;
    private String codigoEvaluacionDetPost;
    private String conforme;
    private String observacion;
    private String tab;
    private String acordeon;

}
