package pe.gob.serfor.mcsniffs.entity.CoreCentral;

import java.io.Serializable;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

public class DistritoEntity extends AuditoriaEntity implements Serializable {
    private Integer idDistrito;
    private String nombreDistrito;
    private String codDistrito;
    private String codProvincia;
    private String codDistritoInei;
    private String codDistritoReniec;
    private String codDistritoSunat;
    private Integer idProvincia;
    private Integer idDepartamento;

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public String getNombreDistrito() {
        return nombreDistrito;
    }

    public void setNombreDistrito(String nombreDistrito) {
        this.nombreDistrito = nombreDistrito;
    }

    public String getCodDistrito() {
        return codDistrito;
    }

    public void setCodDistrito(String codDistrito) {
        this.codDistrito = codDistrito;
    }

    public String getCodProvincia() {
        return codProvincia;
    }

    public void setCodProvincia(String codProvincia) {
        this.codProvincia = codProvincia;
    }

    public String getCodDistritoInei() {
        return codDistritoInei;
    }

    public void setCodDistritoInei(String codDistritoInei) {
        this.codDistritoInei = codDistritoInei;
    }

    public String getCodDistritoReniec() {
        return codDistritoReniec;
    }

    public void setCodDistritoReniec(String codDistritoReniec) {
        this.codDistritoReniec = codDistritoReniec;
    }

    public String getCodDistritoSunat() {
        return codDistritoSunat;
    }

    public void setCodDistritoSunat(String codDistritoSunat) {
        this.codDistritoSunat = codDistritoSunat;
    }

    public Integer getIdProvincia() {
        return idProvincia;
    }

    public void setIdProvincia(Integer idProvincia) {
        this.idProvincia = idProvincia;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }
}
