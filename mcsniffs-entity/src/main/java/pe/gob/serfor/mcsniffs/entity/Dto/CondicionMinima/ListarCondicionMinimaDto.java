package pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima;

import java.util.List;

import lombok.Data;

@Data
public class ListarCondicionMinimaDto {

    private Integer idCondicionMinima;
    private String tipoDocumento;
    private String nroDocumento;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String nombres;
    private String descripcion;
    private String resultadoEvaluacion;
    private String fechaEvaluacion;
    private String estado;
    private List<ListarCondicionMinimaDetalleDto> listaCondicionMinimaDetalle;
 

}
