package pe.gob.serfor.mcsniffs.entity;

public class SolicitudImpugnacionArchivo2Entity   {
    private Integer idSolicitudImpugnacion;
    private Integer idArchivo;
    private String valorPrimario; 
    private String tipoParametro;
    public Integer getIdArchivo() {
        return idArchivo;
    }
    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }
    public String getValorPrimario() {
        return valorPrimario;
    }
    public void setValorPrimario(String valorPrimario) {
        this.valorPrimario = valorPrimario;
    }
    public String getTipoParametro() {
        return tipoParametro;
    }
    public void setTipoParametro(String tipoParametro) {
        this.tipoParametro = tipoParametro;
    }
    public Integer getIdSolicitudImpugnacion() {
        return idSolicitudImpugnacion;
    }
    public void setIdSolicitudImpugnacion(Integer idSolicitudImpugnacion) {
        this.idSolicitudImpugnacion = idSolicitudImpugnacion;
    }    
}
