package pe.gob.serfor.mcsniffs.entity;

public class ParametroValorEntity {
    private String codigo;
    private String valor1;
    private String prefijo;

    public ParametroValorEntity() {
        
    }

    public ParametroValorEntity(String codigo, String valor1, String prefijo){
        this.codigo = codigo;
        this.valor1 = valor1;
        this.prefijo = prefijo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getValor1() {
        return valor1;
    }

    public void setValor1(String valor1) {
        this.valor1 = valor1;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }
}


