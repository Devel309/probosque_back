package pe.gob.serfor.mcsniffs.entity.Parametro;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.ProduccionRecursoForestalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ProduccionRecursoForestalEntity;

import java.io.Serializable;
import java.util.List;

public class ProduccionRecursoForestalEspecieDto extends AuditoriaEntity implements Serializable {
    private Integer idProdRecursoForestalEsp;
    private String idEspecie;
    private String especie;
    private Integer idProdRecursoForestal;
    private List<ProduccionRecursoForestalDetalleEntity> listProduccionRecursoForestalDetalle;

    public Integer getIdProdRecursoForestalEsp() {
        return idProdRecursoForestalEsp;
    }

    public void setIdProdRecursoForestalEsp(Integer idProdRecursoForestalEsp) {
        this.idProdRecursoForestalEsp = idProdRecursoForestalEsp;
    }

    public String getIdEspecie() {
        return idEspecie;
    }

    public void setIdEspecie(String idEspecie) {
        this.idEspecie = idEspecie;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public Integer getIdProdRecursoForestal() {
        return idProdRecursoForestal;
    }

    public void setIdProdRecursoForestal(Integer idProdRecursoForestal) {
        this.idProdRecursoForestal = idProdRecursoForestal;
    }

    public List<ProduccionRecursoForestalDetalleEntity> getListProduccionRecursoForestalDetalle() {
        return listProduccionRecursoForestalDetalle;
    }

    public void setListProduccionRecursoForestalDetalle(List<ProduccionRecursoForestalDetalleEntity> listProduccionRecursoForestalDetalle) {
        this.listProduccionRecursoForestalDetalle = listProduccionRecursoForestalDetalle;
    }
}
