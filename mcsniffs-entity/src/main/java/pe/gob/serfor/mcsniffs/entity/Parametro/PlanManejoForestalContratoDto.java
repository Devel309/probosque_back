package pe.gob.serfor.mcsniffs.entity.Parametro;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class PlanManejoForestalContratoDto {
    private Integer idContrato;
    private String codigoTituloH;
    private String descripcion;
    private String codigoTipoPersonaTitular;
    private String descripcionTipoPersonaTitular;
    private String codigoTipoDocIdentidadTitular;
    private String descripcionTipoDocIdentidadTitular;
    private String numeroDocumentoTitular;
    private String nombreTitular;
    private String apePaternoTitular;
    private String apeMaternoTitular;
    private String razonSocialTitular;
    private String domicilioLegalTitular;
    private String correoTitular;
    private String codigoTipoDocIdentidadRepLegal;
    private String descripcionTipoDocIdentidadRepLegal;
    private String numeroDocumentoRepLegal;
    private String nombreRepresentanteLegal;
    private String apePaternoRepLegal;
    private String apeMaternoRepLegal;
    private String correoRepLegal;
    private String codigoPartidaReg;
    private String numeroContratoConcesion;
    private Boolean firmado;
    private Boolean datosValidados;
    private Integer estadoProceso;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
    private Date fechaIniContrato;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")
    private Date fechaFinContrato;

    public Integer getIdContrato() {
        return idContrato;
    }

    public PlanManejoForestalContratoDto setIdContrato(Integer idContrato) {
        this.idContrato = idContrato;
        return this;
    }

    public String getCodigoTituloH() {
        return codigoTituloH;
    }

    public PlanManejoForestalContratoDto setCodigoTituloH(String codigoTituloH) {
        this.codigoTituloH = codigoTituloH;
        return this;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public PlanManejoForestalContratoDto setDescripcion(String descripcion) {
        this.descripcion = descripcion;
        return this;
    }

    public String getCodigoTipoPersonaTitular() {
        return codigoTipoPersonaTitular;
    }

    public PlanManejoForestalContratoDto setCodigoTipoPersonaTitular(String codigoTipoPersonaTitular) {
        this.codigoTipoPersonaTitular = codigoTipoPersonaTitular;
        return this;
    }

    public String getDescripcionTipoPersonaTitular() {
        return descripcionTipoPersonaTitular;
    }

    public PlanManejoForestalContratoDto setDescripcionTipoPersonaTitular(String descripcionTipoPersonaTitular) {
        this.descripcionTipoPersonaTitular = descripcionTipoPersonaTitular;
        return this;
    }

    public String getCodigoTipoDocIdentidadTitular() {
        return codigoTipoDocIdentidadTitular;
    }

    public PlanManejoForestalContratoDto setCodigoTipoDocumentoTitular(String codigoTipoDocIdentidadTitular) {
        this.codigoTipoDocIdentidadTitular = codigoTipoDocIdentidadTitular;
        return this;
    }

    public String getDescripcionTipoDocIdentidadTitular() { return descripcionTipoDocIdentidadTitular; }

    public PlanManejoForestalContratoDto setDescripcionTipoDocIdentidadTitular(String descripcionTipoDocIdentidadTitular) {
        this.descripcionTipoDocIdentidadTitular = descripcionTipoDocIdentidadTitular;
        return this;
    }

    public String getNumeroDocumentoTitular() {
        return numeroDocumentoTitular;
    }

    public PlanManejoForestalContratoDto setNumeroDocumentoTitular(String numeroDocumentoTitular) {
        this.numeroDocumentoTitular = numeroDocumentoTitular;
        return this;
    }

    public String getNombreTitular() {
        return nombreTitular;
    }

    public PlanManejoForestalContratoDto setNombreTitular(String nombreTitular) {
        this.nombreTitular = nombreTitular;
        return this;
    }

    public String getApePaternoTitular() {
        return apePaternoTitular;
    }

    public PlanManejoForestalContratoDto setApePaternoTitular(String apePaternoTitular) {
        this.apePaternoTitular = apePaternoTitular;
        return this;
    }

    public String getApeMaternoTitular() {
        return apeMaternoTitular;
    }

    public PlanManejoForestalContratoDto setApeMaternoTitular(String apeMaternoTitular) {
        this.apeMaternoTitular = apeMaternoTitular;
        return this;
    }

    public String getRazonSocialTitular() {
        return razonSocialTitular;
    }

    public PlanManejoForestalContratoDto setRazonSocialTitular(String razonSocialTitular) {
        this.razonSocialTitular = razonSocialTitular;
        return this;
    }

    public String getDomicilioLegalTitular() {
        return domicilioLegalTitular;
    }

    public PlanManejoForestalContratoDto setDomicilioLegalTitular(String direccionTitular) {
        this.domicilioLegalTitular = direccionTitular;
        return this;
    }

    public String getCorreoTitular() {
        return correoTitular;
    }

    public PlanManejoForestalContratoDto setCorreoTitular(String correoTitular) {
        this.correoTitular = correoTitular;
        return this;
    }

    public String getCodigoTipoDocIdentidadRepLegal() {
        return codigoTipoDocIdentidadRepLegal;
    }

    public PlanManejoForestalContratoDto setCodigoTipoDocIdentidadRepLegal(String codigoTipoDocIdentidadRepLegal) {
        this.codigoTipoDocIdentidadRepLegal = codigoTipoDocIdentidadRepLegal;
        return this;
    }

    public String getDescripcionTipoDocIdentidadRepLegal() {
        return descripcionTipoDocIdentidadRepLegal;
    }

    public PlanManejoForestalContratoDto setDescripcionTipoDocIdentidadRepLegal(String descripcionTipoDocIdentidadRepLegal) {
        this.descripcionTipoDocIdentidadRepLegal = descripcionTipoDocIdentidadRepLegal;
        return this;
    }

    public String getNumeroDocumentoRepLegal() {
        return numeroDocumentoRepLegal;
    }

    public PlanManejoForestalContratoDto setNumeroDocumentoRepLegal(String documentoRepLegal) {
        this.numeroDocumentoRepLegal = documentoRepLegal;
        return this;
    }

    public String getNombreRepresentanteLegal() {
        return nombreRepresentanteLegal;
    }

    public PlanManejoForestalContratoDto setNombreRepresentanteLegal(String nombreRepLegal) {
        this.nombreRepresentanteLegal = nombreRepLegal;
        return this;
    }

    public String getApePaternoRepLegal() {
        return apePaternoRepLegal;
    }

    public PlanManejoForestalContratoDto setApePaternoRepLegal(String apePaternoRepLegal) {
        this.apePaternoRepLegal = apePaternoRepLegal;
        return this;
    }

    public String getApeMaternoRepLegal() {
        return apeMaternoRepLegal;
    }

    public PlanManejoForestalContratoDto setApeMaternoRepLegal(String apeMaternoRepLegal) {
        this.apeMaternoRepLegal = apeMaternoRepLegal;
        return this;
    }

    public String getCorreoRepLegal() {
        return correoRepLegal;
    }

    public PlanManejoForestalContratoDto setCorreoRepLegal(String correoRepLegal) {
        this.correoRepLegal = correoRepLegal;
        return this;
    }

    public String getCodigoPartidaReg() {
        return codigoPartidaReg;
    }

    public PlanManejoForestalContratoDto setCodigoPartidaReg(String codigoPartidaReg) {
        this.codigoPartidaReg = codigoPartidaReg;
        return this;
    }

    public String getNumeroContratoConcesion() {
        return numeroContratoConcesion;
    }

    public PlanManejoForestalContratoDto setNumeroContratoConcesion(String numeroContratoConcesion) {
        this.numeroContratoConcesion = numeroContratoConcesion;
        return this;
    }

    public Boolean getFirmado() {
        return firmado;
    }

    public PlanManejoForestalContratoDto setFirmado(Boolean firmado) {
        this.firmado = firmado;
        return this;
    }

    public Boolean getDatosValidados() {
        return datosValidados;
    }

    public PlanManejoForestalContratoDto setDatosValidados(Boolean datosValidados) {
        this.datosValidados = datosValidados;
        return this;
    }

    public Integer getEstadoProceso() {
        return estadoProceso;
    }

    public PlanManejoForestalContratoDto setEstadoProceso(Integer estadoProceso) {
        this.estadoProceso = estadoProceso;
        return this;
    }

    public Date getFechaIniContrato() {
        return fechaIniContrato;
    }

    public PlanManejoForestalContratoDto setFechaIniContrato(Date fechaIniContrato) {
        this.fechaIniContrato = fechaIniContrato;
        return this;
    }

    public Date getFechaFinContrato() {
        return fechaFinContrato;
    }

    public PlanManejoForestalContratoDto setFechaFinContrato(Date fechaFinContrato) {
        this.fechaFinContrato = fechaFinContrato;
        return this;
    }
}
