package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RecursoForestalEntity extends AuditoriaEntity implements Serializable {
    private Integer idRecursoForestal;
    private String codigoTipoRecursoForestal;
    private Integer idPlanManejo;
    private Timestamp fechaRealizacionInventario;
    private String descripcionFechaRealizacion;

    public String getDescripcionFechaRealizacion() {
        return descripcionFechaRealizacion;
    }

    public void setDescripcionFechaRealizacion(String descripcionFechaRealizacion) {
        this.descripcionFechaRealizacion = descripcionFechaRealizacion;
    }

    public Timestamp getFechaRealizacionInventario() {
        return fechaRealizacionInventario;
    }

    public void setFechaRealizacionInventario(Timestamp fechaRealizacionInventario) {
        this.fechaRealizacionInventario = fechaRealizacionInventario;
    }

    private List<RecursoForestalDetalleEntity> listRecursoForestal;

    public RecursoForestalEntity(){
        this.listRecursoForestal = new ArrayList<>();
    }

    public Integer getIdRecursoForestal() {
        return idRecursoForestal;
    }

    public void setIdRecursoForestal(Integer idRecursoForestal) {
        this.idRecursoForestal = idRecursoForestal;
    }

    public String getCodigoTipoRecursoForestal() {
        return codigoTipoRecursoForestal;
    }

    public void setCodigoTipoRecursoForestal(String codigoTipoRecursoForestal) {
        this.codigoTipoRecursoForestal = codigoTipoRecursoForestal;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public List<RecursoForestalDetalleEntity> getListRecursoForestal() {
        return listRecursoForestal;
    }

    public RecursoForestalEntity setListRecursoForestal(List<RecursoForestalDetalleEntity> listRecursoForestal) {
        this.listRecursoForestal = listRecursoForestal;
        return this;
    }
}
