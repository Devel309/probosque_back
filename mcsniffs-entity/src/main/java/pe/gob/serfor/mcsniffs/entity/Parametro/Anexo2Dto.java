package pe.gob.serfor.mcsniffs.entity.Parametro;

public class Anexo2Dto {
    String unidadTrabajo;
    String numeroFaja;
    String nArbol;
    String codigo;
    String nombreEspecies;
    String dap;
    String alturaComercial;
    String volumen;
    String calidadFuste;
    String este;
    String norte;
    String categoria;
    String idCensoForestalDetalle;
    String c;
    String nombreComun;
    String nombreCientifico;
    String nombreNativo;
    String producto;
    String condicion;
    String tipoRecurso;
    Integer nArbolesAprovechables;
    Integer nArbolesSemilleros;
    Integer nTotalArboles;
    String unidadMedida;
    String cantidadProducto;
    String  codEspecie;
    Integer numeroDeParcelas;
    Integer cantidadDeEspecies;
    String nombreParcelaCorta;
    String aprov;
    String dmc;
    public String getUnidadTrabajo() {
        return unidadTrabajo;
    }

    public void setUnidadTrabajo(String unidadTrabajo) {
        this.unidadTrabajo = unidadTrabajo;
    }

    public String getNumeroFaja() {
        return numeroFaja;
    }

    public void setNumeroFaja(String numeroFaja) {
        this.numeroFaja = numeroFaja;
    }

    public String getnArbol() {
        return nArbol;
    }

    public void setnArbol(String nArbol) {
        this.nArbol = nArbol;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombreEspecies() {
        return nombreEspecies;
    }

    public void setNombreEspecies(String nombreEspecies) {
        this.nombreEspecies = nombreEspecies;
    }

    public String getDap() {
        return dap;
    }

    public void setDap(String dap) {
        this.dap = dap;
    }

    public String getAlturaComercial() {
        return alturaComercial;
    }

    public void setAlturaComercial(String alturaComercial) {
        this.alturaComercial = alturaComercial;
    }

    public String getVolumen() {
        return volumen;
    }

    public void setVolumen(String volumen) {
        this.volumen = volumen;
    }

    public String getCalidadFuste() {
        return calidadFuste;
    }

    public void setCalidadFuste(String calidadFuste) {
        this.calidadFuste = calidadFuste;
    }

    public String getEste() {
        return este;
    }

    public void setEste(String este) {
        this.este = este;
    }

    public String getNorte() {
        return norte;
    }

    public void setNorte(String norte) {
        this.norte = norte;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getIdCensoForestalDetalle() {
        return idCensoForestalDetalle;
    }

    public void setIdCensoForestalDetalle(String idCensoForestalDetalle) {
        this.idCensoForestalDetalle = idCensoForestalDetalle;
    }
    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }
    public String getNombreComun() {
        return nombreComun;
    }

    public void setNombreComun(String nombreComun) {
        this.nombreComun = nombreComun;
    }

    public String getNombreCientifico() {
        return nombreCientifico;
    }

    public void setNombreCientifico(String nombreCientifico) {
        this.nombreCientifico = nombreCientifico;
    }

    public String getNombreNativo() {
        return nombreNativo;
    }

    public void setNombreNativo(String nombreNativo) {
        this.nombreNativo = nombreNativo;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getCondicion() {
        return condicion;
    }

    public void setCondicion(String condicion) {
        this.condicion = condicion;
    }

    public String getTipoRecurso() {
        return tipoRecurso;
    }

    public void setTipoRecurso(String tipoRecurso) {
        this.tipoRecurso = tipoRecurso;
    }
    public Integer getnArbolesAprovechables() {
        return nArbolesAprovechables;
    }

    public void setnArbolesAprovechables(Integer nArbolesAprovechables) {
        this.nArbolesAprovechables = nArbolesAprovechables;
    }

    public Integer getnArbolesSemilleros() {
        return nArbolesSemilleros;
    }

    public void setnArbolesSemilleros(Integer nArbolesSemilleros) {
        this.nArbolesSemilleros = nArbolesSemilleros;
    }

    public Integer getnTotalArboles() {
        return nTotalArboles;
    }

    public void setnTotalArboles(Integer nTotalArboles) {
        this.nTotalArboles = nTotalArboles;
    }
    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public String getCantidadProducto() {
        return cantidadProducto;
    }

    public void setCantidadProducto(String cantidadProducto) {
        this.cantidadProducto = cantidadProducto;
    }
    public String getCodEspecie() {
        return codEspecie;
    }

    public void setCodEspecie(String codEspecie) {
        this.codEspecie = codEspecie;
    }
    public Integer getNumeroDeParcelas() {
        return numeroDeParcelas;
    }

    public void setNumeroDeParcelas(Integer numeroDeParcelas) {
        this.numeroDeParcelas = numeroDeParcelas;
    }
    public Integer getCantidadDeEspecies() {
        return cantidadDeEspecies;
    }

    public void setCantidadDeEspecies(Integer cantidadDeEspecies) {
        this.cantidadDeEspecies = cantidadDeEspecies;
    }

    public String getNombreParcelaCorta() {
        return nombreParcelaCorta;
    }

    public void setNombreParcelaCorta(String nombreParcelaCorta) {
        this.nombreParcelaCorta = nombreParcelaCorta;
    }
    public String getAprov() {
        return aprov;
    }

    public void setAprov(String aprov) {
        this.aprov = aprov;
    }
    public String getDmc() {
        return dmc;
    }

    public void setDmc(String dmc) {
        this.dmc = dmc;
    }
}
