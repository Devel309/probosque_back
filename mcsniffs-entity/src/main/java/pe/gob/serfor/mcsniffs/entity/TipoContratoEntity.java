package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class TipoContratoEntity implements Serializable {

    private short tipoContrato;
    private String descripcion;
    public short getTipoContrato() { return tipoContrato; }
    public void setTipoContrato(short tipoContrato) { this.tipoContrato = tipoContrato; }
    public String getDescripcion() { return descripcion; }
    public void setDescripcion(String descripcion) { this.descripcion = descripcion; }

}
