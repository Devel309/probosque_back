package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class TipoRegenteEntity implements Serializable {
    private short idTipoRegente;
    private String descripcion;
    public short getIdTipoRegente() { return idTipoRegente; }
    public void setIdTipoRegente(short idTipoRegente) { this.idTipoRegente = idTipoRegente; }
    public String getDescripcion() { return descripcion; }
    public void setDescripcion(String descripcion) { this.descripcion = descripcion; }


}