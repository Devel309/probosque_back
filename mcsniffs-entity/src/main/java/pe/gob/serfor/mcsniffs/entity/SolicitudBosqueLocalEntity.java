package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.ParameterMode;
import java.io.Serializable;

import java.sql.Date;

@Data
public class SolicitudBosqueLocalEntity  extends AuditoriaEntity implements Serializable {

    private Integer idSolBosqueLocal;
    private Integer idSolicitante;
    private String tipoPersonaSolicitante;
    private String tipoDocumentoSolicitante;
    private String numeroDocumentoSolicitante;
    private String nombreSolicitante;
    private String nombresSolicitante;
    private String apellidoMaternoSolicitante;
    private String apellidoPaternoSolicitante;
    private String gobiernoLocal;
    private String idDistritoSolicitante;
    private String nombreDistritoSolicitante;
    private String nombreProvinciaSolicitante;
    private String nombreDepartamentoSolicitante;
    private String direccionSolicitante;
    private Integer idReprLegal;
    private String tipoPersonaReprLegal;
    private String tipoDocumentoReprLegal;
    private String numeroDocumentoReprLegalUnico;
    private String RepresentanteLegal;
    private String correoElectronicoReprLegal;
    private Integer idDepartamentoReprLegal;
    private Integer idProvinciaReprLegal;
    private String idDistritoReprLegal;
    private String nombreDepartamentoReprLegal;
    private String nombreProvinciaReprLegal;
    private String nombreDistritoReprLegal;
    private Date fechaPresentacion;
    private String resultadoSolicitud;
    private String estado;
    private String estadoSolicitud;
    private String estadoSolicitudTexto;
    private String fechaPresentacionTexto;
    private String perfil;
    private Integer idUsuario;
    private String descripcionTipoPersonaSolicitante;
    private String  numeroDocumento;
    private Integer idPersona;
    private String nombresRepresentante;
    private Integer idDepartamento;
    private Integer idProvincia;
    private Boolean solicitudEnviada;
    private String apellidoPaternoUsuario;
    private String apellidoMaternoUsuario;
    private String nombresUsuario;
    private String telefonoUsuario;
    private Integer idDistritoUsuario;
    private String emailUsuario;
    private String  numeroDocumentoUsuario;
    private String  perfilUsuario;
    private Integer idProvinciaUsuario;
    private Integer idDepartamentoUsuario;
    private String nombreDepartamentoUsuario;
    private Boolean evaluacionGabinete;
    private Boolean evaluacionCampo;
    private Integer bandejaBosqueLocal;
    private Integer idSolBosqueLocalGenerarTH;
    private String codigoTH;
    private Integer idGeometria;

}
