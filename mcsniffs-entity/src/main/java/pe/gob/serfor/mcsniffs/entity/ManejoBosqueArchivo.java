package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ManejoBosqueArchivo extends AuditoriaEntity implements Serializable {
    private Integer idManejoBosqueArchivo;
    private Integer idManejoBosque;
    private ArchivoEntity archivo;

    public Integer getIdManejoBosqueArchivo() {
        return idManejoBosqueArchivo;
    }

    public void setIdManejoBosqueArchivo(Integer idManejoBosqueArchivo) {
        this.idManejoBosqueArchivo = idManejoBosqueArchivo;
    }

    public Integer getIdManejoBosque() {
        return idManejoBosque;
    }

    public void setIdManejoBosque(Integer idManejoBosque) {
        this.idManejoBosque = idManejoBosque;
    }

    public ArchivoEntity getArchivo() {
        return archivo;
    }

    public void setArchivo(ArchivoEntity archivo) {
        this.archivo = archivo;
    }
}
