package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class FaunaEntity extends AuditoriaEntity implements Serializable {
    private Integer idFauna;
    private String tipoFauna;
    private String nombre;
    private String nombreCientifico;
    private String familia;
    private String estatus;
    private String adjunto;
    private String estadoSolicitud;
    private Integer idPlanManejo;
    private String codigoTipo;
    private Integer idArchivo;

    public Integer getIdFauna() {
        return idFauna;
    }

    public void setIdFauna(Integer idFauna) {
        this.idFauna = idFauna;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreCientifico() {
        return nombreCientifico;
    }

    public void setNombreCientifico(String nombreCientifico) {
        this.nombreCientifico = nombreCientifico;
    }

    public String getFamilia() {
        return familia;
    }

    public void setFamilia(String familia) {
        this.familia = familia;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getTipoFauna() {
        return tipoFauna;
    }

    public void setTipoFauna(String tipoFauna) {
        this.tipoFauna = tipoFauna;
    }


    public String getAdjunto() {
        return adjunto;
    }

    public FaunaEntity setAdjunto(String adjunto) {
        this.adjunto = adjunto;
        return this;
    }

    public String getEstadoSolicitud() {
        return estadoSolicitud;
    }

    public FaunaEntity setEstadoSolicitud(String estadoSolicitud) {
        this.estadoSolicitud = estadoSolicitud;
        return this;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public FaunaEntity setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
        return this;
    }

    public String getCodigoTipo() {
        return codigoTipo;
    }

    public FaunaEntity setCodigoTipo(String codigoTipo) {
        this.codigoTipo = codigoTipo;
        return this;
    }

    public Integer getIdArchivo() {
        return idArchivo;
    }

    public FaunaEntity setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
        return this;
    }
}
