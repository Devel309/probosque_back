package pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal;


import lombok.Data;
import lombok.EqualsAndHashCode;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper=false)
// @AllArgsConstructor
// @NoArgsConstructor
// @Entity
// @Table(name = "T_MVD_SOLPLANTACIONFOREST_DET", schema = "dbo")
public class SolPlantacionForestalDetalleEntity extends AuditoriaEntity {

    // @id
// @generatedvalue(strategy = generationtype.identity)
//@column(name = "nu_id_solplantforest_det", updatable = false, nullable = false)    
Integer id_solplantforest_det; 
//@column(name = "nu_id_solplantforest", updatable = false, nullable = false) 
Integer id_solplantforest ;
//@column(name = "tx_espcs_nom_comun", updatable = true, nullable = true) 
String  espcs_nom_comun ;
//@column(name = "tx_espcs_nom_cientifico", updatable = true, nullable = true) 
String  espcs_nom_cientifico ;
//@column(name = "nu_total_arbol_matas_espcs_existentes", updatable = true, nullable = true) 
Integer total_arbol_matas_espcs_existentes ;
//@column(name = "nu_produccion_estimada", updatable = true, nullable = true) 
BigDecimal produccion_estimada ;
//@column(name = "tx_producion_estimada_und_medida", updatable = true, nullable = true) 
String  producion_estimada_und_medida ;

String  coord_este ;
String  coord_norte ;








}
