package pe.gob.serfor.mcsniffs.entity.Dto.Resolucion;

import java.io.Serializable;

import lombok.*;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.ResultadoPP.ResultadoPPDto;
import pe.gob.serfor.mcsniffs.entity.ResultadoPPEntity;
@Getter
@Setter
public class ResolucionResultadoDto extends AuditoriaEntity implements Serializable {

    private ResolucionDto resolucion;
    private ResultadoPPDto resultadoPp;
}
