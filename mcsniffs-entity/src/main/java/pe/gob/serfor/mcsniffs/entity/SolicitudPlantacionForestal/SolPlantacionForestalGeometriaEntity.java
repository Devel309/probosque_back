package pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal;

import java.io.Serializable;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Getter
@Setter
public class SolPlantacionForestalGeometriaEntity extends AuditoriaEntity implements Serializable {
    
    private Integer idSolPlantForestGeometria;
    private Integer idSolPlantForest;
    private Integer idArchivo;
    private String tipoGeometria;
    private String geometry_wkt;
    private Integer srid ;
    private String properties;
    private String nombreCapa;
    private Double area;
    private String codigoGeometria;
    private String codigoSeccion;
    private String codigoSubSeccion;
    private String colorCapa;
    private Integer idUsuarioElimina;
    private Integer idUsuarioRegistro;

}
