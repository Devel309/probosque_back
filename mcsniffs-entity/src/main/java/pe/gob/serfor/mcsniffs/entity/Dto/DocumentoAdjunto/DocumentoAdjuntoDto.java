package pe.gob.serfor.mcsniffs.entity.Dto.DocumentoAdjunto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;


@Getter
@Setter
public class DocumentoAdjuntoDto extends AuditoriaEntity implements Serializable{

    private Integer idDocumentoAdjunto;
    private Integer idProcesoPostulacion;
    private Integer idUsuarioAdjunta;
    private String ruta;
    private String descripcion;
    private Integer idTipoDocumento;
    private String codigoTipoDocumento;
    private String asunto;
    private String nombreDocumento;    
    private Integer idPostulacionPFDM;
    private String nombreGenerado;
    private Boolean conforme;
    private String observacion;    

}
