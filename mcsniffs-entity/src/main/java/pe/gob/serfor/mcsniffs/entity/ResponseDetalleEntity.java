package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.List;

public class ResponseDetalleEntity implements Serializable {
    private String mensaje;
    private List<Object> capas;
    private Integer nroCapasConsultadas;
    private Integer nroCapasConResultado;

    public List<Object> getCapas() {
        return capas;
    }

    public void setCapas(List<Object> capas) {
        this.capas = capas;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Integer getNroCapasConsultadas() {
        return nroCapasConsultadas;
    }

    public void setNroCapasConsultadas(Integer nroCapasConsultadas) {
        this.nroCapasConsultadas = nroCapasConsultadas;
    }

    public Integer getNroCapasConResultado() {
        return nroCapasConResultado;
    }

    public void setNroCapasConResultado(Integer nroCapasConResultado) {
        this.nroCapasConResultado = nroCapasConResultado;
    }
}
