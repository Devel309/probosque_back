package pe.gob.serfor.mcsniffs.entity.Dto.Generico;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ParametroDto {
    private String prefijo;
}
