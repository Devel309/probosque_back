package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;


public class InfraestructuraServicioManejoEntity extends AuditoriaEntity implements Serializable {
    private Integer idInfraServicioManejo;
    private InfraestructuraServicioEntity infraestructuraServicio;
    private String descripcion;
    private AreaManejoEntity areaManejo;

    public Integer getIdInfraServicioManejo() {
        return idInfraServicioManejo;
    }

    public void setIdInfraServicioManejo(Integer idInfraServicioManejo) {
        this.idInfraServicioManejo = idInfraServicioManejo;
    }

    public InfraestructuraServicioEntity getInfraestructuraServicio() {
        return infraestructuraServicio;
    }

    public void setInfraestructuraServicio(InfraestructuraServicioEntity infraestructuraServicio) {
        this.infraestructuraServicio = infraestructuraServicio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public AreaManejoEntity getAreaManejo() {
        return areaManejo;
    }

    public void setAreaManejo(AreaManejoEntity areaManejo) {
        this.areaManejo = areaManejo;
    }
}
