package pe.gob.serfor.mcsniffs.entity;

public class ActividadAprovechamientoParam extends AuditoriaEntity{
    private Integer idActvAprove;
    private Integer idPlanManejo;
    private String codActvAprove;
    private String codSubActvAprove;
    private Integer idActvAproveDet;
    private Integer idActvAproveDetSub;
    
    public Integer getIdActvAprove() {
        return idActvAprove;
    }
    public void setIdActvAprove(Integer idActvAprove) {
        this.idActvAprove = idActvAprove;
    }
    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }
    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }
    public String getCodActvAprove() {
        return codActvAprove;
    }
    public void setCodActvAprove(String codActvAprove) {
        this.codActvAprove = codActvAprove;
    }
    public String getCodSubActvAprove() {
        return codSubActvAprove;
    }
    public void setCodSubActvAprove(String codSubActvAprove) {
        this.codSubActvAprove = codSubActvAprove;
    }
    public Integer getIdActvAproveDet() {
        return idActvAproveDet;
    }
    public void setIdActvAproveDet(Integer idActvAproveDet) {
        this.idActvAproveDet = idActvAproveDet;
    }
    public Integer getIdActvAproveDetSub() {
        return idActvAproveDetSub;
    }
    public void setIdActvAproveDetSub(Integer idActvAproveDetSub) {
        this.idActvAproveDetSub = idActvAproveDetSub;
    }
    
}
