package pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Getter
@Setter
public class SolicitudBosqueLocalEvaluacionDto extends AuditoriaEntity implements Serializable {
    private Integer idBosqueLocalEvaluacion;
    private Integer idSolBosqueLocal;
    private String codigoResultadoEvaluacion;
    private String observacion;
    private Integer idArchivoReporte;

    private List<SolicitudBosqueLocalEvaluacionDto> lista;
    private Boolean evalGabineteObservado;
    private Boolean evalCampoObservado;
}
