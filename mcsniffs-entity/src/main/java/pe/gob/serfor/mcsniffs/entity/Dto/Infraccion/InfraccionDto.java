package pe.gob.serfor.mcsniffs.entity.Dto.Infraccion;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;

@Data
public class InfraccionDto extends AuditoriaEntity implements Serializable {
    Integer idInfraccion ;
    String  descripcion ;
    String  calificacion;
    String  sancionNoMonetaria ;
    String sancionMonetaria ;
    String observancia ;
}
