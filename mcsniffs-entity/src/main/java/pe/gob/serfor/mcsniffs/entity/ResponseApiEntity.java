package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ResponseApiEntity implements Serializable {
    private boolean isSuccess;
    private String message;
    private ResponseDetalleEntity data;

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseDetalleEntity getData() {
        return data;
    }

    public void setData(ResponseDetalleEntity data) {
        this.data = data;
    }
}
