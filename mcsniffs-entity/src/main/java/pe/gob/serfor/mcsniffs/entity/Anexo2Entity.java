package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

import pe.gob.serfor.mcsniffs.entity.CoreCentral.DepartamentoEntity;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.DistritoEntity;
import pe.gob.serfor.mcsniffs.entity.CoreCentral.ProvinciaEntity;

public class Anexo2Entity implements Serializable {
    private String nombreapellidos;
    private String dni;
    private String direccion;
    private String sector;
    private DistritoEntity IdDistrito;
    private ProvinciaEntity IdProvincia;
    private DepartamentoEntity IdDepartamento;
    private String telefono;
    private String celular;
    private String correo;
    private String razonsocialsolicitante;
    private String profesion;
    private String nroregistrocolegioprofesional;
    private String nroregistroserfor;
    private String equipotecnico;
    private String lugarfirma;
    private Integer IdUsuarioRegistro;
    private String CodigoAnexo;
    private Integer IdStatusAnexo;
    

    public Integer getIdStatusAnexo() {
        return IdStatusAnexo;
    }

    public void setIdStatusAnexo(Integer idStatusAnexo) {
        IdStatusAnexo = idStatusAnexo;
    }

    public DistritoEntity getIdDistrito() {
        return IdDistrito;
    }

    public void setIdDistrito(DistritoEntity idDistrito) {
        IdDistrito = idDistrito;
    }

    public ProvinciaEntity getIdProvincia() {
        return IdProvincia;
    }

    public void setIdProvincia(ProvinciaEntity idProvincia) {
        IdProvincia = idProvincia;
    }

    public DepartamentoEntity getIdDepartamento() {
        return IdDepartamento;
    }

    public void setIdDepartamento(DepartamentoEntity idDepartamento) {
        IdDepartamento = idDepartamento;
    }

    public String getNombreapellidos() {
        return nombreapellidos;
    }

    public void setNombreapellidos(String nombreapellidos) {
        this.nombreapellidos = nombreapellidos;
    }

    public String getCodigoAnexo() {
        return CodigoAnexo;
    }

    public void setCodigoAnexo(String codigoAnexo) {
        CodigoAnexo = codigoAnexo;
    }

    public Integer getIdUsuarioRegistro() {
        return IdUsuarioRegistro;
    }

    public void setIdUsuarioRegistro(Integer idUsuarioRegistro) {
        IdUsuarioRegistro = idUsuarioRegistro;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getRazonsocialsolicitante() {
        return razonsocialsolicitante;
    }

    public void setRazonsocialsolicitante(String razonsocialsolicitante) {
        this.razonsocialsolicitante = razonsocialsolicitante;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public String getNroregistrocolegioprofesional() {
        return nroregistrocolegioprofesional;
    }

    public void setNroregistrocolegioprofesional(String nroregistrocolegioprofesional) {
        this.nroregistrocolegioprofesional = nroregistrocolegioprofesional;
    }

    public String getNroregistroserfor() {
        return nroregistroserfor;
    }

    public void setNroregistroserfor(String nroregistroserfor) {
        this.nroregistroserfor = nroregistroserfor;
    }

    public String getEquipotecnico() {
        return equipotecnico;
    }

    public void setEquipotecnico(String equipotecnico) {
        this.equipotecnico = equipotecnico;
    }

    public String getLugarfirma() {
        return lugarfirma;
    }

    public void setLugarfirma(String lugarfirma) {
        this.lugarfirma = lugarfirma;
    }
}
