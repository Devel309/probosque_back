package pe.gob.serfor.mcsniffs.entity;

import com.microsoft.sqlserver.jdbc.Geometry;

import java.io.Serializable;
import java.math.BigDecimal;

public class UnidadAprovechamientoEntity extends AuditoriaEntity implements Serializable {
    private Integer IdUnidadAprovechamiento;
    private PropertiesEntity ua;
    private String Descripcion;
    private Integer IdProcesoOferta;
    private Geometry geojson;

    public Geometry getGeojson() {
        return geojson;
    }

    public void setGeojson(Geometry geojson) {
        this.geojson = geojson;
    }

    public Integer getIdProcesoOferta() {
        return IdProcesoOferta;
    }

    public void setIdProcesoOferta(Integer idProcesoOferta) {
        IdProcesoOferta = idProcesoOferta;
    }
    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public Integer getIdUnidadAprovechamiento() {
        return IdUnidadAprovechamiento;
    }

    public void setIdUnidadAprovechamiento(Integer idUnidadAprovechamiento) {
        IdUnidadAprovechamiento = idUnidadAprovechamiento;
    }

    public PropertiesEntity getUa() {
        return ua;
    }

    public void setUa(PropertiesEntity ua) {
        this.ua = ua;
    }
}
