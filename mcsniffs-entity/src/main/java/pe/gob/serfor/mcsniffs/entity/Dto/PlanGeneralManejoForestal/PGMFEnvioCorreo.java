package pe.gob.serfor.mcsniffs.entity.Dto.PlanGeneralManejoForestal;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class PGMFEnvioCorreo implements Serializable {
    private Integer idPlanManejo;
    private String nombreTitular;
    private String idDepartamento;
    private Integer idUsuario;
    private Date fecha;

}
