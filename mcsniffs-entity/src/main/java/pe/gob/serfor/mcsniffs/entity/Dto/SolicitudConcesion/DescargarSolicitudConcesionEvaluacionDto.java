package pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DescargarSolicitudConcesionEvaluacionDto {

    private String tipoSolicitud;
    private String tipoAnexo;
}
