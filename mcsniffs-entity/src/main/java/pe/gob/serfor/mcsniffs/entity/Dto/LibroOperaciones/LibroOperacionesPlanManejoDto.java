package pe.gob.serfor.mcsniffs.entity.Dto.LibroOperaciones;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;

@Data
public class LibroOperacionesPlanManejoDto extends AuditoriaEntity implements Serializable {
    private Integer idLibroOperaciones;
    private Integer idLibroOperacionesPlanManejo;
    private Integer idPlanManejo;
    private String estado;


}
