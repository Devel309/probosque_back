package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;


public class TipoProcedimientoConcesEntity implements Serializable {
    private short tipoProcedimientoConces;
    private String descripcion;
    public short getTipoProcedimientoConces() { return tipoProcedimientoConces; }
    public void setTipoProcedimientoConces(short tipoProcedimientoConces) { this.tipoProcedimientoConces = tipoProcedimientoConces; }
    public String getDescripcion() { return descripcion; }
    public void setDescripcion(String descripcion) { this.descripcion = descripcion; }


}