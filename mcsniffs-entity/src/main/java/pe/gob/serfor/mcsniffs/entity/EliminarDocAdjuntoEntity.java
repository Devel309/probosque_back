package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.Date;

public class EliminarDocAdjuntoEntity implements Serializable {
    private Integer IdDocumentoAdjunto;
    private Integer IdUsuarioModficacion;

    public Integer getIdDocumentoAdjunto() {
        return IdDocumentoAdjunto;
    }

    public void setIdDocumentoAdjunto(Integer idDocumentoAdjunto) {
        IdDocumentoAdjunto = idDocumentoAdjunto;
    }

    public Integer getIdUsuarioModficacion() {
        return IdUsuarioModficacion;
    }

    public void setIdUsuarioModficacion(Integer idUsuarioModficacion) {
        IdUsuarioModficacion = idUsuarioModficacion;
    }
}
