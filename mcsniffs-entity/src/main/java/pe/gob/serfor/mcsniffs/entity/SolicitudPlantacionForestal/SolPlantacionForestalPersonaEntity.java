package pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;

@Data
public class SolPlantacionForestalPersonaEntity extends AuditoriaEntity implements Serializable {


    private Integer idSolPlantacionForestalPersona;
    private Integer idSolPlantacionForestal;
    private String nombres;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String dni;

}



