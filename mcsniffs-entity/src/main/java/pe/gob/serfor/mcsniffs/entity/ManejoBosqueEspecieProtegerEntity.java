package pe.gob.serfor.mcsniffs.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class ManejoBosqueEspecieProtegerEntity extends AuditoriaEntity implements Serializable {
       private Integer idManBosqueEspPro;
       private Integer idPlanManejo;
       private String idTipoEspecie;
       private String tipoEspecie;
       private short idEspecie;
       private String especie;
       private String justification;
       public Integer getIdManBosqueEspPro() {
              return idManBosqueEspPro;
       }
       public void setIdManBosqueEspPro(Integer idManBosqueEspPro) {
              this.idManBosqueEspPro = idManBosqueEspPro;
       }
       public Integer getIdPlanManejo() {
              return idPlanManejo;
       }
       public void setIdPlanManejo(Integer idPlanManejo) {
              this.idPlanManejo = idPlanManejo;
       }
       public String getIdTipoEspecie() {
              return idTipoEspecie;
       }
       public void setIdTipoEspecie(String idTipoEspecie) {
              this.idTipoEspecie = idTipoEspecie;
       }
       public String getTipoEspecie() {
              return tipoEspecie;
       }
       public void setTipoEspecie(String tipoEspecie) {
              this.tipoEspecie = tipoEspecie;
       }
       public short getIdEspecie() {
              return idEspecie;
       }
       public void setIdEspecie(short idEspecie) {
              this.idEspecie = idEspecie;
       }
       public String getEspecie() {
              return especie;
       }
       public void setEspecie(String especie) {
              this.especie = especie;
       }
       public String getJustification() {
              return justification;
       }
       public void setJustification(String justification) {
              this.justification = justification;
       }

}
