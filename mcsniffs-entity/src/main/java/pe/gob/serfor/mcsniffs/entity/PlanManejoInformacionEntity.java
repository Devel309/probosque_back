package pe.gob.serfor.mcsniffs.entity;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;


@Getter
@Setter
public class PlanManejoInformacionEntity {
    private Integer idPlanManejo;
    private String tipoPlan;
    private String nombreTitular;
    private Timestamp fechaRegistro;
    private String departamento;
    private String provincia;
    private String distrito;
    private BigDecimal areaConcesion;
    private BigDecimal areaAprovechada;
    private String codigoTH;
    private Double superficie;
    private String numeroResolucion;
    private Integer numeroParcela;
    private String numeroDocumento;

    private String tituloHabilitante;

}
