package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import java.io.Serializable;

@Data

public class PlanManejoEspecieGrupoEntity extends AuditoriaEntity implements Serializable{

    private Integer idPlanManejoEspecieGrupo;
    private Integer idPlanManejo;
    private Integer idCodigoEspecie;
    private Integer idGrupoComercial;
    private Integer arbolesArea;
    private String producto;
    private String productoAprovechamiento;
    private String observaciones;
}
