package pe.gob.serfor.mcsniffs.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class PermisoForestalGeometriaEntity extends AuditoriaEntity implements Serializable {
    private Integer idPermisoForestalGeometria;
    private Integer idPermisoForestal;
    private Integer idArchivo;
    private String tipoGeometria;
    private String geometry_wkt;
    private Integer srid ;
    private String nombreCapa;
    private Double area;
    private String codigoGeometria;
    private String codigoSeccion;
    private String codigoSubSeccion;
    private String colorCapa;
}