package pe.gob.serfor.mcsniffs.entity.Parametro;

import java.math.BigDecimal;

public class ListaEspecieDto {
    private Integer nombreComun;
    private Integer nombreCientifico;
    private Integer nombreIdiomaNativo;
    private String este;
    private String norte;
    private Integer numeroArbolesAprovechables;
    private Double volumenComercial;
    private Double superficieComercial;
    private Integer numeroArbolSemilleros;
    private Integer numeroArbolTotal;
    private Integer cantProducto;
    private Integer cantNombreComun;
    private Integer cantNombreCientifico;
    private Integer cantNombreNativo;
    private String nombre;
    private String cientifico;
    private String nativo;
    private Integer parcela;
    private String producto;
    private BigDecimal volumenAprovechable;
    private Integer totalBarricas;
    private String unidadMedida;
    private Integer cantidad;
    private String idCodigoEspecie;
    private String textNombreCientifico;
    private String textNombreComun;
    private String familia;
    public String lineaProduccion;
    private String dmc;
    private String gc;
    private String nombreNativo;
    private String categoria;
    private BigDecimal precio;

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public BigDecimal getVolumenAprovechable() {
        return volumenAprovechable;
    }

    public void setVolumenAprovechable(BigDecimal volumenAprovechable) {
        this.volumenAprovechable = volumenAprovechable;
    }

    public Integer getTotalBarricas() {
        return totalBarricas;
    }

    public void setTotalBarricas(Integer totalBarricas) {
        this.totalBarricas = totalBarricas;
    }

    public Integer getParcela() {
        return parcela;
    }

    public void setParcela(Integer parcela) {
        this.parcela = parcela;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getCientifico() {
        return cientifico;
    }

    public void setCientifico(String cientifico) {
        this.cientifico = cientifico;
    }

    public String getNativo() {
        return nativo;
    }

    public void setNativo(String nativo) {
        this.nativo = nativo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCantNombreComun() {
        return cantNombreComun;
    }

    public void setCantNombreComun(Integer cantNombreComun) {
        this.cantNombreComun = cantNombreComun;
    }

    public Integer getCantNombreCientifico() {
        return cantNombreCientifico;
    }

    public void setCantNombreCientifico(Integer cantNombreCientifico) {
        this.cantNombreCientifico = cantNombreCientifico;
    }

    public Integer getCantNombreNativo() {
        return cantNombreNativo;
    }

    public void setCantNombreNativo(Integer cantNombreNativo) {
        this.cantNombreNativo = cantNombreNativo;
    }

    public Integer getCantProducto() {
        return cantProducto;
    }

    public Double getSuperficieComercial() {
        return superficieComercial;
    }

    public void setSuperficieComercial(Double superficieComercial) {
        this.superficieComercial = superficieComercial;
    }

    public void setCantProducto(Integer cantProducto) {
        this.cantProducto = cantProducto;
    }

    public Integer getNombreComun() {
        return nombreComun;
    }

    public void setNombreComun(Integer nombreComun) {
        this.nombreComun = nombreComun;
    }

    public Integer getNombreCientifico() {
        return nombreCientifico;
    }

    public void setNombreCientifico(Integer nombreCientifico) {
        this.nombreCientifico = nombreCientifico;
    }

    public Integer getNombreIdiomaNativo() {
        return nombreIdiomaNativo;
    }

    public void setNombreIdiomaNativo(Integer nombreIdiomaNativo) {
        this.nombreIdiomaNativo = nombreIdiomaNativo;
    }

    public String getEste() {
        return este;
    }

    public void setEste(String este) {
        this.este = este;
    }

    public String getNorte() {
        return norte;
    }

    public void setNorte(String norte) {
        this.norte = norte;
    }

    public Integer getNumeroArbolesAprovechables() {
        return numeroArbolesAprovechables;
    }

    public void setNumeroArbolesAprovechables(Integer numeroArbolesAprovechables) {
        this.numeroArbolesAprovechables = numeroArbolesAprovechables;
    }

    public Double getVolumenComercial() {
        return volumenComercial;
    }

    public void setVolumenComercial(Double volumenComercial) {
        this.volumenComercial = volumenComercial;
    }

    public Integer getNumeroArbolSemilleros() {
        return numeroArbolSemilleros;
    }

    public void setNumeroArbolSemilleros(Integer numeroArbolSemilleros) {
        this.numeroArbolSemilleros = numeroArbolSemilleros;
    }

    public Integer getNumeroArbolTotal() {
        return numeroArbolTotal;
    }

    public void setNumeroArbolTotal(Integer numeroArbolTotal) {
        this.numeroArbolTotal = numeroArbolTotal;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getIdCodigoEspecie() {
        return idCodigoEspecie;
    }

    public void setIdCodigoEspecie(String idCodigoEspecie) {
        idCodigoEspecie=(idCodigoEspecie.equals("") || idCodigoEspecie.equals("null") ) ? "" : idCodigoEspecie;
        this.idCodigoEspecie = idCodigoEspecie;
    }

    public String getFamilia() {
        return familia;
    }

    public void setFamilia(String familia) {
        familia=(familia.equals("") || familia.equals("null") ) ? "" : familia;
        this.familia = familia;
    }

    public String getDmc() {
        return dmc;
    }

    public void setDmc(String dmc) {
        dmc=(dmc.equals("") || dmc.equals("null") ) ? "0" : dmc;
        this.dmc = dmc;
    }
    public String getTextNombreCientifico() {
        return textNombreCientifico;
    }

    public void setTextNombreCientifico(String textNombreCientifico) {
        textNombreCientifico=(textNombreCientifico.equals("") || textNombreCientifico.equals("null") ) ? "" : textNombreCientifico;
        this.textNombreCientifico = textNombreCientifico;
    }

    public String getTextNombreComun() {
        return textNombreComun;
    }

    public void setTextNombreComun(String textNombreComun) {
        textNombreComun=(textNombreComun.equals("") || textNombreComun.equals("null") ) ? "" : textNombreComun;
        this.textNombreComun = textNombreComun;
    }

    public String getLineaProduccion() {
        return lineaProduccion;
    }

    public void setLineaProduccion(String lineaProduccion) {
        this.lineaProduccion = lineaProduccion;
    }
    public String getGc() {
        return gc;
    }

    public void setGc(String gc) {
        this.gc = gc;
    }

    public String getNombreNativo() {
        return nombreNativo;
    }

    public void setNombreNativo(String nombreNativo) {
        this.nombreNativo = nombreNativo;
    }
}
