package pe.gob.serfor.mcsniffs.entity.Dto.DocumentoAdjunto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObtenerDocumentoDto {
    
    private Integer idProcesoPostulacion;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String nombres;
    private String tipoDocumento;
    private String nroDocumento;
    private String fechaEvaluacion;



}
