package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class EvaluacionAmbientalEntity extends AuditoriaEntity implements Serializable {
    private Integer idEvaluacionAmbiental;
    private Integer idEvalAprovechamiento;
    private Integer idEvalActividad;
    private Integer idPlanManejo;
    private String valorEvaluacion;
    private String descripcion;
    private String medidasMitigacion;
    private String adjunto;

    private String codTipoEvaluacionAmbiental;
    private String codTipoEvaluacionAmbientalDet;
    private String subCodTipoEvaluacionAmbiental;
    private String nombreAprovechamiento;
    private String nombreActividad;
    private String caracterizacionImpacto;
    private String medidasMonitoreo;

    private String medidaControl;
    private String auxiliar;
    private String responsableSecundario;

    public String getResponsableSecundario() {
        return responsableSecundario;
    }

    public void setResponsableSecundario(String responsableSecundario) {
        this.responsableSecundario = responsableSecundario;
    }

    public String getAuxiliar() {
        return auxiliar;
    }

    public void setAuxiliar(String auxiliar) {
        this.auxiliar = auxiliar;
    }

    public String getMedidaControl() {
        return medidaControl;
    }

    public void setMedidaControl(String medidaControl) {
        this.medidaControl = medidaControl;
    }

    public String getCodTipoEvaluacionAmbientalDet() {
        return codTipoEvaluacionAmbientalDet;
    }

    public void setCodTipoEvaluacionAmbientalDet(String codTipoEvaluacionAmbientalDet) {
        this.codTipoEvaluacionAmbientalDet = codTipoEvaluacionAmbientalDet;
    }

    public String getCodTipoEvaluacionAmbiental() {
        return codTipoEvaluacionAmbiental;
    }

    public void setCodTipoEvaluacionAmbiental(String codTipoEvaluacionAmbiental) {
        this.codTipoEvaluacionAmbiental = codTipoEvaluacionAmbiental;
    }

    public String getSubCodTipoEvaluacionAmbiental() {
        return subCodTipoEvaluacionAmbiental;
    }

    public void setSubCodTipoEvaluacionAmbiental(String subCodTipoEvaluacionAmbiental) {
        this.subCodTipoEvaluacionAmbiental = subCodTipoEvaluacionAmbiental;
    }

    public Integer getIdEvaluacionAmbiental() {
        return idEvaluacionAmbiental;
    }

    public void setIdEvaluacionAmbiental(Integer idEvaluacionAmbiental) {
        this.idEvaluacionAmbiental = idEvaluacionAmbiental;
    }

    public Integer getIdEvalAprovechamiento() {
        return idEvalAprovechamiento;
    }

    public void setIdEvalAprovechamiento(Integer idEvalAprovechamiento) {
        this.idEvalAprovechamiento = idEvalAprovechamiento;
    }

    public Integer getIdEvalActividad() {
        return idEvalActividad;
    }

    public void setIdEvalActividad(Integer idEvalActividad) {
        this.idEvalActividad = idEvalActividad;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public String getValorEvaluacion() {
        return valorEvaluacion;
    }

    public void setValorEvaluacion(String valorEvaluacion) {
        this.valorEvaluacion = valorEvaluacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMedidasMitigacion() {
        return medidasMitigacion;
    }

    public void setMedidasMitigacion(String medidasMitigacion) {
        this.medidasMitigacion = medidasMitigacion;
    }

    public String getAdjunto() {
        return adjunto;
    }

    public void setAdjunto(String adjunto) {
        this.adjunto = adjunto;
    }

    public String getNombreAprovechamiento() {
        return nombreAprovechamiento;
    }

    public void setNombreAprovechamiento(String nombreAprovechamiento) {
        this.nombreAprovechamiento = nombreAprovechamiento;
    }

    public String getNombreActividad() {
        return nombreActividad;
    }

    public void setNombreActividad(String nombreActividad) {
        this.nombreActividad = nombreActividad;
    }

    public String getCaracterizacionImpacto() {
        return caracterizacionImpacto;
    }

    public void setCaracterizacionImpacto(String caracterizacionImpacto) {
        this.caracterizacionImpacto = caracterizacionImpacto;
    }

    public String getMedidasMonitoreo() {
        return medidasMonitoreo;
    }

    public void setMedidasMonitoreo(String medidasMonitoreo) {
        this.medidasMonitoreo = medidasMonitoreo;
    }
}
