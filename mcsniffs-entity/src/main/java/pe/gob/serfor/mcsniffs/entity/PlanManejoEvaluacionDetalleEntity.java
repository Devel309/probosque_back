package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class PlanManejoEvaluacionDetalleEntity extends AuditoriaEntity implements Serializable {
   
    private Integer idPlanManejoEvalDet;
    private Integer idEspecie;
    private String nombreCientifico;
    private String nombreComun;
    private Boolean conforme;
    private String observacion;
    private String consideracion;
    private String consideracionDescripcion;
    private Integer idPlanManejoEval;

    public Integer getIdPlanManejoEvalDet() {
        return idPlanManejoEvalDet;
    }

    public void setIdPlanManejoEvalDet(Integer idPlanManejoEvalDet) {
        this.idPlanManejoEvalDet = idPlanManejoEvalDet;
    }

    public Integer getIdEspecie() {
        return idEspecie;
    }

    public void setIdEspecie(Integer idEspecie) {
        this.idEspecie = idEspecie;
    }

    public String getNombreCientifico() {
        return nombreCientifico;
    }

    public void setNombreCientifico(String nombreCientifico) {
        this.nombreCientifico = nombreCientifico;
    }

    public String getNombreComun() {
        return nombreComun;
    }

    public void setNombreComun(String nombreComun) {
        this.nombreComun = nombreComun;
    }

    public Boolean getConforme() {
        return conforme;
    }

    public void setConforme(Boolean conforme) {
        this.conforme = conforme;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getConsideracion() {
        return consideracion;
    }

    public void setConsideracion(String consideracion) {
        this.consideracion = consideracion;
    }

    public String getConsideracionDescripcion() {
        return consideracionDescripcion;
    }

    public void setConsideracionDescripcion(String consideracionDescripcion) {
        this.consideracionDescripcion = consideracionDescripcion;
    }

    public Integer getIdPlanManejoEval() {
        return idPlanManejoEval;
    }

    public void setIdPlanManejoEval(Integer idPlanManejoEval) {
        this.idPlanManejoEval = idPlanManejoEval;
    }
}