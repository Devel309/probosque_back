package pe.gob.serfor.mcsniffs.entity.Dto.Fiscalizacion;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity; 

@Getter
@Setter
public class FiscalizacionArchivoDto extends  AuditoriaEntity implements Serializable {
    
    private Integer idFiscalizacionArchivo;
    private Integer idFiscalizacion;
    private Integer idArchivo;
    private String descripcion;
    private String asunto;
    private String tipoDocumento;
    private String nombre;
 
}
