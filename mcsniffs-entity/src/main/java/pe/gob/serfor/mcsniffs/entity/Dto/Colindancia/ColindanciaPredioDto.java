package pe.gob.serfor.mcsniffs.entity.Dto.Colindancia;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
@Getter
@Setter
public class ColindanciaPredioDto extends  AuditoriaEntity implements Serializable{
    
    private Integer idColindancia;
    private Integer idPlanManejo;
    private String codTipoColindancia;
    private String descripcionColindante;
    private String descripcionLimite;
    private String tipoColindancia;
}
