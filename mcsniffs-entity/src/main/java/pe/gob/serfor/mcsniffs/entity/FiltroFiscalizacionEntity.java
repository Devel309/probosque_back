package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

@Data
public class FiltroFiscalizacionEntity extends AuditoriaEntity {
    private Integer IdProcesoPostulacion;
    private String CodigoTituloH;
    private Boolean ConObservacion;
    private Integer IdFiscalizacion;
    private String codigoEstadoContrato;
    
}
