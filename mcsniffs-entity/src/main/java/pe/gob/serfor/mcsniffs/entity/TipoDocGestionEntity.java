package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class TipoDocGestionEntity implements Serializable {
    private short idTipoDocGestion;
    private String descripcion;
    private String sigla;

    public short getIdTipoDocGestion() { return idTipoDocGestion; }
    public void setIdTipoDocGestion(short idTipoDocGestion) { this.idTipoDocGestion = idTipoDocGestion; }
    public String getDescripcion() { return descripcion; }
    public void setDescripcion(String descripcion) { this.descripcion = descripcion; }
    public String getSigla() { return sigla; }
    public void setSigla(String sigla) { this.sigla = sigla; }
}
