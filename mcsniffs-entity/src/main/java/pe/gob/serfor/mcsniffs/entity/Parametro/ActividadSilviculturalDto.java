package pe.gob.serfor.mcsniffs.entity.Parametro;

import pe.gob.serfor.mcsniffs.entity.ActividadSilviculturalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ActividadSilviculturalDto extends AuditoriaEntity implements Serializable {
    private Integer idPlanManejo;
    private Integer idActividadSilvicultural;

    private String codActividad;
    private String actividad;
    private String monitoreo;
    private String anexo;
    private String descripcion;
    private String observacion;
    private List<ActividadSilviculturalDetalleEntity> detalle;

    public ActividadSilviculturalDto(){
        this.detalle = new ArrayList<>();
    }

    public Integer getIdActividadSilvicultural() {
        return idActividadSilvicultural;
    }

    public ActividadSilviculturalDto setIdActividadSilvicultural(Integer idActividadSilvicultural) {
        this.idActividadSilvicultural = idActividadSilvicultural;
        return this;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public ActividadSilviculturalDto setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
        return this;
    }

    public String getCodActividad() {
        return codActividad;
    }

    public ActividadSilviculturalDto setCodActividad(String codActividad) {
        this.codActividad = codActividad;
        return this;
    }

    public String getActividad() {
        return actividad;
    }

    public ActividadSilviculturalDto setActividad(String actividad) {
        this.actividad = actividad;
        return this;
    }

    public String getMonitoreo() {
        return monitoreo;
    }

    public ActividadSilviculturalDto setMonitoreo(String monitoreo) {
        this.monitoreo = monitoreo;
        return this;
    }

    public String getAnexo() {
        return anexo;
    }

    public ActividadSilviculturalDto setAnexo(String anexo) {
        this.anexo = anexo;
        return this;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public ActividadSilviculturalDto setDescripcion(String descripcion) {
        this.descripcion = descripcion;
        return this;
    }

    public String getObservacion() {
        return observacion;
    }

    public ActividadSilviculturalDto setObservacion(String observacion) {
        this.observacion = observacion;
        return this;
    }

    public List<ActividadSilviculturalDetalleEntity> getDetalle() {
        return this.detalle;
    }

    public void setDetalle(List<ActividadSilviculturalDetalleEntity> detalle) {
        this.detalle = detalle;
    }
}
