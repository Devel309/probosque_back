package pe.gob.serfor.mcsniffs.entity.Dto.Oposicion;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;


@Getter
@Setter
public class OposicionArchivoDTO extends AuditoriaEntity implements Serializable {
    private Integer idOposicionArchivo;
    private Integer idOposicion;
    private Integer idArchivo;
    private Boolean resolucion;
    private String asunto;
    private Date fechaEmision;
    private String tipoDocumento;
    private String nombre;
    private String descripcion;
    private Boolean conformidad;
        
}
