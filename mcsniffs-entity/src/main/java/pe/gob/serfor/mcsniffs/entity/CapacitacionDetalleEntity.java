package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class CapacitacionDetalleEntity extends AuditoriaEntity implements Serializable {

    private Integer idCapacitacionDet;
    private String descripcion;

    private String actividad;
    private String personal;
    private String modalidad;
    private String lugar;
    private String periodo;
    private String responsable;
    private CapacitacionEntity capacitacion;

    public Integer getIdCapacitacionDet() {
        return idCapacitacionDet;
    }

    public void setIdCapacitacionDet(Integer idCapacitacionDet) {
        this.idCapacitacionDet = idCapacitacionDet;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getPersonal() {
        return personal;
    }

    public void setPersonal(String personal) {
        this.personal = personal;
    }

    public String getModalidad() {
        return modalidad;
    }

    public void setModalidad(String modalidad) {
        this.modalidad = modalidad;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public CapacitacionEntity getCapacitacion() {
        return capacitacion;
    }

    public void setCapacitacion(CapacitacionEntity capacitacion) {
        this.capacitacion = capacitacion;
    }
}
