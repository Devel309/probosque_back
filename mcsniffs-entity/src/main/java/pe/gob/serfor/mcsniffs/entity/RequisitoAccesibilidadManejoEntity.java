package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;


public class RequisitoAccesibilidadManejoEntity extends AuditoriaEntity implements Serializable {
    private Integer idReqAcceManejo;
    private Integer idAcceManejo;
    private String descripcion;

    public Integer getIdReqAcceManejo() {
        return idReqAcceManejo;
    }

    public void setIdReqAcceManejo(Integer idReqAcceManejo) {
        this.idReqAcceManejo = idReqAcceManejo;
    }

    public Integer getIdAcceManejo() {
        return idAcceManejo;
    }

    public void setIdAcceManejo(Integer idAcceManejo) {
        this.idAcceManejo = idAcceManejo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
