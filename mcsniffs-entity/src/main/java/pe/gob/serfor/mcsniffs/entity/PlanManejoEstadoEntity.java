package pe.gob.serfor.mcsniffs.entity;

public class PlanManejoEstadoEntity extends AuditoriaEntity{
    private Integer idPlanManejoEstado;
    private String codigoProceso;
    private String codigo;
    private String descripcion;
    private String observacion;
    private String codigoEstado;
    private Integer idPlanManejo;
    private String descripcionCodigoEstado;
    private String descripcionCodigo;


    
    public String getDescripcionCodigoEstado() {
        return descripcionCodigoEstado;
    }
    public void setDescripcionCodigoEstado(String descripcionCodigoEstado) {
        this.descripcionCodigoEstado = descripcionCodigoEstado;
    }
    public String getDescripcionCodigo() {
        return descripcionCodigo;
    }
    public void setDescripcionCodigo(String descripcionCodigo) {
        this.descripcionCodigo = descripcionCodigo;
    }
    public Integer getIdPlanManejoEstado() {
        return idPlanManejoEstado;
    }
    public void setIdPlanManejoEstado(Integer idPlanManejoEstado) {
        this.idPlanManejoEstado = idPlanManejoEstado;
    }
    public String getCodigoProceso() {
        return codigoProceso;
    }
    public void setCodigoProceso(String codigoProceso) {
        this.codigoProceso = codigoProceso;
    }
    public String getCodigo() {
        return codigo;
    }
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String getObservacion() {
        return observacion;
    }
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    public String getCodigoEstado() {
        return codigoEstado;
    }
    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }
    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }
    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }
    
}
