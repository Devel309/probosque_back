package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class OposicionRequestEntity extends AuditoriaEntity implements Serializable {
    private Integer idProcesoPostulacion;
    private Boolean fundada;
    private Integer idOposicion;
    private Integer idUsuarioPostulacion;
    private Integer idEstado;
    private Integer numDocumentoGestion;
    private String tipoDocumentoGestion;
}
