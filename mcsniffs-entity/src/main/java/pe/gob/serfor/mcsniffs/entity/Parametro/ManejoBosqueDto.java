package pe.gob.serfor.mcsniffs.entity.Parametro;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.math.BigDecimal;

public class ManejoBosqueDto extends AuditoriaEntity implements Serializable {
    private Integer idManejoBosque;
    private String tipo;
    private String codigoManejo;
    private String descripcionCab;
    private Integer idPlanManejo;

    private Integer idManejoBosqueDet;
    private String codtipoManejoDet ;
            //ACTIVIDAD
    private String catOrdenamiento ;
    private String ordenamiento ;
    private String actividad ;
    private String codOpcion ;
    private String descripcionOrd ;
            //APROVECHAMIENTO
//1
    private String idtipoMetodoAprov ;
    private String tipoMetodoAprov ;
            //2.1
    private String idtipoTransMaderable ;
    private String tipoTransMaderable ;
            //2.2
    private String idtipoTransDestino ;
    private String tipoTransDestino ;
            //2.3
    private BigDecimal nuCaminoAcceso ;
    private BigDecimal nuCaminoPrincipal ;
    private String descripcionCamino ;
    private String descripcionOtro ;
            //3
    private Integer nuIdOperaciones ;
    private String manoObra ;
    private String maquina ;
    private String descripcionManoObra ;

            //ESPECIES
    private Integer nuIdEspecie ;
    private String especie ;
    private String justificacionEspecie ;
            //SILVICULTURAL
    private String tratamientoSilvicultural ;
    private String descripcionTratamiento ;
    private String Reforestacion ;

    private String infraestructura;
    private String caracteristicasTecnicas;
    private String metodoConstruccion;

    private String descripcionOtros;
    private Integer idOperacion;
    private String lineaProduccion;
    private BigDecimal nuDiametro;
    private Boolean accion;
    private String tipoEspecie;

    public String getDescripcionOtros() {
        return descripcionOtros;
    }

    public void setDescripcionOtros(String descripcionOtros) {
        this.descripcionOtros = descripcionOtros;
    }

    public Integer getIdOperacion() {
        return idOperacion;
    }

    public void setIdOperacion(Integer idOperacion) {
        this.idOperacion = idOperacion;
    }

    public String getLineaProduccion() {
        return lineaProduccion;
    }

    public void setLineaProduccion(String lineaProduccion) {
        this.lineaProduccion = lineaProduccion;
    }

    public BigDecimal getNuDiametro() {
        return nuDiametro;
    }

    public void setNuDiametro(BigDecimal nuDiametro) {
        this.nuDiametro = nuDiametro;
    }

    public Boolean getAccion() {
        return accion;
    }

    public void setAccion(Boolean accion) {
        this.accion = accion;
    }

    public String getTipoEspecie() {
        return tipoEspecie;
    }

    public void setTipoEspecie(String tipoEspecie) {
        this.tipoEspecie = tipoEspecie;
    }

    public String getInfraestructura() {
        return infraestructura;
    }

    public void setInfraestructura(String infraestructura) {
        this.infraestructura = infraestructura;
    }

    public String getCaracteristicasTecnicas() {
        return caracteristicasTecnicas;
    }

    public void setCaracteristicasTecnicas(String caracteristicasTecnicas) {
        this.caracteristicasTecnicas = caracteristicasTecnicas;
    }

    public String getMetodoConstruccion() {
        return metodoConstruccion;
    }

    public void setMetodoConstruccion(String metodoConstruccion) {
        this.metodoConstruccion = metodoConstruccion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCodigoManejo() {
        return codigoManejo;
    }

    public void setCodigoManejo(String codigoManejo) {
        this.codigoManejo = codigoManejo;
    }

    public String getDescripcionCab() {
        return descripcionCab;
    }

    public void setDescripcionCab(String descripcionCab) {
        this.descripcionCab = descripcionCab;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public Integer getIdManejoBosqueDet() {
        return idManejoBosqueDet;
    }

    public void setIdManejoBosqueDet(Integer idManejoBosqueDet) {
        this.idManejoBosqueDet = idManejoBosqueDet;
    }

    public Integer getIdManejoBosque() {
        return idManejoBosque;
    }

    public void setIdManejoBosque(Integer idManejoBosque) {
        this.idManejoBosque = idManejoBosque;
    }

    public String getCodtipoManejoDet() {
        return codtipoManejoDet;
    }

    public void setCodtipoManejoDet(String codtipoManejoDet) {
        this.codtipoManejoDet = codtipoManejoDet;
    }

    public String getCatOrdenamiento() {
        return catOrdenamiento;
    }

    public void setCatOrdenamiento(String catOrdenamiento) {
        this.catOrdenamiento = catOrdenamiento;
    }

    public String getOrdenamiento() {
        return ordenamiento;
    }

    public void setOrdenamiento(String ordenamiento) {
        this.ordenamiento = ordenamiento;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getCodOpcion() {
        return codOpcion;
    }

    public void setCodOpcion(String codOpcion) {
        this.codOpcion = codOpcion;
    }

    public String getDescripcionOrd() {
        return descripcionOrd;
    }

    public void setDescripcionOrd(String descripcionOrd) {
        this.descripcionOrd = descripcionOrd;
    }

    public String getIdtipoMetodoAprov() {
        return idtipoMetodoAprov;
    }

    public void setIdtipoMetodoAprov(String idtipoMetodoAprov) {
        this.idtipoMetodoAprov = idtipoMetodoAprov;
    }

    public String getTipoMetodoAprov() {
        return tipoMetodoAprov;
    }

    public void setTipoMetodoAprov(String tipoMetodoAprov) {
        this.tipoMetodoAprov = tipoMetodoAprov;
    }

    public String getIdtipoTransMaderable() {
        return idtipoTransMaderable;
    }

    public void setIdtipoTransMaderable(String idtipoTransMaderable) {
        this.idtipoTransMaderable = idtipoTransMaderable;
    }

    public String getTipoTransMaderable() {
        return tipoTransMaderable;
    }

    public void setTipoTransMaderable(String tipoTransMaderable) {
        this.tipoTransMaderable = tipoTransMaderable;
    }

    public String getIdtipoTransDestino() {
        return idtipoTransDestino;
    }

    public void setIdtipoTransDestino(String idtipoTransDestino) {
        this.idtipoTransDestino = idtipoTransDestino;
    }

    public String getTipoTransDestino() {
        return tipoTransDestino;
    }

    public void setTipoTransDestino(String tipoTransDestino) {
        this.tipoTransDestino = tipoTransDestino;
    }

    public BigDecimal getNuCaminoAcceso() {
        return nuCaminoAcceso;
    }

    public void setNuCaminoAcceso(BigDecimal nuCaminoAcceso) {
        this.nuCaminoAcceso = nuCaminoAcceso;
    }

    public BigDecimal getNuCaminoPrincipal() {
        return nuCaminoPrincipal;
    }

    public void setNuCaminoPrincipal(BigDecimal nuCaminoPrincipal) {
        this.nuCaminoPrincipal = nuCaminoPrincipal;
    }

    public String getDescripcionCamino() {
        return descripcionCamino;
    }

    public void setDescripcionCamino(String descripcionCamino) {
        this.descripcionCamino = descripcionCamino;
    }

    public String getDescripcionOtro() {
        return descripcionOtro;
    }

    public void setDescripcionOtro(String descripcionOtro) {
        this.descripcionOtro = descripcionOtro;
    }

    public Integer getNuIdOperaciones() {
        return nuIdOperaciones;
    }

    public void setNuIdOperaciones(Integer nuIdOperaciones) {
        this.nuIdOperaciones = nuIdOperaciones;
    }

    public String getManoObra() {
        return manoObra;
    }

    public void setManoObra(String manoObra) {
        this.manoObra = manoObra;
    }

    public String getMaquina() {
        return maquina;
    }

    public void setMaquina(String maquina) {
        this.maquina = maquina;
    }

    public String getDescripcionManoObra() {
        return descripcionManoObra;
    }

    public void setDescripcionManoObra(String descripcionManoObra) {
        this.descripcionManoObra = descripcionManoObra;
    }

    public Integer getNuIdEspecie() {
        return nuIdEspecie;
    }

    public void setNuIdEspecie(Integer nuIdEspecie) {
        this.nuIdEspecie = nuIdEspecie;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getJustificacionEspecie() {
        return justificacionEspecie;
    }

    public void setJustificacionEspecie(String justificacionEspecie) {
        this.justificacionEspecie = justificacionEspecie;
    }

    public String getTratamientoSilvicultural() {
        return tratamientoSilvicultural;
    }

    public void setTratamientoSilvicultural(String tratamientoSilvicultural) {
        this.tratamientoSilvicultural = tratamientoSilvicultural;
    }

    public String getDescripcionTratamiento() {
        return descripcionTratamiento;
    }

    public void setDescripcionTratamiento(String descripcionTratamiento) {
        this.descripcionTratamiento = descripcionTratamiento;
    }

    public String getReforestacion() {
        return Reforestacion;
    }

    public void setReforestacion(String reforestacion) {
        Reforestacion = reforestacion;
    }
}
