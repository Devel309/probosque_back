package pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DescargarSolicitudConcesionVerticesDto {

    private String vertice;
    private String este;
    private String norte;

}
