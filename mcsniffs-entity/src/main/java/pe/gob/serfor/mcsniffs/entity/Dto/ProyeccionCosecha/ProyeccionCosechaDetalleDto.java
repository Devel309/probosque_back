package pe.gob.serfor.mcsniffs.entity.Dto.ProyeccionCosecha;

import lombok.Data;

@Data
public class ProyeccionCosechaDetalleDto {
    Integer anio;
    Double valor;
}
