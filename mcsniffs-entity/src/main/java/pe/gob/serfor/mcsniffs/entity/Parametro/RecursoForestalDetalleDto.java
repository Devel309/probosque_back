package pe.gob.serfor.mcsniffs.entity.Parametro;

import com.fasterxml.jackson.annotation.JsonFormat;
import pe.gob.serfor.mcsniffs.entity.ActividadSilviculturalDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class RecursoForestalDetalleDto extends AuditoriaEntity implements Serializable {
    private Integer idPlanManejo;
    private Integer idRecursoForestal;
    private Integer idRecursoForestalDetalle;
    private String codigoTipoRecursoForestal;
    private String codCabecera;
    private String nombreComun;
    private String nombreCientifico;
    private String nombreNativo;
    private String coordenadaEste;
    private String coordenadaNorte;
    private Integer numeroArbolesAprovechables;
    private String volumenComercial;
    private Integer numeroArbolesSemilleros;
    private Integer numeroArbolesTotal;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Timestamp fechaRealizacionInventario;
    private Integer numParcela;
    private String producto;
    private String uniMedida;
    private String descripcionFechaRealizacion;
    private Integer cantidad;

    public Integer getIdRecursoForestalDetalle() {
        return idRecursoForestalDetalle;
    }

    public void setIdRecursoForestalDetalle(Integer idRecursoForestalDetalle) {
        this.idRecursoForestalDetalle = idRecursoForestalDetalle;
    }

    public Integer getNumeroArbolesAprovechables() {
        return numeroArbolesAprovechables;
    }

    public void setNumeroArbolesAprovechables(Integer numeroArbolesAprovechables) {
        this.numeroArbolesAprovechables = numeroArbolesAprovechables;
    }

    public Integer getNumeroArbolesSemilleros() {
        return numeroArbolesSemilleros;
    }

    public void setNumeroArbolesSemilleros(Integer numeroArbolesSemilleros) {
        this.numeroArbolesSemilleros = numeroArbolesSemilleros;
    }

    public Integer getNumeroArbolesTotal() {
        return numeroArbolesTotal;
    }

    public void setNumeroArbolesTotal(Integer numeroArbolesTotal) {
        this.numeroArbolesTotal = numeroArbolesTotal;
    }

    public Timestamp getFechaRealizacionInventario() {
        return fechaRealizacionInventario;
    }

    public void setFechaRealizacionInventario(Timestamp fechaRealizacionInventario) {
        this.fechaRealizacionInventario = fechaRealizacionInventario;
    }

    public Integer getNumParcela() {
        return numParcela;
    }

    public void setNumParcela(Integer numParcela) {
        this.numParcela = numParcela;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public Integer getIdRecursoForestal() {
        return idRecursoForestal;
    }

    public void setIdRecursoForestal(Integer idRecursoForestal) {
        this.idRecursoForestal = idRecursoForestal;
    }

    public String getCodigoTipoRecursoForestal() {
        return codigoTipoRecursoForestal;
    }

    public void setCodigoTipoRecursoForestal(String codigoTipoRecursoForestal) {
        this.codigoTipoRecursoForestal = codigoTipoRecursoForestal;
    }

    public String getCodCabecera() {
        return codCabecera;
    }

    public void setCodCabecera(String codCabecera) {
        this.codCabecera = codCabecera;
    }

    public String getNombreComun() {
        return nombreComun;
    }

    public void setNombreComun(String nombreComun) {
        this.nombreComun = nombreComun;
    }

    public String getNombreCientifico() {
        return nombreCientifico;
    }

    public void setNombreCientifico(String nombreCientifico) {
        this.nombreCientifico = nombreCientifico;
    }

    public String getNombreNativo() {
        return nombreNativo;
    }

    public void setNombreNativo(String nombreNativo) {
        this.nombreNativo = nombreNativo;
    }

    public String getCoordenadaEste() {
        return coordenadaEste;
    }

    public void setCoordenadaEste(String coordenadaEste) {
        this.coordenadaEste = coordenadaEste;
    }

    public String getCoordenadaNorte() {
        return coordenadaNorte;
    }

    public void setCoordenadaNorte(String coordenadaNorte) {
        this.coordenadaNorte = coordenadaNorte;
    }



    public String getVolumenComercial() {
        return volumenComercial;
    }

    public void setVolumenComercial(String volumenComercial) {
        this.volumenComercial = volumenComercial;
    }



    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getUniMedida() {
        return uniMedida;
    }

    public void setUniMedida(String uniMedida) {
        this.uniMedida = uniMedida;
    }

    public String getDescripcionFechaRealizacion() {
        return descripcionFechaRealizacion;
    }

    public void setDescripcionFechaRealizacion(String descripcionFechaRealizacion) {
        this.descripcionFechaRealizacion = descripcionFechaRealizacion;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }
}
