package pe.gob.serfor.mcsniffs.entity.Parametro;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;

public class ActividadSilviculturalDetalleDto extends AuditoriaEntity implements Serializable {
    private Integer idPlanManejo;
    private Integer idActividadSilvicultural;
    private String codActividad;
    private String actividad;
    private String monitoreo;
    private String anexo;
    private String descripcion;
    private String observacion;
    private Integer idActividadSilviculturalDet;
    private String idTipo;
    private String idTipoTratamiento;
    private String tipoTratamiento;
    private boolean accion;
    private String descripcionDetalle;
    private String tratamiento;
    private String justificacion;
    private boolean accionSilvicultural;

    public boolean isAccionSilvicultural() {
        return accionSilvicultural;
    }

    public void setAccionSilvicultural(boolean accionSilvicultural) {
        this.accionSilvicultural = accionSilvicultural;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public ActividadSilviculturalDetalleDto setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
        return this;
    }

    public Integer getIdActividadSilvicultural() {
        return idActividadSilvicultural;
    }

    public ActividadSilviculturalDetalleDto setIdActividadSilvicultural(Integer idActividadSilvicultural) {
        this.idActividadSilvicultural = idActividadSilvicultural;
        return this;
    }

    public String getCodActividad() {
        return codActividad;
    }

    public ActividadSilviculturalDetalleDto setCodActividad(String codActividad) {
        this.codActividad = codActividad;
        return this;
    }

    public String getActividad() {
        return actividad;
    }

    public ActividadSilviculturalDetalleDto setActividad(String actividad) {
        this.actividad = actividad;
        return this;
    }

    public String getMonitoreo() {
        return monitoreo;
    }

    public ActividadSilviculturalDetalleDto setMonitoreo(String monitoreo) {
        this.monitoreo = monitoreo;
        return this;
    }

    public String getAnexo() {
        return anexo;
    }

    public ActividadSilviculturalDetalleDto setAnexo(String anexo) {
        this.anexo = anexo;
        return this;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public ActividadSilviculturalDetalleDto setDescripcion(String descripcion) {
        this.descripcion = descripcion;
        return this;
    }

    public String getObservacion() {
        return observacion;
    }

    public ActividadSilviculturalDetalleDto setObservacion(String observacion) {
        this.observacion = observacion;
        return this;
    }

    public Integer getIdActividadSilviculturalDet() {
        return idActividadSilviculturalDet;
    }

    public ActividadSilviculturalDetalleDto setIdActividadSilviculturalDet(Integer idActividadSilviculturalDet) {
        this.idActividadSilviculturalDet = idActividadSilviculturalDet;
        return this;
    }

    public String getIdTipo() {
        return idTipo;
    }

    public ActividadSilviculturalDetalleDto setIdTipo(String idTipo) {
        this.idTipo = idTipo;
        return this;
    }

    public String getIdTipoTratamiento() {
        return idTipoTratamiento;
    }

    public ActividadSilviculturalDetalleDto setIdTipoTratamiento(String idTipoTratamiento) {
        this.idTipoTratamiento = idTipoTratamiento;
        return this;
    }

    public String getTipoTratamiento() {
        return tipoTratamiento;
    }

    public ActividadSilviculturalDetalleDto setTipoTratamiento(String tipoTratamiento) {
        this.tipoTratamiento = tipoTratamiento;
        return this;
    }

    public boolean isAccion() {
        return accion;
    }

    public ActividadSilviculturalDetalleDto setAccion(boolean accion) {
        this.accion = accion;
        return this;
    }

    public String getDescripcionDetalle() {
        return descripcionDetalle;
    }

    public ActividadSilviculturalDetalleDto setDescripcionDetalle(String descripcionDetalle) {
        this.descripcionDetalle = descripcionDetalle;
        return this;
    }

    public String getTratamiento() {
        return tratamiento;
    }

    public ActividadSilviculturalDetalleDto setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
        return this;
    }

    public String getJustificacion() {
        return justificacion;
    }

    public ActividadSilviculturalDetalleDto setJustificacion(String justificacion) {
        this.justificacion = justificacion;
        return this;
    }
}
