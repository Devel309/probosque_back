package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProcesoPostulacionEntity extends AuditoriaEntity implements Serializable {
    private Integer idProcesoPostulacion;
    private Integer idUsuarioPostulacion;
    private Integer IdRegenteForestal;
    private Integer IdEstatusProceso;
    private Date FechaPostulacion;
    private String Observacion;
    private Boolean RecepcionDocumentos;
    private Date FechaRecepcionDocumentos;
    private Integer idProcesoOferta;
    private String NombreRegente;
    private String UnidadAProvechamiento;
    private String StatusProceso;
    private String NumeroTramite;
    private Integer autorizacionpublicacion;
    private Integer pruebaspublicacion;
    private String codigoTipoPersonaPostulante;
    private String codigoTipoDocumentoPostulante;
    private String numeroDocumentoPostulante;
    private String nombrePostulante;
    private String correoPostulante;
    private String domicilio;
    private String departamento;
    private String provincia;
    private String distrito;
    private String codigoTipoPersonaRepresentante;
    private String codigoTipoDocumentoRepresentante;
    private String numeroDocumentoRepresentante;
    private String nombreRepresentante;
    private String apellidoPaternoPostulante;
    private String apellidoMaternoPostulante;
    private String apellidoPaternoRepresentante;
    private String apellidoMaternoRepresentante;
    private String observacionARFFS;
    private Boolean observado;
    private Boolean envioPrueba;
    private Integer numeroResolucion;
    private Date fechaResolucion;
    private Integer diaAmpliacion;
    private Integer autorizaDiaPublicacion;
    private Boolean publicacionPrueba;
    private Boolean autorizacionExploracion;

    private Integer idContrato;
    private String nroContrato;


    private Integer idResolucion;

    private Boolean resolucionVigencia;
}