package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ActividadEconomicaManejoEntity extends AuditoriaEntity implements Serializable {
    private Integer idActiEconomicaManejo;
    private String descripcion;
    private PlanManejoEntity planManejo;
    private ActividadEconomicaEntity actividadEconomical;

    public Integer getIdActiEconomicaManejo() {
        return idActiEconomicaManejo;
    }

    public void setIdActiEconomicaManejo(Integer idActiEconomicaManejo) {
        this.idActiEconomicaManejo = idActiEconomicaManejo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public PlanManejoEntity getPlanManejo() {
        return planManejo;
    }

    public void setPlanManejo(PlanManejoEntity planManejo) {
        this.planManejo = planManejo;
    }

    public ActividadEconomicaEntity getActividadEconomical() {
        return actividadEconomical;
    }

    public void setActividadEconomical(ActividadEconomicaEntity actividadEconomical) {
        this.actividadEconomical = actividadEconomical;
    }
}
