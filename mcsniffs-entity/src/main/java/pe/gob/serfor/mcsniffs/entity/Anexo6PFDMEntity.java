package pe.gob.serfor.mcsniffs.entity;

public class Anexo6PFDMEntity extends FirmaAnexosPFDMEntity{
    private Integer IdPostulacionPFDM;
    private Integer IdUsuarioPostulante;
    private String posNombreRS;
    private String posDni;
    private String posRuc;
    private String posDomicilioLegal;
    private String posTelefonoCelular;
    private String posCorreo;
    private String posNombreRepresentante;
    private String posDomiciloRepresentante;
    private String areaTipoConcesion;
    private String areaVigencia;
    private String areaSuperficie;
    private String areaSector;
    private String areaIdDepartamento;
    private String areaIdProvincia;
    private String areaIdDistrito;
    private String areaRecursosAproveConserv;
    private String areaMaderable;
    private String areaPFDM;
    private String areaEcoturismo;
    private String areaFuanaSilvestre;
    private String areaServEcosistematicos;
    private String objetivos;
    private String metas;
    private String caractFisica;
    private String idAdjuntoFisica;
    private String caracBiologica;
    private String idAdjuntoBiologica;
    private String caracSocioeconomica;
    private String idAdjuntoSocioeconomica;
    private String justiFactoresAmbienBiolo;
    private String idAdjuntoAmbienBiolo;
    private String justiFactoresSocioEconoCultu;
    private String idAdjuntoSocioEconoCultu;
    private String justiManejoConcesion;
    private String idAdjuntoManejoConcesion;
    private String sustePresupuesto;
    private String idAdjuntoPresupuesto;
    private String susteAnalisisFinanciero;
    private String idAdjuntoAnalisisFinanciero;
    private String expeSolicitanteActv;
    private String idAdjuntoSolicitanteActv;
    private String idAdjuntoAreaSolicitada;
    private String idAdjuntoVertices;
    private String idAdjuntoMapaUbicacion;
    private String idAdjuntoMapaBaseArea;
    private String idAdjuntoDocLevantamiento;
    private String idAdjuntoDocFuentesFinanciamiento;
    
    public Integer getIdPostulacionPFDM() {
        return IdPostulacionPFDM;
    }
    public void setIdPostulacionPFDM(Integer idPostulacionPFDM) {
        IdPostulacionPFDM = idPostulacionPFDM;
    }
    public Integer getIdUsuarioPostulante() {
        return IdUsuarioPostulante;
    }
    public void setIdUsuarioPostulante(Integer idUsuarioPostulante) {
        IdUsuarioPostulante = idUsuarioPostulante;
    }
    public String getPosNombreRS() {
        return posNombreRS;
    }
    public void setPosNombreRS(String posNombreRS) {
        this.posNombreRS = posNombreRS;
    }
    public String getPosDni() {
        return posDni;
    }
    public void setPosDni(String posDni) {
        this.posDni = posDni;
    }
    public String getPosRuc() {
        return posRuc;
    }
    public void setPosRuc(String posRuc) {
        this.posRuc = posRuc;
    }
    public String getPosDomicilioLegal() {
        return posDomicilioLegal;
    }
    public void setPosDomicilioLegal(String posDomicilioLegal) {
        this.posDomicilioLegal = posDomicilioLegal;
    }
    public String getPosTelefonoCelular() {
        return posTelefonoCelular;
    }
    public void setPosTelefonoCelular(String posTelefonoCelular) {
        this.posTelefonoCelular = posTelefonoCelular;
    }
    public String getPosCorreo() {
        return posCorreo;
    }
    public void setPosCorreo(String posCorreo) {
        this.posCorreo = posCorreo;
    }
    public String getPosNombreRepresentante() {
        return posNombreRepresentante;
    }
    public void setPosNombreRepresentante(String posNombreRepresentante) {
        this.posNombreRepresentante = posNombreRepresentante;
    }
    public String getPosDomiciloRepresentante() {
        return posDomiciloRepresentante;
    }
    public void setPosDomiciloRepresentante(String posDomiciloRepresentante) {
        this.posDomiciloRepresentante = posDomiciloRepresentante;
    }
    public String getAreaTipoConcesion() {
        return areaTipoConcesion;
    }
    public void setAreaTipoConcesion(String areaTipoConcesion) {
        this.areaTipoConcesion = areaTipoConcesion;
    }
    public String getAreaVigencia() {
        return areaVigencia;
    }
    public void setAreaVigencia(String areaVigencia) {
        this.areaVigencia = areaVigencia;
    }
    public String getAreaSuperficie() {
        return areaSuperficie;
    }
    public void setAreaSuperficie(String areaSuperficie) {
        this.areaSuperficie = areaSuperficie;
    }
    public String getAreaSector() {
        return areaSector;
    }
    public void setAreaSector(String areaSector) {
        this.areaSector = areaSector;
    }
    public String getAreaIdDepartamento() {
        return areaIdDepartamento;
    }
    public void setAreaIdDepartamento(String areaIdDepartamento) {
        this.areaIdDepartamento = areaIdDepartamento;
    }
    public String getAreaIdProvincia() {
        return areaIdProvincia;
    }
    public void setAreaIdProvincia(String areaIdProvincia) {
        this.areaIdProvincia = areaIdProvincia;
    }
    public String getAreaIdDistrito() {
        return areaIdDistrito;
    }
    public void setAreaIdDistrito(String areaIdDistrito) {
        this.areaIdDistrito = areaIdDistrito;
    }
    public String getAreaRecursosAproveConserv() {
        return areaRecursosAproveConserv;
    }
    public void setAreaRecursosAproveConserv(String areaRecursosAproveConserv) {
        this.areaRecursosAproveConserv = areaRecursosAproveConserv;
    }
    public String getAreaMaderable() {
        return areaMaderable;
    }
    public void setAreaMaderable(String areaMaderable) {
        this.areaMaderable = areaMaderable;
    }
    public String getAreaPFDM() {
        return areaPFDM;
    }
    public void setAreaPFDM(String areaPFDM) {
        this.areaPFDM = areaPFDM;
    }
    public String getAreaEcoturismo() {
        return areaEcoturismo;
    }
    public void setAreaEcoturismo(String areaEcoturismo) {
        this.areaEcoturismo = areaEcoturismo;
    }
    public String getAreaFuanaSilvestre() {
        return areaFuanaSilvestre;
    }
    public void setAreaFuanaSilvestre(String areaFuanaSilvestre) {
        this.areaFuanaSilvestre = areaFuanaSilvestre;
    }
    public String getAreaServEcosistematicos() {
        return areaServEcosistematicos;
    }
    public void setAreaServEcosistematicos(String areaServEcosistematicos) {
        this.areaServEcosistematicos = areaServEcosistematicos;
    }
    public String getObjetivos() {
        return objetivos;
    }
    public void setObjetivos(String objetivos) {
        this.objetivos = objetivos;
    }
    public String getMetas() {
        return metas;
    }
    public void setMetas(String metas) {
        this.metas = metas;
    }
    public String getCaractFisica() {
        return caractFisica;
    }
    public void setCaractFisica(String caractFisica) {
        this.caractFisica = caractFisica;
    }
    public String getIdAdjuntoFisica() {
        return idAdjuntoFisica;
    }
    public void setIdAdjuntoFisica(String idAdjuntoFisica) {
        this.idAdjuntoFisica = idAdjuntoFisica;
    }
    public String getCaracBiologica() {
        return caracBiologica;
    }
    public void setCaracBiologica(String caracBiologica) {
        this.caracBiologica = caracBiologica;
    }
    public String getIdAdjuntoBiologica() {
        return idAdjuntoBiologica;
    }
    public void setIdAdjuntoBiologica(String idAdjuntoBiologica) {
        this.idAdjuntoBiologica = idAdjuntoBiologica;
    }
    public String getCaracSocioeconomica() {
        return caracSocioeconomica;
    }
    public void setCaracSocioeconomica(String caracSocioeconomica) {
        this.caracSocioeconomica = caracSocioeconomica;
    }
    public String getIdAdjuntoSocioeconomica() {
        return idAdjuntoSocioeconomica;
    }
    public void setIdAdjuntoSocioeconomica(String idAdjuntoSocioeconomica) {
        this.idAdjuntoSocioeconomica = idAdjuntoSocioeconomica;
    }
    public String getJustiFactoresAmbienBiolo() {
        return justiFactoresAmbienBiolo;
    }
    public void setJustiFactoresAmbienBiolo(String justiFactoresAmbienBiolo) {
        this.justiFactoresAmbienBiolo = justiFactoresAmbienBiolo;
    }
    public String getIdAdjuntoAmbienBiolo() {
        return idAdjuntoAmbienBiolo;
    }
    public void setIdAdjuntoAmbienBiolo(String idAdjuntoAmbienBiolo) {
        this.idAdjuntoAmbienBiolo = idAdjuntoAmbienBiolo;
    }
    public String getJustiFactoresSocioEconoCultu() {
        return justiFactoresSocioEconoCultu;
    }
    public void setJustiFactoresSocioEconoCultu(String justiFactoresSocioEconoCultu) {
        this.justiFactoresSocioEconoCultu = justiFactoresSocioEconoCultu;
    }
    public String getIdAdjuntoSocioEconoCultu() {
        return idAdjuntoSocioEconoCultu;
    }
    public void setIdAdjuntoSocioEconoCultu(String idAdjuntoSocioEconoCultu) {
        this.idAdjuntoSocioEconoCultu = idAdjuntoSocioEconoCultu;
    }
    public String getJustiManejoConcesion() {
        return justiManejoConcesion;
    }
    public void setJustiManejoConcesion(String justiManejoConcesion) {
        this.justiManejoConcesion = justiManejoConcesion;
    }
    public String getIdAdjuntoManejoConcesion() {
        return idAdjuntoManejoConcesion;
    }
    public void setIdAdjuntoManejoConcesion(String idAdjuntoManejoConcesion) {
        this.idAdjuntoManejoConcesion = idAdjuntoManejoConcesion;
    }
    public String getSustePresupuesto() {
        return sustePresupuesto;
    }
    public void setSustePresupuesto(String sustePresupuesto) {
        this.sustePresupuesto = sustePresupuesto;
    }
    public String getIdAdjuntoPresupuesto() {
        return idAdjuntoPresupuesto;
    }
    public void setIdAdjuntoPresupuesto(String idAdjuntoPresupuesto) {
        this.idAdjuntoPresupuesto = idAdjuntoPresupuesto;
    }
    public String getSusteAnalisisFinanciero() {
        return susteAnalisisFinanciero;
    }
    public void setSusteAnalisisFinanciero(String susteAnalisisFinanciero) {
        this.susteAnalisisFinanciero = susteAnalisisFinanciero;
    }
    public String getIdAdjuntoAnalisisFinanciero() {
        return idAdjuntoAnalisisFinanciero;
    }
    public void setIdAdjuntoAnalisisFinanciero(String idAdjuntoAnalisisFinanciero) {
        this.idAdjuntoAnalisisFinanciero = idAdjuntoAnalisisFinanciero;
    }
    public String getExpeSolicitanteActv() {
        return expeSolicitanteActv;
    }
    public void setExpeSolicitanteActv(String expeSolicitanteActv) {
        this.expeSolicitanteActv = expeSolicitanteActv;
    }
    public String getIdAdjuntoSolicitanteActv() {
        return idAdjuntoSolicitanteActv;
    }
    public void setIdAdjuntoSolicitanteActv(String idAdjuntoSolicitanteActv) {
        this.idAdjuntoSolicitanteActv = idAdjuntoSolicitanteActv;
    }
    public String getIdAdjuntoAreaSolicitada() {
        return idAdjuntoAreaSolicitada;
    }
    public void setIdAdjuntoAreaSolicitada(String idAdjuntoAreaSolicitada) {
        this.idAdjuntoAreaSolicitada = idAdjuntoAreaSolicitada;
    }
    public String getIdAdjuntoVertices() {
        return idAdjuntoVertices;
    }
    public void setIdAdjuntoVertices(String idAdjuntoVertices) {
        this.idAdjuntoVertices = idAdjuntoVertices;
    }
    public String getIdAdjuntoMapaUbicacion() {
        return idAdjuntoMapaUbicacion;
    }
    public void setIdAdjuntoMapaUbicacion(String idAdjuntoMapaUbicacion) {
        this.idAdjuntoMapaUbicacion = idAdjuntoMapaUbicacion;
    }
    public String getIdAdjuntoMapaBaseArea() {
        return idAdjuntoMapaBaseArea;
    }
    public void setIdAdjuntoMapaBaseArea(String idAdjuntoMapaBaseArea) {
        this.idAdjuntoMapaBaseArea = idAdjuntoMapaBaseArea;
    }
    public String getIdAdjuntoDocLevantamiento() {
        return idAdjuntoDocLevantamiento;
    }
    public void setIdAdjuntoDocLevantamiento(String idAdjuntoDocLevantamiento) {
        this.idAdjuntoDocLevantamiento = idAdjuntoDocLevantamiento;
    }
    public String getIdAdjuntoDocFuentesFinanciamiento() {
        return idAdjuntoDocFuentesFinanciamiento;
    }
    public void setIdAdjuntoDocFuentesFinanciamiento(String idAdjuntoDocFuentesFinanciamiento) {
        this.idAdjuntoDocFuentesFinanciamiento = idAdjuntoDocFuentesFinanciamiento;
    }
    




}
