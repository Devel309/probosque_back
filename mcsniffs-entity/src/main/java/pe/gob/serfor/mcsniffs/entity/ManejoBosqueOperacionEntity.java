package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ManejoBosqueOperacionEntity extends AuditoriaEntity implements Serializable {
    private Integer idManBosqueOperacion;
    private PlanManejoEntity planManejo;
    private String idTipoOperacion;
    private String tipoOperacion;
    private String manoObra;
    private String maquina;
    private String descripcion;
    private String operacionDescripcion;

    public Integer getIdManBosqueOperacion() {
        return idManBosqueOperacion;
    }

    public void setIdManBosqueOperacion(Integer idManBosqueOperacion) {
        this.idManBosqueOperacion = idManBosqueOperacion;
    }

    public PlanManejoEntity getPlanManejo() {
        return planManejo;
    }

    public void setPlanManejo(PlanManejoEntity planManejo) {
        this.planManejo = planManejo;
    }

    public String getIdTipoOperacion() {
        return idTipoOperacion;
    }

    public void setIdTipoOperacion(String idTipoOperacion) {
        this.idTipoOperacion = idTipoOperacion;
    }

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public String getManoObra() {
        return manoObra;
    }

    public void setManoObra(String manoObra) {
        this.manoObra = manoObra;
    }

    public String getMaquina() {
        return maquina;
    }

    public void setMaquina(String maquina) {
        this.maquina = maquina;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getOperacionDescripcion() {
        return operacionDescripcion;
    }

    public void setOperacionDescripcion(String operacionDescripcion) {
        this.operacionDescripcion = operacionDescripcion;
    }
}
