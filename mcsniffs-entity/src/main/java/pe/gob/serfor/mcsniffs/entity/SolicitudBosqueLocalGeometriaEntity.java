package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SolicitudBosqueLocalGeometriaEntity extends AuditoriaEntity implements Serializable {
    private Integer idSolBosqueLocalGeometria;
    private Integer idSolBosqueLocal;
    private Integer idArchivo;
    private String tipoGeometria;
    private String geometry_wkt;
    private Integer srid ;
    private String properties;
    private String nombreCapa;
    private Double area;
    private String codigoGeometria;
    private String codigoSeccion;
    private String codigoSubSeccion;
    private String colorCapa;
    private Integer idUsuarioElimina;
    private Integer idUsuarioRegistro;
}
