package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;


public class CronogramaActividadEntity extends AuditoriaEntity implements Serializable {
    private Integer idCronogramaActividad;
    private Integer idPlanManejo;
    private String actividad;
    private Integer anio;
    private Integer mes;
    private String mesesAnios;
    private BigDecimal importe;
    private String[] ListaMarca;
    private Integer idCronogramaActividadDetalle;
    private String codigoProceso;
    private String codigoActividad;
    private String descripcionDetalle;
    private List<CronogramaActividadDetalleEntity> detalle;

    private String tipoDocumento;
    private String nroDocumento;
    private String codigoProcesoTitular;


    public String getCodigoProcesoTitular() {
        return codigoProcesoTitular;
    }

    public void setCodigoProcesoTitular(String codigoProcesoTitular) {
        this.codigoProcesoTitular = codigoProcesoTitular;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

   
    public String getCodigoProceso() {
        return codigoProceso;
    }
    public void setCodigoProceso(String codigoProceso) {
        this.codigoProceso = codigoProceso;
    }
    public String getCodigoActividad() {
        return codigoActividad;
    }
    public void setCodigoActividad(String codigoActividad) {
        this.codigoActividad = codigoActividad;
    }
    public String getMesesAnios() {
        return mesesAnios;
    }
    public void setMesesAnios(String mesesAnios) {
        this.mesesAnios = mesesAnios;
    }
    public List<CronogramaActividadDetalleEntity> getDetalle() {
        return detalle;
    }
    public void setDetalle(List<CronogramaActividadDetalleEntity> detalle) {
        this.detalle = detalle;
    }
    public String getDescripcionDetalle() {
        return descripcionDetalle;
    }
    public void setDescripcionDetalle(String descripcionDetalle) {
        this.descripcionDetalle = descripcionDetalle;
    }
    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }
    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }
    public String getActividad() {
        return actividad;
    }
    public void setActividad(String actividad) {
        this.actividad = actividad;
    }
    public Integer getAnio() {
        return anio;
    }
    public void setAnio(Integer anio) {
        this.anio = anio;
    }
    public BigDecimal getImporte() {
        return importe;
    }
    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }
    public String[] getListaMarca() {return ListaMarca;}
    public void setListaMarca(String[] listaMarca) {ListaMarca = listaMarca;}
    public Integer getMes() {return mes;}
    public void setMes(Integer mes) {this.mes = mes;}
    public Integer getIdCronogramaActividad() { return idCronogramaActividad;}
    public void setIdCronogramaActividad(Integer idCronogramaActividad) {this.idCronogramaActividad = idCronogramaActividad;}
    public Integer getIdCronogramaActividadDetalle() {return idCronogramaActividadDetalle;}
    public void setIdCronogramaActividadDetalle(Integer idCronogramaActividadDetalle) {this.idCronogramaActividadDetalle = idCronogramaActividadDetalle;}
}