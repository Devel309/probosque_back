package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.Date;

public class Anexo2CensoEntity extends AuditoriaEntity implements Serializable {
    private String codArbol;
    private Integer nombreComun;
    private Double diametro;
    private Double altura;
    private Double volumen;
    private String este;
    private String norte;
    private String tipoArbol;
    private String nombreComunDesc;
    private String nombreCientificoDesc;

    public String getNombreComunDesc() {
        return nombreComunDesc;
    }

    public void setNombreComunDesc(String nombreComunDesc) {
        this.nombreComunDesc = nombreComunDesc;
    }

    public String getNombreCientificoDesc() {
        return nombreCientificoDesc;
    }

    public void setNombreCientificoDesc(String nombreCientificoDesc) {
        this.nombreCientificoDesc = nombreCientificoDesc;
    }

    public String getEste() {
        return este;
    }

    public void setEste(String este) {
        this.este = este;
    }

    public String getNorte() {
        return norte;
    }

    public void setNorte(String norte) {
        this.norte = norte;
    }

    public String getCodArbol() {
        return codArbol;
    }

    public void setCodArbol(String codArbol) {
        this.codArbol = codArbol;
    }

    public Integer getNombreComun() {
        return nombreComun;
    }

    public void setNombreComun(Integer nombreComun) {
        this.nombreComun = nombreComun;
    }

    public Double getDiametro() {
        return diametro;
    }

    public void setDiametro(Double diametro) {
        this.diametro = diametro;
    }

    public Double getAltura() {
        return altura;
    }

    public void setAltura(Double altura) {
        this.altura = altura;
    }

    public Double getVolumen() {
        return volumen;
    }

    public void setVolumen(Double volumen) {
        this.volumen = volumen;
    }

    public String getTipoArbol() {
        return tipoArbol;
    }

    public void setTipoArbol(String tipoArbol) {
        this.tipoArbol = tipoArbol;
    }
}
