package pe.gob.serfor.mcsniffs.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class InfGeneralEntity implements Serializable {

    private Integer idPlanManejo;
    private String codigo;
    private Integer idUsuarioRegistro;
    private String codigoEstado;
}
