package pe.gob.serfor.mcsniffs.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
// import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
public class CodigoCifradoEntity implements Serializable {
    private String codigo;
    private String codigoCifrado;
    private String codigoDesCifrado;
    private String proceso;
}
