package pe.gob.serfor.mcsniffs.entity.Dto.ResultadoPP;

import lombok.*;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.util.Date;

public class ResultadoPPDto extends AuditoriaEntity implements Serializable {
    private Integer idResultadoPP;
    private Integer idProcesoPostulacion;
    private String asuntoMail;
    private String contenidoMail;
    private Integer idAutoridadRegional;
    private Date fechaEnvioResultado;
    private Integer notaTotal;
    private Boolean ganador;
    private Integer idDocumentoAdjunto;
    private Integer idUsuarioPostulacion;
    private String correoPostulante;

    public Integer getIdResultadoPP() {
        return idResultadoPP;
    }

    public void setIdResultadoPP(Integer idResultadoPP) {
        this.idResultadoPP = idResultadoPP;
    }

    public Integer getIdProcesoPostulacion() {
        return idProcesoPostulacion;
    }

    public void setIdProcesoPostulacion(Integer idProcesoPostulacion) {
        this.idProcesoPostulacion = idProcesoPostulacion;
    }

    public String getAsuntoMail() {
        return asuntoMail;
    }

    public void setAsuntoMail(String asuntoMail) {
        this.asuntoMail = asuntoMail;
    }

    public String getContenidoMail() {
        return contenidoMail;
    }

    public void setContenidoMail(String contenidoMail) {
        this.contenidoMail = contenidoMail;
    }

    public Integer getIdAutoridadRegional() {
        return idAutoridadRegional;
    }

    public void setIdAutoridadRegional(Integer idAutoridadRegional) {
        this.idAutoridadRegional = idAutoridadRegional;
    }

    public Date getFechaEnvioResultado() {
        return fechaEnvioResultado;
    }

    public void setFechaEnvioResultado(Date fechaEnvioResultado) {
        this.fechaEnvioResultado = fechaEnvioResultado;
    }

    public Integer getNotaTotal() {
        return notaTotal;
    }

    public void setNotaTotal(Integer notaTotal) {
        this.notaTotal = notaTotal;
    }

    public Boolean getGanador() {
        return ganador;
    }

    public void setGanador(Boolean ganador) {
        this.ganador = ganador;
    }

    public Integer getIdDocumentoAdjunto() {
        return idDocumentoAdjunto;
    }

    public void setIdDocumentoAdjunto(Integer idDocumentoAdjunto) {
        this.idDocumentoAdjunto = idDocumentoAdjunto;
    }

    public Integer getIdUsuarioPostulacion() {
        return idUsuarioPostulacion;
    }

    public void setIdUsuarioPostulacion(Integer idUsuarioPostulacion) {
        this.idUsuarioPostulacion = idUsuarioPostulacion;
    }

    public String getCorreoPostulante() {
        return correoPostulante;
    }

    public void setCorreoPostulante(String correoPostulante) {
        this.correoPostulante = correoPostulante;
    }
}