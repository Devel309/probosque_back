package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ManejoBosqueActividadEntity extends AuditoriaEntity implements Serializable {
    private Integer idManejoBosqueAct;
    private String catOrdenamiento;
    private String actividad;
    private String Ordenamiento;
    private PlanManejoEntity planManejo;

    public PlanManejoEntity getPlanManejo() {
        return planManejo;
    }

    public void setPlanManejo(PlanManejoEntity planManejo) {
        this.planManejo = planManejo;
    }

    public String getOrdenamiento() {
        return Ordenamiento;
    }

    public void setOrdenamiento(String ordenamiento) {
        Ordenamiento = ordenamiento;
    }

    public Integer getIdManejoBosqueAct() {
        return idManejoBosqueAct;
    }

    public void setIdManejoBosqueAct(Integer idManejoBosqueAct) {
        this.idManejoBosqueAct = idManejoBosqueAct;
    }

    public String getCatOrdenamiento() {
        return catOrdenamiento;
    }

    public void setCatOrdenamiento(String catOrdenamiento) {
        this.catOrdenamiento = catOrdenamiento;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }
}
