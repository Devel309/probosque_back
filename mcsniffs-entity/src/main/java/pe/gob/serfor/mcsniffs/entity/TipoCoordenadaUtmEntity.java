package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;


public class TipoCoordenadaUtmEntity extends AuditoriaEntity implements Serializable {
    private Integer idTipoCoordUtm;
    private String descripcion;

    public Integer getIdTipoCoordUtm() {
        return idTipoCoordUtm;
    }

    public void setIdTipoCoordUtm(Integer idTipoCoordUtm) {
        this.idTipoCoordUtm = idTipoCoordUtm;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
