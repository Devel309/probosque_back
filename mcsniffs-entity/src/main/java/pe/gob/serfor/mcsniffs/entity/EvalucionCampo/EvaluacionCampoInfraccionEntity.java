package pe.gob.serfor.mcsniffs.entity.EvalucionCampo;

import java.io.Serializable;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EvaluacionCampoInfraccionEntity  extends AuditoriaEntity  implements Serializable{
    
   Integer idEvaluacionCampoInfraccion;
   Integer idEvaluacionCampo ;
   Integer idInfraccion ;
   String calificacion;
   String sancionNoMonetaria ;
   String sancionMonetaria ;

}
