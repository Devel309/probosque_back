package pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionArchivoEntity;

import java.util.List;
import java.util.Date;

@Getter
@Setter
public class SolicitudConcesionEvaluacionDto extends AuditoriaEntity implements Serializable {

    private Integer idSolicitudConcesionEvaluacion;
    private Integer idSolicitudConcesion;
    private Date fechaInicio;
    private Date fechaFin;
    private Integer diasEvaluacion;
    private String codigoCatZonificacion;
    private Boolean sinCategoria;
    private Integer idArchivoSutento;
    private Boolean solicitaOpinionSernamp;
    private Boolean solicitaOpinionAna;
    private String codigoResultadoEvaluacion;
    private String observacion;
    private String codigoMotivDenegacion;
    private Integer finalizarCalificacion;

    //
    private Integer idSolicitudConcesionEvaluacionDetalle;
    private String codigoSeccion;
    private String codigoSubSeccion;
    private Boolean lineamiento;
    private String codigoLineamiento;
    private Boolean conforme;
    private String estado;
    private Integer idUsuarioRegistro;
    private Integer idUsuarioModificacion;
    private Date fechaRegistro;
    private Date fechaModificacion;
    private Integer idUsuarioElimina;
    private Date fechaElimina;
    private String strFechaInicio;
    private String strFechaFin;

    private Double puntajeObtenido;
    private Double puntajeExtra;
    private Double puntajeTotal;
    private Boolean comunicadoEnviado;
    private Boolean calificacionFinalizada;

    private List<SolicitudConcesionEvaluacionCalificacionDto> lstSolicitudConcesionEvaluacionCalificacion;
    private List<SolicitudConcesionArchivoEntity> lstSolicitudConcesionEvaluacionCalificacionArchivo;
    private Integer idArchivoInformeFinal;
}
