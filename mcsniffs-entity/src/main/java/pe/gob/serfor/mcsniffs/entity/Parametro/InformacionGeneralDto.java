package pe.gob.serfor.mcsniffs.entity.Parametro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.PGMFArchivoEntity;

import javax.persistence.ParameterMode;

@Data
public class InformacionGeneralDto extends AuditoriaEntity implements Serializable {

	private Integer idContrato;
	private String tipoPersonaTitularTH;
	private String tipoPersonaTitularTHDesc;
	private String tipoDocumentoTitularTH;
	private String tipoDocumentoTitularTHDesc;
	private String numeroDocumentoTitularTH;
	private String nombreTitularTH;
	private String apellidoPaternoTitularTH;
	private String apellidoMaternoTitularTH;
	private String razonSocialTitularTH;
	private String emailTitularTH;
	private String direccionTitularTH;
	private String nombreDepartamentoContrato;
	private String nombreProvinciaContrato;
	private String nombreDistritoContrato;

	private String tipoPersonaRepLegal;
	private String tipoPersonaRepLegalDesc;
	private String tipoDocumentoRepLegal;
	private String tipoDocumentoRepLegalDesc;
	private String numeroDocumentoRepLegal;
	private String apellidoPaternoRepLegal;
	private String apellidoMaternoRepLegal;
	private String emailRepLegal;
	private Integer contratoPrincipal;
	private Boolean principal;



	private Number idInformacionGeneral;
	private Number idPlanManejo;
	private String nombreTitular;
	private String nombreRepresentanteLegal;
	private String dni;
	private String domicilioLegal;
	private String nroContratoConcesion;
	private String distrito;
	private String departamento;
	private String provincia;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
	private Date fechaPresentacion;
	private Number duracion;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
	private Date fechaInicio;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
	private Date fechaFin;

	private BigDecimal areaTotalConcesion;
	private BigDecimal areaBosqueProduccionForestal;
	private BigDecimal areaProteccion;
	private String numeroBloquesQuinquenales;
	private BigDecimal potencialMaderable;
	private BigDecimal volumenCortaAnualPermisible;

	private Number idRegenteForestal;
	private String nombreRegenteForestal;
	private String domicilioLegalRegente;
	private String contratoSuscritoTitularTituloHabilitante;
	private String certificadoHabilitacionProfesionalRegenteForestal;
	private String numeroInscripcionRegistroRegente;

	private List<PGMFArchivoEntity> archivos;
	private Integer idPersona;

	private String tipoDocElaborador;
	private String dniElaborador;
	private String nombreElaborador;
	private String apellidoPaternoElaborador;
	private String apellidoMaternoElaborador;

	private String tipoDocRepresentante;
	private String docRepresentante;
	private String nombreRepresentante;
	private String apellidoPaternoRepresentante;
	private String apellidoMaternoRepresentante;
	private String descripcionPredio;
	private String nroContrato;
}
