package pe.gob.serfor.mcsniffs.entity.Impugnacion;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImpugnacionEtapaEntity  extends AuditoriaEntity implements Serializable{
    @Id
    Integer idImpugnacionEtapa ;
    Integer idImpugnacion;
    String etapaImpugnacion ;
    Integer idEtapaImpugnacion ;
    Integer idUsuarioResponsable ;
}
