package pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion;

import lombok.Data;

@Data
public class ClonarSolicitudConcesionDto {
    
    private Integer idSolicitudConcesion;
    private Integer idSolicitudConcesionNuevo;
    private Integer tipoClonacion;
    private Integer idUsuarioRegistro;

}
