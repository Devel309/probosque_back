package pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.util.Date;

@Data
public class SolicitudConcesionCalificacionDto extends AuditoriaEntity implements Serializable {

    private Integer idSolicitudConcesion;
    private Integer idSolicitante;
    private String nroPartidaRegistral;
    private String numeroDireccion;
    private String sectorCaserio;
    private String telefonoFijo;
    private String telefonoCelular;
    private String codigoModalidad;
    private Integer vigenciaAnio;
    private String objetivo;
    private String recurso;
    private String importancia;
    private String descripcionProyecto;
    private String correoElectronico;
    private String estadoSolicitud;
    private String direccion;
    private String codigoFuenteFinanciamiento;
    private String correoElectronicoNotif;
    private String descripcionOtros;
    private Integer ubigeo;
    private Integer idSolicitudConcesionPadre;
    private Integer idConcesion;
    private String descripcionModalidad;
    private String numeroDocumentoSolicitante;
    private String nombreSolicitanteNatural;
    private String nombreSolicitanteJuridico;
    private String fecha;
    private String estadoSolicitudTexto;
    private String tipoDocumentoSolicitante;
    private String numeroDocumentoSolicitanteUnico;
    private String nombreSolicitanteUnico;
    private String direccionSolicitante;
    private Integer idDepartamentosolicitante;
    private String nombreDepartamentoSolicitante;
    private Integer idProvinciaSolicitante;
    private String nombreProvinciaSolicitante;
    private Integer idDistritoSolicitante;
    private String nombreDistritoSolicitante;
    private String tipoModalidad;
    private String descripcionFuenteFinanciamiento;
    private String codigoPerfil;
    private String nombreReprLegal;
    private String direccionReprLegal;
    private String nroTramite;
    private Date fechaTramite;
    private String metas;
    private String tipoEnvio;
    private Boolean anexoCompletado;
    private Date fechaEvaluacion;
    private Date fechaObservacion;

    private Integer diaEvaluacion;
    private Integer nroOpinion;
    private Date fechaRequisitoObs;
    private Boolean comunicadoEnviado;

    private Boolean flagReutilizado;
    private Double puntajeObtenido;
    private String ordenPuntaje;
    private Integer IdSolicitudConcesionEvaluacion;
    private Integer actividadComplementaria;
    private Double capacidadFinanciera;
    private Double puntajeTotal;
    private Boolean calificacionFinalizada;
    private Integer idArchivoInformeFinal;

}
