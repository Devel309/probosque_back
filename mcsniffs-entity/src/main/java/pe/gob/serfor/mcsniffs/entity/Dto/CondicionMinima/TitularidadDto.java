package pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima;

import lombok.Data;

@Data
public class TitularidadDto {

    private String tipoParticipante;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String nombres;
    private String razonSocial;
 
}