package pe.gob.serfor.mcsniffs.entity.seguridad;

import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity {
    @Id
    Integer IDUsuario;
    String CodigoUsuario;
    String CodigoEmpleado;
    String Contrasenia;
    String Usuario;
    String Estado;
    String NumeroDocumento;
    String CorreoElectronico;
    Integer IDTipoDocumento;
    Integer IDEntidad;
    Integer IDEmpresa;
    String  Nombres;
    String ApellidoPaterno;
    String ApellidoMaterno;
    Integer IDAutoridad;
    String Region;
    Integer SYSUsuario;
    Date SYSFecha;

    String TipoDocumento;
    String UsuarioDescripcion;
    String Entidad;
    String Empresa;

}
