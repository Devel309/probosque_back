package pe.gob.serfor.mcsniffs.entity;

import java.util.List;

public class Anexo3PFDMEntity extends FirmaAnexosPFDMEntity{
    private Integer IdPostulacionPFDM;
    private Integer IdUsuarioPostulante;
    private String funcioNombresApellidos;
    private String funcioCargo;
    private String postulanteNombresApellidos;
    private String postulanteDniRuc;
    private String postulanteDomicilio;
    private String postulanteConsecion;
    private String areaSuperficie;
    private String areaSector;
    private String areIdDistrito;
    private String areaIdProvincia;
    private String areaIdDepartamento;
    private List<IngresosCuentasEntity> postulanteIngresos;


    
    public String getPostulanteNombresApellidos() {
        return postulanteNombresApellidos;
    }
    public void setPostulanteNombresApellidos(String postulanteNombresApellidos) {
        this.postulanteNombresApellidos = postulanteNombresApellidos;
    }
    public Integer getIdPostulacionPFDM() {
        return IdPostulacionPFDM;
    }
    public void setIdPostulacionPFDM(Integer idPostulacionPFDM) {
        IdPostulacionPFDM = idPostulacionPFDM;
    }
    public Integer getIdUsuarioPostulante() {
        return IdUsuarioPostulante;
    }
    public void setIdUsuarioPostulante(Integer idUsuarioPostulante) {
        IdUsuarioPostulante = idUsuarioPostulante;
    }
    public String getFuncioNombresApellidos() {
        return funcioNombresApellidos;
    }
    public void setFuncioNombresApellidos(String funcioNombresApellidos) {
        this.funcioNombresApellidos = funcioNombresApellidos;
    }
    public String getFuncioCargo() {
        return funcioCargo;
    }
    public void setFuncioCargo(String funcioCargo) {
        this.funcioCargo = funcioCargo;
    }
    public String getPostulanteDniRuc() {
        return postulanteDniRuc;
    }
    public void setPostulanteDniRuc(String postulanteDniRuc) {
        this.postulanteDniRuc = postulanteDniRuc;
    }
    public String getPostulanteDomicilio() {
        return postulanteDomicilio;
    }
    public void setPostulanteDomicilio(String postulanteDomicilio) {
        this.postulanteDomicilio = postulanteDomicilio;
    }
    public String getPostulanteConsecion() {
        return postulanteConsecion;
    }
    public void setPostulanteConsecion(String postulanteConsecion) {
        this.postulanteConsecion = postulanteConsecion;
    }
    public String getAreaSuperficie() {
        return areaSuperficie;
    }
    public void setAreaSuperficie(String areaSuperficie) {
        this.areaSuperficie = areaSuperficie;
    }
    public String getAreaSector() {
        return areaSector;
    }
    public void setAreaSector(String areaSector) {
        this.areaSector = areaSector;
    }
    public String getAreIdDistrito() {
        return areIdDistrito;
    }
    public void setAreIdDistrito(String areIdDistrito) {
        this.areIdDistrito = areIdDistrito;
    }
    public String getAreaIdProvincia() {
        return areaIdProvincia;
    }
    public void setAreaIdProvincia(String areaIdProvincia) {
        this.areaIdProvincia = areaIdProvincia;
    }
    public String getAreaIdDepartamento() {
        return areaIdDepartamento;
    }
    public void setAreaIdDepartamento(String areaIdDepartamento) {
        this.areaIdDepartamento = areaIdDepartamento;
    }
    public List<IngresosCuentasEntity> getPostulanteIngresos() {
        return postulanteIngresos;
    }
    public void setPostulanteIngresos(List<IngresosCuentasEntity> postulanteIngresos) {
        this.postulanteIngresos = postulanteIngresos;
    }

    
}
