package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;


public class TipoPlanEntity extends AuditoriaEntity implements Serializable {
    private Integer idTipoPlan;
    private String descripcion;

    public Integer getIdTipoPlan() {
        return idTipoPlan;
    }

    public void setIdTipoPlan(Integer idTipoPlan) {
        this.idTipoPlan = idTipoPlan;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
