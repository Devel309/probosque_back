package pe.gob.serfor.mcsniffs.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class PagoPeriodoEntity extends AuditoriaEntity implements Serializable {
    private Integer idPagoPeriodo;
    private String codTipoPagoPeriodo;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaLimite;
    private BigDecimal monto;
    private BigDecimal montoFinal;
    private BigDecimal balancePago;
    private String observacion;
    private String detalle;
    private String descripcion;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaComprobante;
    private Integer idPagoCronograma;
    private Integer idPago;
    private String perfil;
}
