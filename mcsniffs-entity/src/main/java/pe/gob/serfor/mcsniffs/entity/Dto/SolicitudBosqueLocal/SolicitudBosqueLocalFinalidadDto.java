package pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Getter
@Setter
public class SolicitudBosqueLocalFinalidadDto extends AuditoriaEntity implements Serializable {
    private Integer idBosqueLocalFinalidad;
    private Integer idSolBosqueLocal;
    private String codigoFinalidad;
    private Boolean seleccionado;
    private String descripcion;

    private List<SolicitudBosqueLocalFinalidadDto> lista;
}
