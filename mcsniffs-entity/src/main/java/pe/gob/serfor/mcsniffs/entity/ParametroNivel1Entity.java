package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal.PotencialProduccionForestalVariableEntity;

import java.io.Serializable;
import java.util.List;

@Data
public class ParametroNivel1Entity implements Serializable {
    Integer idParametro;
    String codigo;
    String valorPrimario;
    String valorSecundario;
    String valorTerciario;
    String codigoSniffs;
    String codigoSniffsDescripcion;
    String prefijo;
    Integer idPlanManejo;
    String Observacion;
    Integer idEvaluacionDet;

    private List<ParametroNivel2Entity> listNivel2;
}
