package pe.gob.serfor.mcsniffs.entity.Parametro;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ProduccionRecursoForestalDetalleEntity;

import java.io.Serializable;
import java.util.List;

public class ProduccionRecursoForestalDto extends AuditoriaEntity implements Serializable {
    private Integer idProdRecursoForestal;
    private String idTipoBosque;
    private String tipoBosque;
    private Integer idPlanManejo;
    private String idTipoProdRecursoForestal;
    private List<ProduccionRecursoForestalEspecieDto> listproduccionRecursoForestalEspecieDto;

    public Integer getIdProdRecursoForestal() {
        return idProdRecursoForestal;
    }

    public void setIdProdRecursoForestal(Integer idProdRecursoForestal) {
        this.idProdRecursoForestal = idProdRecursoForestal;
    }

    public String getIdTipoBosque() {
        return idTipoBosque;
    }

    public void setIdTipoBosque(String idTipoBosque) {
        this.idTipoBosque = idTipoBosque;
    }

    public String getTipoBosque() {
        return tipoBosque;
    }

    public void setTipoBosque(String tipoBosque) {
        this.tipoBosque = tipoBosque;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public String getIdTipoProdRecursoForestal() {
        return idTipoProdRecursoForestal;
    }

    public void setIdTipoProdRecursoForestal(String idTipoProdRecursoForestal) {
        this.idTipoProdRecursoForestal = idTipoProdRecursoForestal;
    }

    public List<ProduccionRecursoForestalEspecieDto> getListproduccionRecursoForestalEspecieDto() {
        return listproduccionRecursoForestalEspecieDto;
    }

    public void setListproduccionRecursoForestalEspecieDto(List<ProduccionRecursoForestalEspecieDto> listproduccionRecursoForestalEspecieDto) {
        this.listproduccionRecursoForestalEspecieDto = listproduccionRecursoForestalEspecieDto;
    }    
}
