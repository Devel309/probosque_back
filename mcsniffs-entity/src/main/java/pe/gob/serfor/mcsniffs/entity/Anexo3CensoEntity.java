package pe.gob.serfor.mcsniffs.entity;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;

public class Anexo3CensoEntity extends AuditoriaEntity implements Serializable {
    private Integer nombreComun;
    private Integer nombreCientifico;
    private Double volumenComercial;
    private String producto;
    private String idIndividuo;
    private String nombreNativo;
    private String este;
    private String norte;
    private String observaciones;
    private String nombreComunDesc;
    private String nombreCientificoDesc;

    public String getNombreComunDesc() {
        return nombreComunDesc;
    }

    public void setNombreComunDesc(String nombreComunDesc) {
        this.nombreComunDesc = nombreComunDesc;
    }

    public String getNombreCientificoDesc() {
        return nombreCientificoDesc;
    }

    public void setNombreCientificoDesc(String nombreCientificoDesc) {
        this.nombreCientificoDesc = nombreCientificoDesc;
    }

    public Integer getNombreComun() {
        return nombreComun;
    }

    public void setNombreComun(Integer nombreComun) {
        this.nombreComun = nombreComun;
    }

    public Integer getNombreCientifico() {
        return nombreCientifico;
    }

    public void setNombreCientifico(Integer nombreCientifico) {
        this.nombreCientifico = nombreCientifico;
    }

    public Double getVolumenComercial() {
        return volumenComercial;
    }

    public void setVolumenComercial(Double volumenComercial) {
        this.volumenComercial = volumenComercial;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getIdIndividuo() {
        return idIndividuo;
    }

    public void setIdIndividuo(String idIndividuo) {
        this.idIndividuo = idIndividuo;
    }

    public String getNombreNativo() {
        return nombreNativo;
    }

    public void setNombreNativo(String nombreNativo) {
        this.nombreNativo = nombreNativo;
    }

    public String getEste() {
        return este;
    }

    public void setEste(String este) {
        this.este = este;
    }

    public String getNorte() {
        return norte;
    }

    public void setNorte(String norte) {
        this.norte = norte;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
}
