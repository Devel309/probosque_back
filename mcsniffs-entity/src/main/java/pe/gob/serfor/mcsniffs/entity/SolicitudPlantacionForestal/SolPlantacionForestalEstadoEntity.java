package pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal;

import java.sql.Date;

import lombok.Data;

@Data
public class SolPlantacionForestalEstadoEntity {
	Integer id_solplantforestal;
   Integer id_solplantforestal_estado ;
    String	descripcion ;
	String  estado ;
	Integer    id_usuario_registro ;
	Date    fecha_registro ;
}
