package pe.gob.serfor.mcsniffs.entity.Dto.Fiscalizacion;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FiltroFiscalizacionDto{
    
    private Integer idFiscalizacion;
    private Integer idContrato;
    private Integer idResultadoProcesoPostulacion;
}
