package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class PlanManejoAppEntity implements Serializable {
    private String codigoTipoInfogeneral;
    private String idContratoTH;
    private  String idPlanManejoTH;
    private String areaTotalConcesion;
    private  String areaBosqueProduccionForestal;
    private String areaProteccion;
    private String bloqueQuinquenales;
    private String superficieMaderable;
    private  String superficieNoMaderable;
    private String nombreCompleto;
    public String ubigeoPlanManejo;
    public String idCensoForestal;
    public String tipoCensoForestal;
    public String getCodigoTipoInfogeneral() {
        return codigoTipoInfogeneral;
    }

    public void setCodigoTipoInfogeneral(String codigoTipoInfogeneral) {
        this.codigoTipoInfogeneral = codigoTipoInfogeneral;
    }

    public String getIdContratoTH() {
        return idContratoTH;
    }

    public void setIdContratoTH(String idContratoTH) {
        this.idContratoTH = idContratoTH;
    }

    public String getIdPlanManejoTH() {
        return idPlanManejoTH;
    }

    public void setIdPlanManejoTH(String idPlanManejoTH) {
        this.idPlanManejoTH = idPlanManejoTH;
    }

    public String getAreaTotalConcesion() {
        return areaTotalConcesion;
    }

    public void setAreaTotalConcesion(String areaTotalConcesion) {
        this.areaTotalConcesion = areaTotalConcesion;
    }

    public String getAreaBosqueProduccionForestal() {
        return areaBosqueProduccionForestal;
    }

    public void setAreaBosqueProduccionForestal(String areaBosqueProduccionForestal) {
        this.areaBosqueProduccionForestal = areaBosqueProduccionForestal;
    }

    public String getAreaProteccion() {
        return areaProteccion;
    }

    public void setAreaProteccion(String areaProteccion) {
        this.areaProteccion = areaProteccion;
    }

    public String getBloqueQuinquenales() {
        return bloqueQuinquenales;
    }

    public void setBloqueQuinquenales(String bloqueQuinquenales) {
        this.bloqueQuinquenales = bloqueQuinquenales;
    }

    public String getSuperficieMaderable() {
        return superficieMaderable;
    }

    public void setSuperficieMaderable(String superficieMaderable) {
        this.superficieMaderable = superficieMaderable;
    }

    public String getSuperficieNoMaderable() {
        return superficieNoMaderable;
    }

    public void setSuperficieNoMaderable(String superficieNoMaderable) {
        this.superficieNoMaderable = superficieNoMaderable;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }


    public String getUbigeoPlanManejo() {
        return ubigeoPlanManejo;
    }

    public void setUbigeoPlanManejo(String ubigeoPlanManejo) {
        this.ubigeoPlanManejo = ubigeoPlanManejo;
    }
    public String getTipoCensoForestal() {
        return tipoCensoForestal;
    }

    public void setTipoCensoForestal(String tipoCensoForestal) {
        this.tipoCensoForestal = tipoCensoForestal;
    }
    public String getIdCensoForestal() {
        return idCensoForestal;
    }

    public void setIdCensoForestal(String idCensoForestal) {
        this.idCensoForestal = idCensoForestal;
    }
}
