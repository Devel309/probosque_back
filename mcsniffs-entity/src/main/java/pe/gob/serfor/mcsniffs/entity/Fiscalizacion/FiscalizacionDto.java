package pe.gob.serfor.mcsniffs.entity.Fiscalizacion;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.Dto.Fiscalizacion.FiscalizacionArchivoDto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class FiscalizacionDto extends AuditoriaEntity implements Serializable {
    private Integer idProcesoPostulacion;
    private Integer idUsuarioPostulacion;
    private Integer idProcesoOferta;
    private String codigoTituloH;
    private Integer idResultadoPP;
    private String nombresRepresentanteLegal;
    private Integer idContrato;
    private Date fechaInicioContrato;
    private Date fechaFinContrato;
    private String regente;
    private Integer idFiscalizacion;
    private String comentario;
    private Boolean resultado;
    private Date fechaInforme;
    private Integer idStatusContrato;
    private String estadoContrato;
    private String codigoEstadoContrato;
    private String numeroInforme;
    private String asunto;
    private String tipoPersonaTitular;
    private String nombreTitular;
    private Boolean emite;
    private String numeroExpediente;
    private Date fechaPresentacion;
    private List<FiscalizacionArchivoDto> listaFiscalizacionArchivo;
}
