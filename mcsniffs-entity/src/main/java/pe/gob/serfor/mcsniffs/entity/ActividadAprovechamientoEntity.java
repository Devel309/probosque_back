package pe.gob.serfor.mcsniffs.entity;

import java.math.BigDecimal;
import java.util.List;

public class ActividadAprovechamientoEntity extends AuditoriaEntity{
    private Integer idActvAprove;
    private Integer idPlanManejo;
    private String codActvAprove;
    private String codSubActvAprove;
    private String metodologia;
    private String trochas;
    private String postes;
    private String limites;
    private BigDecimal distancia;
    private String subdivision;
	private String anexo;
    private BigDecimal dimension;
    private BigDecimal superficie;
    private BigDecimal numero;
	private BigDecimal areaTotal;
    private Integer nroEspecies;
	private Integer nroArboles;
	private String descripcion;
    private String observacion;
    private String detalle;

	private List<ActividadAprovechamientoDetEntity> lstactividadDet;
	

	public List<ActividadAprovechamientoDetEntity> getLstactividadDet() {
		return lstactividadDet;
	}
	public void setLstactividadDet(List<ActividadAprovechamientoDetEntity> lstactividadDet) {
		this.lstactividadDet = lstactividadDet;
	}
	public Integer getIdActvAprove() {
		return idActvAprove;
	}
	public void setIdActvAprove(Integer idActvAprove) {
		this.idActvAprove = idActvAprove;
	}
	public Integer getIdPlanManejo() {
		return idPlanManejo;
	}
	public void setIdPlanManejo(Integer idPlanManejo) {
		this.idPlanManejo = idPlanManejo;
	}
	public String getCodActvAprove() {
		return codActvAprove;
	}
	public void setCodActvAprove(String codActvAprove) {
		this.codActvAprove = codActvAprove;
	}
	public String getCodSubActvAprove() {
		return codSubActvAprove;
	}
	public void setCodSubActvAprove(String codSubActvAprove) {
		this.codSubActvAprove = codSubActvAprove;
	}
	public String getMetodologia() {
		return metodologia;
	}
	public void setMetodologia(String metodologia) {
		this.metodologia = metodologia;
	}
	public String getTrochas() {
		return trochas;
	}
	public void setTrochas(String trochas) {
		this.trochas = trochas;
	}
	public String getPostes() {
		return postes;
	}
	public void setPostes(String postes) {
		this.postes = postes;
	}
	public String getLimites() {
		return limites;
	}
	public void setLimites(String limites) {
		this.limites = limites;
	}
	public BigDecimal getDistancia() {
		return distancia;
	}
	public void setDistancia(BigDecimal distancia) {
		this.distancia = distancia;
	}
	public String getSubdivision() {
		return subdivision;
	}
	public void setSubdivision(String subdivision) {
		this.subdivision = subdivision;
	}
	public String getAnexo() {
		return anexo;
	}
	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}
	public BigDecimal getDimension() {
		return dimension;
	}
	public void setDimension(BigDecimal dimension) {
		this.dimension = dimension;
	}
	public BigDecimal getSuperficie() {
		return superficie;
	}
	public void setSuperficie(BigDecimal superficie) {
		this.superficie = superficie;
	}
	public BigDecimal getNumero() {
		return numero;
	}
	public void setNumero(BigDecimal numero) {
		this.numero = numero;
	}
	public BigDecimal getAreaTotal() {
		return areaTotal;
	}
	public void setAreaTotal(BigDecimal areaTotal) {
		this.areaTotal = areaTotal;
	}
	public Integer getNroEspecies() {
		return nroEspecies;
	}
	public void setNroEspecies(Integer nroEspecies) {
		this.nroEspecies = nroEspecies;
	}
	public Integer getNroArboles() {
		return nroArboles;
	}
	public void setNroArboles(Integer nroArboles) {
		this.nroArboles = nroArboles;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	

}
