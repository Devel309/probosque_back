package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class AccesibilidadManejoMatrizEntity extends AuditoriaEntity  implements Serializable {

    private Integer idAcceManejomatriz;
    private String anexosector;
    private Integer idAcceManejo;

    public Integer getIdAcceManejomatriz() {
        return idAcceManejomatriz;
    }

    public void setIdAcceManejomatriz(Integer idAcceManejomatriz) {
        this.idAcceManejomatriz = idAcceManejomatriz;
    }

    public String getAnexosector() {
        return anexosector;
    }

    public void setAnexosector(String anexosector) {
        this.anexosector = anexosector;
    }

    public Integer getIdAcceManejo() {
        return idAcceManejo;
    }

    public void setIdAcceManejo(Integer idAcceManejo) {
        this.idAcceManejo = idAcceManejo;
    }

}
