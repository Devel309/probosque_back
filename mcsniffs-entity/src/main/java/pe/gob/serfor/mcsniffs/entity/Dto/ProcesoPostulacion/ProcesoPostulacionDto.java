package pe.gob.serfor.mcsniffs.entity.Dto.ProcesoPostulacion;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ProcesoPostulacionDto implements Serializable {
  private Integer idProcesoPostulacion;
  private Integer idEstatusProceso;
  private Integer recepcionDocumentos;
  private Date fechaRecepcionDocumentos;
  private String numeroTramite;
  private Boolean tieneObservacion;
  private Integer idUsuarioModificacion;
  private Boolean observado;
  private String observacionARFFS;
  private Integer numeroResolucion;
  private Date fechaResolucion;
  private Boolean envioPrueba;
  private String codigoTipoDocumentoPostulante;
  private String numeroDocumentoPostulante;
  private Integer idProcesoOferta;
}
