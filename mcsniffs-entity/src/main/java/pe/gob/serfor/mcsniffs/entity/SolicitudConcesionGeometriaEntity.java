package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import java.io.Serializable;
@Data
public class SolicitudConcesionGeometriaEntity  extends AuditoriaEntity implements Serializable {
    private Integer idSolicitudConcesionGeometria;
    private Integer idSolicitudConcesion;
    private Integer idArchivo;
    private String tipoGeometria;
    private String geometry_wkt;
    private Integer srid ;
    private String nombreCapa;
    private Double area;
    private String codigoGeometria;
    private String codigoSeccion;
    private String codigoSubSeccion;
    private String colorCapa;
    private Integer idUsuarioElimina;
    private Integer idUsuarioRegistro;
}
