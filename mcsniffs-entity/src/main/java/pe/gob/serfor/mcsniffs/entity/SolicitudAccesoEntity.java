package pe.gob.serfor.mcsniffs.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

public class SolicitudAccesoEntity extends AuditoriaEntity implements Serializable{

    private Integer idSolicitudAcceso;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaIngreso;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaRespuesta;
    private String codigoTipoPersona;
    private char esReprLegal;
    private char tieneAdjReprLegal;
    private String codigoTipoDocumento;
    private String numeroDocumento;
    private String codigoEvalSolic;
    private String nombres;
    private String apellidoMaterno;
    private String apellidoPaterno;
    private String email;
    private String numeroRucEmpresa;
    private String razonSocialEmpresa;
    private String direccionEmpresa;
    private String telefonoEmpresa;
    private String emailEmpresa;
    private String codigoEvalEmpresa;
    private String codigoTipoActor;
    private char tieneAdjActor;
    private char notificaRevision;
    private char notificaRegistro;
    private String observacion;
    private Integer idUsuarioRevision;
    private String codigoTipoCncc;
    private String codigoEstadoSolicitud;
    private String idDepartamentoPersona;
    private String idProvinciaPersona;
    private String idDistritoPersona;
    private String codigoAccion;

    //valores de tabla parámetro
    private String tipoPersona;
    private String tipoDocumento;
    private String evalSolic;
    private String evalEmpresa;
    private String tipoActor;
    private String tipoCncc;
    private String estadoSolicitud;
    private String codigoUsuario;

    private String nombreDepartamento;
    private String nombreProvincia;
    private String nombreDistrito;
    private String nombreUsuarioRevision;
    private String idSolicitudAccesoTexto;

    private Integer idPerfil;
    private String codigoAplicacion;
    //getter y setter

    public String getCodigoAccion() {
        return codigoAccion;
    }

    public void setCodigoAccion(String codigoAccion) {
        this.codigoAccion = codigoAccion;
    }

    public String getIdDepartamentoPersona() {
        return idDepartamentoPersona;
    }

    public void setIdDepartamentoPersona(String idDepartamentoPersona) {
        this.idDepartamentoPersona = idDepartamentoPersona;
    }

    public String getIdProvinciaPersona() {
        return idProvinciaPersona;
    }

    public void setIdProvinciaPersona(String idProvinciaPersona) {
        this.idProvinciaPersona = idProvinciaPersona;
    }

    public String getIdDistritoPersona() {
        return idDistritoPersona;
    }

    public void setIdDistritoPersona(String idDistritoPersona) {
        this.idDistritoPersona = idDistritoPersona;
    }

    public String getCodigoEstadoSolicitud() {
        return codigoEstadoSolicitud;
    }

    public void setCodigoEstadoSolicitud(String codigoEstadoSolicitud) {
        this.codigoEstadoSolicitud = codigoEstadoSolicitud;
    }

    public String getIdSolicitudAccesoTexto() {
        return idSolicitudAccesoTexto;
    }

    public void setIdSolicitudAccesoTexto(String idSolicitudAccesoTexto) {
        this.idSolicitudAccesoTexto = idSolicitudAcceso.toString();
    }

    public String getTipoCncc() {
        return tipoCncc;
    }

    public void setTipoCncc(String tipoCncc) {
        this.tipoCncc = tipoCncc;
    }

    public String getEstadoSolicitud() {
        return estadoSolicitud;
    }

    public void setEstadoSolicitud(String estadoSolicitud) {
        this.estadoSolicitud = estadoSolicitud;
    }

    public String getCodigoTipoCncc() {
        return codigoTipoCncc;
    }

    public void setCodigoTipoCncc(String codigoTipoCncc) {
        this.codigoTipoCncc = codigoTipoCncc;
    }

    public String getNombreUsuarioRevision() {
        return nombreUsuarioRevision;
    }

    public void setNombreUsuarioRevision(String nombreUsuarioRevision) {
        this.nombreUsuarioRevision = nombreUsuarioRevision;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getEvalSolic() {
        return evalSolic;
    }

    public void setEvalSolic(String evalSolic) {
        this.evalSolic = evalSolic;
    }

    public String getEvalEmpresa() {
        return evalEmpresa;
    }

    public void setEvalEmpresa(String evalEmpresa) {
        this.evalEmpresa = evalEmpresa;
    }

    public String getTipoActor() {
        return tipoActor;
    }

    public void setTipoActor(String tipoActor) {
        this.tipoActor = tipoActor;
    }

    public String getNombreDepartamento() {
        return nombreDepartamento;
    }

    public void setNombreDepartamento(String nombreDepartamento) {
        this.nombreDepartamento = nombreDepartamento;
    }

    public String getNombreProvincia() {
        return nombreProvincia;
    }

    public void setNombreProvincia(String nombreProvincia) {
        this.nombreProvincia = nombreProvincia;
    }

    public String getNombreDistrito() {
        return nombreDistrito;
    }

    public void setNombreDistrito(String nombreDistrito) {
        this.nombreDistrito = nombreDistrito;
    }

    public Integer getIdSolicitudAcceso() {
        return idSolicitudAcceso;
    }

    public void setIdSolicitudAcceso(Integer idSolicitudAcceso) {
        this.idSolicitudAcceso = idSolicitudAcceso;
    }



    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Date getFechaRespuesta() {
        return fechaRespuesta;
    }

    public void setFechaRespuesta(Date fechaRespuesta) {
        this.fechaRespuesta = fechaRespuesta;
    }

    public String getCodigoTipoPersona() {
        return codigoTipoPersona;
    }

    public void setCodigoTipoPersona(String codigoTipoPersona) {
        this.codigoTipoPersona = codigoTipoPersona;
    }

    public char getEsReprLegal() {
        return esReprLegal;
    }

    public void setEsReprLegal(char esReprLegal) {
        this.esReprLegal = esReprLegal;
    }

    public char getTieneAdjReprLegal() {
        return tieneAdjReprLegal;
    }

    public void setTieneAdjReprLegal(char tieneAdjReprLegal) {
        this.tieneAdjReprLegal = tieneAdjReprLegal;
    }

    public String getCodigoTipoDocumento() {
        return codigoTipoDocumento;
    }

    public void setCodigoTipoDocumento(String codigoTipoDocumento) {
        this.codigoTipoDocumento = codigoTipoDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getCodigoEvalSolic() {
        return codigoEvalSolic;
    }

    public void setCodigoEvalSolic(String codigoEvalSolic) {
        this.codigoEvalSolic = codigoEvalSolic;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumeroRucEmpresa() {
        return numeroRucEmpresa;
    }

    public void setNumeroRucEmpresa(String numeroRucEmpresa) {
        this.numeroRucEmpresa = numeroRucEmpresa;
    }

    public String getRazonSocialEmpresa() {
        return razonSocialEmpresa;
    }

    public void setRazonSocialEmpresa(String razonSocialEmpresa) {
        this.razonSocialEmpresa = razonSocialEmpresa;
    }

    public String getDireccionEmpresa() {
        return direccionEmpresa;
    }

    public void setDireccionEmpresa(String direccionEmpresa) {
        this.direccionEmpresa = direccionEmpresa;
    }

    public String getTelefonoEmpresa() {
        return telefonoEmpresa;
    }

    public void setTelefonoEmpresa(String telefonoEmpresa) {
        this.telefonoEmpresa = telefonoEmpresa;
    }

    public String getEmailEmpresa() {
        return emailEmpresa;
    }

    public void setEmailEmpresa(String emailEmpresa) {
        this.emailEmpresa = emailEmpresa;
    }

    public String getCodigoEvalEmpresa() {
        return codigoEvalEmpresa;
    }

    public void setCodigoEvalEmpresa(String codigoEvalEmpresa) {
        this.codigoEvalEmpresa = codigoEvalEmpresa;
    }

    public String getCodigoTipoActor() {
        return codigoTipoActor;
    }

    public void setCodigoTipoActor(String codigoTipoActor) {
        this.codigoTipoActor = codigoTipoActor;
    }

    public char getTieneAdjActor() {
        return tieneAdjActor;
    }

    public void setTieneAdjActor(char tieneAdjActor) {
        this.tieneAdjActor = tieneAdjActor;
    }

    public char getNotificaRevision() {
        return notificaRevision;
    }

    public void setNotificaRevision(char notificaRevision) {
        this.notificaRevision = notificaRevision;
    }

    public char getNotificaRegistro() {
        return notificaRegistro;
    }

    public void setNotificaRegistro(char notificaRegistro) {
        this.notificaRegistro = notificaRegistro;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getIdUsuarioRevision() {
        return idUsuarioRevision;
    }

    public void setIdUsuarioRevision(Integer idUsuarioRevision) {
        this.idUsuarioRevision = idUsuarioRevision;
    }

    public String getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(String codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public Integer getIdPerfil() { return this.idPerfil; }

    public void setIdPerfil(Integer idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getCodigoAplicacion() {
        return this.codigoAplicacion;
    }

    public void setCodigoAplicacion(String codigoAplicacion) {
        this.codigoAplicacion = codigoAplicacion;
    }
}
