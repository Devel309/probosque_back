package pe.gob.serfor.mcsniffs.entity.Evaluacion;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EvaluacionActualizarPlan {
    
    private Integer idPlanManejo;
    private String codigo;
    private Integer idUsuarioModificacion; 
}
