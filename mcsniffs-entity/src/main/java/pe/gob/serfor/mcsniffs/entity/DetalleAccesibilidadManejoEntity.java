package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;


public class DetalleAccesibilidadManejoEntity extends AuditoriaEntity implements Serializable {
    private Integer idDetalleAcceManejo;
    private String anexo;
    private String sector;
    private Integer subParcela;
    private String referencia;
    private String via;
    private Double distancia;
    private Double tiempo;
    private Double tiempoVehiculo;
    private Integer idAcceManejo;

    public Integer getIdDetalleAcceManejo() {
        return idDetalleAcceManejo;
    }

    public void setIdDetalleAcceManejo(Integer idDetalleAcceManejo) {
        this.idDetalleAcceManejo = idDetalleAcceManejo;
    }

    public String getAnexo() {
        return anexo;
    }

    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public Integer getSubParcela() {
        return subParcela;
    }

    public void setSubParcela(Integer subParcela) {
        this.subParcela = subParcela;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public Double getDistancia() {
        return distancia;
    }

    public void setDistancia(Double distancia) {
        this.distancia = distancia;
    }

    public Double getTiempo() {
        return tiempo;
    }

    public void setTiempo(Double tiempo) {
        this.tiempo = tiempo;
    }

    public Double getTiempoVehiculo() {
        return tiempoVehiculo;
    }

    public void setTiempoVehiculo(Double tiempoVehiculo) {
        this.tiempoVehiculo = tiempoVehiculo;
    }

    public Integer getIdAcceManejo() {
        return idAcceManejo;
    }

    public void setIdAcceManejo(Integer idAcceManejo) {
        this.idAcceManejo = idAcceManejo;
    }
}
