package pe.gob.serfor.mcsniffs.entity;

import java.util.Date;
import java.util.List;

public class CensoForestalEntity {
    private Integer idCensoForestal;
    private String correlativo;
    private Integer idUsuarioModificacion;
    private Date fechaModificacion;
    private String estado;
    private Integer idUsuarioElimina;
    private Date fechaElimina;
    private Integer idUsuarioRegistro;
    private Date fechaRegistro;
    private String tipoCenso;
    private Integer idPlanManejo;
    private Date fechaInicio;
    private Date fechaFin;
    private String strFechaInicio;
    private String strFechaFin;

    private List<CensoForestalDetalleEntity> listCensoForestalDetalle;

    public List<CensoForestalDetalleEntity> getListCensoForestalDetalle() {
        return listCensoForestalDetalle;
    }

    public void setListCensoForestalDetalle(List<CensoForestalDetalleEntity> listCensoForestalDetalle) {
        this.listCensoForestalDetalle = listCensoForestalDetalle;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public Integer getIdCensoForestal() {
        return idCensoForestal;
    }

    public void setIdCensoForestal(Integer idCensoForestal) {
        this.idCensoForestal = idCensoForestal;
    }

    public String getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(String correlativo) {
        this.correlativo = correlativo;
    }

    public Integer getIdUsuarioModificacion() {
        return idUsuarioModificacion;
    }

    public void setIdUsuarioModificacion(Integer idUsuarioModificacion) {
        this.idUsuarioModificacion = idUsuarioModificacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Integer getIdUsuarioElimina() {
        return idUsuarioElimina;
    }

    public void setIdUsuarioElimina(Integer idUsuarioElimina) {
        this.idUsuarioElimina = idUsuarioElimina;
    }

    public Date getFechaElimina() {
        return fechaElimina;
    }

    public void setFechaElimina(Date fechaElimina) {
        this.fechaElimina = fechaElimina;
    }

    public Integer getIdUsuarioRegistro() {
        return idUsuarioRegistro;
    }

    public void setIdUsuarioRegistro(Integer idUsuarioRegistro) {
        this.idUsuarioRegistro = idUsuarioRegistro;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getTipoCenso() {
        return tipoCenso;
    }

    public void setTipoCenso(String tipoCenso) {
        this.tipoCenso = tipoCenso;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getStrFechaInicio() {
        return strFechaInicio;
    }

    public void setStrFechaInicio(String fechaInicio) {
        this.strFechaInicio = fechaInicio;
    }

    public String getStrFechaFin() {
        return strFechaFin;
    }

    public void setStrFechaFin(String fechaFin) {
        this.strFechaFin = fechaFin;
    }
}
