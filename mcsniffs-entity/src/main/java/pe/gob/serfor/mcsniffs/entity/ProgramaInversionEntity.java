package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.List;


public class ProgramaInversionEntity extends AuditoriaEntity implements Serializable {
    private Integer idPgmf;
    private Integer idInversion;
    private String[] Descripcion;
    private List< ProgramaInversionDetalleEntity> Detalle;
    
    public Integer getIdPgmf() {
        return idPgmf;
    }
    public void setIdPgmf(Integer idPgmf) {
        this.idPgmf = idPgmf;
    }
    public Integer getIdInversion() {
        return idInversion;
    }
    public void setIdInversion(Integer idInversion) {
        this.idInversion = idInversion;
    }
    public String[] getDescripcion() {
        return Descripcion;
    }
    public void setDescripcion(String[] descripcion) {
        Descripcion = descripcion;
    }
    public List<ProgramaInversionDetalleEntity> getDetalle() {
        return Detalle;
    }
    public void setDetalle(List<ProgramaInversionDetalleEntity> detalle) {
        Detalle = detalle;
    }
     
}