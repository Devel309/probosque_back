package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
public class SolicitudRequestEntity extends AuditoriaEntity implements Serializable {
    private Integer IdSolicitud;
    private Integer IdProcesoPostulacion;
    private Integer IdTipoSolicitud;
    private Date FechaInicioSolicitud;
    private Date FechaFinSolicitud;
    private Boolean Aprobado;
    private String MotivoSolicitud;
    private Integer IdUsuarioSolicitante;
    private String TipoSolicitudDesc;
    private String Observacion;
    private Integer IdDocumentoAdjunto;
    private Date FechaRecepcionDocsFisicos;
    private String EmailSolicitante;
    private String IdProcesoOferta;
    private Integer IdUsuarioPostulacion;
    private Integer IdProcesoPostulacionGanador;


    private String nroPropiedad;
    private String nroPartidaRegistral;
    private String nroActaAsamblea;
    private String regenteForestal;
    private String nombreRegenteForestal;
    private String nroContratoRegente;
    private String escalaManejo;
    private String nroComprobantePago;

    private String descSolicitudReconocimiento;
    private String descSolicitudAmpliacion;
    private String descSolicitudTitulacion;
    private String descDocumentoResidencia;

    private String nroActaAsambleaAcuerdo;
    private String nroRucEmpresa;
    private String tipoAprovechamiento;
    private Integer idSolicitudAcceso;
    private Integer idPersonaRepresentante;
    private String nroContratoTercero;
    private String nroActaAprovechamiento;

    private String asunto;
    private Boolean flagAutorizacion;
}
