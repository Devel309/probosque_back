package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class SolicitudConcesionCriterioAreaEntity  extends AuditoriaEntity implements Serializable {

    private Integer idSolicitudConcesionCriterioArea;
    private Integer idSolicitudConcesionArea;
    private Integer idSolicitudConcesion;
    private String codigoCriterio;
    private String nombreCriterio;
    private String descripcionOtroCriterio;
    private Boolean estadoCriterio;
}
