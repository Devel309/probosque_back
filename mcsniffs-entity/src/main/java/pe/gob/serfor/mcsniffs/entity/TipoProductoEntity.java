package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.Date;

public class TipoProductoEntity implements Serializable {
    private short idTipoProducto;
    private String tipoProducto;
    private Integer idUnidad;
    private Integer idRecurso;
    private String idGrupoRecurso;
    private String estado;
    private Date fechaInicio;
    private Date fechaFin;

    public short getIdTipoProducto() { return idTipoProducto; }
    public void setIdTipoProducto(short idTipoProducto) { this.idTipoProducto = idTipoProducto; }
    public String getTipoProducto() { return tipoProducto; }
    public void setTipoProducto(String tipoProducto) { this.tipoProducto = tipoProducto; }
    public Integer getIdUnidad() { return idUnidad; }
    public void setIdUnidad(Integer idUnidad) { this.idUnidad = idUnidad; }
    public Integer getIdRecurso() { return idRecurso; }
    public void setIdRecurso(Integer idRecurso) { this.idRecurso = idRecurso; }
    public String getIdGrupoRecurso() { return idGrupoRecurso; }
    public void setIdGrupoRecurso(String idGrupoRecurso) { this.idGrupoRecurso = idGrupoRecurso; }
    public String getEstado() { return estado; }
    public void setEstado(String estado) { this.estado = estado; }
    public Date getFechaInicio() { return fechaInicio; }
    public void setFechaInicio(Date fechaInicio) { this.fechaInicio = fechaInicio; }
    public Date getFechaFin() { return fechaFin; }
    public void setFechaFin(Date fechaFin) { this.fechaFin = fechaFin; }
}