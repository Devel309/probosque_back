package pe.gob.serfor.mcsniffs.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.sql.Timestamp;

public class FeriadoEntity extends AuditoriaEntity implements Serializable {

    private Integer idFeriado;
    private Timestamp fechFeriado;
    private String descFeriado;
    private String codUbigeo;
    private Integer numFeriado;
    private Integer numAnio;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private FeriadoEntity feriadoEntity;
    private String fechFeriadoString;

    public FeriadoEntity getFeriadoEntity() {
        return feriadoEntity;
    }

    public void setFeriadoEntity(FeriadoEntity feriadoEntity) {
        this.feriadoEntity = feriadoEntity;
    }

    public String getFechFeriadoString() {
        return fechFeriadoString;
    }

    public void setFechFeriadoString(String fechFeriadoString) {
        this.fechFeriadoString = fechFeriadoString;
    }

    public Integer getIdFeriado() {
        return idFeriado;
    }

    public void setIdFeriado(Integer idFeriado) {
        this.idFeriado = idFeriado;
    }

    public Timestamp getFechFeriado() {
        return fechFeriado;
    }

    public void setFechFeriado(Timestamp fechFeriado) {
        this.fechFeriado = fechFeriado;
    }

    public String getDescFeriado() {
        return descFeriado;
    }

    public void setDescFeriado(String descFeriado) {
        this.descFeriado = descFeriado;
    }

    public String getCodUbigeo() {
        return codUbigeo;
    }

    public void setCodUbigeo(String codUbigeo) {
        this.codUbigeo = codUbigeo;
    }

    public Integer getNumFeriado() {
        return numFeriado;
    }

    public void setNumFeriado(Integer numFeriado) {
        this.numFeriado = numFeriado;
    }

    public Integer getNumAnio() {
        return numAnio;
    }

    public void setNumAnio(Integer numAnio) {
        this.numAnio = numAnio;
    }
}
