package pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EvaluacionPgmfDto extends AuditoriaEntity implements Serializable {
 
    private Integer idPlanManejo;
    private Boolean conforme;
}
