package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ResultadoPPEntity extends AuditoriaEntity implements Serializable {
    private Integer IdResultadoPP;
    private Integer IdProcesoPostulacion;
    private String AsuntoEmail;
    private String ContenidoEmail;
    private Integer IdAutoridadRegional;
    private Date FechaEnvioResultado;
    private Integer NotaTotal;
    private Boolean Ganador;
    private Integer IdDocumentoAdjunto;
    private Integer IdUsuarioPostulacion;
    private String CorreoPostulante;

    public String getCorreoPostulante() {
        return CorreoPostulante;
    }

    public void setCorreoPostulante(String correoPostulante) {
        CorreoPostulante = correoPostulante;
    }

    public Integer getIdUsuarioPostulacion() {
        return IdUsuarioPostulacion;
    }

    public void setIdUsuarioPostulacion(Integer idUsuarioPostulacion) {
        IdUsuarioPostulacion = idUsuarioPostulacion;
    }

    public Integer getIdDocumentoAdjunto() {
        return IdDocumentoAdjunto;
    }

    public void setIdDocumentoAdjunto(Integer idDocumentoAdjunto) {
        IdDocumentoAdjunto = idDocumentoAdjunto;
    }

    public Integer getIdResultadoPP() {
        return IdResultadoPP;
    }

    public void setIdResultadoPP(Integer idResultadoPP) {
        IdResultadoPP = idResultadoPP;
    }

    public Integer getIdProcesoPostulacion() {
        return IdProcesoPostulacion;
    }

    public void setIdProcesoPostulacion(Integer idProcesoPostulacion) {
        IdProcesoPostulacion = idProcesoPostulacion;
    }

    public String getAsuntoEmail() {
        return AsuntoEmail;
    }

    public void setAsuntoEmail(String asuntoEmail) {
        AsuntoEmail = asuntoEmail;
    }

    public String getContenidoEmail() {
        return ContenidoEmail;
    }

    public void setContenidoEmail(String contenidoEmail) {
        ContenidoEmail = contenidoEmail;
    }

    public Integer getIdAutoridadRegional() {
        return IdAutoridadRegional;
    }

    public void setIdAutoridadRegional(Integer idAutoridadRegional) {
        IdAutoridadRegional = idAutoridadRegional;
    }

    public Date getFechaEnvioResultado() {
        return FechaEnvioResultado;
    }

    public void setFechaEnvioResultado(Date fechaEnvioResultado) {
        FechaEnvioResultado = fechaEnvioResultado;
    }

    public Integer getNotaTotal() {
        return NotaTotal;
    }

    public void setNotaTotal(Integer notaTotal) {
        NotaTotal = notaTotal;
    }

    public Boolean getGanador() {
        return Ganador;
    }

    public void setGanador(Boolean ganador) {
        Ganador = ganador;
    }
}
