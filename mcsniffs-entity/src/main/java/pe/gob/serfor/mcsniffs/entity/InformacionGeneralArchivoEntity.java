package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class InformacionGeneralArchivoEntity extends AuditoriaEntity implements Serializable {
   private Integer idInfGeneralArch;
   private Integer idArchivo;
   private InformacionGeneralEntity informacionGeneral;

    public Integer getIdInfGeneralArch() {
        return idInfGeneralArch;
    }

    public void setIdInfGeneralArch(Integer idInfGeneralArch) {
        this.idInfGeneralArch = idInfGeneralArch;
    }

    public Integer getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }

    public InformacionGeneralEntity getInformacionGeneral() {
        return informacionGeneral;
    }

    public void setInformacionGeneral(InformacionGeneralEntity informacionGeneral) {
        this.informacionGeneral = informacionGeneral;
    }
}
