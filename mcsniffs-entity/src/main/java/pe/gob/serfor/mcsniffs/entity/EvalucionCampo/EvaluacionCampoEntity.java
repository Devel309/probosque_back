package pe.gob.serfor.mcsniffs.entity.EvalucionCampo;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EvaluacionCampoEntity extends AuditoriaEntity implements Serializable{
    
    Integer idEvaluacionCampo ;
    Integer  idEvaluacionCampoEstado;
    Integer  idPlanManejo ;
    Boolean  cites ;
    Date fechaInicioEvaluacion;
    Date fechaFinEvaluacion ;
    Date fechaInicioImpedimentos ;
    Date fechaFinImpedimentos;
    Boolean  comunidadNatidadCampesina ;
    Boolean  resultadoFavorable;
    Boolean  notificacion;
    Integer  idEvaluacionCampoVersion ;
    String  evaluacionCampoEstado;
    String   titular;
    String   apellidoTitular;
    String   numThActoAdmin;
    Integer documentoGestion;
    String tipoDocumentoGestion;
    String tipoDocumentoGestionTexto;
}
