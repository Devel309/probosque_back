package pe.gob.serfor.mcsniffs.entity.CoreCentral;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

public class DepartamentoEntity extends AuditoriaEntity {
    Integer idDepartamento;
    String nombreDepartamento;
    String codDepartamento;
    String codDepartamentoInei;
    String codDepartamentoReniec;
    String codDepartamentoSunat;


    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public String getNombreDepartamento() {
        return nombreDepartamento;
    }

    public void setNombreDepartamento(String nombreDepartamento) {
        this.nombreDepartamento = nombreDepartamento;
    }

    public String getCodDepartamento() {
        return codDepartamento;
    }

    public void setCodDepartamento(String codDepartamento) {
        this.codDepartamento = codDepartamento;
    }

    public String getCodDepartamentoInei() {
        return codDepartamentoInei;
    }

    public void setCodDepartamentoInei(String codDepartamentoInei) {
        this.codDepartamentoInei = codDepartamentoInei;
    }

    public String getCodDepartamentoReniec() {
        return codDepartamentoReniec;
    }

    public void setCodDepartamentoReniec(String codDepartamentoReniec) {
        this.codDepartamentoReniec = codDepartamentoReniec;
    }

    public String getCodDepartamentoSunat() {
        return codDepartamentoSunat;
    }

    public void setCodDepartamentoSunat(String codDepartamentoSunat) {
        this.codDepartamentoSunat = codDepartamentoSunat;
    }
}
