package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class UnidadMedidaEntity implements Serializable {
    private short idUnidadMedida;
    private String descripcion;
    private String simbolo;
    private short codigoSunat;

    public short getIdUnidadMedida() { return idUnidadMedida; }
    public void setIdUnidadMedida(short idUnidadMedida) { this.idUnidadMedida = idUnidadMedida; }
    public String getDescripcion() { return descripcion; }
    public void setDescripcion(String descripcion) { this.descripcion = descripcion; }
    public String getSimbolo() { return simbolo; }
    public void setSimbolo(String simbolo) { this.simbolo = simbolo; }
    public short getCodigoSunat() { return codigoSunat; }
    public void setCodigoSunat(short codigoSunat) { this.codigoSunat = codigoSunat; }

}