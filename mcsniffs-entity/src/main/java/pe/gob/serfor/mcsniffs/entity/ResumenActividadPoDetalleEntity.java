package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ResumenActividadPoDetalleEntity extends AuditoriaEntity implements Serializable {
    private Integer idResumenActDet;
    private Integer tipoResumen;
    private String actividad;
    private String indicador;
    private String programado;
    private String realizado;
    private String resumen;
    private String aspectoRecomendacion;
    private Integer idResumenAct;
    private String aspectoPositivo;
    private String aspectoNegativo;

    public String getAspectoPositivo() {
        return aspectoPositivo;
    }

    public void setAspectoPositivo(String aspectoPositivo) {
        this.aspectoPositivo = aspectoPositivo;
    }

    public String getAspectoNegativo() {
        return aspectoNegativo;
    }

    public void setAspectoNegativo(String aspectoNegativo) {
        this.aspectoNegativo = aspectoNegativo;
    }

    public Integer getTipoResumen() {
        return tipoResumen;
    }

    public ResumenActividadPoDetalleEntity setTipoResumen(Integer tipoResumen) {
        this.tipoResumen = tipoResumen;
        return this;
    }

    public Integer getIdResumenActDet() {
        return idResumenActDet;
    }

    public void setIdResumenActDet(Integer idResumenActDet) {
        this.idResumenActDet = idResumenActDet;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getIndicador() {
        return indicador;
    }

    public void setIndicador(String indicador) {
        this.indicador = indicador;
    }

    public String getProgramado() {
        return programado;
    }

    public void setProgramado(String programado) {
        this.programado = programado;
    }

    public String getRealizado() {
        return realizado;
    }

    public void setRealizado(String realizado) {
        this.realizado = realizado;
    }

    public String getResumen() {
        return resumen;
    }

    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

    public String getAspectoRecomendacion() {
        return aspectoRecomendacion;
    }

    public void setAspectoRecomendacion(String aspectoRecomendacion) {
        this.aspectoRecomendacion = aspectoRecomendacion;
    }

    public Integer getIdResumenAct() {
        return idResumenAct;
    }

    public void setIdResumenAct(Integer idResumenAct) {
        this.idResumenAct = idResumenAct;
    }
}
