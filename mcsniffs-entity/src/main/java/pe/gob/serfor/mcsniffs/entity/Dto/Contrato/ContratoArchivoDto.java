package pe.gob.serfor.mcsniffs.entity.Dto.Contrato;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity; 

@Getter
@Setter
public class ContratoArchivoDto extends  AuditoriaEntity implements Serializable {
    
    private Integer idArchivoContrato;
    private Integer idContrato;
    private Integer idArchivo;
    private String tipoDocumento;
    private String nombreArchivo;
    private String nombreGenerado;
    private String prefijo;
    private String descripcionTipoDocumento;
    private String tipoArchivo;
}
