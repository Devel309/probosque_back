package pe.gob.serfor.mcsniffs.entity.Parametro;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.RecursoForestalEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RecursoForestalDto extends AuditoriaEntity implements Serializable {
    private Integer idPlanManejo;
    private Integer idRecursoForestal;

    private String codigoTipoRecursoForestal;
    private String nombreComun;
    private String nombreCientifico;
    private String nombreNativo;
    private String coordenadaEste;
    private String coordenadaNorte;
    private Integer numeroArbolesAprovechables;
    private String volumenComercial;
    private Integer numeroArbolesSemilleros;
    private Integer numeroArbolesTotal;
    private List<RecursoForestalEntity> listRecursoForestal;

    public RecursoForestalDto(){
        this.listRecursoForestal = new ArrayList<>();
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public Integer getIdRecursoForestal() {
        return idRecursoForestal;
    }

    public void setIdRecursoForestal(Integer idRecursoForestal) {
        this.idRecursoForestal = idRecursoForestal;
    }

    public String getCodigoTipoRecursoForestal() {
        return codigoTipoRecursoForestal;
    }

    public void setCodigoTipoRecursoForestal(String codigoTipoRecursoForestal) {
        this.codigoTipoRecursoForestal = codigoTipoRecursoForestal;
    }

    public String getNombreComun() {
        return nombreComun;
    }

    public void setNombreComun(String nombreComun) {
        this.nombreComun = nombreComun;
    }

    public String getNombreCientifico() {
        return nombreCientifico;
    }

    public void setNombreCientifico(String nombreCientifico) {
        this.nombreCientifico = nombreCientifico;
    }

    public String getNombreNativo() {
        return nombreNativo;
    }

    public void setNombreNativo(String nombreNativo) {
        this.nombreNativo = nombreNativo;
    }

    public String getCoordenadaEste() {
        return coordenadaEste;
    }

    public void setCoordenadaEste(String coordenadaEste) {
        this.coordenadaEste = coordenadaEste;
    }

    public String getCoordenadaNorte() {
        return coordenadaNorte;
    }

    public void setCoordenadaNorte(String coordenadaNorte) {
        this.coordenadaNorte = coordenadaNorte;
    }

    public Integer getNumeroArbolesAprovechables() {
        return numeroArbolesAprovechables;
    }

    public void setNumeroArbolesAprovechables(Integer numeroArbolesAprovechables) {
        this.numeroArbolesAprovechables = numeroArbolesAprovechables;
    }

    public String getVolumenComercial() {
        return volumenComercial;
    }

    public void setVolumenComercial(String volumenComercial) {
        this.volumenComercial = volumenComercial;
    }

    public Integer getNumeroArbolesSemilleros() {
        return numeroArbolesSemilleros;
    }

    public void setNumeroArbolesSemilleros(Integer numeroArbolesSemilleros) {
        this.numeroArbolesSemilleros = numeroArbolesSemilleros;
    }

    public Integer getNumeroArbolesTotal() {
        return numeroArbolesTotal;
    }

    public void setNumeroArbolesTotal(Integer numeroArbolesTotal) {
        this.numeroArbolesTotal = numeroArbolesTotal;
    }

    public List<RecursoForestalEntity> getListRecursoForestal() {
        return listRecursoForestal;
    }

    public void setListRecursoForestal(List<RecursoForestalEntity> listRecursoForestal) {
        this.listRecursoForestal = listRecursoForestal;
    }
}
