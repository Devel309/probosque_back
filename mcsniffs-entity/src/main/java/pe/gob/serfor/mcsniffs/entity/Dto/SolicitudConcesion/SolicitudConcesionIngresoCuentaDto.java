package pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;

@Data
public class SolicitudConcesionIngresoCuentaDto extends AuditoriaEntity implements Serializable {

    private Integer idSolicitudConcesionIngresoCuenta;
    private Integer idSolicitudConcesion;
    private String codigoTipoDocumento;
    private String numeroTipoDocumento;
    private String descripcion;
    private Double monto;


}
