package pe.gob.serfor.mcsniffs.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

public class ParticipacionComunalEntity extends AuditoriaEntity implements Serializable {
    private Integer idPartComunal;
    private PlanManejoEntity planManejo;
    private Integer idPlanManejo;
    private String actividad;
    private String responsable;
    private String seguimientoActividad;
    private String codTipoPartComunal;
    private String lugar;
    private List<ParticipacionComunalDetEntity> lstDetalle;


    
    public List<ParticipacionComunalDetEntity> getLstDetalle() {
        return lstDetalle;
    }

    public void setLstDetalle(List<ParticipacionComunalDetEntity> lstDetalle) {
        this.lstDetalle = lstDetalle;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="America/Lima")

    private Date fecha;

    public Integer getIdPartComunal() {
        return idPartComunal;
    }

    public void setIdPartComunal(Integer idPartComunal) {
        this.idPartComunal = idPartComunal;
    }

    public PlanManejoEntity getPlanManejo() {
        return planManejo;
    }

    public void setPlanManejo(PlanManejoEntity planManejo) {
        this.planManejo = planManejo;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getSeguimientoActividad() {
        return seguimientoActividad;
    }

    public void setSeguimientoActividad(String seguimientoActividad) {
        this.seguimientoActividad = seguimientoActividad;
    }

    public String getCodTipoPartComunal() {
        return codTipoPartComunal;
    }

    public void setCodTipoPartComunal(String codTipoPartComunal) {
        this.codTipoPartComunal = codTipoPartComunal;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

}
