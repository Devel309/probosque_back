package pe.gob.serfor.mcsniffs.entity;

public class ContratoAdjuntosEntity {
    private ContratoResponseRequest contrato;
    private byte[] documento;
    private String nombredocumento;
    private Integer IdTipoDocumento;
    
    
   
    public ContratoResponseRequest getContrato() {
        return contrato;
    }
    public void setContrato(ContratoResponseRequest contrato) {
        this.contrato = contrato;
    }
    public Integer getIdTipoDocumento() {
        return IdTipoDocumento;
    }
    public void setIdTipoDocumento(Integer idTipoDocumento) {
        IdTipoDocumento = idTipoDocumento;
    }
    public byte[] getDocumento() {
        return documento;
    }
    public void setDocumento(byte[] documento) {
        this.documento = documento;
    }
    public String getNombredocumento() {
        return nombredocumento;
    }
    public void setNombredocumento(String nombredocumento) {
        this.nombredocumento = nombredocumento;
    }
    
}
