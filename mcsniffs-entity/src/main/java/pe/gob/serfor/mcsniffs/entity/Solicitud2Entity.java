package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;


public class Solicitud2Entity  implements Serializable {
    private Integer idSolicitud;
    private String motivoSolicitud; 
    private String motivoPostulacion;
    public Integer getIdSolicitud() {
        return idSolicitud;
    }
    public void setIdSolicitud(Integer idSolicitud) {
        this.idSolicitud = idSolicitud;
    }
    public String getMotivoSolicitud() {
        return motivoSolicitud;
    }
    public void setMotivoSolicitud(String motivoSolicitud) {
        this.motivoSolicitud = motivoSolicitud;
    }
    public String getMotivoPostulacion() {
        return motivoPostulacion;
    }
    public void setMotivoPostulacion(String motivoPostulacion) {
        this.motivoPostulacion = motivoPostulacion;
    } 
    
}
