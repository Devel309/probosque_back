package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ManejoBosqueEspecieEntity extends AuditoriaEntity implements Serializable {
    private Integer idManejoBosqueEsp;
    private ManejoBosqEntity manejoBosque;
    private short idEspecie;
    private String especie;
    private Double diametro;
    private String lineaProduccion;

    public Integer getIdManejoBosqueEsp() {
        return idManejoBosqueEsp;
    }

    public void setIdManejoBosqueEsp(Integer idManejoBosqueEsp) {
        this.idManejoBosqueEsp = idManejoBosqueEsp;
    }

    public ManejoBosqEntity getManejoBosque() {
        return manejoBosque;
    }

    public void setManejoBosque(ManejoBosqEntity manejoBosque) {
        this.manejoBosque = manejoBosque;
    }

    public short getIdEspecie() {
        return idEspecie;
    }

    public void setIdEspecie(short idEspecie) {
        this.idEspecie = idEspecie;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public Double getDiametro() {
        return diametro;
    }

    public void setDiametro(Double diametro) {
        this.diametro = diametro;
    }

    public String getLineaProduccion() {
        return lineaProduccion;
    }

    public void setLineaProduccion(String lineaProduccion) {
        this.lineaProduccion = lineaProduccion;
    }
}
