package pe.gob.serfor.mcsniffs.entity.Dto.Resolucion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
@Data
public class ResolucionDto extends AuditoriaEntity implements Serializable {

    Integer idResolucion;
    String tipoDocumentoGestion;
    Integer nroDocumentoGestion;
    Integer idArchivoResolucion;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    Date fechaResolucion;
    String estadoResolucion;
    String comentarioResolucion;
    String textoConcatenado;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    Date fechaCancelacion;
    Integer idResolucionPrevia;
    Integer idPlanManejoEvaluacion;
    String asunto;
    List<ResolucionArchivoDto>listaResolucionArchivo;
    Integer idSolicitante;
    Integer idSolicitud;
}
