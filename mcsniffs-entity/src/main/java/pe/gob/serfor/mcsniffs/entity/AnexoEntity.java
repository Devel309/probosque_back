package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class AnexoEntity extends AuditoriaEntity  implements Serializable {
    private Integer IdAnexo;
    private Integer IdPlanManejo;

    public Integer getIdPlanManejo() { return IdPlanManejo;  }
    public void setIdPlanManejo(Integer idPlanManejo) { IdPlanManejo = idPlanManejo;  }
    public Integer getIdAnexo() {return IdAnexo;}
    public void setIdAnexo(Integer idAnexo) {IdAnexo = idAnexo;}

}
