package pe.gob.serfor.mcsniffs.entity.Dto.Contrato;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity; 

@Getter
@Setter
public class ContratoPersonaDto extends  AuditoriaEntity implements Serializable {
    
    private Integer idContratoPersona;
    private Integer idContrato;
    private String tipoPersona;
    private String tipoDocumento;
    private String numeroDocumento;
    private String nombres;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String razonSocial;
    private String tipoRelacion;
 
}
