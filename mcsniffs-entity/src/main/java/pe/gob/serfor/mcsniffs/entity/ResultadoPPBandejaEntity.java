package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.Date;

public class ResultadoPPBandejaEntity implements Serializable {
    private Integer idProcesoOferta;
    private Integer idProcesoPostulacion;
    private Integer idUsuarioPostulacion;
    private String nombreUsuarioPostulacion;
    private Integer idStatusProcesoPostulacion;
    private String nombreStatusProcesoPostulacion;
    private Date fechaPostulacion;
    private Integer notaTotal;
    private Boolean ganador;
    private Integer idResultadoPP;
    private String representanteLegal;

    public String getRepresentanteLegal() {
        return representanteLegal;
    }

    public void setRepresentanteLegal(String representanteLegal) {
        this.representanteLegal = representanteLegal;
    }

    public Integer getIdResultadoPP() {
        return idResultadoPP;
    }

    public void setIdResultadoPP(Integer idResultadoPP) {
        this.idResultadoPP = idResultadoPP;
    }

    public Integer getIdProcesoOferta() {
        return idProcesoOferta;
    }

    public void setIdProcesoOferta(Integer idProcesoOferta) {
        this.idProcesoOferta = idProcesoOferta;
    }

    public Integer getIdProcesoPostulacion() {
        return idProcesoPostulacion;
    }

    public void setIdProcesoPostulacion(Integer idProcesoPostulacion) {
        this.idProcesoPostulacion = idProcesoPostulacion;
    }

    public Integer getIdUsuarioPostulacion() {
        return idUsuarioPostulacion;
    }

    public void setIdUsuarioPostulacion(Integer idUsuarioPostulacion) {
        this.idUsuarioPostulacion = idUsuarioPostulacion;
    }

    public String getNombreUsuarioPostulacion() {
        return nombreUsuarioPostulacion;
    }

    public void setNombreUsuarioPostulacion(String nombreUsuarioPostulacion) {
        this.nombreUsuarioPostulacion = nombreUsuarioPostulacion;
    }

    public Integer getIdStatusProcesoPostulacion() {
        return idStatusProcesoPostulacion;
    }

    public void setIdStatusProcesoPostulacion(Integer idStatusProcesoPostulacion) {
        this.idStatusProcesoPostulacion = idStatusProcesoPostulacion;
    }

    public String getNombreStatusProcesoPostulacion() {
        return nombreStatusProcesoPostulacion;
    }

    public void setNombreStatusProcesoPostulacion(String nombreStatusProcesoPostulacion) {
        this.nombreStatusProcesoPostulacion = nombreStatusProcesoPostulacion;
    }

    public Date getFechaPostulacion() {
        return fechaPostulacion;
    }

    public void setFechaPostulacion(Date fechaPostulacion) {
        this.fechaPostulacion = fechaPostulacion;
    }

    public Integer getNotaTotal() {
        return notaTotal;
    }

    public void setNotaTotal(Integer notaTotal) {
        this.notaTotal = notaTotal;
    }

    public Boolean getGanador() {
        return ganador;
    }

    public void setGanador(Boolean ganador) {
        this.ganador = ganador;
    }
}
