package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ArchivoSolicitudAccesoEntity extends AuditoriaEntity implements Serializable {

    private Integer idArchivoSolicitud ;
    private Integer idSolicitudAcceso;
    private Integer idArchivo;

    public Integer getIdArchivoSolicitud() {
        return idArchivoSolicitud;
    }

    public void setIdArchivoSolicitud(Integer idArchivoSolicitud) {
        this.idArchivoSolicitud = idArchivoSolicitud;
    }

    public Integer getIdSolicitudAcceso() {
        return idSolicitudAcceso;
    }

    public void setIdSolicitudAcceso(Integer idSolicitudAcceso) {
        this.idSolicitudAcceso = idSolicitudAcceso;
    }

    public Integer getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }
}
