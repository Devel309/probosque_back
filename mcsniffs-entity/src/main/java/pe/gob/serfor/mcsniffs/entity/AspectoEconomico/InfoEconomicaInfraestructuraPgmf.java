package pe.gob.serfor.mcsniffs.entity.AspectoEconomico;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;


@Data
public class InfoEconomicaInfraestructuraPgmf extends AuditoriaEntity {
    Integer idInfoEconomicaInfraestructura;
    Integer idInformacionSocioEconomica;
    String idTipoinfraestructura;
    String tipoInfraestructura;
    Integer este;
    Integer norte;
    String zonautm;
    String referencia;
    Boolean activo;
    String descripcion;
}
