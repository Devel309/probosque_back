package pe.gob.serfor.mcsniffs.entity.Parametro;

import pe.gob.serfor.mcsniffs.entity.AccesibilidadManejoMatrizDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.util.List;

public class AccesibilidadManejoMatrizDto extends AuditoriaEntity implements Serializable {

    private Integer idAcceManejomatriz;
    private String anexosector;
    private Integer idAcceManejo;
    private List<AccesibilidadManejoMatrizDetalleEntity> listAccesibilidadManejoMatrizDetalle;

    public Integer getIdAcceManejomatriz() {
        return idAcceManejomatriz;
    }

    public void setIdAcceManejomatriz(Integer idAcceManejomatriz) {
        this.idAcceManejomatriz = idAcceManejomatriz;
    }

    public String getAnexosector() {
        return anexosector;
    }

    public void setAnexosector(String anexosector) {
        this.anexosector = anexosector;
    }

    public Integer getIdAcceManejo() {
        return idAcceManejo;
    }

    public void setIdAcceManejo(Integer idAcceManejo) {
        this.idAcceManejo = idAcceManejo;
    }

    public List<AccesibilidadManejoMatrizDetalleEntity> getListAccesibilidadManejoMatrizDetalle() {
        return listAccesibilidadManejoMatrizDetalle;
    }

    public void setListAccesibilidadManejoMatrizDetalle(List<AccesibilidadManejoMatrizDetalleEntity> listAccesibilidadManejoMatrizDetalle) {
        this.listAccesibilidadManejoMatrizDetalle = listAccesibilidadManejoMatrizDetalle;
    }
}
