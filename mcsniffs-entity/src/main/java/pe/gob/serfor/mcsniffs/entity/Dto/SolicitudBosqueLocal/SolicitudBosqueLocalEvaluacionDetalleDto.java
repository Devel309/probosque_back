package pe.gob.serfor.mcsniffs.entity.Dto.SolicitudBosqueLocal;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import lombok.Data;
import java.io.Serializable;

@Data
public class SolicitudBosqueLocalEvaluacionDetalleDto extends AuditoriaEntity implements Serializable{

    private Integer idSolBosqueLocalEvaluacionDetalle;
    private Integer idSolBosqueLocalEvaluacion;
    private Integer idSolBosqueLocal;
    private String codigoSeccion;
    private String codigoSubSeccion;
    private Boolean validado;
    private String observacion;
}
