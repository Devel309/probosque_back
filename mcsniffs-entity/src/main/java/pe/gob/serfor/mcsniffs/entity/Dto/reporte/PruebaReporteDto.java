package pe.gob.serfor.mcsniffs.entity.Dto.reporte;

import lombok.Data;

@Data
public class PruebaReporteDto {

    
    private String descripcion;
    private String titulo;
    private String logo;


}
