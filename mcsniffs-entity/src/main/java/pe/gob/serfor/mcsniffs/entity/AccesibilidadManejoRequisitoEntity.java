package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class AccesibilidadManejoRequisitoEntity extends AuditoriaEntity implements Serializable {

    private Integer idAcceManejoreq;
    private String requisito;
    private Integer idAcceManejo;


    public Integer getIdAcceManejoreq() {
        return idAcceManejoreq;
    }

    public void setIdAcceManejoreq(Integer idAcceManejoreq) {
        this.idAcceManejoreq = idAcceManejoreq;
    }

    public String getRequisito() {
        return requisito;
    }

    public void setRequisito(String requisito) {
        this.requisito = requisito;
    }

    public Integer getIdAcceManejo() {
        return idAcceManejo;
    }

    public void setIdAcceManejo(Integer idAcceManejo) {
        this.idAcceManejo = idAcceManejo;
    }
}
