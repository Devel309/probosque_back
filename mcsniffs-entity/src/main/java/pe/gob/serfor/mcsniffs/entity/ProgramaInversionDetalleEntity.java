package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.math.BigDecimal;


public class ProgramaInversionDetalleEntity extends AuditoriaEntity implements Serializable {
    
    private Integer idInversionDetalle;
    private String Ingreso;
    private String Egreso;
    private BigDecimal IngresoMonto;
    private BigDecimal EgresoMonto;
    private Integer anio;
    private String Rubro;

    
    public String getRubro() {
        return Rubro;
    }
    public void setRubro(String rubro) {
        Rubro = rubro;
    }
    public Integer getIdInversionDetalle() {
        return idInversionDetalle;
    }
    public void setIdInversionDetalle(Integer idInversionDetalle) {
        this.idInversionDetalle = idInversionDetalle;
    }
    public String getIngreso() {
        return Ingreso;
    }
    public void setIngreso(String ingreso) {
        Ingreso = ingreso;
    }
    public String getEgreso() {
        return Egreso;
    }
    public void setEgreso(String egreso) {
        Egreso = egreso;
    }
    public BigDecimal getIngresoMonto() {
        return IngresoMonto;
    }
    public void setIngresoMonto(BigDecimal ingresoMonto) {
        IngresoMonto = ingresoMonto;
    }
    public BigDecimal getEgresoMonto() {
        return EgresoMonto;
    }
    public void setEgresoMonto(BigDecimal egresoMonto) {
        EgresoMonto = egresoMonto;
    }
    public Integer getAnio() {
        return anio;
    }
    public void setAnio(Integer anio) {
        this.anio = anio;
    }    
}