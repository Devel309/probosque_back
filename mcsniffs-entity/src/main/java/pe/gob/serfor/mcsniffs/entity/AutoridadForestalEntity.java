package pe.gob.serfor.mcsniffs.entity;

public class AutoridadForestalEntity extends AuditoriaEntity {
    Integer IdAutoridadForestal;
     String Nombre;

    public Integer getIdAutoridadForestal() {
        return IdAutoridadForestal;
    }

    public void setIdAutoridadForestal(Integer idAutoridadForestal) {
        IdAutoridadForestal = idAutoridadForestal;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }
}
