package pe.gob.serfor.mcsniffs.entity;

public class AreaSolicitadaUTMEntity {
    //value1 - en la tabla anexosPFDM
    private String vertice;
    //value2 - en la tabla anexosPFDM
    private String este;
    //value3 - en la tabla anexosPFDM
    private String norte;
    
    public String getVertice() {
        return vertice;
    }
    public void setVertice(String vertice) {
        this.vertice = vertice;
    }
    public String getEste() {
        return este;
    }
    public void setEste(String este) {
        this.este = este;
    }
    public String getNorte() {
        return norte;
    }
    public void setNorte(String norte) {
        this.norte = norte;
    }
    
}
