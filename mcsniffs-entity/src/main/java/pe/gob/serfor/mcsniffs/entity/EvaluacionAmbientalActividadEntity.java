package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class EvaluacionAmbientalActividadEntity extends AuditoriaEntity implements Serializable {
    private Integer idEvalActividad;
    private String subCodTipoActividad;
    private String codTipoActividad;
    private String tipoActividad;
    private String tipoNombreActividad;
    private String nombreActividad;

    private String medidasControl;
    private String medidasMonitoreo;
    private String frecuencia;
    private String acciones;
    private String responsable;
    private Integer idPlanManejo;

    public String getSubCodTipoActividad() {
        return subCodTipoActividad;
    }

    public void setSubCodTipoActividad(String subCodTipoActividad) {
        this.subCodTipoActividad = subCodTipoActividad;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public EvaluacionAmbientalActividadEntity setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
        return this;
    }

    public Integer getIdEvalActividad() {
        return idEvalActividad;
    }

    public void setIdEvalActividad(Integer idEvalActividad) {
        this.idEvalActividad = idEvalActividad;
    }

    public String getCodTipoActividad() {
        return codTipoActividad;
    }

    public void setCodTipoActividad(String codTipoActividad) {
        this.codTipoActividad = codTipoActividad;
    }

    public String getTipoActividad() {
        return tipoActividad;
    }

    public void setTipoActividad(String tipoActividad) {
        this.tipoActividad = tipoActividad;
    }

    public String getTipoNombreActividad() {
        return tipoNombreActividad;
    }

    public void setTipoNombreActividad(String tipoNombreActividad) {
        this.tipoNombreActividad = tipoNombreActividad;
    }

    public String getNombreActividad() {
        return nombreActividad;
    }

    public void setNombreActividad(String nombreActividad) {
        this.nombreActividad = nombreActividad;
    }

    public String getMedidasControl() {
        return medidasControl;
    }

    public void setMedidasControl(String medidasControl) {
        this.medidasControl = medidasControl;
    }

    public String getMedidasMonitoreo() {
        return medidasMonitoreo;
    }

    public void setMedidasMonitoreo(String medidasMonitoreo) {
        this.medidasMonitoreo = medidasMonitoreo;
    }

    public String getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(String frecuencia) {
        this.frecuencia = frecuencia;
    }

    public String getAcciones() {
        return acciones;
    }

    public void setAcciones(String acciones) {
        this.acciones = acciones;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }
}
