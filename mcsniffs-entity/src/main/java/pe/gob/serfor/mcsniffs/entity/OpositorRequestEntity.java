package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class OpositorRequestEntity implements Serializable {
    private String Dni;
    private String Ruc;
    private Integer IdOpositor;

    public String getDni() {
        return Dni;
    }

    public void setDni(String dni) {
        Dni = dni;
    }

    public String getRuc() {
        return Ruc;
    }

    public void setRuc(String ruc) {
        Ruc = ruc;
    }

    public Integer getIdOpositor() {
        return IdOpositor;
    }

    public void setIdOpositor(Integer idOpositor) {
        IdOpositor = idOpositor;
    }
}
