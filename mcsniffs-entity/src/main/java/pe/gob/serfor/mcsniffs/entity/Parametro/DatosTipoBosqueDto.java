package pe.gob.serfor.mcsniffs.entity.Parametro;

public class DatosTipoBosqueDto {

    String nArboles;
    String volumenComercial;
    String areaBasal;
    String unidadCantidad;
    public String getnArboles() {
        return nArboles;
    }

    public void setnArboles(String nArboles) {
        this.nArboles = nArboles;
    }

    public String getVolumenComercial() {
        return volumenComercial;
    }

    public void setVolumenComercial(String volumenComercial) {
        this.volumenComercial = volumenComercial;
    }
    public String getAreaBasal() {
        return areaBasal;
    }

    public void setAreaBasal(String areaBasal) {
        this.areaBasal = areaBasal;
    }
    public String getUnidadCantidad() {
        return unidadCantidad;
    }

    public void setUnidadCantidad(String unidadCantidad) {
        this.unidadCantidad = unidadCantidad;
    }

}
