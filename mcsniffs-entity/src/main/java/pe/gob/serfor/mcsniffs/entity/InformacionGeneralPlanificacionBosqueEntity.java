package pe.gob.serfor.mcsniffs.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

public class InformacionGeneralPlanificacionBosqueEntity extends AuditoriaEntity implements Serializable {
    private Integer idInformacionGeneral;
    private String tipoInfoGeneral;
    private String nombreTitular;
    private String docIdentidadTitular;
    private String rucTitular;
    private String domicilioLegal;
    private String departamento;
    private String provincia;
    private String distrito;
    private String nombreRepresentanteLegal;
    private String rucRepLegal;
    private String objetivoGeneral;
    private Integer numeroPO;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaPresentacion;
    private Integer duracion;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaInicio;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaFin;
    private Double areaTotalConcesion;
    private Double potencialMaderable;
    private Double potencialNoMaderable;
    private Integer nroBloquesQuinquenales;
    private Double volumenCortaPermisible;
    private Integer nroPcPo;
    private Double areaPcPo;
    private Double areaBosqueHa;
    private Double areaProteccionHa;
    private Double volumenTotalSolicitado;
    private Double volumenNoMaderable;
    private Integer idPlanManejo;
    private Integer idRegente;

    private String nombreElaborador;
    private String apellidosElaborador;
    private String dniElaborador;
    private String nroRucComunidad;

    private PGMFArchivoEntity constanciaRepLegal;

    public Integer getIdInformacionGeneral() {
        return idInformacionGeneral;
    }

    public InformacionGeneralPlanificacionBosqueEntity setIdInformacionGeneral(Integer idInformacionGeneral) {
        this.idInformacionGeneral = idInformacionGeneral;
        return this;
    }

    public String getTipoInfoGeneral() {
        return tipoInfoGeneral;
    }

    public InformacionGeneralPlanificacionBosqueEntity setTipoInfoGeneral(String tipoInfoGeneral) {
        this.tipoInfoGeneral = tipoInfoGeneral;
        return this;
    }

    public String getNombreTitular() {
        return nombreTitular;
    }

    public InformacionGeneralPlanificacionBosqueEntity setNombreTitular(String nombreTitular) {
        this.nombreTitular = nombreTitular;
        return this;
    }

    public String getDocIdentidadTitular() {
        return docIdentidadTitular;
    }

    public InformacionGeneralPlanificacionBosqueEntity setDocIdentidadTitular(String docIdentidadTitular) {
        this.docIdentidadTitular = docIdentidadTitular;
        return this;
    }

    public String getRucTitular() {
        return rucTitular;
    }

    public InformacionGeneralPlanificacionBosqueEntity setRucTitular(String rucTitular) {
        this.rucTitular = rucTitular;
        return this;
    }

    public String getDomicilioLegal() {
        return domicilioLegal;
    }

    public InformacionGeneralPlanificacionBosqueEntity setDomicilioLegal(String domicilioLegal) {
        this.domicilioLegal = domicilioLegal;
        return this;
    }

    public String getDepartamento() {
        return departamento;
    }

    public InformacionGeneralPlanificacionBosqueEntity setDepartamento(String departamento) {
        this.departamento = departamento;
        return this;
    }

    public String getProvincia() {
        return provincia;
    }

    public InformacionGeneralPlanificacionBosqueEntity setProvincia(String provincia) {
        this.provincia = provincia;
        return this;
    }

    public String getDistrito() {
        return distrito;
    }

    public InformacionGeneralPlanificacionBosqueEntity setDistrito(String distrito) {
        this.distrito = distrito;
        return this;
    }

    public String getNombreRepresentanteLegal() {
        return nombreRepresentanteLegal;
    }

    public InformacionGeneralPlanificacionBosqueEntity setNombreRepresentanteLegal(String nombreRepresentanteLegal) {
        this.nombreRepresentanteLegal = nombreRepresentanteLegal;
        return this;
    }

    public String getRucRepLegal() {
        return rucRepLegal;
    }

    public InformacionGeneralPlanificacionBosqueEntity setRucRepLegal(String rucRepLegal) {
        this.rucRepLegal = rucRepLegal;
        return this;
    }

    public String getObjetivoGeneral() {
        return objetivoGeneral;
    }

    public InformacionGeneralPlanificacionBosqueEntity setObjetivoGeneral(String objetivoGeneral) {
        this.objetivoGeneral = objetivoGeneral;
        return this;
    }

    public Integer getNumeroPO() {
        return numeroPO;
    }

    public InformacionGeneralPlanificacionBosqueEntity setNumeroPO(Integer numeroPO) {
        this.numeroPO = numeroPO;
        return this;
    }

    public Date getFechaPresentacion() {
        return fechaPresentacion;
    }

    public InformacionGeneralPlanificacionBosqueEntity setFechaPresentacion(Date fechaPresentacion) {
        this.fechaPresentacion = fechaPresentacion;
        return this;
    }

    public Integer getDuracion() {
        return duracion;
    }

    public InformacionGeneralPlanificacionBosqueEntity setDuracion(Integer duracion) {
        this.duracion = duracion;
        return this;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public InformacionGeneralPlanificacionBosqueEntity setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
        return this;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public InformacionGeneralPlanificacionBosqueEntity setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
        return this;
    }

    public Double getAreaTotalConcesion() {
        return areaTotalConcesion;
    }

    public InformacionGeneralPlanificacionBosqueEntity setAreaTotalConcesion(Double areaTotalConcesion) {
        this.areaTotalConcesion = areaTotalConcesion;
        return this;
    }

    public Double getPotencialMaderable() {
        return potencialMaderable;
    }

    public InformacionGeneralPlanificacionBosqueEntity setPotencialMaderable(Double potencialMaderable) {
        this.potencialMaderable = potencialMaderable;
        return this;
    }

    public Double getPotencialNoMaderable() {
        return potencialNoMaderable;
    }

    public InformacionGeneralPlanificacionBosqueEntity setPotencialNoMaderable(Double potencialNoMaderable) {
        this.potencialNoMaderable = potencialNoMaderable;
        return this;
    }

    public Integer getNroBloquesQuinquenales() {
        return nroBloquesQuinquenales;
    }

    public InformacionGeneralPlanificacionBosqueEntity setNroBloquesQuinquenales(Integer nroBloquesQuinquenales) {
        this.nroBloquesQuinquenales = nroBloquesQuinquenales;
        return this;
    }

    public Double getVolumenCortaPermisible() {
        return volumenCortaPermisible;
    }

    public InformacionGeneralPlanificacionBosqueEntity setVolumenCortaPermisible(Double volumenCortaPermisible) {
        this.volumenCortaPermisible = volumenCortaPermisible;
        return this;
    }

    public Integer getNroPcPo() {
        return nroPcPo;
    }

    public InformacionGeneralPlanificacionBosqueEntity setNroPcPo(Integer nroPcPo) {
        this.nroPcPo = nroPcPo;
        return this;
    }

    public Double getAreaPcPo() {
        return areaPcPo;
    }

    public InformacionGeneralPlanificacionBosqueEntity setAreaPcPo(Double areaPcPo) {
        this.areaPcPo = areaPcPo;
        return this;
    }

    public Double getAreaBosqueHa() {
        return areaBosqueHa;
    }

    public InformacionGeneralPlanificacionBosqueEntity setAreaBosqueHa(Double areaBosqueHa) {
        this.areaBosqueHa = areaBosqueHa;
        return this;
    }

    public Double getAreaProteccionHa() {
        return areaProteccionHa;
    }

    public InformacionGeneralPlanificacionBosqueEntity setAreaProteccionHa(Double areaProteccionHa) {
        this.areaProteccionHa = areaProteccionHa;
        return this;
    }

    public Double getVolumenTotalSolicitado() {
        return volumenTotalSolicitado;
    }

    public InformacionGeneralPlanificacionBosqueEntity setVolumenTotalSolicitado(Double volumenTotalSolicitado) {
        this.volumenTotalSolicitado = volumenTotalSolicitado;
        return this;
    }

    public Double getVolumenNoMaderable() {
        return volumenNoMaderable;
    }

    public InformacionGeneralPlanificacionBosqueEntity setVolumenNoMaderable(Double volumenNoMaderable) {
        this.volumenNoMaderable = volumenNoMaderable;
        return this;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public InformacionGeneralPlanificacionBosqueEntity setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
        return this;
    }

    public Integer getIdRegente() {
        return idRegente;
    }

    public InformacionGeneralPlanificacionBosqueEntity setIdRegente(Integer idRegente) {
        this.idRegente = idRegente;
        return this;
    }

    public PGMFArchivoEntity getConstanciaRepLegal() {
        return constanciaRepLegal;
    }

    public void setConstanciaRepLegal(PGMFArchivoEntity constanciaRepLegal) {
        this.constanciaRepLegal = constanciaRepLegal;
    }

    public String getNombreElaborador() {
        return nombreElaborador;
    }

    public void setNombreElaborador(String nombreElaborador) {
        this.nombreElaborador = nombreElaborador;
    }

    public String getApellidosElaborador() {
        return apellidosElaborador;
    }

    public void setApellidosElaborador(String apellidosElaborador) {
        this.apellidosElaborador = apellidosElaborador;
    }

    public String getDniElaborador() {
        return dniElaborador;
    }

    public void setDniElaborador(String dniElaborador) {
        this.dniElaborador = dniElaborador;
    }

    public String getNroRucComunidad() {
        return nroRucComunidad;
    }

    public void setNroRucComunidad(String nroRucComunidad) {
        this.nroRucComunidad = nroRucComunidad;
    }

    

}
