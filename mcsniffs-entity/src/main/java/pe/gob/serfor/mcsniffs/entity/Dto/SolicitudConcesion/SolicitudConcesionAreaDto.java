package pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionArchivoEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionAreaEntity;

import java.io.Serializable;
import java.util.List;

@Data
public class SolicitudConcesionAreaDto extends AuditoriaEntity implements Serializable {
    private Integer idSolicitudConcesionArea;
    private Integer idSolicitudConcesion;
    private Double superficieHa;
    private String codigoUbigeo;
    private Integer zonaUTM;
    private String observacion;
    private String justificacion;
    private String analisisMapa;
    private String delimitacion;
    private String observacionTamanio;
    private String fuenteBibliografica;
    private Integer idDistritoArea;
    private Integer idProvinciaArea;
    private Integer idDepartamentoArea;
    private String nombreDistritoArea;
    private String nombreProvinciaArea;
    private String nombreDepartamentoArea;

    private String caractFisica;
    private String caractBiologica;
    private String caractSocioeconomica;
    private String factorAmbientalBiologico;
    private String factorSocioEconocultural;
    private String manejoConcesion;

    private List<SolicitudConcesionArchivoEntity> lstArchivo;
}
