package pe.gob.serfor.mcsniffs.entity;

import java.sql.Timestamp;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.Dto.Fiscalizacion.FiscalizacionArchivoDto;

@Getter
@Setter
public class FiscalizacionEntity extends AuditoriaEntity{
    private Integer idFiscalizacion;
    private Integer idResultadoPP;
    private Boolean conObservacion;
    private String observacion;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Timestamp fechaFinContrato;

    private List<FiscalizacionArchivoDto> listaFiscalizacionArchivo;
    
}
