package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.ParameterMode;
import java.io.Serializable;

import java.sql.Date;
import java.sql.Timestamp;

@Data
public class NotificacionEntity  extends AuditoriaEntity implements Serializable {
private Integer idNotificacion;
private String codigoDocgestion ;
private Integer numDocgestion ;
private String  mensaje ;
private String codigoTipoMensaje ;
private Integer idUsuario ;
private String codigoPerfil ;
@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
private Timestamp fechaInicio ;
private Integer cantidadDias ;
private String url ;}
