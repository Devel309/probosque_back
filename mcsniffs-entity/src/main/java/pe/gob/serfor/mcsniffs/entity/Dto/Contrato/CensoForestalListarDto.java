package pe.gob.serfor.mcsniffs.entity.Dto.Contrato;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity; 

@Getter
@Setter
public class CensoForestalListarDto extends  AuditoriaEntity implements Serializable {
    
    private Integer idPlanManejo ;
    private Integer idPlanManejoEspecieGrupo;
    private String nombreComun;
    private String nombreCientifico;
    private String esteUTM;
    private String norteUTM;
    private Integer nroArbolesArea;
    private String producto;
    private String productoAprov;
    private String observaciones;
}
