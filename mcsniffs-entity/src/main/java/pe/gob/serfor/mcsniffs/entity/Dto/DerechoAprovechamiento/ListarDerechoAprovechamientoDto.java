package pe.gob.serfor.mcsniffs.entity.Dto.DerechoAprovechamiento;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListarDerechoAprovechamientoDto {
    private Integer idContrato;
    private String mecanismoPago;
    private String nombreTitular;
    private String dniTitular;
    private String codigoTituloTh;
    private Long pageNum;
    private Long pageSize;
}
