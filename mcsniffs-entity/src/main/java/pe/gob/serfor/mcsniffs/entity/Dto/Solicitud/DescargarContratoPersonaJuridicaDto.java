package pe.gob.serfor.mcsniffs.entity.Dto.Solicitud;

import java.io.Serializable;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;


@Data
public class DescargarContratoPersonaJuridicaDto extends AuditoriaEntity implements Serializable {
 
    private Integer idContrato;
    private String tipoDocumentoGestion;
}