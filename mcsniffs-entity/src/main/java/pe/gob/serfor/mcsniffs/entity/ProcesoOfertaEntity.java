package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.time.DateTimeException;
import java.util.Date;

public class ProcesoOfertaEntity extends AuditoriaEntity implements Serializable {
    private Integer idProcesoOferta;//
    private Integer idTipoProceso;
    private Boolean agrupada;
    private Boolean asociada;
    private String codigoGrupo;
    private Integer idStatusProceso;

    private TipoProcesoEntity tipoproceso;
    private UnidadAprovechamientoEntity unidadaprovechamiento;
    private StatusProcesoEntity statusproceso;

    public TipoProcesoEntity getTipoproceso() {
        return tipoproceso;
    }

    //

    private String nombreTipoProceso;
    private String idUnidadAprovechamientoTexto;
    private String ubicacionUnidadAprovechamiento;
    private String superficieUnidadAprovechamiento;
    private String agrupadaTexto;
    private String asociadaTexto;
    private String nombreStatusProceso;
    private String razonSocial;
    private String nombrePersona;
    private String numeroDocumento;
    private String NombreRegente;

    private Date fechaPostulacion;
//


    //setter getter

    public String getUbicacionUnidadAprovechamiento() {
        return ubicacionUnidadAprovechamiento;
    }

    public void setUbicacionUnidadAprovechamiento(String ubicacionUnidadAprovechamiento) {
        this.ubicacionUnidadAprovechamiento = ubicacionUnidadAprovechamiento;
    }

    public Integer getIdProcesoOferta() {
        return idProcesoOferta;
    }

    public void setIdProcesoOferta(Integer idProcesoOferta) {
        this.idProcesoOferta = idProcesoOferta;
    }

    public Integer getIdTipoProceso() {
        return idTipoProceso;
    }

    public void setIdTipoProceso(Integer idTipoProceso) {
        this.idTipoProceso = idTipoProceso;
    }

    public Boolean getAgrupada() {
        return agrupada;
    }

    public void setAgrupada(Boolean agrupada) {
        this.agrupada = agrupada;
    }

    public Boolean getAsociada() {
        return asociada;
    }

    public void setAsociada(Boolean asociada) {
        this.asociada = asociada;
    }

    public String getCodigoGrupo() {
        return codigoGrupo;
    }

    public void setCodigoGrupo(String codigoGrupo) {
        this.codigoGrupo = codigoGrupo;
    }

    public Integer getIdStatusProceso() {
        return idStatusProceso;
    }

    public void setIdStatusProceso(Integer idStatusProceso) {
        this.idStatusProceso = idStatusProceso;
    }

    public void setTipoproceso(TipoProcesoEntity tipoproceso) {
        this.tipoproceso = tipoproceso;
    }

    public UnidadAprovechamientoEntity getUnidadaprovechamiento() {
        return unidadaprovechamiento;
    }

    public void setUnidadaprovechamiento(UnidadAprovechamientoEntity unidadaprovechamiento) {
        this.unidadaprovechamiento = unidadaprovechamiento;
    }

    public StatusProcesoEntity getStatusproceso() {
        return statusproceso;
    }

    public void setStatusproceso(StatusProcesoEntity statusproceso) {
        this.statusproceso = statusproceso;
    }

    public String getNombreTipoProceso() {
        return nombreTipoProceso;
    }

    public void setNombreTipoProceso(String nombreTipoProceso) {
        this.nombreTipoProceso = nombreTipoProceso;
    }

    public String getIdUnidadAprovechamientoTexto() {
        return idUnidadAprovechamientoTexto;
    }

    public void setIdUnidadAprovechamientoTexto(String idUnidadAprovechamientoTexto) {
        this.idUnidadAprovechamientoTexto = idUnidadAprovechamientoTexto;
    }

    public String getSuperficieUnidadAprovechamiento() {
        return superficieUnidadAprovechamiento;
    }

    public void setSuperficieUnidadAprovechamiento(String superficieUnidadAprovechamiento) {
        this.superficieUnidadAprovechamiento = superficieUnidadAprovechamiento;
    }

    public String getAgrupadaTexto() {
        return agrupadaTexto;
    }

    public void setAgrupadaTexto(String agrupadaTexto) {
        this.agrupadaTexto = agrupadaTexto;
    }

    public String getAsociadaTexto() {
        return asociadaTexto;
    }

    public void setAsociadaTexto(String asociadaTexto) {
        this.asociadaTexto = asociadaTexto;
    }

    public String getNombreStatusProceso() {
        return nombreStatusProceso;
    }

    public void setNombreStatusProceso(String nombreStatusProceso) {
        this.nombreStatusProceso = nombreStatusProceso;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNombrePersona() {
        return nombrePersona;
    }

    public void setNombrePersona(String nombrePersona) {
        this.nombrePersona = nombrePersona;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getNombreRegente() {
        return NombreRegente;
    }

    public void setNombreRegente(String nombreRegente) {
        NombreRegente = nombreRegente;
    }

    public Date getFechaPostulacion() {
        return fechaPostulacion;
    }

    public void setFechaPostulacion(Date fechaPostulacion) {
        this.fechaPostulacion = fechaPostulacion;
    }
}
