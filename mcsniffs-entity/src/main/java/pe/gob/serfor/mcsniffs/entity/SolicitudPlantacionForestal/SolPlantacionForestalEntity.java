package pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.PersonaEntity;

import java.io.Serializable;
import java.util.Date;

@Data
public class SolPlantacionForestalEntity extends AuditoriaEntity implements Serializable {

    private Integer idSolicitudPlantacionForestal;
    private Integer idSolPersona;
    private Integer idSolRepresentante;

    private Integer idSolicitante;
    private Integer idSolicitudConcesion;
    private Integer idSolicitudRepresentante;
    private Integer id_des_estado;
    private String des_estado;

    private String observacion;
    private PersonaEntity solicitante;
    private PersonaEntity representanteLegal;
    private SolPlantacionForestalEstadoEntity estadoSolPlanta;
    private String tipoModalidad;
    private String tipoPersonaSolicitante;
    private String nombreSolicitante;
    private String numeroDocumentoSolicitante;
    private String distritoSolicitante;
    private Integer idSolPlantacion;
    private Boolean bloqueTab01;
    private Boolean bloqueTab02;
    private Boolean bloqueTab03;
    private Boolean bloqueTab04;
    private Boolean bloqueTab05;

    private String nroTramite;
    private Date fechaTramite;
    private String estadoSolicitud;
    private String tipoEnvio;
    private Boolean remitir;
    private Boolean notificarSolicitante;
    private Boolean favorable;
    private Boolean notificarPasSolicitante;
    private Boolean notificarPasSerfor;
    private String nroRegistro;
    private String numeroContrato;
    private Boolean tipoContratoTH;
    private Integer idUsuarioElimina;
    private Boolean ccnn;
    private String  codigoAsamblea;

    private Integer idEvaluacionCampo;
    private Integer idGeometria;

    private Boolean notificarActualizadoARFFS;
    private Boolean actualizarInformacionRNPF;
}
