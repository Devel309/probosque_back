package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.Date;

public class Anexo3Entity implements Serializable {
    private String nomapellfuncionrespon;
    private String nomapellreprelegal;
    private String dnireprelegal;
    private String denomipersojuridica;
    private String rucpersojuridica;
    private String direccpersojuridica;
    private String descdocumentos;
    private String Monto1;
    private String Monto2;
    private String Monto3;
    private String lugarfirma;
    private Date fechafirma;
    private String CodigoAnexo;
    private Integer IdUsuarioRegistro;
    private String CargoFuncioArffssRespo;

    private Integer IdStatusAnexo;
    

    public Integer getIdStatusAnexo() {
        return IdStatusAnexo;
    }

    public void setIdStatusAnexo(Integer idStatusAnexo) {
        IdStatusAnexo = idStatusAnexo;
    }

    public String getCargoFuncioArffssRespo() {
        return CargoFuncioArffssRespo;
    }

    public void setCargoFuncioArffssRespo(String cargoFuncioArffssRespo) {
        CargoFuncioArffssRespo = cargoFuncioArffssRespo;
    }

    public String getCodigoAnexo() {
        return CodigoAnexo;
    }

    public void setCodigoAnexo(String codigoAnexo) {
        CodigoAnexo = codigoAnexo;
    }

    public Integer getIdUsuarioRegistro() {
        return IdUsuarioRegistro;
    }

    public void setIdUsuarioRegistro(Integer idUsuarioRegistro) {
        IdUsuarioRegistro = idUsuarioRegistro;
    }

    public String getNomapellfuncionrespon() {
        return nomapellfuncionrespon;
    }

    public void setNomapellfuncionrespon(String nomapellfuncionrespon) {
        this.nomapellfuncionrespon = nomapellfuncionrespon;
    }

    public String getNomapellreprelegal() {
        return nomapellreprelegal;
    }

    public void setNomapellreprelegal(String nomapellreprelegal) {
        this.nomapellreprelegal = nomapellreprelegal;
    }

    public String getDnireprelegal() {
        return dnireprelegal;
    }

    public void setDnireprelegal(String dnireprelegal) {
        this.dnireprelegal = dnireprelegal;
    }

    public String getDenomipersojuridica() {
        return denomipersojuridica;
    }

    public void setDenomipersojuridica(String denomipersojuridica) {
        this.denomipersojuridica = denomipersojuridica;
    }

    public String getRucpersojuridica() {
        return rucpersojuridica;
    }

    public void setRucpersojuridica(String rucpersojuridica) {
        this.rucpersojuridica = rucpersojuridica;
    }

    public String getDireccpersojuridica() {
        return direccpersojuridica;
    }

    public void setDireccpersojuridica(String direccpersojuridica) {
        this.direccpersojuridica = direccpersojuridica;
    }

    public String getDescdocumentos() {
        return descdocumentos;
    }

    public void setDescdocumentos(String descdocumentos) {
        this.descdocumentos = descdocumentos;
    }

    public String getMonto1() {
        return Monto1;
    }

    public void setMonto1(String monto1) {
        Monto1 = monto1;
    }

    public String getMonto2() {
        return Monto2;
    }

    public void setMonto2(String monto2) {
        Monto2 = monto2;
    }

    public String getMonto3() {
        return Monto3;
    }

    public void setMonto3(String monto3) {
        Monto3 = monto3;
    }

    public String getLugarfirma() {
        return lugarfirma;
    }

    public void setLugarfirma(String lugarfirma) {
        this.lugarfirma = lugarfirma;
    }

    public Date getFechafirma() {
        return fechafirma;
    }

    public void setFechafirma(Date fechafirma) {
        this.fechafirma = fechafirma;
    }
}
