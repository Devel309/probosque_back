package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.sql.Date;

public class TalaDetalleEntity extends AuditoriaEntity implements Serializable{
    private int idTalaDetalle;
    private int idTalaDetalleCab;
    private Date tipoTalaDetalle;
    private int idTala;
    private Double diametroMa;
    private Double diametroMe;
    private Double longitud;
    private Double volumen;
    private String observaciones;
    private int nivelTrozado;
    private int idOrdenProduccion;
    private String estadoTalaDetalle;
    
    public int getIdTalaDetalle() {
        return idTalaDetalle;
    }
    public void setIdTalaDetalle(int idTalaDetalle) {
        this.idTalaDetalle = idTalaDetalle;
    }
    public int getIdTalaDetalleCab() {
        return idTalaDetalleCab;
    }
    public void setIdTalaDetalleCab(int idTalaDetalleCab) {
        this.idTalaDetalleCab = idTalaDetalleCab;
    }
    public Date getTipoTalaDetalle() {
        return tipoTalaDetalle;
    }
    public void setTipoTalaDetalle(Date tipoTalaDetalle) {
        this.tipoTalaDetalle = tipoTalaDetalle;
    }
    public int getIdTala() {
        return idTala;
    }
    public void setIdTala(int idTala) {
        this.idTala = idTala;
    }
    public Double getDiametroMa() {
        return diametroMa;
    }
    public void setDiametroMa(Double diametroMa) {
        this.diametroMa = diametroMa;
    }
    public Double getDiametroMe() {
        return diametroMe;
    }
    public void setDiametroMe(Double diametroMe) {
        this.diametroMe = diametroMe;
    }
    public Double getLongitud() {
        return longitud;
    }
    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }
    public Double getVolumen() {
        return volumen;
    }
    public void setVolumen(Double volumen) {
        this.volumen = volumen;
    }
    public String getObservaciones() {
        return observaciones;
    }
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
    public int getNivelTrozado() {
        return nivelTrozado;
    }
    public void setNivelTrozado(int nivelTrozado) {
        this.nivelTrozado = nivelTrozado;
    }
    public int getIdOrdenProduccion() {
        return idOrdenProduccion;
    }
    public void setIdOrdenProduccion(int idOrdenProduccion) {
        this.idOrdenProduccion = idOrdenProduccion;
    }
    public String getEstadoTalaDetalle() {
        return estadoTalaDetalle;
    }
    public void setEstadoTalaDetalle(String estadoTalaDetalle) {
        this.estadoTalaDetalle = estadoTalaDetalle;
    }

    
}
