package pe.gob.serfor.mcsniffs.entity.Dto.AreaSistemaPlantacion;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Getter
@Setter
public class AreaSistemaPlantacionDto extends AuditoriaEntity implements Serializable  {
    private Integer idAreaSistemaPlantacion;
	private Integer idSolPlantForestArea;
	private Integer idSolPlantForest;
	private String nombre;
 	private String codigoUm;
	private BigDecimal cantidad;
	private String fines;
    private Integer idUsuario;
	private Boolean especiesEstablecidas = false;
}
