package pe.gob.serfor.mcsniffs.entity;

import java.math.BigDecimal;

public class ActividadAprovechamientoDetSubEntity extends AuditoriaEntity{
    private Integer idActvAproveDetSub;
    private Integer idActvAproveDet;
    private String codActvAproveDetSub;
    private String codSubActvAproveDetSub;
    private String var;
    private BigDecimal totalHa;
    private BigDecimal totalPC;
    private String dapCM;
    private BigDecimal area;
    private Integer nroPC;
	private Integer nroArbol;
    private BigDecimal volM;
	private String descripcion;
    private String observacion;
    private String detalle;
    
    public Integer getIdActvAproveDetSub() {
        return idActvAproveDetSub;
    }
    public void setIdActvAproveDetSub(Integer idActvAproveDetSub) {
        this.idActvAproveDetSub = idActvAproveDetSub;
    }
    public Integer getIdActvAproveDet() {
        return idActvAproveDet;
    }
    public void setIdActvAproveDet(Integer idActvAproveDet) {
        this.idActvAproveDet = idActvAproveDet;
    }
    public String getCodActvAproveDetSub() {
        return codActvAproveDetSub;
    }
    public void setCodActvAproveDetSub(String codActvAproveDetSub) {
        this.codActvAproveDetSub = codActvAproveDetSub;
    }
    public String getCodSubActvAproveDetSub() {
        return codSubActvAproveDetSub;
    }
    public void setCodSubActvAproveDetSub(String codSubActvAproveDetSub) {
        this.codSubActvAproveDetSub = codSubActvAproveDetSub;
    }
    public String getVar() {
        return var;
    }
    public void setVar(String var) {
        this.var = var;
    }
    public BigDecimal getTotalHa() {
        return totalHa;
    }
    public void setTotalHa(BigDecimal totalHa) {
        this.totalHa = totalHa;
    }
    public BigDecimal getTotalPC() {
        return totalPC;
    }
    public void setTotalPC(BigDecimal totalPC) {
        this.totalPC = totalPC;
    }
    public String getDapCM() {
        return dapCM;
    }
    public void setDapCM(String dapCM) {
        this.dapCM = dapCM;
    }
    public BigDecimal getArea() {
        return area;
    }
    public void setArea(BigDecimal area) {
        this.area = area;
    }
    public Integer getNroPC() {
        return nroPC;
    }
    public void setNroPC(Integer nroPC) {
        this.nroPC = nroPC;
    }
    public Integer getNroArbol() {
        return nroArbol;
    }
    public void setNroArbol(Integer nroArbol) {
        this.nroArbol = nroArbol;
    }
    public BigDecimal getVolM() {
        return volM;
    }
    public void setVolM(BigDecimal volM) {
        this.volM = volM;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String getObservacion() {
        return observacion;
    }
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    public String getDetalle() {
        return detalle;
    }
    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    
}
