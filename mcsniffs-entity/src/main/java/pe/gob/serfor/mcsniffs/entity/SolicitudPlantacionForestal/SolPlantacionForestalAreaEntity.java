package pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.management.loading.PrivateClassLoader;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
// @AllArgsConstructor
// @NoArgsConstructor
// @Entity
// @Table(name = "T_MAD_SOL_PLANTACIONFORESTAL_AREA", schema = "dbo")
public class SolPlantacionForestalAreaEntity extends AuditoriaEntity {
    private Integer idSolPlantaForestalArea;
    private Integer idSolPlantaForestal;
    private String predio;
    private BigDecimal area;
    private String ruc;
    private String caserioComunidad;
    private Integer idDistrito;
    private Integer idDocAutorizaPlantacion;
    private String docAutorizaPlantacion;
    private String numCesionAgroforetsal;
    private String NumConcesionAgroforestal;
    private Boolean inversionista;
    private String codigoTipodocInversionista;
    private String numerDocInversionista;
    private Boolean propietario;
    private String codigoTipoDocPropietario;
    private String numeroDocPropietario;
    private BigDecimal areaTotalPlantacion;
    private Timestamp mesAnhoPlantacion;
    private String numCesionAgroforestal;
    private String desAdjuntoInversionista;
    private String desAdjuntoPropietario;
    private String zona;
    private Integer idProvincia;
    private Integer idDepartamento;
    private BigDecimal alturaPromedio;

    private String nombreDistrito;
    private String nombreProvincia;
    private String nombreDepartamento;
}
