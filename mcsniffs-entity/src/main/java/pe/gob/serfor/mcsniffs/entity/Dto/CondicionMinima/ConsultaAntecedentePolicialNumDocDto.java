package pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima;

import lombok.Data;

@Data
public class ConsultaAntecedentePolicialNumDocDto {
    
    private String clienteClave;
    private String clienteIp;
    private String clienteMac;
    private String clienteSistema;
    private String clienteUsuario;
    private String numeroDocUserClieFin;
    private String numeroDocumento;
    private String servicioCodigo;
    private String tipoDocUserClieFin;
}
