package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;


public class TipoEscalaEntity implements Serializable {
    private short tipoEscala;
    private String descripcion;

    public short getTipoEscala() { return tipoEscala; }
    public void setTipoEscala(short tipoEscala) { this.tipoEscala = tipoEscala; }
    public String getDescripcion() { return descripcion; }
    public void setDescripcion(String descripcion) { this.descripcion = descripcion; }


}
