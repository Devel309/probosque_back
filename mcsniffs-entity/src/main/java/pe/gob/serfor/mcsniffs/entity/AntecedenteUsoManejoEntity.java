package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class AntecedenteUsoManejoEntity extends AuditoriaEntity implements Serializable {
    private Integer idAnteUsoManejo;
    private AntecedenteUsoEntity antecedenteUso;
    private String especie;
    private String observacion;
    private PlanManejoEntity planManejo;

    public Integer getIdAnteUsoManejo() {
        return idAnteUsoManejo;
    }

    public void setIdAnteUsoManejo(Integer idAnteUsoManejo) {
        this.idAnteUsoManejo = idAnteUsoManejo;
    }

    public AntecedenteUsoEntity getAntecedenteUso() {
        return antecedenteUso;
    }

    public void setAntecedenteUso(AntecedenteUsoEntity antecedenteUso) {
        this.antecedenteUso = antecedenteUso;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public PlanManejoEntity getPlanManejo() {
        return planManejo;
    }

    public void setPlanManejo(PlanManejoEntity planManejo) {
        this.planManejo = planManejo;
    }
}
