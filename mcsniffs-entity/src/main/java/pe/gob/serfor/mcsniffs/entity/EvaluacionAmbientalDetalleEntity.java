package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class EvaluacionAmbientalDetalleEntity extends AuditoriaEntity implements Serializable {
    private Integer idEvaluacionAmbientalDetalle;
    private String tipoActividadEvaluacion;
    private String detalleActividadEvaluacion;
    private EvaluacionAmbientalEntity evaluacionAmbiental;

    public Integer getIdEvaluacionAmbientalDetalle() {
        return idEvaluacionAmbientalDetalle;
    }

    public void setIdEvaluacionAmbientalDetalle(Integer idEvaluacionAmbientalDetalle) {
        this.idEvaluacionAmbientalDetalle = idEvaluacionAmbientalDetalle;
    }

    public String getTipoActividadEvaluacion() {
        return tipoActividadEvaluacion;
    }

    public void setTipoActividadEvaluacion(String tipoActividadEvaluacion) {
        this.tipoActividadEvaluacion = tipoActividadEvaluacion;
    }

    public String getDetalleActividadEvaluacion() {
        return detalleActividadEvaluacion;
    }

    public void setDetalleActividadEvaluacion(String detalleActividadEvaluacion) {
        this.detalleActividadEvaluacion = detalleActividadEvaluacion;
    }

    public EvaluacionAmbientalEntity getEvaluacionAmbiental() {
        return evaluacionAmbiental;
    }

    public void setEvaluacionAmbiental(EvaluacionAmbientalEntity evaluacionAmbiental) {
        this.evaluacionAmbiental = evaluacionAmbiental;
    }

    public EvaluacionAmbientalDetalleEntity(Integer idEvaluacionAmbientalDetalle, String tipoActividadEvaluacion, String detalleActividadEvaluacion, EvaluacionAmbientalEntity evaluacionAmbiental) {
        this.idEvaluacionAmbientalDetalle = idEvaluacionAmbientalDetalle;
        this.tipoActividadEvaluacion = tipoActividadEvaluacion;
        this.detalleActividadEvaluacion = detalleActividadEvaluacion;
        this.evaluacionAmbiental = evaluacionAmbiental;
    }
}
