package pe.gob.serfor.mcsniffs.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class
RegistroTabEntity  implements Serializable {
    private String tab;
    private String codigoTab;
    private String conformidad;
    private Integer idPlanManejo;
    private String codigoProceso;

}
