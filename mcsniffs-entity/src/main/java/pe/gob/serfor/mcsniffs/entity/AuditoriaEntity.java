package pe.gob.serfor.mcsniffs.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuditoriaEntity extends PaginacionEntity{

    //@Column(name = "TX_ESTADO",  nullable = false)  
    private String estado;
    // @Column(name = "NU_ID_USUARIO_REGISTRO",  nullable = false)  
    private Integer idUsuarioRegistro;
    // @Column(name = "FE_FECHA_REGISTRO",  nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaRegistro;
    // @Column(name = "NU_ID_USUARIO_MODIFICACION",  nullable = false)  
    private Integer idUsuarioModificacion;
    // @Column(name = "FE_FECHA_MODIFICACION",  nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaModificacion;
    // @Column(name = "NU_ID_USUARIO_ELIMINA",  nullable = false)  
    private Integer idUsuarioElimina;
    // @Column(name = "FE_FECHA_MODIFICACION",  nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaElimina;
    private String token;
    private Integer nuAlerta;


}
