package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class SolicitudImpugnacionArchivoEntity extends AuditoriaEntity implements Serializable {

    Integer idSolicitudImpugnacionArchivo;
    Integer getIdSolicitudImpugnacion;
    ArchivoEntity archivo;
    String  parametro;
    String  tipoParametro;

    public Integer getIdSolicitudImpugnacionArchivo() {
        return idSolicitudImpugnacionArchivo;
    }

    public void setIdSolicitudImpugnacionArchivo(Integer idSolicitudImpugnacionArchivo) {
        this.idSolicitudImpugnacionArchivo = idSolicitudImpugnacionArchivo;
    }

    public Integer getGetIdSolicitudImpugnacion() {
        return getIdSolicitudImpugnacion;
    }

    public void setGetIdSolicitudImpugnacion(Integer getIdSolicitudImpugnacion) {
        this.getIdSolicitudImpugnacion = getIdSolicitudImpugnacion;
    }

    public ArchivoEntity getArchivo() {
        return archivo;
    }

    public void setArchivoEntity(ArchivoEntity archivo) {
        this.archivo = archivo;
    }

    public String getParametro() {
        return parametro;
    }

    public void setParametro(String parametro) {
        this.parametro = parametro;
    }

    public String getTipoParametro() {
        return tipoParametro;
    }

    public void setTipoParametro(String tipoParametro) {
        this.tipoParametro = tipoParametro;
    }
}
