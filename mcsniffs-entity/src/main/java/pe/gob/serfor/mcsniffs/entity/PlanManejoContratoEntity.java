package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class PlanManejoContratoEntity extends AuditoriaEntity implements Serializable {

    private Integer idContrato;
    private Integer idUsuarioTitular;
    private String nombreTitular;
    private String dniTitular;
    private String rucTitular;
    private Integer distritoTitular;
    private Integer provinciaTitular;
    private Integer departamentoTitular;
    private String domicilioLegalTitular;
    private String dniRepresentanteLegal;
    private String nombreRepresentanteLegal;
    private String  tituloHabilitante;
    private String superficieUA;
    private String idUA;
    private Integer idPlanManejo;
    private Integer idPlanManejoContrato;
    private Boolean contratoPrincipal;
    

    public Integer getIdContrato() {
        return idContrato;
    }
    public void setIdContrato(Integer idContrato) {
        this.idContrato = idContrato;
    }
    public String getTituloHabilitante() {
        return tituloHabilitante;
    }
    public void setTituloHabilitante(String tituloHabilitante) {
        this.tituloHabilitante = tituloHabilitante;
    }
    public Integer getIdUsuarioTitular() {
        return idUsuarioTitular;
    }
    public void setIdUsuarioTitular(Integer idUsuarioTitular) {
        this.idUsuarioTitular = idUsuarioTitular;
    }
    public String getNombreTitular() {
        return nombreTitular;
    }
    public void setNombreTitular(String nombreTitular) {
        this.nombreTitular = nombreTitular;
    }
    public String getDniTitular() {
        return dniTitular;
    }
    public void setDniTitular(String dniTitular) {
        this.dniTitular = dniTitular;
    }
    public String getRucTitular() {
        return rucTitular;
    }
    public void setRucTitular(String rucTitular) {
        this.rucTitular = rucTitular;
    }
    public Integer getDistritoTitular() {
        return distritoTitular;
    }
    public void setDistritoTitular(Integer distritoTitular) {
        this.distritoTitular = distritoTitular;
    }
    public Integer getProvinciaTitular() {
        return provinciaTitular;
    }
    public void setProvinciaTitular(Integer provinciaTitular) {
        this.provinciaTitular = provinciaTitular;
    }
    public Integer getDepartamentoTitular() {
        return departamentoTitular;
    }
    public void setDepartamentoTitular(Integer departamentoTitular) {
        this.departamentoTitular = departamentoTitular;
    }
    public String getDomicilioLegalTitular() {
        return domicilioLegalTitular;
    }
    public void setDomicilioLegalTitular(String domicilioLegalTitular) {
        this.domicilioLegalTitular = domicilioLegalTitular;
    }
    public String getDniRepresentanteLegal() {
        return dniRepresentanteLegal;
    }
    public void setDniRepresentanteLegal(String dniRepresentanteLegal) {
        this.dniRepresentanteLegal = dniRepresentanteLegal;
    }
    public String getNombreRepresentanteLegal() {
        return nombreRepresentanteLegal;
    }
    public void setNombreRepresentanteLegal(String nombreRepresentanteLegal) {
        this.nombreRepresentanteLegal = nombreRepresentanteLegal;
    }
  
    public String getSuperficieUA() {
        return superficieUA;
    }
    public void setSuperficieUA(String superficieUA) {
        this.superficieUA = superficieUA;
    }
    public String getIdUA() {
        return idUA;
    }
    public void setIdUA(String idUA) {
        this.idUA = idUA;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public Integer getIdPlanManejoContrato() {
        return idPlanManejoContrato;
    }
    
    public void setIdPlanManejoContrato(Integer idPlanManejoContrato) {
        this.idPlanManejoContrato = idPlanManejoContrato;
    }

    public Boolean getContratoPrincipal() {
        return contratoPrincipal;
    }

    public void setContratoPrincipal(Boolean contratoPrincipal) {
        this.contratoPrincipal = contratoPrincipal;
    }
}
