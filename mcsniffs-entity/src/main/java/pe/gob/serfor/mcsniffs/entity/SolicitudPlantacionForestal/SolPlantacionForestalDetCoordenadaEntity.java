package pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
@AllArgsConstructor
@NoArgsConstructor
// @Entity
// @Table(name = "T_MVD_SOLPLANTACIONFOREST_COORD_DET", schema = "dbo")
public class SolPlantacionForestalDetCoordenadaEntity extends AuditoriaEntity {
    

    // @id
    // @generatedvalue(strategy = generationtype.identity)
    //@column(name = "nu_id_solplantforest_coord_det", updatable = false, nullable = false)     
    Integer id_solplantforest_coord_det ; 
    //@column(name = "nu_id_solplantforest", updatable = false, nullable = false)  
    Integer id_solplantforest ;
    //@column(name = "nu_id_bloquesector", updatable = false, nullable = false)  
    Integer id_bloquesector ;
    
    String bloquesector ;

    String especies_establecidas;

    Integer areabloque;
    String areabloque_unidad;

    List<SolPlantacionForestalDetCoordenadaDetEntity> detalle;

    public Integer getId_solplantforest_coord_det() {
        return id_solplantforest_coord_det;
    }

    public void setId_solplantforest_coord_det(Integer id_solplantforest_coord_det) {
        this.id_solplantforest_coord_det = id_solplantforest_coord_det;
    }

    public Integer getId_solplantforest() {
        return id_solplantforest;
    }

    public void setId_solplantforest(Integer id_solplantforest) {
        this.id_solplantforest = id_solplantforest;
    }

    public Integer getId_bloquesector() {
        return id_bloquesector;
    }

    public void setId_bloquesector(Integer id_bloquesector) {
        this.id_bloquesector = id_bloquesector;
    }

    public String getBloquesector() {
        return bloquesector;
    }

    public void setBloquesector(String bloquesector) {
        this.bloquesector = bloquesector;
    }

    public String getEspecies_establecidas() {
        return especies_establecidas;
    }

    public void setEspecies_establecidas(String especies_establecidas) {
        this.especies_establecidas = especies_establecidas;
    }

    public Integer getAreabloque() {
        return areabloque;
    }

    public void setAreabloque(Integer areabloque) {
        this.areabloque = areabloque;
    }

    public String getAreabloque_unidad() {
        return areabloque_unidad;
    }

    public void setAreabloque_unidad(String areabloque_unidad) {
        this.areabloque_unidad = areabloque_unidad;
    }

    public List<SolPlantacionForestalDetCoordenadaDetEntity> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<SolPlantacionForestalDetCoordenadaDetEntity> detalle) {
        this.detalle = detalle;
    }







}
