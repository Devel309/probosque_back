package pe.gob.serfor.mcsniffs.entity.PlanificacionBosque.PGMF.PotencialProdRecursoForestal;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.PlanManejoEntity;
import pe.gob.serfor.mcsniffs.entity.ProteccionBosqueDetalleEntity;
import pe.gob.serfor.mcsniffs.entity.ResultArchivoEntity;

import java.math.BigDecimal;
import java.util.List;

public class PotencialProduccionForestalEntity extends AuditoriaEntity {
    private PlanManejoEntity planManejo;
    private Integer idPotProdForestal;
    private String codigoTipoPotProdForestal;
    private String especie;
    private String variable;
    private String anexo;
    private String disenio;
    private Double diametroMinimoInventariadaCM;
    private Double tamanioParcela;
    private Integer nroParcela;
    private Double distanciaParcela;
    private Double totalAreaInventariada;
    private Double rangoDiametroCM;
    private Double areaMuestreada;
    private String metodoMuestreo;
    private Double intensidadMuestreoPorcentaje;
    private Double errorMuestreoPorcentaje;
    private ResultArchivoEntity archivo;
    private Integer idTipoBosque;
    private String tipoBosque;
    private String codigoSubTipoPotencialProdForestal;
    private Integer idPlanManejo;
    private BigDecimal errorMuestreo;
    private List<PotencialProduccionForestalDto> listPotencialProduccion;

    public BigDecimal getErrorMuestreo() {
        return errorMuestreo;
    }

    public void setErrorMuestreo(BigDecimal errorMuestreo) {
        this.errorMuestreo = errorMuestreo;
    }

    public String getCodigoSubTipoPotencialProdForestal() {
        return codigoSubTipoPotencialProdForestal;
    }

    public void setCodigoSubTipoPotencialProdForestal(String codigoSubTipoPotencialProdForestal) {
        this.codigoSubTipoPotencialProdForestal = codigoSubTipoPotencialProdForestal;
    }

    public Integer getIdTipoBosque() {
        return idTipoBosque;
    }

    public void setIdTipoBosque(Integer idTipoBosque) {
        this.idTipoBosque = idTipoBosque;
    }

    public String getTipoBosque() {
        return tipoBosque;
    }

    public void setTipoBosque(String tipoBosque) {
        this.tipoBosque = tipoBosque;
    }

       public List<PotencialProduccionForestalDto> getListPotencialProduccion() {
        return listPotencialProduccion;
    }

    public void setListPotencialProduccion(List<PotencialProduccionForestalDto> listPotencialProduccion) {
        this.listPotencialProduccion = listPotencialProduccion;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public Integer getIdPotProdForestal() {
        return idPotProdForestal;
    }

    public void setIdPotProdForestal(Integer idPotProdForestal) {
        this.idPotProdForestal = idPotProdForestal;
    }

    public String getCodigoTipoPotProdForestal() {
        return codigoTipoPotProdForestal;
    }

    public void setCodigoTipoPotProdForestal(String codigoTipoPotProdForestal) {
        this.codigoTipoPotProdForestal = codigoTipoPotProdForestal;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public String getAnexo() {
        return anexo;
    }

    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    public String getDisenio() {
        return disenio;
    }

    public void setDisenio(String disenio) {
        this.disenio = disenio;
    }

    public Double getDiametroMinimoInventariadaCM() {
        return diametroMinimoInventariadaCM;
    }

    public void setDiametroMinimoInventariadaCM(Double diametroMinimoInventariadaCM) {
        this.diametroMinimoInventariadaCM = diametroMinimoInventariadaCM;
    }

    public Double getTamanioParcela() {
        return tamanioParcela;
    }

    public void setTamanioParcela(Double tamanioParcela) {
        this.tamanioParcela = tamanioParcela;
    }

    public Integer getNroParcela() {
        return nroParcela;
    }

    public void setNroParcela(Integer nroParcela) {
        this.nroParcela = nroParcela;
    }

    public Double getDistanciaParcela() {
        return distanciaParcela;
    }

    public void setDistanciaParcela(Double distanciaParcela) {
        this.distanciaParcela = distanciaParcela;
    }

    public Double getTotalAreaInventariada() {
        return totalAreaInventariada;
    }

    public void setTotalAreaInventariada(Double totalAreaInventariada) {
        this.totalAreaInventariada = totalAreaInventariada;
    }

    public Double getRangoDiametroCM() {
        return rangoDiametroCM;
    }

    public void setRangoDiametroCM(Double rangoDiametroCM) {
        this.rangoDiametroCM = rangoDiametroCM;
    }

    public Double getAreaMuestreada() {
        return areaMuestreada;
    }

    public void setAreaMuestreada(Double areaMuestreada) {
        this.areaMuestreada = areaMuestreada;
    }

    public String getMetodoMuestreo() {
        return metodoMuestreo;
    }

    public void setMetodoMuestreo(String metodoMuestreo) {
        this.metodoMuestreo = metodoMuestreo;
    }

    public Double getIntensidadMuestreoPorcentaje() {
        return intensidadMuestreoPorcentaje;
    }

    public void setIntensidadMuestreoPorcentaje(Double intensidadMuestreoPorcentaje) {
        this.intensidadMuestreoPorcentaje = intensidadMuestreoPorcentaje;
    }

    public Double getErrorMuestreoPorcentaje() {
        return errorMuestreoPorcentaje;
    }

    public void setErrorMuestreoPorcentaje(Double errorMuestreoPorcentaje) {
        this.errorMuestreoPorcentaje = errorMuestreoPorcentaje;
    }

    public PlanManejoEntity getPlanManejo() {
        return planManejo;
    }

    public void setPlanManejo(PlanManejoEntity planManejo) {
        this.planManejo = planManejo;
    }
    public ResultArchivoEntity getArchivo() {
		return archivo;
	}
	public void setArchivo(ResultArchivoEntity archivo) {
		this.archivo = archivo;
	}
}   
