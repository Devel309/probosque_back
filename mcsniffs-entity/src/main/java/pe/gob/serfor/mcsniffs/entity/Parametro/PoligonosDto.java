package pe.gob.serfor.mcsniffs.entity.Parametro;
import java.util.List;
public class PoligonosDto {
    String idPlanManejo;
    String tipoGeometria;
    String geometria;
    String idArchivo;
    List<OrdenamientoPoligonoDto> ordenamientoPoligonoDtoList;

    public String getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(String idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public String getTipoGeometria() {
        return tipoGeometria;
    }

    public void setTipoGeometria(String tipoGeometria) {
        this.tipoGeometria = tipoGeometria;
    }

    public String getGeometria() {
        return geometria;
    }

    public void setGeometria(String geometria) {
        this.geometria = geometria;
    }

    public String getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(String idArchivo) {
        this.idArchivo = idArchivo;
    }

    public List<OrdenamientoPoligonoDto> getOrdenamientoPoligonoDtoList() {
        return ordenamientoPoligonoDtoList;
    }

    public void setOrdenamientoPoligonoDtoList(List<OrdenamientoPoligonoDto> ordenamientoPoligonoDtoList) {
        this.ordenamientoPoligonoDtoList = ordenamientoPoligonoDtoList;
    }
}
