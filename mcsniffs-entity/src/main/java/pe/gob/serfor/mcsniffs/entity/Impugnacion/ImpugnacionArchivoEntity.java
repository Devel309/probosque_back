package pe.gob.serfor.mcsniffs.entity.Impugnacion;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.ArchivoEntity;
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ImpugnacionArchivoEntity extends AuditoriaEntity implements Serializable{
 
    @Id
    Integer idImpugnacionArchivo ;
    Integer idImpugnacion ;
    Integer idArchivo ;
    String Parametro ;
    Integer idTipoArchivo ;
    ArchivoEntity archivo ;
}
