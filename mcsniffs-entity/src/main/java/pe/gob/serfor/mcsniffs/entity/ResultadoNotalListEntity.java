package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ResultadoNotalListEntity implements Serializable {
    private Integer idResultadoPP;
    private String codigoSeccion;
    private String codigoSubSeccion;
    private Integer puntajeOtorgado;
    private Integer puntajeAdecuadoMeta;
    private Integer puntajeNoAdecuadoMeta;
    private Integer puntajeNoTiene;
    private Integer puntajeCoherente;
    private Integer puntajeNoCoherente;
    private Integer puntajeNoConsigna;
    private String observacion;
    private Integer CalificacionTotal;

    public Integer getCalificacionTotal() {
        return CalificacionTotal;
    }

    public void setCalificacionTotal(Integer calificacionTotal) {
        CalificacionTotal = calificacionTotal;
    }

    public Integer getIdResultadoPP() {
        return idResultadoPP;
    }

    public void setIdResultadoPP(Integer idResultadoPP) {
        this.idResultadoPP = idResultadoPP;
    }

    public String getCodigoSeccion() {
        return codigoSeccion;
    }

    public void setCodigoSeccion(String codigoSeccion) {
        this.codigoSeccion = codigoSeccion;
    }

    public String getCodigoSubSeccion() {
        return codigoSubSeccion;
    }

    public void setCodigoSubSeccion(String codigoSubSeccion) {
        this.codigoSubSeccion = codigoSubSeccion;
    }

    public Integer getPuntajeOtorgado() {
        return puntajeOtorgado;
    }

    public void setPuntajeOtorgado(Integer puntajeOtorgado) {
        this.puntajeOtorgado = puntajeOtorgado;
    }

    public Integer getPuntajeAdecuadoMeta() {
        return puntajeAdecuadoMeta;
    }

    public void setPuntajeAdecuadoMeta(Integer puntajeAdecuadoMeta) {
        this.puntajeAdecuadoMeta = puntajeAdecuadoMeta;
    }

    public Integer getPuntajeNoAdecuadoMeta() {
        return puntajeNoAdecuadoMeta;
    }

    public void setPuntajeNoAdecuadoMeta(Integer puntajeNoAdecuadoMeta) {
        this.puntajeNoAdecuadoMeta = puntajeNoAdecuadoMeta;
    }

    public Integer getPuntajeNoTiene() {
        return puntajeNoTiene;
    }

    public void setPuntajeNoTiene(Integer puntajeNoTiene) {
        this.puntajeNoTiene = puntajeNoTiene;
    }

    public Integer getPuntajeCoherente() {
        return puntajeCoherente;
    }

    public void setPuntajeCoherente(Integer puntajeCoherente) {
        this.puntajeCoherente = puntajeCoherente;
    }

    public Integer getPuntajeNoCoherente() {
        return puntajeNoCoherente;
    }

    public void setPuntajeNoCoherente(Integer puntajeNoCoherente) {
        this.puntajeNoCoherente = puntajeNoCoherente;
    }

    public Integer getPuntajeNoConsigna() {
        return puntajeNoConsigna;
    }

    public void setPuntajeNoConsigna(Integer puntajeNoConsigna) {
        this.puntajeNoConsigna = puntajeNoConsigna;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
}
