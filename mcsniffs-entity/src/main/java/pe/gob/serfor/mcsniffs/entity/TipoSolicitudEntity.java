package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class TipoSolicitudEntity extends AuditoriaEntity implements Serializable {
    private Integer IdTipoSolicitud;
    private String Descripcion;

    public Integer getIdTipoSolicitud() {
        return IdTipoSolicitud;
    }

    public void setIdTipoSolicitud(Integer idTipoSolicitud) {
        IdTipoSolicitud = idTipoSolicitud;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }
}
