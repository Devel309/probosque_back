package pe.gob.serfor.mcsniffs.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class InformacionGeneralPermisoForestalEntity extends AuditoriaEntity implements Serializable {
    private Integer idInfGeneral;
    private String codTipoInfGeneral;
    private Integer idPermisoForestal;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaPresentacion;
    private String nombreElaborador;
    private String apellidoPaternoElaborador;
    private String apellidoMaternoElaborador;
    private String apellidosElaborador;
    private String tipoDocumentoElaborador;
    private String documentoElaborador;
    private String rucComunidad;
    private String federacionComunidad;
    private String representanteLegal;
    private String domicilioLegal;
    private String domicilioLegalRegente;
    private String codTipoDocumento;
    private String tipoDocumento;
    private String codTipoPersona;
    private String tipoPersona;
    private String codTipoActor;
    private String tipoActor;
    private String codTipoCncc;
    private String tipoCncc;
    private String esReprLegal;
    private String ubigeoTitular;
    private String ubigeo;
    private String correo;
    private String correoEmpresa;
    private String descripcion;
    private String detalle;
    private String observacion;
    private String distritoTitular;
    private String provinciaTitular;
    private String departamentoTitular;
    private String distrito;
    private String provincia;
    private String departamento;
    private Integer idDistrito;
    private Integer idProvincia;
    private Integer idDepartamento;
    private String codDistrito;
    private String codProvincia;
    private String codDepartamento;
    private String codEstadoPF;
    private String codTipoDocumentoElaborador;
}
