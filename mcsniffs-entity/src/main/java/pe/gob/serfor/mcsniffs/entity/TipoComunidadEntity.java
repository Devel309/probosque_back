package pe.gob.serfor.mcsniffs.entity;

public class TipoComunidadEntity extends AuditoriaEntity {
     short IdTipoComunidad;
     String Descripcion;

    public short getIdTipoComunidad() {
        return IdTipoComunidad;
    }

    public void setIdTipoComunidad(short idTipoComunidad) {
        IdTipoComunidad = idTipoComunidad;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }
}
