package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ManejoBosqueAprovechamientoCaminoEntity extends AuditoriaEntity implements Serializable {
     private Integer idManBosqueAproveCamino;
     private ManejoBosqueAprovechamientoEntity manejoBosqueAprovechamiento;
     private String idTipoInfraestructura;
     private String tipoInfraestructura;
     private String caracteristica;
     private String construccion;
     private String manoObra;
     private String maquina;

    public String getTipoInfraestructura() {
        return tipoInfraestructura;
    }

    public void setTipoInfraestructura(String tipoInfraestructura) {
        this.tipoInfraestructura = tipoInfraestructura;
    }

    public Integer getIdManBosqueAproveCamino() {
        return idManBosqueAproveCamino;
    }

    public void setIdManBosqueAproveCamino(Integer idManBosqueAproveCamino) {
        this.idManBosqueAproveCamino = idManBosqueAproveCamino;
    }

    public ManejoBosqueAprovechamientoEntity getManejoBosqueAprovechamiento() {
        return manejoBosqueAprovechamiento;
    }

    public void setManejoBosqueAprovechamiento(ManejoBosqueAprovechamientoEntity manejoBosqueAprovechamiento) {
        this.manejoBosqueAprovechamiento = manejoBosqueAprovechamiento;
    }

    public String getIdTipoInfraestructura() {
        return idTipoInfraestructura;
    }

    public void setIdTipoInfraestructura(String idTipoInfraestructura) {
        this.idTipoInfraestructura = idTipoInfraestructura;
    }

    public String getCaracteristica() {
        return caracteristica;
    }

    public void setCaracteristica(String caracteristica) {
        this.caracteristica = caracteristica;
    }

    public String getConstruccion() {
        return construccion;
    }

    public void setConstruccion(String construccion) {
        this.construccion = construccion;
    }

    public String getManoObra() {
        return manoObra;
    }

    public void setManoObra(String manoObra) {
        this.manoObra = manoObra;
    }

    public String getMaquina() {
        return maquina;
    }

    public void setMaquina(String maquina) {
        this.maquina = maquina;
    }
}
