package pe.gob.serfor.mcsniffs.entity;

import java.util.Date;

public class ParticipacionComunalDetalleEntity {
    private Integer idPartCiudadanaDet;
    private String participacion;
    private String metodologia;
    private String lugar;
    private Date fecha;
    private ParticipacionComunalEntity participacionComunal;

    public Integer getIdPartCiudadanaDet() {
        return idPartCiudadanaDet;
    }

    public void setIdPartCiudadanaDet(Integer idPartCiudadanaDet) {
        this.idPartCiudadanaDet = idPartCiudadanaDet;
    }

    public String getParticipacion() {
        return participacion;
    }

    public void setParticipacion(String participacion) {
        this.participacion = participacion;
    }

    public String getMetodologia() {
        return metodologia;
    }

    public void setMetodologia(String metodologia) {
        this.metodologia = metodologia;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public ParticipacionComunalEntity getParticipacionComunal() {
        return participacionComunal;
    }

    public void setParticipacionComunal(ParticipacionComunalEntity participacionComunal) {
        this.participacionComunal = participacionComunal;
    }
}
