package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class FederacionComunidadEntity extends AuditoriaEntity implements Serializable {
    private Integer idFedComunidad;
    private String descripcion;

    public Integer getIdFedComunidad() {
        return idFedComunidad;
    }

    public void setIdFedComunidad(Integer idFedComunidad) {
        this.idFedComunidad = idFedComunidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
