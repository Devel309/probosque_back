package pe.gob.serfor.mcsniffs.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class PagoCronogramaEntity extends AuditoriaEntity implements Serializable {
    private Integer idPagoCronograma;
    private String codTipoPagoCronograma;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaInicio;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaFin;
    private Integer anio;
    private String observacion;
    private String detalle;
    private String descripcion;
    private Integer idPago;
    private Integer nrDocumentoGestion;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fecha;
    private String perfil;
    private List <PagoPeriodoEntity> listaPeriodo;
}
