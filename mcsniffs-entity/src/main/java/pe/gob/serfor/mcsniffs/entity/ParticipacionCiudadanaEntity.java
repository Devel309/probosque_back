package pe.gob.serfor.mcsniffs.entity;
import java.io.Serializable;
import java.util.Date;

public class ParticipacionCiudadanaEntity extends AuditoriaEntity implements Serializable {
    private Number idPGMF;
    private Number id;
    private String tipo;
    private String descripcion;
    private String mecanismoParticipacion;
    private String metodologia;
    private String lugar;
    private Date fecha;

    

    public ParticipacionCiudadanaEntity(){}

    

    public Number getIdPGMF() {
        return idPGMF;
    }

    public void setIdPGMF(Number idPGMF) {
        this.idPGMF = idPGMF;
    }

    public Number getId() {
        return id;
    }

    public void setId(Number id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMecanismoParticipacion() {
        return mecanismoParticipacion;
    }

    public void setMecanismoParticipacion(String mecanismoParticipacion) {
        this.mecanismoParticipacion = mecanismoParticipacion;
    }

    public String getMetodologia() {
        return metodologia;
    }

    public void setMetodologia(String metodologia) {
        this.metodologia = metodologia;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    
}