package pe.gob.serfor.mcsniffs.entity.Parametro;

import pe.gob.serfor.mcsniffs.entity.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SistemaManejoForestalDto extends AuditoriaEntity implements Serializable {
	private Integer idSistemaManejoForestal;
	private Integer idPlanManejo;
	private String codigoProceso;
	private String descripcionFinMaderable;
	private String descripcionCicloCorta;
	private String descripcionFinNoMaderable;
	private String descripcionCicloAprovechamiento;
	private String seccion;
	private String subSeccion;
	private List<SistemaManejoForestalDetalleEntity> detalle;

	public SistemaManejoForestalDto() {
		this.detalle = new ArrayList<>();
	}

	public String getSeccion() {
		return seccion;
	}

	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}

	public String getSubSeccion() {
		return subSeccion;
	}

	public void setSubSeccion(String subSeccion) {
		this.subSeccion = subSeccion;
	}

	public Integer getIdSistemaManejoForestal() {
		return idSistemaManejoForestal;
	}

	public void setIdSistemaManejoForestal(Integer idSistemaManejoForestal) {
		this.idSistemaManejoForestal = idSistemaManejoForestal;
	}

	public Integer getIdPlanManejo() {
		return idPlanManejo;
	}

	public void setIdPlanManejo(Integer idPlanManejo) {
		this.idPlanManejo = idPlanManejo;
	}

	public String getCodigoProceso() {
		return codigoProceso;
	}

	public void setCodigoProceso(String codigoProceso) {
		this.codigoProceso = codigoProceso;
	}

	public String getDescripcionFinMaderable() {
		return descripcionFinMaderable;
	}

	public void setDescripcionFinMaderable(String descripcionFinMaderable) {
		this.descripcionFinMaderable = descripcionFinMaderable;
	}

	public String getDescripcionCicloCorta() {
		return descripcionCicloCorta;
	}

	public void setDescripcionCicloCorta(String descripcionCicloCorta) {
		this.descripcionCicloCorta = descripcionCicloCorta;
	}

	public String getDescripcionFinNoMaderable() {
		return descripcionFinNoMaderable;
	}

	public void setDescripcionFinNoMaderable(String descripcionFinNoMaderable) {
		this.descripcionFinNoMaderable = descripcionFinNoMaderable;
	}

	public String getDescripcionCicloAprovechamiento() {
		return descripcionCicloAprovechamiento;
	}

	public void setDescripcionCicloAprovechamiento(String descripcionCicloAprovechamiento) {
		this.descripcionCicloAprovechamiento = descripcionCicloAprovechamiento;
	}

	public List<SistemaManejoForestalDetalleEntity> getDetalle() {
		return this.detalle;
	}

	public void setDetalle(List<SistemaManejoForestalDetalleEntity> detalle) {
		this.detalle = detalle;
	}
}
