package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class OrdenamientoProteccionEntity extends AuditoriaEntity implements Serializable {

    private Integer idOrdenamientoProteccion;
    private String codTipoOrdenamiento;
    private String subCodTipoOrdenamiento;
    private String bloque;
    private String superficie;
    private Integer punto;
    private BigDecimal coordenadaEste;
    private BigDecimal coordenadaNorte;
    private String anexo;
    private String observaciones;
    private String medidas;
    private String descripcion;
    private Boolean esSistemaPoliciclico;
    private String justificacion;
    private Integer idPlanManejo;

    private String tipoDocumento;
    private String nroDocumento;
    private String codigoProcesoTitular;

    public String getCodigoProcesoTitular() {
        return codigoProcesoTitular;
    }

    public void setCodigoProcesoTitular(String codigoProcesoTitular) {
        this.codigoProcesoTitular = codigoProcesoTitular;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    private List<OrdenamientoProteccionDetalleEntity> listOrdenamientoProteccionDet;

    public String getSubCodTipoOrdenamiento() {
        return subCodTipoOrdenamiento;
    }

    public void setSubCodTipoOrdenamiento(String subCodTipoOrdenamiento) {
        this.subCodTipoOrdenamiento = subCodTipoOrdenamiento;
    }

    public BigDecimal getCoordenadaEste() {
        return coordenadaEste;
    }

    public void setCoordenadaEste(BigDecimal coordenadaEste) {
        this.coordenadaEste = coordenadaEste;
    }

    public BigDecimal getCoordenadaNorte() {
        return coordenadaNorte;
    }

    public void setCoordenadaNorte(BigDecimal coordenadaNorte) {
        this.coordenadaNorte = coordenadaNorte;
    }

    public Integer getIdOrdenamientoProteccion() {
        return idOrdenamientoProteccion;
    }

    public void setIdOrdenamientoProteccion(Integer idOrdenamientoProteccion) {
        this.idOrdenamientoProteccion = idOrdenamientoProteccion;
    }

    public String getCodTipoOrdenamiento() {
        return codTipoOrdenamiento;
    }

    public void setCodTipoOrdenamiento(String codTipoOrdenamiento) {
        this.codTipoOrdenamiento = codTipoOrdenamiento;
    }

    public String getBloque() {
        return bloque;
    }

    public void setBloque(String bloque) {
        this.bloque = bloque;
    }

    public String getSuperficie() {
        return superficie;
    }

    public void setSuperficie(String superficie) {
        this.superficie = superficie;
    }

    public Integer getPunto() {
        return punto;
    }

    public void setPunto(Integer punto) {
        this.punto = punto;
    }

 

    public String getAnexo() {
        return anexo;
    }

    public void setAnexo(String anexo) {
        this.anexo = anexo;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getMedidas() {
        return medidas;
    }

    public void setMedidas(String medidas) {
        this.medidas = medidas;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public Boolean getEsSistemaPoliciclico() {
        return esSistemaPoliciclico;
    }

    public void setEsSistemaPoliciclico(Boolean esSistemaPoliciclico) {
        this.esSistemaPoliciclico = esSistemaPoliciclico;
    }

    public String getJustificacion() {
        return justificacion;
    }

    public void setJustificacion(String justificacion) {
        this.justificacion = justificacion;
    }

    public List<OrdenamientoProteccionDetalleEntity> getListOrdenamientoProteccionDet() {
        return listOrdenamientoProteccionDet;
    }

    public void setListOrdenamientoProteccionDet(List<OrdenamientoProteccionDetalleEntity> listOrdenamientoProteccionDet) {
        this.listOrdenamientoProteccionDet = listOrdenamientoProteccionDet;
    }

    public OrdenamientoProteccionEntity(){

    }
    public OrdenamientoProteccionEntity(
            Integer idOrdenamientoProteccion,
            String codTipoOrdenamiento,
            String bloque,
            String superficie,
            Integer punto,
            BigDecimal coordenadaEste,
            BigDecimal coordenadaNorte,
            String anexo,
            String observaciones,
            String medidas,
            String descripcion
            ) {
        this.idOrdenamientoProteccion = idOrdenamientoProteccion;
        this.codTipoOrdenamiento = codTipoOrdenamiento;
        this.bloque = bloque;
        this.superficie = superficie;
        this.punto = punto;
        this.coordenadaEste = coordenadaEste;
        this.coordenadaNorte = coordenadaNorte;
        this.anexo = anexo;
        this.observaciones = observaciones;
        this.medidas = medidas;
        this.descripcion = descripcion;

    }
}
