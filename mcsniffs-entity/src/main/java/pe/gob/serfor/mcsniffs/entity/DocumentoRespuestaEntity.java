package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.Date;

public class DocumentoRespuestaEntity extends AuditoriaEntity implements Serializable {
    private Integer IdDocumentoRespuesta;
    private Integer IdDocumentoAdjunto;
    private Integer IdProcesoPostulacion;
    private Date FechaEnvioCorreo;
    private String AsuntoEmail;
    private String ContenidoEmail;
    private String Ruta;
    private Integer IdSolicitante;
    private String str_fechaenviocorreo;

    

    public Integer getIdSolicitante() {
        return IdSolicitante;
    }

    public void setIdSolicitante(Integer idSolicitante) {
        IdSolicitante = idSolicitante;
    }

    public String getStr_fechaenviocorreo() {
        return str_fechaenviocorreo;
    }

    public void setStr_fechaenviocorreo(String str_fechaenviocorreo) {
        this.str_fechaenviocorreo = str_fechaenviocorreo;
    }

    public Date getFechaEnvioCorreo() {
        return FechaEnvioCorreo;
    }

    public void setFechaEnvioCorreo(Date fechaEnvioCorreo) {
        FechaEnvioCorreo = fechaEnvioCorreo;
    }

    public String getRuta() {
        return Ruta;
    }

    public void setRuta(String ruta) {
        Ruta = ruta;
    }

    public Integer getIdDocumentoRespuesta() {
        return IdDocumentoRespuesta;
    }

    public void setIdDocumentoRespuesta(Integer idDocumentoRespuesta) {
        IdDocumentoRespuesta = idDocumentoRespuesta;
    }

    public Integer getIdDocumentoAdjunto() {
        return IdDocumentoAdjunto;
    }

    public void setIdDocumentoAdjunto(Integer idDocumentoAdjunto) {
        IdDocumentoAdjunto = idDocumentoAdjunto;
    }

    public Integer getIdProcesoPostulacion() {
        return IdProcesoPostulacion;
    }

    public void setIdProcesoPostulacion(Integer idProcesoPostulacion) {
        IdProcesoPostulacion = idProcesoPostulacion;
    }


    public String getAsuntoEmail() {
        return AsuntoEmail;
    }

    public void setAsuntoEmail(String asuntoEmail) {
        AsuntoEmail = asuntoEmail;
    }

    public String getContenidoEmail() {
        return ContenidoEmail;
    }

    public void setContenidoEmail(String contenidoEmail) {
        ContenidoEmail = contenidoEmail;
    }
}
