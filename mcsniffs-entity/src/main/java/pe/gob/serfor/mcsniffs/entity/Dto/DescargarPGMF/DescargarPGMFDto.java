package pe.gob.serfor.mcsniffs.entity.Dto.DescargarPGMF;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Getter
@Setter
public class DescargarPGMFDto extends  AuditoriaEntity implements Serializable {
    private Integer idPlanManejo;
 
}
