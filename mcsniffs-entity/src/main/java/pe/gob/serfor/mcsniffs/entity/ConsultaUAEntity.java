package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ConsultaUAEntity implements Serializable {
    private ConsultUAParametrosEntity filtros;

    public ConsultUAParametrosEntity getFiltros() {
        return filtros;
    }

    public void setFiltros(ConsultUAParametrosEntity filtros) {
        this.filtros = filtros;
    }
}
