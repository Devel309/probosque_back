package pe.gob.serfor.mcsniffs.entity.Parametro;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.ImpactoAmbientalDetalleEntity;

import java.io.Serializable;
import java.util.List;

public class ImpactoAmbientalDto extends AuditoriaEntity implements Serializable {

    private Integer idImpactoambiental;
    private Integer idPlanManejo;
    private String actividad;
    private String descripcion;
    private List<ImpactoAmbientalDetalleEntity> detalle;

    public Integer getIdImpactoambiental() {
        return idImpactoambiental;
    }

    public void setIdImpactoambiental(Integer idImpactoambiental) {
        this.idImpactoambiental = idImpactoambiental;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<ImpactoAmbientalDetalleEntity> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<ImpactoAmbientalDetalleEntity> detalle) {
        this.detalle = detalle;
    }
}
