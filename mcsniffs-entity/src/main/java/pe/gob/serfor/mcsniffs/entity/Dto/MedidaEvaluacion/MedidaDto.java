package pe.gob.serfor.mcsniffs.entity.Dto.MedidaEvaluacion;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Getter
@Setter
public class MedidaDto extends AuditoriaEntity implements Serializable{
    
    private Integer idMedida;
    private Integer idPlanManejo;
    private String codigoMedida;
    private String numeroContrato;
    private String cumplimiento;
    private Integer idArchivo;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaCumplimiento;
    private Integer proceso;
    private String detalle;
    private String observacion;
    private String descripcion;
    private String conforme;

    private List<MedidaDetalleDto> listaMedidaDetalle;
}
