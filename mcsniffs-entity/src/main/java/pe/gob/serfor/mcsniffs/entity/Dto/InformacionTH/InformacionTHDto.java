package pe.gob.serfor.mcsniffs.entity.Dto.InformacionTH;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.util.Date;

@Data
public class InformacionTHDto extends AuditoriaEntity implements Serializable {
    private String tituloHabilitante;
    private String nombreTitular;
    private String direccion;
    private String ubigeo;
    private Integer nroPlan;
    private String nroResolucion;
    private String parcelas;
    private Date fechaIniPlan;
    private Date fechaFinPlan;
    private Integer idContrato;
    private Integer idUsuarioTitularTH;
    private String departamento;   
    private String provincia;
    private String distrito;
    private String ruc;
    private String tipoPlan;
    private String nroRegistroLO;
    private String codigoRegente;
    private String nombreRegente;
}
