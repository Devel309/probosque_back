package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class SolicitudMovimientoFlujoEntity extends AuditoriaEntity implements Serializable {
    private Integer idSolicitudMovimientoFlujo;
    private Integer idSolicitud;
    private String estadoSolicitud;
    private String tipoValidacion;
    private Boolean flagObservado;

    public Integer getIdSolicitudMovimientoFlujo() {
        return idSolicitudMovimientoFlujo;
    }

    public void setIdSolicitudMovimientoFlujo(Integer idSolicitudMovimientoFlujo) {
        this.idSolicitudMovimientoFlujo = idSolicitudMovimientoFlujo;
    }

    public Integer getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(Integer idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public String getEstadoSolicitud() {
        return estadoSolicitud;
    }

    public void setEstadoSolicitud(String estadoSolicitud) {
        this.estadoSolicitud = estadoSolicitud;
    }

    public String getTipoValidacion() {
        return tipoValidacion;
    }

    public void setTipoValidacion(String tipoValidacion) {
        this.tipoValidacion = tipoValidacion;
    }

    public Boolean getFlagObservado() {
        return flagObservado;
    }

    public void setFlagObservado(Boolean flagObservado) {
        this.flagObservado = flagObservado;
    }
}
