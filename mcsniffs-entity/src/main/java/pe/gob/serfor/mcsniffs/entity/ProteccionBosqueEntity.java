package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class ProteccionBosqueEntity extends AuditoriaEntity implements Serializable {
    private Integer idProBosque;
    private Integer idPlanManejo;
    private String codPlanGeneral;
    private String subCodPlanGeneral;
    private String descripcion;

    public String getCodPlanGeneral() {
        return codPlanGeneral;
    }

    public void setCodPlanGeneral(String codPlanGeneral) {
        this.codPlanGeneral = codPlanGeneral;
    }

    public String getSubCodPlanGeneral() {
        return subCodPlanGeneral;
    }

    public void setSubCodPlanGeneral(String subCodPlanGeneral) {
        this.subCodPlanGeneral = subCodPlanGeneral;
    }

    private List<ProteccionBosqueDetalleEntity> listProteccionBosque;

    private List<ProteccionBosqueDetalleEntity> listProteccionAnalisis;

    private List<ProteccionBosqueDetalleEntity> listProteccionGestion;

    public List<ProteccionBosqueDetalleEntity> getListProteccionAnalisis() {
        return listProteccionAnalisis;
    }

    public void setListProteccionAnalisis(List<ProteccionBosqueDetalleEntity> listProteccionAnalisis) {
        this.listProteccionAnalisis = listProteccionAnalisis;
    }

    public List<ProteccionBosqueDetalleEntity> getListProteccionGestion() {
        return listProteccionGestion;
    }

    public void setListProteccionGestion(List<ProteccionBosqueDetalleEntity> listProteccionGestion) {
        this.listProteccionGestion = listProteccionGestion;
    }

    public ProteccionBosqueEntity(){
        this.listProteccionBosque = new ArrayList<>();
    }

    public Integer getIdProBosque() {
        return idProBosque;
    }

    public void setIdProBosque(Integer idProBosque) {
        this.idProBosque = idProBosque;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }


    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<ProteccionBosqueDetalleEntity> getListProteccionBosque() {
        return listProteccionBosque;
    }

    public void setListProteccionBosque(List<ProteccionBosqueDetalleEntity> listProteccionBosque) {
        this.listProteccionBosque = listProteccionBosque;
    }
}
