package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class ResponsableFormacionAcademicaEntity extends AuditoriaEntity implements Serializable{

    private Integer idResponsableFormacionAcademica;
    private Integer idSolicitudConcesionResponsable;
    private Integer idSolicitudConcesion;
    private String codigoGrado;
    private String descripcionGrado;
    private String codigoEspecialidad;
    private String descripcionEspecialidad;
    private String nombreUniversidad;
    private Integer anioTitulo;

}
