package pe.gob.serfor.mcsniffs.entity.Dto.Contrato;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContratoGeometriaDto implements Serializable {
    private Integer idContratoArea;
    private Integer idContrato;
    private String nombreDepartamento;
    private Double areaHA;
    private Double areaAprobada;
    private String tipoOrigen;
    private Integer idOrigen;
    private Integer idUnidadAprovechamiento;
    private String estadoUA;
    private String ubigeo;
    private String geometry;
    private String descripcionContrato;
}
