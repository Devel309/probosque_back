package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ManejoBosqueAprovechamientoEntity extends AuditoriaEntity implements Serializable {
    private Integer idManBosqueAprove;
    private String idTipo;
    private String idTipoMetodoAprove;
    private String tipoMetodoAprove;
    private String idTipoTransMaderable;
    private String tipoTransMaderable;
    private String idTipoTransAreaDestino;
    private String tipoTransAreaDestino;
    private Double caminoAcceso;
    private Double caminoPrincipal;
    private String descripcion;
    private String caminoDescripcion;
    private PlanManejoEntity planManejo;

    public String getCaminoDescripcion() {
        return caminoDescripcion;
    }
    public void setCaminoDescripcion(String caminoDescripcion) {
        this.caminoDescripcion = caminoDescripcion;
    }
    public Integer getIdManBosqueAprove() {
        return idManBosqueAprove;
    }

    public void setIdManBosqueAprove(Integer idManBosqueAprove) {
        this.idManBosqueAprove = idManBosqueAprove;
    }

    public String getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(String idTipo) {
        this.idTipo = idTipo;
    }

    public String getIdTipoMetodoAprove() {
        return idTipoMetodoAprove;
    }

    public void setIdTipoMetodoAprove(String idTipoMetodoAprove) {
        this.idTipoMetodoAprove = idTipoMetodoAprove;
    }

    public String getTipoMetodoAprove() {
        return tipoMetodoAprove;
    }

    public void setTipoMetodoAprove(String tipoMetodoAprove) {
        this.tipoMetodoAprove = tipoMetodoAprove;
    }

    public String getIdTipoTransMaderable() {
        return idTipoTransMaderable;
    }

    public void setIdTipoTransMaderable(String idTipoTransMaderable) {
        this.idTipoTransMaderable = idTipoTransMaderable;
    }

    public String getTipoTransMaderable() {
        return tipoTransMaderable;
    }

    public void setTipoTransMaderable(String tipoTransMaderable) {
        this.tipoTransMaderable = tipoTransMaderable;
    }

    public String getIdTipoTransAreaDestino() {
        return idTipoTransAreaDestino;
    }

    public void setIdTipoTransAreaDestino(String idTipoTransAreaDestino) {
        this.idTipoTransAreaDestino = idTipoTransAreaDestino;
    }

    public String getTipoTransAreaDestino() {
        return tipoTransAreaDestino;
    }

    public void setTipoTransAreaDestino(String tipoTransAreaDestino) {
        this.tipoTransAreaDestino = tipoTransAreaDestino;
    }

    public Double getCaminoAcceso() {
        return caminoAcceso;
    }

    public void setCaminoAcceso(Double caminoAcceso) {
        this.caminoAcceso = caminoAcceso;
    }

    public Double getCaminoPrincipal() {
        return caminoPrincipal;
    }

    public void setCaminoPrincipal(Double caminoPrincipal) {
        this.caminoPrincipal = caminoPrincipal;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public PlanManejoEntity getPlanManejo() {
        return planManejo;
    }

    public void setPlanManejo(PlanManejoEntity planManejo) {
        this.planManejo = planManejo;
    }
}
