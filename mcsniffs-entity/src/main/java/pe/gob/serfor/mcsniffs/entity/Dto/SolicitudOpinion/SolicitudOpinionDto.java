package pe.gob.serfor.mcsniffs.entity.Dto.SolicitudOpinion;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.util.Date;
@Data
public class SolicitudOpinionDto extends AuditoriaEntity implements Serializable {

    Integer idSolicitudOpinion;
    String siglaEntidad;
    String tipoDocGestion;
    Integer nroDocGestion;
    Date fechaDocGestion;
    String estSolicitudOpinion;
    Integer documentoGestion;
    Boolean esFavorable;
    String descripcion;
    String tipoDoc;
    Date fechaEmision;
    Integer documentoOficio;
    Integer documentoRespuesta;
    String tipoDocGestionTexto;
    String estadoSolicitudOpinion;
    String estadoSolicitudOpinionTexto;
    String asunto;
    Integer totalRegistro;
    String idTipoDocumento;
    Integer documento;
    Integer idArchivo;
    String nombreDocumentoOficio;
    String nombreDocumentoRespuesta;
    String tipoDocumento;
    String numeroDocumento;
    String nombreDocumentoArchivo;
    Integer idPlanManejo;
}
