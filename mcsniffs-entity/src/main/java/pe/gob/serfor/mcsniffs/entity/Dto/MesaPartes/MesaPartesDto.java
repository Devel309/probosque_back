package pe.gob.serfor.mcsniffs.entity.Dto.MesaPartes;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Getter
@Setter
public class MesaPartesDto extends AuditoriaEntity implements Serializable {
    
    private Integer idMesaPartes;
    private Integer idPlanManejo;
    private String codigo;
    private String conforme;
    private String comentario;
    private String observacion;
    private String detalle;
    private String descripcion;
    private List<MesaPartesDetalleDto> listarMesaPartesDetalle;

    private String correo;

    private List<MesaPartesDetalleDto> listarMesaPartesRequisitosTUPA;
    private Timestamp fechaTramite;
}
