package pe.gob.serfor.mcsniffs.entity;

public class AnexosPFDMResquestEntity {
    private String codigoAnexo;
    private Integer IdPostulacionPFDM, IdUsuarioPostulacion;
    
    public String getCodigoAnexo() {
        return codigoAnexo;
    }
    public void setCodigoAnexo(String codigoAnexo) {
        this.codigoAnexo = codigoAnexo;
    }
    public Integer getIdPostulacionPFDM() {
        return IdPostulacionPFDM;
    }
    public void setIdPostulacionPFDM(Integer idPostulacionPFDM) {
        IdPostulacionPFDM = idPostulacionPFDM;
    }
    public Integer getIdUsuarioPostulacion() {
        return IdUsuarioPostulacion;
    }
    public void setIdUsuarioPostulacion(Integer idUsuarioPostulacion) {
        IdUsuarioPostulacion = idUsuarioPostulacion;
    }
    
}
