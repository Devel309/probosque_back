package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;

import java.sql.Date;

@Data
public class MunicipioPersonaEntity  extends AuditoriaEntity implements Serializable {

    private Integer idMunicipio;
    private Integer idDistrito;
    private Integer idPersona;
    private String numeroDocumento;
    private String nombrePersona;


}
