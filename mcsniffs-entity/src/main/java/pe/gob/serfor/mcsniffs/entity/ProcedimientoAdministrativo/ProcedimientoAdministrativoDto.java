package pe.gob.serfor.mcsniffs.entity.ProcedimientoAdministrativo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

 
@Getter
@Setter
public class ProcedimientoAdministrativoDto  extends AuditoriaEntity implements Serializable{    

    private Integer idProcedimiento;
    private String codigoProcedimiento;
    private String procedimientoAdministrativo;
    private String descripcion;
    private String observacion;
    private String ubigeo;
}
