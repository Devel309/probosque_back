package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.Date;

public class MedioTransporteEntity  extends AuditoriaEntity implements Serializable {
    private Integer idMedioTransporte;
    private String codTipoMedio;
    private String medioTransporte;

    public Integer getIdMedioTransporte() {
        return idMedioTransporte;
    }

    public void setIdMedioTransporte(Integer idMedioTransporte) {
        this.idMedioTransporte = idMedioTransporte;
    }

    public String getCodTipoMedio() {
        return codTipoMedio;
    }

    public void setCodTipoMedio(String codTipoMedio) {
        this.codTipoMedio = codTipoMedio;
    }

    public String getMedioTransporte() {
        return medioTransporte;
    }

    public void setMedioTransporte(String medioTransporte) {
        this.medioTransporte = medioTransporte;
    }

}
