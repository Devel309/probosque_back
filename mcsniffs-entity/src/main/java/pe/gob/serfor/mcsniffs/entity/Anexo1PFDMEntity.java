package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.List;

public class Anexo1PFDMEntity extends FirmaAnexosPFDMEntity implements Serializable{
    private String soliNombreRS;
    private String soliDni;
    private String soliRuc;
    private String soliNroPartidaRegistral;
    private String soliDireccion;
    private String soliNroDireccion;
    private String soliSectorCaserio;
    private String soliIdDistrito;
    private String soliIdProvincia;
    private String soliIdDepartamento;
    private String soliTelefono;
    private String soliCelular;
    private String soliCorreoPersonal;
    private String soliModalidad;
    private String soliVigencia;
    private String soliObjetivos;
    private String soliRecursos;
    private String soliImportancia;
    private String soliDescripcionProyecto;
    private String areaSuperficie;
    private String areaIdDistrito;
    private String areaIdProvincia;
    private String areaIdDepartamento;
    private String areaZona;
    private String areaObservaciones;
    private List<AreaSolicitadaUTMEntity> areaCoordenadasUTM;
    private String idDocumentoAdjuntoAreaSolicitada;
    private String idDocumentoAdjuntoVertices;
    private List<ActividadesDesarrolloPFDMEntity> actividadesDesarrollar;
    private String ecoturismoDescripcion;
    private String faunaSilvestreDescripcion;
    private String pfdmDescripcion;
    private String ecosistemasDescripcion;
    private String maderableDescripcion;
    private String financiamientoDescripcion;
    private String soliCorreolegal;
    private String codigoAnexo;
    

    public String getIdDocumentoAdjuntoAreaSolicitada() {
        return idDocumentoAdjuntoAreaSolicitada;
    }
    public void setIdDocumentoAdjuntoAreaSolicitada(String idDocumentoAdjuntoAreaSolicitada) {
        this.idDocumentoAdjuntoAreaSolicitada = idDocumentoAdjuntoAreaSolicitada;
    }
    public String getIdDocumentoAdjuntoVertices() {
        return idDocumentoAdjuntoVertices;
    }
    public void setIdDocumentoAdjuntoVertices(String idDocumentoAdjuntoVertices) {
        this.idDocumentoAdjuntoVertices = idDocumentoAdjuntoVertices;
    }
    public String getCodigoAnexo() {
        return codigoAnexo;
    }
    public void setCodigoAnexo(String codigoAnexo) {
        this.codigoAnexo = codigoAnexo;
    }
    public String getSoliTelefono() {
        return soliTelefono;
    }
    public void setSoliTelefono(String soliTelefono) {
        this.soliTelefono = soliTelefono;
    }
    public String getSoliCelular() {
        return soliCelular;
    }
    public void setSoliCelular(String soliCelular) {
        this.soliCelular = soliCelular;
    }
    public String getSoliCorreoPersonal() {
        return soliCorreoPersonal;
    }
    public void setSoliCorreoPersonal(String soliCorreoPersonal) {
        this.soliCorreoPersonal = soliCorreoPersonal;
    }
    public String getSoliCorreolegal() {
        return soliCorreolegal;
    }
    public void setSoliCorreolegal(String soliCorreolegal) {
        this.soliCorreolegal = soliCorreolegal;
    }
    public String getAreaObservaciones() {
        return areaObservaciones;
    }
    public void setAreaObservaciones(String areaObservaciones) {
        this.areaObservaciones = areaObservaciones;
    }
    public String getSoliNombreRS() {
        return soliNombreRS;
    }
    public void setSoliNombreRS(String soliNombreRS) {
        this.soliNombreRS = soliNombreRS;
    }
    public String getSoliDni() {
        return soliDni;
    }
    public void setSoliDni(String soliDni) {
        this.soliDni = soliDni;
    }
    public String getSoliRuc() {
        return soliRuc;
    }
    public void setSoliRuc(String soliRuc) {
        this.soliRuc = soliRuc;
    }
    public String getSoliNroPartidaRegistral() {
        return soliNroPartidaRegistral;
    }
    public void setSoliNroPartidaRegistral(String soliNroPartidaRegistral) {
        this.soliNroPartidaRegistral = soliNroPartidaRegistral;
    }
    public String getSoliDireccion() {
        return soliDireccion;
    }
    public void setSoliDireccion(String soliDireccion) {
        this.soliDireccion = soliDireccion;
    }
    public String getSoliNroDireccion() {
        return soliNroDireccion;
    }
    public void setSoliNroDireccion(String soliNroDireccion) {
        this.soliNroDireccion = soliNroDireccion;
    }
    public String getSoliSectorCaserio() {
        return soliSectorCaserio;
    }
    public void setSoliSectorCaserio(String soliSectorCaserio) {
        this.soliSectorCaserio = soliSectorCaserio;
    }
    public String getSoliIdDistrito() {
        return soliIdDistrito;
    }
    public void setSoliIdDistrito(String soliIdDistrito) {
        this.soliIdDistrito = soliIdDistrito;
    }
    public String getSoliIdProvincia() {
        return soliIdProvincia;
    }
    public void setSoliIdProvincia(String soliIdProvincia) {
        this.soliIdProvincia = soliIdProvincia;
    }
    public String getSoliIdDepartamento() {
        return soliIdDepartamento;
    }
    public void setSoliIdDepartamento(String soliIdDepartamento) {
        this.soliIdDepartamento = soliIdDepartamento;
    }
    public String getSoliModalidad() {
        return soliModalidad;
    }
    public void setSoliModalidad(String soliModalidad) {
        this.soliModalidad = soliModalidad;
    }
    public String getSoliVigencia() {
        return soliVigencia;
    }
    public void setSoliVigencia(String soliVigencia) {
        this.soliVigencia = soliVigencia;
    }
    public String getSoliObjetivos() {
        return soliObjetivos;
    }
    public void setSoliObjetivos(String soliObjetivos) {
        this.soliObjetivos = soliObjetivos;
    }
    public String getSoliRecursos() {
        return soliRecursos;
    }
    public void setSoliRecursos(String soliRecursos) {
        this.soliRecursos = soliRecursos;
    }
    public String getSoliImportancia() {
        return soliImportancia;
    }
    public void setSoliImportancia(String soliImportancia) {
        this.soliImportancia = soliImportancia;
    }
    public String getSoliDescripcionProyecto() {
        return soliDescripcionProyecto;
    }
    public void setSoliDescripcionProyecto(String soliDescripcionProyecto) {
        this.soliDescripcionProyecto = soliDescripcionProyecto;
    }
    public String getAreaSuperficie() {
        return areaSuperficie;
    }
    public void setAreaSuperficie(String areaSuperficie) {
        this.areaSuperficie = areaSuperficie;
    }
    public String getAreaIdDistrito() {
        return areaIdDistrito;
    }
    public void setAreaIdDistrito(String areaIdDistrito) {
        this.areaIdDistrito = areaIdDistrito;
    }
    public String getAreaIdProvincia() {
        return areaIdProvincia;
    }
    public void setAreaIdProvincia(String areaIdProvincia) {
        this.areaIdProvincia = areaIdProvincia;
    }
    public String getAreaIdDepartamento() {
        return areaIdDepartamento;
    }
    public void setAreaIdDepartamento(String areaIdDepartamento) {
        this.areaIdDepartamento = areaIdDepartamento;
    }
    public String getAreaZona() {
        return areaZona;
    }
    public void setAreaZona(String areaZona) {
        this.areaZona = areaZona;
    }
    public List<AreaSolicitadaUTMEntity> getAreaCoordenadasUTM() {
        return areaCoordenadasUTM;
    }
    public void setAreaCoordenadasUTM(List<AreaSolicitadaUTMEntity> areaCoordenadasUTM) {
        this.areaCoordenadasUTM = areaCoordenadasUTM;
    }

    public List<ActividadesDesarrolloPFDMEntity> getActividadesDesarrollar() {
        return actividadesDesarrollar;
    }
    public void setActividadesDesarrollar(List<ActividadesDesarrolloPFDMEntity> actividadesDesarrollar) {
        this.actividadesDesarrollar = actividadesDesarrollar;
    }
    public String getEcoturismoDescripcion() {
        return ecoturismoDescripcion;
    }
    public void setEcoturismoDescripcion(String ecoturismoDescripcion) {
        this.ecoturismoDescripcion = ecoturismoDescripcion;
    }
    public String getFaunaSilvestreDescripcion() {
        return faunaSilvestreDescripcion;
    }
    public void setFaunaSilvestreDescripcion(String faunaSilvestreDescripcion) {
        this.faunaSilvestreDescripcion = faunaSilvestreDescripcion;
    }
    public String getPfdmDescripcion() {
        return pfdmDescripcion;
    }
    public void setPfdmDescripcion(String pfdmDescripcion) {
        this.pfdmDescripcion = pfdmDescripcion;
    }
    public String getEcosistemasDescripcion() {
        return ecosistemasDescripcion;
    }
    public void setEcosistemasDescripcion(String ecosistemasDescripcion) {
        this.ecosistemasDescripcion = ecosistemasDescripcion;
    }
    public String getMaderableDescripcion() {
        return maderableDescripcion;
    }
    public void setMaderableDescripcion(String maderableDescripcion) {
        this.maderableDescripcion = maderableDescripcion;
    }
    public String getFinanciamientoDescripcion() {
        return financiamientoDescripcion;
    }
    public void setFinanciamientoDescripcion(String financiamientoDescripcion) {
        this.financiamientoDescripcion = financiamientoDescripcion;
    }
    
}
