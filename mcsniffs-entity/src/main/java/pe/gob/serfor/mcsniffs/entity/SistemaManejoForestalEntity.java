package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class SistemaManejoForestalEntity extends AuditoriaEntity implements Serializable {
    private Integer idSistemaManejoForestal;
    private Integer idPlanManejo;
    private String descripcionFinMaderable;
    private String descripcionCicloCorta;
    private String descripcionFinNoMaderable;
    private String descripcionCicloAprovechamiento;

    private SistemaManejoForestalDetalleEntity sistemaManejoForestalDetalle;

    public Integer getIdSistemaManejoForestal() {
        return idSistemaManejoForestal;
    }

    public void setIdSistemaManejoForestal(Integer idSistemaManejoForestal) {
        this.idSistemaManejoForestal = idSistemaManejoForestal;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public String getDescripcionFinMaderable() {
        return descripcionFinMaderable;
    }

    public void setDescripcionFinMaderable(String descripcionFinMaderable) {
        this.descripcionFinMaderable = descripcionFinMaderable;
    }

    public String getDescripcionCicloCorta() {
        return descripcionCicloCorta;
    }

    public void setDescripcionCicloCorta(String descripcionCicloCorta) {
        this.descripcionCicloCorta = descripcionCicloCorta;
    }

    public String getDescripcionFinNoMaderable() {
        return descripcionFinNoMaderable;
    }

    public void setDescripcionFinNoMaderable(String descripcionFinNoMaderable) {
        this.descripcionFinNoMaderable = descripcionFinNoMaderable;
    }

    public String getDescripcionCicloAprovechamiento() {
        return descripcionCicloAprovechamiento;
    }

    public void setDescripcionCicloAprovechamiento(String descripcionCicloAprovechamiento) {
        this.descripcionCicloAprovechamiento = descripcionCicloAprovechamiento;
    }

    public SistemaManejoForestalDetalleEntity getSistemaManejoForestalDetalle() {
        return sistemaManejoForestalDetalle;
    }

    public void setSistemaManejoForestalDetalle(SistemaManejoForestalDetalleEntity sistemaManejoForestalDetalle) {
        this.sistemaManejoForestalDetalle = sistemaManejoForestalDetalle;
    }
}
