package pe.gob.serfor.mcsniffs.entity.Evaluacion;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.sql.Timestamp;

@Getter
@Setter
public class EvaluacionPermisoForestalDetalleDto extends AuditoriaEntity implements Serializable{
    private Integer idEvaluacionPermisoDet;
    private Integer idEvaluacionPermiso;
    private String codigoEvaluacion;
    private String codigoEvaluacionDet;
    private String codigoEvaluacionDetSub;
    private String codigoEvaluacionDetPost;
    private String conforme;
    private String observacion;
    private String detalle;
    private String descripcion;
    private String estadoEvaluacionDet;
    private Integer idArchivo;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Timestamp fechaEvaluacionDetInicial;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Timestamp fechaEvaluacionDetFinal;

}
