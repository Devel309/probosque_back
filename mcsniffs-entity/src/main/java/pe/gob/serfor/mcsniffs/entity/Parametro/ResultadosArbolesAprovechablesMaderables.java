package pe.gob.serfor.mcsniffs.entity.Parametro;

import java.util.List;

public class ResultadosArbolesAprovechablesMaderables {
    String nHaInventariadas;
    List<String>  listaTipoBosque;
    List<TablaArbolesAprovechablesMaderablesDto> tablaArbolesAprovechablesMaderablesDtoList;
    public String getnHaInventariadas() {
        return nHaInventariadas;
    }

    public void setnHaInventariadas(String nHaInventariadas) {
        this.nHaInventariadas = nHaInventariadas;
    }

    public List<String> getListaTipoBosque() {
        return listaTipoBosque;
    }

    public void setListaTipoBosque(List<String> listaTipoBosque) {
        this.listaTipoBosque = listaTipoBosque;
    }

    public List<TablaArbolesAprovechablesMaderablesDto> getTablaArbolesAprovechablesMaderablesDtoList() {
        return tablaArbolesAprovechablesMaderablesDtoList;
    }

    public void setTablaArbolesAprovechablesMaderablesDtoList(List<TablaArbolesAprovechablesMaderablesDto> tablaArbolesAprovechablesMaderablesDtoList) {
        this.tablaArbolesAprovechablesMaderablesDtoList = tablaArbolesAprovechablesMaderablesDtoList;
    }
}
