package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class OrdenamientoDetalleEntity extends AuditoriaEntity implements Serializable {
    Integer idPlanManejo ;
    String codigoTipoDetalle; 
    String vertice; 
    String lugar; 
    String coordenadas; 
    String material; 
    String observacion; 

    Integer filaExcel ;

    String infraestructuraPersonalReq;
	String actividadVigilancia;
	String idRiesgoVulnerabilidad;
	String longitudLindero;
	String riesgoIdentificado;
	String sectorLindero;

    
    public String getInfraestructuraPersonalReq() {
        return infraestructuraPersonalReq;
    }

    public void setInfraestructuraPersonalReq(String infraestructuraPersonalReq) {
        this.infraestructuraPersonalReq = infraestructuraPersonalReq;
    }

    public String getActividadVigilancia() {
        return actividadVigilancia;
    }

    public void setActividadVigilancia(String actividadVigilancia) {
        this.actividadVigilancia = actividadVigilancia;
    }

    public String getIdRiesgoVulnerabilidad() {
        return idRiesgoVulnerabilidad;
    }

    public void setIdRiesgoVulnerabilidad(String idRiesgoVulnerabilidad) {
        this.idRiesgoVulnerabilidad = idRiesgoVulnerabilidad;
    }

    public String getLongitudLindero() {
        return longitudLindero;
    }

    public void setLongitudLindero(String longitudLindero) {
        this.longitudLindero = longitudLindero;
    }

    public String getRiesgoIdentificado() {
        return riesgoIdentificado;
    }

    public void setRiesgoIdentificado(String riesgoIdentificado) {
        this.riesgoIdentificado = riesgoIdentificado;
    }

    public String getSectorLindero() {
        return sectorLindero;
    }

    public void setSectorLindero(String sectorLindero) {
        this.sectorLindero = sectorLindero;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public String getCodigoTipoDetalle() {
        return codigoTipoDetalle;
    }

    public void setCodigoTipoDetalle(String codigoTipoDetalle) {
        this.codigoTipoDetalle = codigoTipoDetalle;
    }

    public String getVertice() {
        return vertice;
    }

    public void setVertice(String vertice) {
        this.vertice = vertice;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        this.coordenadas = coordenadas;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getFilaExcel() {
        return filaExcel;
    }

    public void setFilaExcel(Integer filaExcel) {
        this.filaExcel = filaExcel;
    }
    
}
