package pe.gob.serfor.mcsniffs.entity.Dto.ProcesoOfertaSolicitudOpinion;

import lombok.Data;
import java.io.Serializable;

@Data
public class ProcesoOfertaSolicitudOpinionDto implements Serializable {

    Integer idProcesoOfertaSolicitudOpinion;
    Integer idProcesoOferta;
    Integer idSolicitudOpinion;
    Integer pageNum;
    Integer pageSize;
    String siglaEntidad;
    String asunto;
    String estado;
    Integer idUsuarioRegistro;
    Integer idUsuarioModificacion;
    Integer idUsuarioElimina;

}