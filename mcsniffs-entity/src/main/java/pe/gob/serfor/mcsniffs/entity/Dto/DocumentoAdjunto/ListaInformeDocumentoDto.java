package pe.gob.serfor.mcsniffs.entity.Dto.DocumentoAdjunto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListaInformeDocumentoDto {
 
    private String descripcion;
    private String resultado;
}
