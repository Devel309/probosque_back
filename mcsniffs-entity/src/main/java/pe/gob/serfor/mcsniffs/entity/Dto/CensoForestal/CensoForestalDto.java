package pe.gob.serfor.mcsniffs.entity.Dto.CensoForestal;

import java.util.Date;

public class CensoForestalDto {

    private Integer idCensoForestal;
    private String correlativo;
    private Integer idPlanManejo;
    private String tipoCensoForestal;
    private String estado;
    private Integer idUsuarioRegistro;
    private Date fechaRegistro;
    private Integer idUsuarioModificacion;
    private Date fechaModificacion;
    private Integer idUsuarioElimina;
    private Date fechaElimina;

    public Integer getIdCensoForestal() {
        return idCensoForestal;
    }

    public void setIdCensoForestal(Integer idCensoForestal) {
        this.idCensoForestal = idCensoForestal;
    }

    public String getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(String correlativo) {
        this.correlativo = correlativo;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public String getTipoCensoForestal() {
        return tipoCensoForestal;
    }

    public void setTipoCensoForestal(String tipoCensoForestal) {
        this.tipoCensoForestal = tipoCensoForestal;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Integer getIdUsuarioRegistro() {
        return idUsuarioRegistro;
    }

    public void setIdUsuarioRegistro(Integer idUsuarioRegistro) {
        this.idUsuarioRegistro = idUsuarioRegistro;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getIdUsuarioModificacion() {
        return idUsuarioModificacion;
    }

    public void setIdUsuarioModificacion(Integer idUsuarioModificacion) {
        this.idUsuarioModificacion = idUsuarioModificacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Integer getIdUsuarioElimina() {
        return idUsuarioElimina;
    }

    public void setIdUsuarioElimina(Integer idUsuarioElimina) {
        this.idUsuarioElimina = idUsuarioElimina;
    }

    public Date getFechaElimina() {
        return fechaElimina;
    }

    public void setFechaElimina(Date fechaElimina) {
        this.fechaElimina = fechaElimina;
    }
}
