package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class InformacionBasicaDetalleEntity extends AuditoriaEntity implements Serializable {
    private Integer idInfBasicaDet;
    private String codInfBasicaDet;
    private String codSubInfBasicaDet;
    private String puntoVertice;
    private BigDecimal coordenadaEsteIni;
    private BigDecimal coordenadaNorteIni;
    private BigDecimal coordenadaEsteFin;
    private BigDecimal coordenadaNorteFin;
    private String referencia;
    private BigDecimal distanciaKm;
    private BigDecimal tiempo;
    private String medioTransporte;
    private String descripcion;
    private String rio;
    private String quebrada;
    private String laguna;
    private String unidadFisiografica;
    private String valor;
    private BigDecimal areaHa;
    private BigDecimal areaHaPorcentaje;
    private String actividad;
    private String especieExtraida;
    private String observaciones;
    private String conflicto;
    private String solucion;
    private Integer idInfBasica;
    private Integer idFauna;
    private Integer idInfraestructura;
    private Integer idTipoBosque;
    private String coordenadaUtm;
    private String epoca;
    private Integer idRios;
    private String zonaVida;
    private Integer idFlora;
    private String nombreRio;
    private String nombreLaguna;
    private String nombreQuebrada;
    private String nombre;
    private String acceso;
    private Integer numeroFamilia;
    private Boolean subsistencia;
    private Boolean perenne;
    private Boolean ganaderia;
    private Boolean caza;
    private Boolean pesca;
    private Boolean madera;
    private Boolean otroProducto;
    private Boolean ampliarAnexo;
    private String justificacion;

    private Boolean marcar;

    private String nombreComun;
    private String nombreCientifico;
    private String familia;

    private List<InformacionBasicaDetalleSubEntity> listInformacionBasicaDetSub;

    public List<InformacionBasicaDetalleSubEntity> getListInformacionBasicaDetSub() {
        return listInformacionBasicaDetSub;
    }

    public void setListInformacionBasicaDetSub(List<InformacionBasicaDetalleSubEntity> listInformacionBasicaDetSub) {
        this.listInformacionBasicaDetSub = listInformacionBasicaDetSub;
    }

    public Boolean getMarcar() {
        return marcar;
    }

    public void setMarcar(Boolean marcar) {
        this.marcar = marcar;
    }

    public Boolean getAmpliarAnexo() {
        return ampliarAnexo;
    }

    public void setAmpliarAnexo(Boolean ampliarAnexo) {
        this.ampliarAnexo = ampliarAnexo;
    }

    public String getJustificacion() {
        return justificacion;
    }

    public void setJustificacion(String justificacion) {
        this.justificacion = justificacion;
    }

    public Boolean getSubsistencia() {
        return subsistencia;
    }

    public void setSubsistencia(Boolean subsistencia) {
        this.subsistencia = subsistencia;
    }

    public Boolean getPerenne() {
        return perenne;
    }

    public void setPerenne(Boolean perenne) {
        this.perenne = perenne;
    }

    public Boolean getGanaderia() {
        return ganaderia;
    }

    public void setGanaderia(Boolean ganaderia) {
        this.ganaderia = ganaderia;
    }

    public Boolean getCaza() {
        return caza;
    }

    public void setCaza(Boolean caza) {
        this.caza = caza;
    }

    public Boolean getPesca() {
        return pesca;
    }

    public void setPesca(Boolean pesca) {
        this.pesca = pesca;
    }

    public Boolean getMadera() {
        return madera;
    }

    public void setMadera(Boolean madera) {
        this.madera = madera;
    }

    public Boolean getOtroProducto() {
        return otroProducto;
    }

    public void setOtroProducto(Boolean otroProducto) {
        this.otroProducto = otroProducto;
    }

    public Integer getNumeroFamilia() {
        return numeroFamilia;
    }

    public void setNumeroFamilia(Integer numeroFamilia) {
        this.numeroFamilia = numeroFamilia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAcceso() {
        return acceso;
    }

    public void setAcceso(String acceso) {
        this.acceso = acceso;
    }

    public String getNombreComun() {
        return nombreComun;
    }

    public void setNombreComun(String nombreComun) {
        this.nombreComun = nombreComun;
    }

    public String getNombreCientifico() {
        return nombreCientifico;
    }

    public void setNombreCientifico(String nombreCientifico) {
        this.nombreCientifico = nombreCientifico;
    }

    public String getFamilia() {
        return familia;
    }

    public void setFamilia(String familia) {
        this.familia = familia;
    }


    public Integer getIdRios() {
        return idRios;
    }

    public void setIdRios(Integer idRios) {
        this.idRios = idRios;
    }

    public String getZonaVida() {
        return zonaVida;
    }

    public void setZonaVida(String zonaVida) {
        this.zonaVida = zonaVida;
    }

    public Integer getIdFlora() {
        return idFlora;
    }

    public void setIdFlora(Integer idFlora) {
        this.idFlora = idFlora;
    }

    public String getEpoca() {
        return epoca;
    }

    public void setEpoca(String epoca) {
        this.epoca = epoca;
    }

    public Integer getIdInfBasicaDet() {
        return idInfBasicaDet;
    }

    public InformacionBasicaDetalleEntity setIdInfBasicaDet(Integer idInfBasicaDet) {
        this.idInfBasicaDet = idInfBasicaDet;
        return this;
    }

    public String getCodInfBasicaDet() {
        return codInfBasicaDet;
    }

    public InformacionBasicaDetalleEntity setCodInfBasicaDet(String codInfBasicaDet) {
        this.codInfBasicaDet = codInfBasicaDet;
        return this;
    }

    public String getCodSubInfBasicaDet() {
        return codSubInfBasicaDet;
    }

    public InformacionBasicaDetalleEntity setCodSubInfBasicaDet(String codSubInfBasicaDet) {
        this.codSubInfBasicaDet = codSubInfBasicaDet;
        return this;
    }

    public String getPuntoVertice() {
        return puntoVertice;
    }

    public InformacionBasicaDetalleEntity setPuntoVertice(String puntoVertice) {
        this.puntoVertice = puntoVertice;
        return this;
    }

  
    public String getReferencia() {
        return referencia;
    }

    public InformacionBasicaDetalleEntity setReferencia(String referencia) {
        this.referencia = referencia;
        return this;
    }

 
    public String getMedioTransporte() {
        return medioTransporte;
    }

    public InformacionBasicaDetalleEntity setMedioTransporte(String medioTransporte) {
        this.medioTransporte = medioTransporte;
        return this;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public InformacionBasicaDetalleEntity setDescripcion(String descripcion) {
        this.descripcion = descripcion;
        return this;
    }

    public String getRio() {
        return rio;
    }

    public InformacionBasicaDetalleEntity setRio(String rio) {
        this.rio = rio;
        return this;
    }

    public String getQuebrada() {
        return quebrada;
    }

    public InformacionBasicaDetalleEntity setQuebrada(String quebrada) {
        this.quebrada = quebrada;
        return this;
    }

    public String getLaguna() {
        return laguna;
    }

    public InformacionBasicaDetalleEntity setLaguna(String laguna) {
        this.laguna = laguna;
        return this;
    }

    public String getUnidadFisiografica() {
        return unidadFisiografica;
    }

    public InformacionBasicaDetalleEntity setUnidadFisiografica(String unidadFisiografica) {
        this.unidadFisiografica = unidadFisiografica;
        return this;
    }

    public String getValor() {
        return valor;
    }

    public InformacionBasicaDetalleEntity setValor(String valor) {
        this.valor = valor;
        return this;
    }

    public String getActividad() {
        return actividad;
    }

    public InformacionBasicaDetalleEntity setActividad(String actividad) {
        this.actividad = actividad;
        return this;
    }

    public String getEspecieExtraida() {
        return especieExtraida;
    }

    public InformacionBasicaDetalleEntity setEspecieExtraida(String especieExtraida) {
        this.especieExtraida = especieExtraida;
        return this;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public InformacionBasicaDetalleEntity setObservaciones(String observaciones) {
        this.observaciones = observaciones;
        return this;
    }

    public String getConflicto() {
        return conflicto;
    }

    public InformacionBasicaDetalleEntity setConflicto(String conflicto) {
        this.conflicto = conflicto;
        return this;
    }

    public String getSolucion() {
        return solucion;
    }

    public InformacionBasicaDetalleEntity setSolucion(String solucion) {
        this.solucion = solucion;
        return this;
    }

    public Integer getIdInfBasica() {
        return idInfBasica;
    }

    public InformacionBasicaDetalleEntity setIdInfBasica(Integer idInfBasica) {
        this.idInfBasica = idInfBasica;
        return this;
    }

    public Integer getIdFauna() {
        return idFauna;
    }

    public InformacionBasicaDetalleEntity setIdFauna(Integer idFauna) {
        this.idFauna = idFauna;
        return this;
    }

    public Integer getIdInfraestructura() {
        return idInfraestructura;
    }

    public InformacionBasicaDetalleEntity setIdInfraestructura(Integer idInfraestructura) {
        this.idInfraestructura = idInfraestructura;
        return this;
    }

    public Integer getIdTipoBosque() {
        return idTipoBosque;
    }

    public InformacionBasicaDetalleEntity setIdTipoBosque(Integer idTipoBosque) {
        this.idTipoBosque = idTipoBosque;
        return this;
    }

    public String getCoordenadaUtm() {
        return coordenadaUtm;
    }

    public void setCoordenadaUtm(String coordenadaUtm) {
        this.coordenadaUtm = coordenadaUtm;
    }

    public BigDecimal getCoordenadaEsteIni() {
        return coordenadaEsteIni;
    }

    public void setCoordenadaEsteIni(BigDecimal coordenadaEsteIni) {
        this.coordenadaEsteIni = coordenadaEsteIni;
    }

    public BigDecimal getCoordenadaNorteIni() {
        return coordenadaNorteIni;
    }

    public void setCoordenadaNorteIni(BigDecimal coordenadaNorteIni) {
        this.coordenadaNorteIni = coordenadaNorteIni;
    }

    public BigDecimal getCoordenadaEsteFin() {
        return coordenadaEsteFin;
    }

    public void setCoordenadaEsteFin(BigDecimal coordenadaEsteFin) {
        this.coordenadaEsteFin = coordenadaEsteFin;
    }

    public BigDecimal getCoordenadaNorteFin() {
        return coordenadaNorteFin;
    }

    public void setCoordenadaNorteFin(BigDecimal coordenadaNorteFin) {
        this.coordenadaNorteFin = coordenadaNorteFin;
    }

    public BigDecimal getAreaHa() {
        return areaHa;
    }

    public void setAreaHa(BigDecimal areaHa) {
        this.areaHa = areaHa;
    }

    public BigDecimal getAreaHaPorcentaje() {
        return areaHaPorcentaje;
    }

    public void setAreaHaPorcentaje(BigDecimal areaHaPorcentaje) {
        this.areaHaPorcentaje = areaHaPorcentaje;
    }

    public BigDecimal getDistanciaKm() {
        return distanciaKm;
    }

    public void setDistanciaKm(BigDecimal distanciaKm) {
        this.distanciaKm = distanciaKm;
    }

    public BigDecimal getTiempo() {
        return tiempo;
    }

    public void setTiempo(BigDecimal tiempo) {
        this.tiempo = tiempo;
    }

    public String getNombreRio() {
        return nombreRio;
    }

    public void setNombreRio(String nombreRio) {
        this.nombreRio = nombreRio;
    }

    public String getNombreLaguna() {
        return nombreLaguna;
    }

    public void setNombreLaguna(String nombreLaguna) {
        this.nombreLaguna = nombreLaguna;
    }

    public String getNombreQuebrada() {
        return nombreQuebrada;
    }

    public void setNombreQuebrada(String nombreQuebrada) {
        this.nombreQuebrada = nombreQuebrada;
    }
}


