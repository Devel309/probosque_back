package pe.gob.serfor.mcsniffs.entity;

import java.util.Date;
import java.util.List;

public class CensoForestalMaderablesEntity {
    private Date fechaInicio;
    private Date fechaFin;
    private List<CensoForestalDetalleEntity> listCensoForestalDetalleEntity;


    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public List<CensoForestalDetalleEntity> getListCensoForestalDetalleEntity() {
        return listCensoForestalDetalleEntity;
    }

    public void setListCensoForestalDetalleEntity(List<CensoForestalDetalleEntity> listCensoForestalDetalleEntity) {
        this.listCensoForestalDetalleEntity = listCensoForestalDetalleEntity;
    }
}
