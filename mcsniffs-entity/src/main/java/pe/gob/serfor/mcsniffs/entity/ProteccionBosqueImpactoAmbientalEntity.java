package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ProteccionBosqueImpactoAmbientalEntity extends AuditoriaEntity implements Serializable {
    private Integer idProBosqueImpacAmb;
    private PlanManejoEntity planManejo;
    private String factorAmbiental;
    private String impacto;
    private Boolean censo;
    private Boolean demarcacionLineal;
    private Boolean construccionCampamento;
    private Boolean construccionCamino;
    private Boolean tala;
    private Boolean arrastre;
    private Boolean otra;
    private String descripccionOtra;

    public Integer getIdProBosqueImpacAmb() {
        return idProBosqueImpacAmb;
    }

    public void setIdProBosqueImpacAmb(Integer idProBosqueImpacAmb) {
        this.idProBosqueImpacAmb = idProBosqueImpacAmb;
    }

    public PlanManejoEntity getPlanManejo() {
        return planManejo;
    }

    public void setPlanManejo(PlanManejoEntity planManejo) {
        this.planManejo = planManejo;
    }

    public String getFactorAmbiental() {
        return factorAmbiental;
    }

    public void setFactorAmbiental(String factorAmbiental) {
        this.factorAmbiental = factorAmbiental;
    }

    public String getImpacto() {
        return impacto;
    }

    public void setImpacto(String impacto) {
        this.impacto = impacto;
    }

    public Boolean getCenso() {
        return censo;
    }

    public void setCenso(Boolean censo) {
        this.censo = censo;
    }

    public Boolean getDemarcacionLineal() {
        return demarcacionLineal;
    }

    public void setDemarcacionLineal(Boolean demarcacionLineal) {
        this.demarcacionLineal = demarcacionLineal;
    }

    public Boolean getConstruccionCampamento() {
        return construccionCampamento;
    }

    public void setConstruccionCampamento(Boolean construccionCampamento) {
        this.construccionCampamento = construccionCampamento;
    }

    public Boolean getConstruccionCamino() {
        return construccionCamino;
    }

    public void setConstruccionCamino(Boolean construccionCamino) {
        this.construccionCamino = construccionCamino;
    }

    public Boolean getTala() {
        return tala;
    }

    public void setTala(Boolean tala) {
        this.tala = tala;
    }

    public Boolean getArrastre() {
        return arrastre;
    }

    public void setArrastre(Boolean arrastre) {
        this.arrastre = arrastre;
    }

    public Boolean getOtra() {
        return otra;
    }

    public void setOtra(Boolean otra) {
        this.otra = otra;
    }

    public String getDescripccionOtra() {
        return descripccionOtra;
    }

    public void setDescripccionOtra(String descripccionOtra) {
        this.descripccionOtra = descripccionOtra;
    }
}
