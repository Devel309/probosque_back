package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ImpactoAmbientalDetalleEntity extends AuditoriaEntity implements Serializable {
    private Integer idImpactoambientaldetalle;
    private Integer idImpactoambiental;
    private String medidasprevencion;
    private String codigoImpactoAmbientalDet;
    private String medidasProteccion;
    private String medidasMitigacion;
    private String descripcion;
    private String actividad;

    public Integer getIdImpactoambientaldetalle() {
        return idImpactoambientaldetalle;
    }

    public void setIdImpactoambientaldetalle(Integer idImpactoambientaldetalle) {
        this.idImpactoambientaldetalle = idImpactoambientaldetalle;
    }

    public Integer getIdImpactoambiental() {
        return idImpactoambiental;
    }

    public void setIdImpactoambiental(Integer idImpactoambiental) {
        this.idImpactoambiental = idImpactoambiental;
    }

    public String getMedidasprevencion() {
        return medidasprevencion;
    }

    public void setMedidasprevencion(String medidasprevencion) {
        this.medidasprevencion = medidasprevencion;
    }

    public String getCodigoImpactoAmbientalDet() {
        return codigoImpactoAmbientalDet;
    }

    public void setCodigoImpactoAmbientalDet(String codigoImpactoAmbientalDet) {
        this.codigoImpactoAmbientalDet = codigoImpactoAmbientalDet;
    }

    public String getMedidasProteccion() {
        return medidasProteccion;
    }

    public void setMedidasProteccion(String medidasProteccion) {
        this.medidasProteccion = medidasProteccion;
    }

    public String getMedidasMitigacion() {
        return medidasMitigacion;
    }

    public void setMedidasMitigacion(String medidasMitigacion) {
        this.medidasMitigacion = medidasMitigacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }
}
