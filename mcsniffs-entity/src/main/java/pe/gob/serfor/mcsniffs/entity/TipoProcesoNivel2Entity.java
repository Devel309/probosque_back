package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.List;

public class TipoProcesoNivel2Entity implements Serializable {
    private Integer IdTipoProceso;
    private String Descripcion;
    private List<TipoProcesoEntity> nivel3;
    private Integer Nivel;
    private String Codigo;
    private String CodigoPadre;

    public Integer getNivel() {
        return Nivel;
    }

    public void setNivel(Integer nivel) {
        Nivel = nivel;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getCodigoPadre() {
        return CodigoPadre;
    }

    public void setCodigoPadre(String codigoPadre) {
        CodigoPadre = codigoPadre;
    }

    public Integer getIdTipoProceso() {
        return IdTipoProceso;
    }

    public void setIdTipoProceso(Integer idTipoProceso) {
        IdTipoProceso = idTipoProceso;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public List<TipoProcesoEntity> getNivel3() {
        return nivel3;
    }

    public void setNivel3(List<TipoProcesoEntity> nivel3) {
        this.nivel3 = nivel3;
    }
}
