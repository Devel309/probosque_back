package pe.gob.serfor.mcsniffs.entity;
import java.io.Serializable;
import java.util.List;


public class CronogramaActividadDetalleEntity extends AuditoriaEntity implements Serializable {
    
    private Integer idPlanManejo;
    private Integer idCronActividad;
    private String actividad;
    List<Boolean> Meses ;
    List<Boolean> Anios ;

    //agregado por Jaqueline
    private Integer anio;
    private Integer mes;
    private Integer idCronogramaActividadDetalle;
    private String descripcionDetalle;
    private Integer marcado;
    private String codigoTipo;
    


    public String getCodigoTipo() {
        return codigoTipo;
    }
    public void setCodigoTipo(String codigoTipo) {
        this.codigoTipo = codigoTipo;
    }
    public Integer isMarcado() {
        return marcado;
    }
    public void setMarcado(Integer marcado) {
        this.marcado = marcado;
    }
    public Integer getAnio() {
        return anio;
    }
    public void setAnio(Integer anio) {
        this.anio = anio;
    }
    public Integer getMes() {
        return mes;
    }
    public void setMes(Integer mes) {
        this.mes = mes;
    }
    public Integer getIdCronogramaActividadDetalle() {
        return idCronogramaActividadDetalle;
    }
    public void setIdCronogramaActividadDetalle(Integer idCronogramaActividadDetalle) {
        this.idCronogramaActividadDetalle = idCronogramaActividadDetalle;
    }
    public String getDescripcionDetalle() {
        return descripcionDetalle;
    }
    public void setDescripcionDetalle(String descripcionDetalle) {
        this.descripcionDetalle = descripcionDetalle;
    }
    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }
    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }
    public Integer getIdCronActividad() {
        return idCronActividad;
    }
    public void setIdCronActividad(Integer idCronActividad) {
        this.idCronActividad = idCronActividad;
    }
    public String getActividad() {
        return actividad;
    }
    public void setActividad(String actividad) {
        this.actividad = actividad;
    }
    public List<Boolean> getMeses() {
        return Meses;
    }
    public void setMeses(List<Boolean> meses) {
        Meses = meses;
    }
    public List<Boolean> getAnios() {
        return Anios;
    }
    public void setAnios(List<Boolean> anios) {
        Anios = anios;
    }
}