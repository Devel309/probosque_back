package pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
public class AreaBloqueDetalleEntity extends AuditoriaEntity implements Serializable {
    
    private Integer idAreaBloqueDetalle;

    private Integer nroVertice;

    private BigDecimal coordenadaEste; 

    private BigDecimal coordenadaNorte; 

    private String observacion;
    

}
