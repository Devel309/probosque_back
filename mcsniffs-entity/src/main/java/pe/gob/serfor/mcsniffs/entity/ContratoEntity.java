package pe.gob.serfor.mcsniffs.entity;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.ContratoArchivoDto;
import pe.gob.serfor.mcsniffs.entity.Dto.Contrato.ContratoPersonaDto;

import javax.persistence.ParameterMode;

@Getter
@Setter
public class ContratoEntity extends AuditoriaEntity{
    private Integer idContrato;
    private String codigoTituloH;
    private String descripcion;
    private String dniRucRepresentanteLegal;
    private String nombreRaSoRepresentanteLegal;

    private String nroContratoConsecion;

    private Boolean datosValidados;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Timestamp fechaInicioContrato;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Timestamp fechaFinContrato;
    private String codigoTipoContrato;
    private String codigoEstadoContrato;    
    private Integer idStatusContrato;
    private String tipoDocumentoTitular;
    private String titular;
    private Integer archivoLegal;
    private Integer archivoGarantia;
    private Integer archivoBienes;
    private Integer archivoContrato;
    private String numeroExpediente;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Timestamp fechaPresentacion;

    private List<ContratoPersonaDto> listaContratoPersona;
    private List<ContratoArchivoDto> listaContratoArchivo;

    private Integer idUsuarioTitularTH;
    private Integer idGeometria;
    private Boolean contratoRemitido;

    private String tipoDocumentoGestion;
    private Integer documentoGestion;
    private String tipoDocumentoGestionDescripcion;

    private Integer idUsuarioPostulacion;
    private String perfil;

    private Boolean firmado;




    private Integer idProcesoOferta;
    private Integer idProcesoPostulacion;
    private Integer idPlanManejo;
    private Integer idPlanManejoContrato;
    private String nombreUsuarioPostulante;
    private Integer idStatusProcesoPostulacion;
    private String nombreStatusPP;
    private Date fechaPostulacion;
    private Integer notaTotal;
    private Boolean ganador;
    private Integer idResultadoPP;
    private String dniRepresentanteLegal;
    private String nombreRepresentanteLegal;
    private String codigoPartidaRegistral;
    private String nombresApellidosRegente;
    private String numeroDocumentoRegente;
    private String superficieUA;
    private String objectIdUa;
    private String statusContrato;
    private Integer archivoPlContrato;
    private String descripcionEstadoContrato;
    private String tituloHabilitante;
    private Double areaTotal;
    private String descripcionUnidadAprovechamiento;
    private String descripcionRegion;
    private String descripcionContrato;
    private Boolean contratoPrincipal;
    private Boolean contratoRemitir;
    private String idDepartamento;
    private String Departamento;
    private String idProvincia;
    private String provincia;
    private String idDistrito;
    private String distrito;
    private String codigoTipoDocumentoTitular;
    private String codigoTipoPersonaTitular;
    private String numeroDocumentoTitular;
    private String nombreCompletoTitular;
    private String razonSocialTitular;
    private String descripcionTipoPersonaTitular;
    private String descripcionTipoDocIdentidadTitular;

    private String codigoTipoDocIdentidadRepLegal;
    private String descripcionTipoDocIdentidadRepLegal;
    private String numeroDocumentoRepLegal;
    private String apePaternoRepLegal;
    private String apeMaternoRepLegal;
    private String correoRepLegal;
    private Integer idDepartamentoContrato;
    private Integer idProvinciaContrato;
    private Integer idDistritoContrato;

    private String diaLetra;
    private String MesLetra;
    private String AnioLetra;
}

