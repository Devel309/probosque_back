package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
import java.util.List;
public class OrganizacionManejoEntity extends AuditoriaEntity implements Serializable {
    private Integer idPGMF; 
    
    private Integer id; 
    private String tipo;
    private String subTipo;
    private String nombreSubTipo;
    private String funcion;
    private String descripcion;
    private Integer numero;
    private String maquinaEquipo;
    private String tipoActividad;
    private Boolean esCabecera;
    private List<OrganizacionManejoEntity> listaDetalle;

    
    public List<OrganizacionManejoEntity> getListaDetalle() {
        return listaDetalle;
    }
    public void setListaDetalle(List<OrganizacionManejoEntity> listaDetalle) {
        this.listaDetalle = listaDetalle;
    }
    public Integer getIdPGMF() {
        return idPGMF;
    }
    public void setIdPGMF(Integer idPGMF) {
        this.idPGMF = idPGMF;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getTipo() {
        return tipo;
    }
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    public String getSubTipo() {
        return subTipo;
    }
    public void setSubTipo(String subTipo) {
        this.subTipo = subTipo;
    }
    public String getNombreSubTipo() {
        return nombreSubTipo;
    }
    public void setNombreSubTipo(String nombreSubTipo) {
        this.nombreSubTipo = nombreSubTipo;
    }
    public String getFuncion() {
        return funcion;
    }
    public void setFuncion(String funcion) {
        this.funcion = funcion;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getMaquinaEquipo() {
        return maquinaEquipo;
    }
    public void setMaquinaEquipo(String maquinaEquipo) {
        this.maquinaEquipo = maquinaEquipo;
    }
    public String getTipoActividad() {
        return tipoActividad;
    }
    public void setTipoActividad(String tipoActividad) {
        this.tipoActividad = tipoActividad;
    }
    public Boolean getEsCabecera() {
        return esCabecera;
    }
    public void setEsCabecera(Boolean esCabecera) {
        this.esCabecera = esCabecera;
    }
    
    
}
