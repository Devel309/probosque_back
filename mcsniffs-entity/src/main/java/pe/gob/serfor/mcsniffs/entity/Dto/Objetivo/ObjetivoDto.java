package pe.gob.serfor.mcsniffs.entity.Dto.Objetivo;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;



import java.io.Serializable;

public class ObjetivoDto extends AuditoriaEntity implements Serializable {
    private Integer idObjManejo;
    private Integer idPlanManejo;
    private String codigoObjetivo;
    private String descripcion;
    private Integer idObjetivoDet;
    private String  descripcionDetalle;
    private String detalle;
    private String activo;
    private String observacionCabera;
    private String detalleCabera;
    private String descripcionCabera;
    

    public String getObservacionCabera() {
        return observacionCabera;
    }

    public void setObservacionCabera(String observacionCabera) {
        this.observacionCabera = observacionCabera;
    }

    public String getDetalleCabera() {
        return detalleCabera;
    }

    public void setDetalleCabera(String detalleCabera) {
        this.detalleCabera = detalleCabera;
    }

    public String getDescripcionCabera() {
        return descripcionCabera;
    }

    public void setDescripcionCabera(String descripcionCabera) {
        this.descripcionCabera = descripcionCabera;
    }

    public Integer getIdObjManejo() {
        return idObjManejo;
    }

    public ObjetivoDto setIdObjManejo(Integer idObjManejo) {
        this.idObjManejo = idObjManejo;
        return this;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public ObjetivoDto setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
        return this;
    }

    public String getCodigoObjetivo() {
        return codigoObjetivo;
    }

    public ObjetivoDto setCodigoObjetivo(String codigoObjetivo) {
        this.codigoObjetivo = codigoObjetivo;
        return this;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public ObjetivoDto setDescripcion(String descripcion) {
        this.descripcion = descripcion;
        return this;
    }

    public Integer getIdObjetivoDet() {
        return idObjetivoDet;
    }

    public ObjetivoDto setIdObjetivoDet(Integer idObjetivoDet) {
        this.idObjetivoDet = idObjetivoDet;
        return this;
    }

    public String getDescripcionDetalle() {
        return descripcionDetalle;
    }

    public ObjetivoDto setDescripcionDetalle(String descripcionDetalle) {
        this.descripcionDetalle = descripcionDetalle;
        return this;
    }

    public String getDetalle() {
        return detalle;
    }

    public ObjetivoDto setDetalle(String detalle) {
        this.detalle = detalle;
        return this;
    }

    public String getActivo() {
        return activo;
    }

    public ObjetivoDto setActivo(String activo) {
        this.activo = activo;
        return this;
    }
}
