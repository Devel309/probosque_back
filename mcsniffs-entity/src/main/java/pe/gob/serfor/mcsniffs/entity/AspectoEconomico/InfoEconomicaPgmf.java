package pe.gob.serfor.mcsniffs.entity.AspectoEconomico;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
public class InfoEconomicaPgmf extends AuditoriaEntity{
Integer idInformacionSocioEconomica;
Integer idPlanManejo ;
Integer personasEmpadronada ;
Integer peronasTrabajan ;
}
