package pe.gob.serfor.mcsniffs.entity.CoreCentral;

import java.io.Serializable;
import java.util.Date;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

public class ProvinciaEntity extends AuditoriaEntity implements Serializable {
    private Integer idProvincia;
    private String nombreProvincia;
    private String codProvincia;
    private String codDepartamento;
    private String codProvinciaInei;
    private String codProvinciaReniec;
    private String codProvinciaSunat;
    private Integer idDepartamento;

    public Integer getIdProvincia() {
        return idProvincia;
    }

    public void setIdProvincia(Integer idProvincia) {
        this.idProvincia = idProvincia;
    }

    public String getNombreProvincia() {
        return nombreProvincia;
    }

    public void setNombreProvincia(String nombreProvincia) {
        this.nombreProvincia = nombreProvincia;
    }

    public String getCodProvincia() {
        return codProvincia;
    }

    public void setCodProvincia(String codProvincia) {
        this.codProvincia = codProvincia;
    }

    public String getCodDepartamento() {
        return codDepartamento;
    }

    public void setCodDepartamento(String codDepartamento) {
        this.codDepartamento = codDepartamento;
    }

    public String getCodProvinciaInei() {
        return codProvinciaInei;
    }

    public void setCodProvinciaInei(String codProvinciaInei) {
        this.codProvinciaInei = codProvinciaInei;
    }

    public String getCodProvinciaReniec() {
        return codProvinciaReniec;
    }

    public void setCodProvinciaReniec(String codProvinciaReniec) {
        this.codProvinciaReniec = codProvinciaReniec;
    }

    public String getCodProvinciaSunat() {
        return codProvinciaSunat;
    }

    public void setCodProvinciaSunat(String codProvinciaSunat) {
        this.codProvinciaSunat = codProvinciaSunat;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }
}
