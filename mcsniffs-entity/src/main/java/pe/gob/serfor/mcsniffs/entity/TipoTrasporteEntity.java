package pe.gob.serfor.mcsniffs.entity;

public class TipoTrasporteEntity extends AuditoriaEntity {
     short IdTipoTrasporte;
     String Descripcion;

    public TipoTrasporteEntity(){}

    public short getIdTipoTrasporte() {
        return IdTipoTrasporte;
    }

    public void setIdTipoTrasporte(short idTipoTrasporte) {
        IdTipoTrasporte = idTipoTrasporte;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }
}
