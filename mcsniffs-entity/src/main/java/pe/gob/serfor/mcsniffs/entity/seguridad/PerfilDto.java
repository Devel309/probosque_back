package pe.gob.serfor.mcsniffs.entity.seguridad;

public class PerfilDto {
    Integer idPerfil ;
    String CodigoPerfil ;
    String Descripcion ;
    String CodigoAplicacion;
    
    public Integer getIdPerfil() {
        return idPerfil;
    }
    public void setIdPerfil(Integer idPerfil) {
        this.idPerfil = idPerfil;
    }
    public String getCodigoPerfil() {
        return CodigoPerfil;
    }
    public void setCodigoPerfil(String codigoPerfil) {
        CodigoPerfil = codigoPerfil;
    }
    public String getDescripcion() {
        return Descripcion;
    }
    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }
    public String getCodigoAplicacion() {
        return CodigoAplicacion;
    }
    public void setCodigoAplicacion(String codigoAplicacion) {
        CodigoAplicacion = codigoAplicacion;
    }
    
    
}
