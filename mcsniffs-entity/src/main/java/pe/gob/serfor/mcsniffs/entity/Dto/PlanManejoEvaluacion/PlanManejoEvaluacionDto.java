package pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
@Data
public class PlanManejoEvaluacionDto extends AuditoriaEntity implements Serializable {

    private Integer idPlanManejoEval;
    private Integer idPlanManejoEvaluacion;
    private Integer idEvaluacionCampo;
    private Integer idPlanManejo;
    private Integer idTitularRegente;
    private String TitularRegente;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaPresentacion;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaEstadoEvaluacion;
    private String descEstadoPlanManejoEvaluacion;
    private Integer TotalRegistro;
    private String perfil;
    private String estadoPlanManejoEvaluacion;
    private String estadoPlanManejo;
    private Boolean conformeReqTupa;
    private Boolean conformeConsideraciones;
    private Boolean observacionRestrictiva;
    private Boolean aprobado;
    private Integer maximoObs;
    private String  descEstadoPlanManejo;
    private Integer idMincul;
    private Integer idAna;
    private Integer idSernap;
    private Boolean notificacion;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaNotificacion;
    private String codigoTituloHabilitante;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaAprobacion;
    private Integer idResolucion;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaInicioVigencia;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaFinVigencia;
    private Boolean evaluado;
    private String estadoEvaluacion;

}
