package pe.gob.serfor.mcsniffs.entity.Parametro;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.EvaluacionAmbientalActividadEntity;
import pe.gob.serfor.mcsniffs.entity.EvaluacionAmbientalAprovechamientoEntity;

import java.io.Serializable;
import java.util.List;

public class EvaluacionAmbientalDto  extends AuditoriaEntity implements Serializable {
    private Integer idEvaluacionAmbiental;
    private String valorEvaluacion;
    private String descripcion;
    private String medidasMitigacion;
    private String adjunto;
    private Integer idPlanManejo;
    private Integer idEvalActividad;
    private String codTipoActividad;
    private String tipoActividad;
    private String tipoNombreActividad;
    private String nombreActividad;
    private String medidasControl;
    private String medidasMonitoreo;
    private String frecuencia;
    private String acciones;
    private String responsable;
    private Integer idEvalAprovechamiento;
    private String codTipoAprovechamiento;
    private String tipoAprovechamiento;
    private String tipoNombreAprovechamiento;
    private String nombreAprovechamiento;

    private String codTipoEvaluacionAmbiental;
    private String subCodTipoEvaluacionAmbiental;
    private String codTipoEvaluacionAmbientalDet;
    private String subCodTipoActividad;
    private String subCodTipoAprovechamiento;

    public String getCodTipoEvaluacionAmbientalDet() {
        return codTipoEvaluacionAmbientalDet;
    }

    public void setCodTipoEvaluacionAmbientalDet(String codTipoEvaluacionAmbientalDet) {
        this.codTipoEvaluacionAmbientalDet = codTipoEvaluacionAmbientalDet;
    }

    public String getCodTipoEvaluacionAmbiental() {
        return codTipoEvaluacionAmbiental;
    }

    public void setCodTipoEvaluacionAmbiental(String codTipoEvaluacionAmbiental) {
        this.codTipoEvaluacionAmbiental = codTipoEvaluacionAmbiental;
    }

    public String getSubCodTipoEvaluacionAmbiental() {
        return subCodTipoEvaluacionAmbiental;
    }

    public void setSubCodTipoEvaluacionAmbiental(String subCodTipoEvaluacionAmbiental) {
        this.subCodTipoEvaluacionAmbiental = subCodTipoEvaluacionAmbiental;
    }

    public String getSubCodTipoActividad() {
        return subCodTipoActividad;
    }

    public void setSubCodTipoActividad(String subCodTipoActividad) {
        this.subCodTipoActividad = subCodTipoActividad;
    }

    public String getSubCodTipoAprovechamiento() {
        return subCodTipoAprovechamiento;
    }

    public void setSubCodTipoAprovechamiento(String subCodTipoAprovechamiento) {
        this.subCodTipoAprovechamiento = subCodTipoAprovechamiento;
    }

    public Integer getIdEvaluacionAmbiental() {
        return idEvaluacionAmbiental;
    }

    public void setIdEvaluacionAmbiental(Integer idEvaluacionAmbiental) {
        this.idEvaluacionAmbiental = idEvaluacionAmbiental;
    }

    public String getValorEvaluacion() {
        return valorEvaluacion;
    }

    public void setValorEvaluacion(String valorEvaluacion) {
        this.valorEvaluacion = valorEvaluacion;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public Integer getIdEvalActividad() {
        return idEvalActividad;
    }

    public void setIdEvalActividad(Integer idEvalActividad) {
        this.idEvalActividad = idEvalActividad;
    }

    public String getCodTipoActividad() {
        return codTipoActividad;
    }

    public void setCodTipoActividad(String codTipoActividad) {
        this.codTipoActividad = codTipoActividad;
    }

    public String getTipoActividad() {
        return tipoActividad;
    }

    public void setTipoActividad(String tipoActividad) {
        this.tipoActividad = tipoActividad;
    }

    public String getTipoNombreActividad() {
        return tipoNombreActividad;
    }

    public void setTipoNombreActividad(String tipoNombreActividad) {
        this.tipoNombreActividad = tipoNombreActividad;
    }

    public String getNombreActividad() {
        return nombreActividad;
    }

    public void setNombreActividad(String nombreActividad) {
        this.nombreActividad = nombreActividad;
    }

    public String getMedidasControl() {
        return medidasControl;
    }

    public void setMedidasControl(String medidasControl) {
        this.medidasControl = medidasControl;
    }

    public String getMedidasMonitoreo() {
        return medidasMonitoreo;
    }

    public void setMedidasMonitoreo(String medidasMonitoreo) {
        this.medidasMonitoreo = medidasMonitoreo;
    }

    public String getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(String frecuencia) {
        this.frecuencia = frecuencia;
    }

    public String getAcciones() {
        return acciones;
    }

    public void setAcciones(String acciones) {
        this.acciones = acciones;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public Integer getIdEvalAprovechamiento() {
        return idEvalAprovechamiento;
    }

    public void setIdEvalAprovechamiento(Integer idEvalAprovechamiento) {
        this.idEvalAprovechamiento = idEvalAprovechamiento;
    }

    public String getCodTipoAprovechamiento() {
        return codTipoAprovechamiento;
    }

    public void setCodTipoAprovechamiento(String codTipoAprovechamiento) {
        this.codTipoAprovechamiento = codTipoAprovechamiento;
    }

    public String getTipoAprovechamiento() {
        return tipoAprovechamiento;
    }

    public void setTipoAprovechamiento(String tipoAprovechamiento) {
        this.tipoAprovechamiento = tipoAprovechamiento;
    }

    public String getTipoNombreAprovechamiento() {
        return tipoNombreAprovechamiento;
    }

    public void setTipoNombreAprovechamiento(String tipoNombreAprovechamiento) {
        this.tipoNombreAprovechamiento = tipoNombreAprovechamiento;
    }

    public String getNombreAprovechamiento() {
        return nombreAprovechamiento;
    }

    public void setNombreAprovechamiento(String nombreAprovechamiento) {
        this.nombreAprovechamiento = nombreAprovechamiento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMedidasMitigacion() {
        return medidasMitigacion;
    }

    public void setMedidasMitigacion(String medidasMitigacion) {
        this.medidasMitigacion = medidasMitigacion;
    }

    public String getAdjunto() {
        return adjunto;
    }

    public void setAdjunto(String adjunto) {
        this.adjunto = adjunto;
    }
}
