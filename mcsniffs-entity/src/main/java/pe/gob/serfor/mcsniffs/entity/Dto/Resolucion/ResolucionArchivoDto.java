package pe.gob.serfor.mcsniffs.entity.Dto.Resolucion;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResolucionArchivoDto extends AuditoriaEntity implements Serializable {


    private Integer idResolucionArchivo;
    private Integer idResolucion;
    private Integer idArchivo;
    private String tipoDocumento;
    private String nombre;
}
