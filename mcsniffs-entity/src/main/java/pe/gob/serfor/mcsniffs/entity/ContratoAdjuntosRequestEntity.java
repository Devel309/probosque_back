package pe.gob.serfor.mcsniffs.entity;

public class ContratoAdjuntosRequestEntity {
    private Integer IdContrato;
    private Integer IdDocumentoAdjunto;
    private Integer IdUsuarioRegistro;
    
    public Integer getIdContrato() {
        return IdContrato;
    }
    public void setIdContrato(Integer idContrato) {
        IdContrato = idContrato;
    }
    public Integer getIdDocumentoAdjunto() {
        return IdDocumentoAdjunto;
    }
    public void setIdDocumentoAdjunto(Integer idDocumentoAdjunto) {
        IdDocumentoAdjunto = idDocumentoAdjunto;
    }
    public Integer getIdUsuarioRegistro() {
        return IdUsuarioRegistro;
    }
    public void setIdUsuarioRegistro(Integer idUsuarioRegistro) {
        IdUsuarioRegistro = idUsuarioRegistro;
    }
    
}
