package pe.gob.serfor.mcsniffs.entity.Dto.EstatusProceso;

import lombok.Data;

@Data
public class ListarEstadoDto {

    private Integer idStatusProceso;
    private String descripcion;
    private String tipoStatus;

}
