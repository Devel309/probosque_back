package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

@Data
public class PaginacionEntity {
    String search;
    Integer pageNum;
    Integer pageSize;
    Integer totalRecord;

    Integer totalPage;
    Integer startIndex;
    String codigoPerfil;


}
