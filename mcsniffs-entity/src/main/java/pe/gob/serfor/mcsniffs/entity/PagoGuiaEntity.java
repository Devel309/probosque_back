package pe.gob.serfor.mcsniffs.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class PagoGuiaEntity extends AuditoriaEntity implements Serializable {
    private Integer idPagoGuia;
    private String codPagoGuia;
    private Integer nrDocumentoGestion;
    private String tipoDocumentoGestion;
    private String observacion;
    private String detalle;
    private String descripcion;
    private Integer idPago;
    private List<PagoGuiaEspecieEntity> listaGuiaEspecie;
}
