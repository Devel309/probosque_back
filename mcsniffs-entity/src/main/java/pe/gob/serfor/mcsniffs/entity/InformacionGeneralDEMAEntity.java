package pe.gob.serfor.mcsniffs.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

@Data
public class InformacionGeneralDEMAEntity extends AuditoriaEntity{
    //Generales
    private Integer idPlanManejo;
    private Integer idPersonaComunidad;
    private String codigoProceso;
    private Integer idInformacionGeneralDema;
    //Comunidad
    private String nombreComunidad;
    private String nombresJefeComunidad;
    private String ApellidoMaternoJefeComunidad;
    private String ApellidoPaternoJefeComunidad;
    private String dniJefecomunidad;
    private String federacionComunidad;
    private Integer idDepartamento;
    private String departamento;
    private Integer idProvincia;
    private String provincia;
    private Integer idDistrito;
    private String distrito;
    private String Cuenca;
    private String SubCuenca;
    private Integer nroAnexosComunidad;
    private String nroResolucionComunidad;
    private String nroTituloPropiedadComunidad;
    private Integer nroTotalFamiliasComunidad;
    private String nroRucComunidad;
    //dema
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaInicioDema;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaFinDema;
    //proceso PMFI
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaElaboracionPmfi;
    //
    private String nombreElaboraDema;
    private String apellidoPaternoElaboraDema;
    private String apellidoMaternoElaboraDema;
    private String dniElaboraDema;
    private Integer nroPersonasInvolucradas;
    //Aprovechamiento maderable
    private Integer nroArbolesMaderables;
    private Integer nroArbolesMaderablesSemilleros;
    private String superficieHaMaderables;
    private String volumenM3rMaderables;
    //aprovechamiento no maderable
    private Boolean aprovechamientoNoMaderable;
    private Integer nroArbolesNoMaderables;
    private Integer nroArbolesNoMaderablesSemilleros;
    private String superficieHaNoMaderables;
    private String volumenM3rNoMaderables;
    //utilidades
    private BigDecimal totalIngresosEstimado;
    private BigDecimal totalCostoEstimado;
    private BigDecimal totalUtilidadesEstimado;
    //regimenes promocionales
    private List<InformacionGeneralDetalle> lstUmf;
    //regente JaquelineDB
    private Boolean conRegente;
    private RegenteEntity regente;
    //para informacion general pfmi
    private String direccionLegalTitular;
    private Integer idDistritoTitular;
    private String direccionLegalRepresentante;
    private Integer idDistritoRepresentante;
    private Integer idContrato;
    private Integer vigencia;
    private Integer vigenciaFinal;
    private String representanteLegal;
    private String celularTitular;
    private String correoTitular;
    //ubigeo
    private String departamentoRepresentante;
    private String provinciaRepresentante;
    private String distritoRepresentante;
    //para PGMFA
    private BigDecimal areaTotal;

    //Para POAC
    private String detalle;
    private String observacion;
    private String descripcion;
    //Para PFCR
    private String codTipoDocumento;
    private String tipoDocumento;
    private String codTipoPersona;
    private String tipoPersona;
    private String codTipoActor;
    private String tipoActor;
    private String codTipoCncc;
    private String tipoCncc;
    private String esReprLegal;
    private String correoEmpresa;
    private Integer cantidadProceso;
    //POCC
    private String codTipoDocumentoElaborador;
    private String tipoDocumentoElaborador;
    private String codTipoDocumentoRepresentante;
    private String tipoDocumentoRepresentante;
    private String documentoRepresentante;
    private String nombreRepresentante;
    private String apellidoPaternoRepresentante;
    private String apellidoMaternoRepresentante;
    //PMFIC
    private Integer idPermisoForestal;
    private BigDecimal areaBosqueProduccionForestal;
    //
    private String zona;
    private String datum;
    private String horizontal;
    private String nombreTituloHabilitante;
    
    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public String getHorizontal() {
        return horizontal;
    }

    public void setHorizontal(String horizontal) {
        this.horizontal = horizontal;
    }

    private Integer idProvinciaTitular;
    private Integer idDepartamentoTitular;

    public String getNombreTituloHabilitante() {
        return nombreTituloHabilitante;
    }

    public void setNombreTituloHabilitante(String nombreTituloHabilitante) {
        this.nombreTituloHabilitante = nombreTituloHabilitante;
    }

}
