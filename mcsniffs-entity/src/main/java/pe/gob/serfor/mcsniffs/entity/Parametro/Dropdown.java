package pe.gob.serfor.mcsniffs.entity.Parametro;

import java.io.Serializable;
import java.util.List;

public class Dropdown  implements Serializable {
    private String label;
    private String value;
    private Integer valor;
    private Integer IdPadre;
    private Boolean activo;
    private List<item> items;


    public static  class item implements Serializable{
        private String label;
        private String value;
        private Integer valor;
        private Boolean activo;
        public String getLabel() {
            return label;
        }
    
        public void setLabel(String label) {
            this.label = label;
        }
    
        public String getValue() {
            return value;
        }
    
        public void setValue(String value) {
            this.value = value;
        }
        public Integer getValor() {
            return valor;
        }
    
        public void setValor(Integer valor) {
            this.valor = valor;
        }

        public Boolean getActivo() {
            return activo;
        }

        public void setActivo(Boolean activo) {
            this.activo = activo;
        }
    }
    public Integer getIdPadre() {
        return IdPadre;
    }

    public void setIdPadre(Integer idPadre) {
        IdPadre = idPadre;
    }
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<item> getItems() {
        return items;
    }

    public void setItems(List<item> items) {
        this.items = items;
    }
    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }
    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }
}



 
