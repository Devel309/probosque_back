package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class TipoPermisoEntity implements Serializable {
    private short tipoPermiso;
    private String descripcion;
    public short getTipoPermiso() { return tipoPermiso; }
    public void setTipoPermiso(short tipoPermiso) { this.tipoPermiso = tipoPermiso; }
    public String getDescripcion() { return descripcion; }
    public void setDescripcion(String descripcion) { this.descripcion = descripcion; }


}