package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;
public class MonitoreoAbreviadoEntity extends AuditoriaEntity implements Serializable {
    private Integer idPGMF; 
    
    private Integer id; 
    private String tipo;
    private String subTipo;
    private String nombreSubTipo;
    private String operacion;
    private String descripcion;
    private Boolean check;
    
    public Boolean getCheck() {
        return check;
    }
    public void setCheck(Boolean check) {
        this.check = check;
    }
    public String getSubTipo() {
        return subTipo;
    }
    public void setSubTipo(String subTipo) {
        this.subTipo = subTipo;
    }
    public Integer getIdPGMF() {
        return idPGMF;
    }
    public void setIdPGMF(Integer idPGMF) {
        this.idPGMF = idPGMF;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getTipo() {
        return tipo;
    }
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    public String getNombreSubTipo() {
        return nombreSubTipo;
    }
    public void setNombreSubTipo(String nombreSubTipo) {
        this.nombreSubTipo = nombreSubTipo;
    }
    public String getOperacion() {
        return operacion;
    }
    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    

    
    
}
