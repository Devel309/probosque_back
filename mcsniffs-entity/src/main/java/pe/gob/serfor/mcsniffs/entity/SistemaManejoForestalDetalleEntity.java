package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class SistemaManejoForestalDetalleEntity extends AuditoriaEntity implements Serializable {
	private Integer idSistemaManejoForestalDetalle;
	private Integer idSistemaManejoForestal;
	private String codigoTipoDetalle;
	private String categoriaZona;
	private String usoPotencial;
	private Double usoPotencialNumerico;
	private String actividadRealizar;

	private String actividades;
	private String descripcionSistema;
	private String maquinariasInsumos;
	private String personalRequerido;
	private String observacion;

	private String codigoTipoSilvicultural;
	private String nombreTipoSilvicultural;
	private String descripcionTipoSilvicultural;
	private String codigoProceso;
	private Boolean editable;

	public Integer getIdSistemaManejoForestalDetalle() {
		return idSistemaManejoForestalDetalle;
	}

	public void setIdSistemaManejoForestalDetalle(Integer idSistemaManejoForestalDetalle) {
		this.idSistemaManejoForestalDetalle = idSistemaManejoForestalDetalle;
	}

	public Integer getIdSistemaManejoForestal() {
		return idSistemaManejoForestal;
	}

	public void setIdSistemaManejoForestal(Integer idSistemaManejoForestal) {
		this.idSistemaManejoForestal = idSistemaManejoForestal;
	}

	public String getCodigoTipoDetalle() {
		return codigoTipoDetalle;
	}

	public void setCodigoTipoDetalle(String codigoTipoDetalle) {
		this.codigoTipoDetalle = codigoTipoDetalle;
	}

	public String getCategoriaZona() {
		return categoriaZona;
	}

	public void setCategoriaZona(String categoriaZona) {
		this.categoriaZona = categoriaZona;
	}

	public String getUsoPotencial() {
		return usoPotencial;
	}

	public void setUsoPotencial(String usoPotencial) {
		this.usoPotencial = usoPotencial;
	}

	public String getActividadRealizar() {
		return actividadRealizar;
	}

	public void setActividadRealizar(String actividadRealizar) {
		this.actividadRealizar = actividadRealizar;
	}

	public String getActividades() {
		return actividades;
	}

	public void setActividades(String actividades) {
		this.actividades = actividades;
	}

	public String getDescripcionSistema() {
		return descripcionSistema;
	}

	public void setDescripcionSistema(String descripcionSistema) {
		this.descripcionSistema = descripcionSistema;
	}

	public String getMaquinariasInsumos() {
		return maquinariasInsumos;
	}

	public void setMaquinariasInsumos(String maquinariasInsumos) {
		this.maquinariasInsumos = maquinariasInsumos;
	}

	public String getPersonalRequerido() {
		return personalRequerido;
	}

	public void setPersonalRequerido(String personalRequerido) {
		this.personalRequerido = personalRequerido;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getCodigoTipoSilvicultural() {
		return codigoTipoSilvicultural;
	}

	public void setCodigoTipoSilvicultural(String codigoTipoSilvicultural) {
		this.codigoTipoSilvicultural = codigoTipoSilvicultural;
	}

	public String getNombreTipoSilvicultural() {
		return nombreTipoSilvicultural;
	}

	public void setNombreTipoSilvicultural(String nombreTipoSilvicultural) {
		this.nombreTipoSilvicultural = nombreTipoSilvicultural;
	}

	public String getDescripcionTipoSilvicultural() {
		return descripcionTipoSilvicultural;
	}

	public void setDescripcionTipoSilvicultural(String descripcionTipoSilvicultural) {
		this.descripcionTipoSilvicultural = descripcionTipoSilvicultural;
	}

	public Double getUsoPotencialNumerico() {
		return usoPotencialNumerico;
	}

	public void setUsoPotencialNumerico(Double usoPotencialNumerico) {
		this.usoPotencialNumerico = usoPotencialNumerico;
	}

	public Boolean getEditable() {
		return editable;
	}

	public void setEditable(Boolean editable) {
		this.editable = editable;
	}

	public String getCodigoProceso() {
		return codigoProceso;
	}

	public void setCodigoProceso(String codigoProceso) {
		this.codigoProceso = codigoProceso;
	}

}
