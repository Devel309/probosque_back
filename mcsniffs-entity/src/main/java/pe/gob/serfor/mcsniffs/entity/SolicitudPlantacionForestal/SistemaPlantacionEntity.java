package pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal;

import java.util.Date;

public class SistemaPlantacionEntity {
    Integer   id_sistemaplantacion ;
    String	descripcion ;
    String	estado ;
    Integer	id_usuario_registro ;
    Date	fecha_registro ;
    
    public Integer getId_sistemaplantacion() {
        return id_sistemaplantacion;
    }
    public void setId_sistemaplantacion(Integer id_sistemaplantacion) {
        this.id_sistemaplantacion = id_sistemaplantacion;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }
    public Integer getId_usuario_registro() {
        return id_usuario_registro;
    }
    public void setId_usuario_registro(Integer id_usuario_registro) {
        this.id_usuario_registro = id_usuario_registro;
    }
    public Date getFecha_registro() {
        return fecha_registro;
    }
    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }
    
 

    
}
