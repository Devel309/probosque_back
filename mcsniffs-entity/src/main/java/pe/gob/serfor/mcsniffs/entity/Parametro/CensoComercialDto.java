package pe.gob.serfor.mcsniffs.entity.Parametro;

import java.util.List;

public class CensoComercialDto {
    private List<ListaEspecieDto> listaEspecieDtos;
    private ResultadosRecursosForestalesMaderablesDto resultadosRecursosForestalesMaderablesDto;
    private  VolumenCortaDto volumenCortaDto;


    public List<ListaEspecieDto> getListaEspecieDtos() {
        return listaEspecieDtos;
    }

    public void setListaEspecieDtos(List<ListaEspecieDto> listaEspecieDtos) {
        this.listaEspecieDtos = listaEspecieDtos;
    }

    public ResultadosRecursosForestalesMaderablesDto getResultadosRecursosForestalesMaderablesDto() {
        return resultadosRecursosForestalesMaderablesDto;
    }

    public void setResultadosRecursosForestalesMaderablesDto(ResultadosRecursosForestalesMaderablesDto resultadosRecursosForestalesMaderablesDto) {
        this.resultadosRecursosForestalesMaderablesDto = resultadosRecursosForestalesMaderablesDto;
    }

    public VolumenCortaDto getVolumenCortaDto() {
        return volumenCortaDto;
    }

    public void setVolumenCortaDto(VolumenCortaDto volumenCortaDto) {
        this.volumenCortaDto = volumenCortaDto;
    }

}
