package pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SolPlantacionForestalDetCoordenadaDetEntity {
    

    Integer id_solplantforest_coord_det ; 
    
    //@column(name = "nu_id_bloquesector", updatable = false, nullable = false)  
   // Integer id_bloquesector ;

   
    //@column(name = "nu_vertice", updatable = false, nullable = false)  
    Integer vertice ;
    //@column(name = "tx_zonaeste", updatable = false, nullable = false)  
    String zonaeste;
    //@column(name = "tx_zonanorte", updatable = false, nullable = false)  
    String zonanorte;
    //@column(name = "tx_observciones", updatable = false, nullable = false)  
    String observaciones;
    //@column(name = "nu_areabloque", updatable = false, nullable = false)  
    public Integer getId_solplantforest_coord_det() {
        return id_solplantforest_coord_det;
    }
    public void setId_solplantforest_coord_det(Integer id_solplantforest_coord_det) {
        this.id_solplantforest_coord_det = id_solplantforest_coord_det;
    }
    public Integer getVertice() {
        return vertice;
    }
    public void setVertice(Integer vertice) {
        this.vertice = vertice;
    }
    public String getZonaeste() {
        return zonaeste;
    }
    public void setZonaeste(String zonaeste) {
        this.zonaeste = zonaeste;
    }
    public String getZonanorte() {
        return zonanorte;
    }
    public void setZonanorte(String zonanorte) {
        this.zonanorte = zonanorte;
    }
    public String getObservaciones() {
        return observaciones;
    }
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }


    
   

}
