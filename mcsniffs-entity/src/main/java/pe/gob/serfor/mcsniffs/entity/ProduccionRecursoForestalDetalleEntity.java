package pe.gob.serfor.mcsniffs.entity;

public class ProduccionRecursoForestalDetalleEntity {
    private Integer idProdRecursoForestalDet;
    private String idVariable;
    private String variable;
    private Double totalTipo;
    private Double totalArea;
    private Boolean accion;

    public Boolean getAccion() {
        return accion;
    }

    public void setAccion(Boolean accion) {
        this.accion = accion;
    }

    public Integer getIdProdRecursoForestalDet() {
        return idProdRecursoForestalDet;
    }

    public void setIdProdRecursoForestalDet(Integer idProdRecursoForestalDet) {
        this.idProdRecursoForestalDet = idProdRecursoForestalDet;
    }

    public String getIdVariable() {
        return idVariable;
    }

    public void setIdVariable(String idVariable) {
        this.idVariable = idVariable;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public Double getTotalTipo() {
        return totalTipo;
    }

    public void setTotalTipo(Double totalTipo) {
        this.totalTipo = totalTipo;
    }

    public Double getTotalArea() {
        return totalArea;
    }

    public void setTotalArea(Double totalArea) {
        this.totalArea = totalArea;
    }

}
