package pe.gob.serfor.mcsniffs.entity.Dto.EvaluacionCampoInfraccion;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
@Data
public class EvaluacionCampoInfraccionDto  extends AuditoriaEntity implements Serializable {

    Integer idEvaluacionCampoInfraccion;
    Integer idEvaluacionCampo ;
    Integer idInfraccion ;
    String infraccion ;
    String calificacion;
    String sancionNoMonetaria ;
    String sancionMonetaria ;
    String subsanable ;
    String estado ;
}
