package pe.gob.serfor.mcsniffs.entity.Parametro;

import java.util.List;

public class AprovechamientoRFNMDto {
    String especie;
    String productoAprovechar;
    List<ResultadosAprovechamientoRFNMDto> resultadosAprovechamientoRFNMDtos;
    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getProductoAprovechar() {
        return productoAprovechar;
    }

    public void setProductoAprovechar(String productoAprovechar) {
        this.productoAprovechar = productoAprovechar;
    }

    public List<ResultadosAprovechamientoRFNMDto> getResultadosAprovechamientoRFNMDtos() {
        return resultadosAprovechamientoRFNMDtos;
    }

    public void setResultadosAprovechamientoRFNMDtos(List<ResultadosAprovechamientoRFNMDto> resultadosAprovechamientoRFNMDtos) {
        this.resultadosAprovechamientoRFNMDtos = resultadosAprovechamientoRFNMDtos;
    }
}
