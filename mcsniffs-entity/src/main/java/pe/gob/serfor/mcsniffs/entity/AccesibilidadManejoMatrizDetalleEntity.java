package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class AccesibilidadManejoMatrizDetalleEntity extends AuditoriaEntity implements Serializable {

    private Integer idAcceManejomatrizdetalle;
    private Integer idAcceManejomatriz;
    private Integer subparcela;
    private String referencia;
    private String via;
    private Double distancia;
    private Double tiempo;
    private String tipovehiculo;

    public Integer getIdAcceManejomatrizdetalle() {
        return idAcceManejomatrizdetalle;
    }

    public void setIdAcceManejomatrizdetalle(Integer idAcceManejomatrizdetalle) {
        this.idAcceManejomatrizdetalle = idAcceManejomatrizdetalle;
    }

    public Integer getIdAcceManejomatriz() {
        return idAcceManejomatriz;
    }

    public void setIdAcceManejomatriz(Integer idAcceManejomatriz) {
        this.idAcceManejomatriz = idAcceManejomatriz;
    }

    public Integer getSubparcela() {
        return subparcela;
    }

    public void setSubparcela(Integer subparcela) {
        this.subparcela = subparcela;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public Double getDistancia() {
        return distancia;
    }

    public void setDistancia(Double distancia) {
        this.distancia = distancia;
    }

    public Double getTiempo() {
        return tiempo;
    }

    public void setTiempo(Double tiempo) {
        this.tiempo = tiempo;
    }

    public String getTipovehiculo() {
        return tipovehiculo;
    }

    public void setTipovehiculo(String tipovehiculo) {
        this.tipovehiculo = tipovehiculo;
    }
}
