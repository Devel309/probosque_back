package pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima;

import lombok.Data;

@Data
public class ConsultarSancionVigenteOsceDto {
    
    private String ruc;
}
