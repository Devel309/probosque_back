package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class RegenteComunidadEntity extends AuditoriaEntity implements Serializable {
    private Integer idRegenteComunidad;
    private PersonaEntity persona;
    private String codInrena;

    public Integer getIdRegenteComunidad() {
        return idRegenteComunidad;
    }

    public void setIdRegenteComunidad(Integer idRegenteComunidad) {
        this.idRegenteComunidad = idRegenteComunidad;
    }

    public PersonaEntity getPersona() {
        return persona;
    }

    public void setPersona(PersonaEntity persona) {
        this.persona = persona;
    }

    public String getCodInrena() {
        return codInrena;
    }

    public void setCodInrena(String codInrena) {
        this.codInrena = codInrena;
    }
}
