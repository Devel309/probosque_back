package pe.gob.serfor.mcsniffs.entity.Evaluacion;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Getter
@Setter
public class EvaluacionDetalleDto extends AuditoriaEntity implements Serializable{
    private Integer idEvaluacionDet;
    private Integer idEvaluacion;
    private String codigoEvaluacion;
    private String codigoEvaluacionDet;
    private String codigoEvaluacionDetSub;
    private String codigoEvaluacionDetPost;
    private String conforme;
    private String observacion;
    private String detalle;
    private String descripcion;
    private String estadoEvaluacionDet;
    private Integer idArchivo;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Timestamp fechaEvaluacionDetInicial;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Timestamp fechaEvaluacionDetFinal;

}
