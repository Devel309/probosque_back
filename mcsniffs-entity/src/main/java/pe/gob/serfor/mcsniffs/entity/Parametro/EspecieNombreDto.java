package pe.gob.serfor.mcsniffs.entity.Parametro;

import java.util.List;

import pe.gob.serfor.mcsniffs.entity.ParametroValorEntity;

public class EspecieNombreDto {
    private List<ParametroValorEntity> ListaEspecieNombreComun;
    private List<ParametroValorEntity> ListaEspecieNombreCientifico ;

    public List<ParametroValorEntity> getListaEspecieNombreCientifico() {
        return ListaEspecieNombreCientifico;
    }
    public List<ParametroValorEntity> getListaEspecieNombreComun() {
        return ListaEspecieNombreComun;
    }
    public void setListaEspecieNombreComun(List<ParametroValorEntity> listaEspecieNombreComun) {
        this.ListaEspecieNombreComun = listaEspecieNombreComun;
    }
    public void setListaEspecieNombreCientifico(List<ParametroValorEntity> listaEspecieNombreCientifico) {
        this.ListaEspecieNombreCientifico = listaEspecieNombreCientifico;
    }
}
