package pe.gob.serfor.mcsniffs.entity;

import java.io.File;

public class EmailEntity {
    private String[] email;
    private String[] emailCc;
    private String content;
    private String subject;
    private File adjunto;

    public File getAdjunto() {
        return adjunto;
    }

    public void setAdjunto(File adjunto) {
        this.adjunto = adjunto;
    }

   

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return "EmailBody [email=" + email + ", content=" + content + ", subject=" + subject + "]";
    }

    public String[] getEmailCc() {
        return emailCc;
    }

    public void setEmailCc(String[] emailCc) {
        this.emailCc = emailCc;
    }

    public String[] getEmail() {
        return email;
    }

    public void setEmail(String[] email) {
        this.email = email;
    }



    
}
