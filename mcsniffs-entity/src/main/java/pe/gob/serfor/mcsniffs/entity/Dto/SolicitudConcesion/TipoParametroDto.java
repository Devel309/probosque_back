package pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;

@Data
public class TipoParametroDto extends AuditoriaEntity implements Serializable {

    private Integer idParametro;
    private Integer idTipoParametro; 
    private String descripcion;
    private String codigo;
    private String valorPrimario;
    private String descripcionValorPrimario;
    private String valorTerciario;
    private String nombre;
}
