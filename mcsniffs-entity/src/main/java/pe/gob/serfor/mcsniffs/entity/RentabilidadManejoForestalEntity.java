package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class RentabilidadManejoForestalEntity extends AuditoriaEntity implements Serializable {
    private Integer idRentManejoForestal;
    private Integer idTipoRubro;
    private String  rubro;
    private String  descripcion;
    private PlanManejoEntity planManejo;

    public String getRubro() {
        return rubro;
    }

    public void setRubro(String rubro) {
        this.rubro = rubro;
    }

    public PlanManejoEntity getPlanManejo() {
        return planManejo;
    }

    public void setPlanManejo(PlanManejoEntity planManejo) {
        this.planManejo = planManejo;
    }

    public Integer getIdRentManejoForestal() {
        return idRentManejoForestal;
    }

    public void setIdRentManejoForestal(Integer idRentManejoForestal) {
        this.idRentManejoForestal = idRentManejoForestal;
    }

    public Integer getIdTipoRubro() {
        return idTipoRubro;
    }

    public void setIdTipoRubro(Integer idTipoRubro) {
        this.idTipoRubro = idTipoRubro;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
