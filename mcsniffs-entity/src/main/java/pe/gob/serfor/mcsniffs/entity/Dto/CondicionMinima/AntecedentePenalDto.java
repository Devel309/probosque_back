package pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data",
        "totalRegistro",
        "dataService",
        "message",
        "stackTrace",
        "codigo",
        "innerException",
        "success",
        "informacion",
        "messageExeption"
})
public class AntecedentePenalDto{

    @JsonProperty("data")
    private String data;
    @JsonProperty("totalRegistro")
    private Integer totalRegistro;
    @JsonProperty("dataService")
    private DataService dataService;
    @JsonProperty("message")
    private String message;
    @JsonProperty("stackTrace")
    private String stackTrace;
    @JsonProperty("codigo")
    private String codigo;
    @JsonProperty("innerException")
    private String innerException;
    @JsonProperty("success")
    private boolean success;
    @JsonProperty("informacion")
    private String informacion;
    @JsonProperty("messageExeption")
    private String messageExeption;

    
    
    public DataService getDataService() {
        return dataService;
    }
    public void setDataService(DataService dataService) {
        this.dataService = dataService;
    }
    public String getData() {
        return data;
    }
    public void setData(String data) {
        this.data = data;
    }
    public Integer getTotalRegistro() {
        return totalRegistro;
    }
    public void setTotalRegistro(Integer totalRegistro) {
        this.totalRegistro = totalRegistro;
    }
 
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public String getStackTrace() {
        return stackTrace;
    }
    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }
    public String getCodigo() {
        return codigo;
    }
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    public String getInnerException() {
        return innerException;
    }
    public void setInnerException(String innerException) {
        this.innerException = innerException;
    }
    public boolean isSuccess() {
        return success;
    }
    public void setSuccess(boolean success) {
        this.success = success;
    }
    public String getInformacion() {
        return informacion;
    }
    public void setInformacion(String informacion) {
        this.informacion = informacion;
    }
    public String getMessageExeption() {
        return messageExeption;
    }
    public void setMessageExeption(String messageExeption) {
        this.messageExeption = messageExeption;
    }

    @Data
    public static class DataService{
        private String xcodigoRespuesta;
        private String xmensajeRespuesta;
    }
}