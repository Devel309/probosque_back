package pe.gob.serfor.mcsniffs.entity.Dto.SolicitudConcesion;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;
import pe.gob.serfor.mcsniffs.entity.SolicitudConcesionArchivoEntity;

import java.io.Serializable;
import java.util.List;

@Data
public class SolicitudConcesionEvaluacionCalificacionDto extends AuditoriaEntity implements Serializable {
    private Integer idSolicitudConcesionEvaluacionCalificacion;
    private Integer idSolicitudConcesionEvaluacion;
    private String codigoCriterio;
    private Integer numPuntaje;
}
