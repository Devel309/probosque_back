package pe.gob.serfor.mcsniffs.entity.Evaluacion;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.gob.serfor.mcsniffs.entity.ParametroEntity;
import pe.gob.serfor.mcsniffs.entity.TipoDocumentoEntity;

@Getter
@Setter
public class InspeccionDocumentoDto implements Serializable{
    private List<ParametroEntity> listaParametro;
    private List<TipoDocumentoEntity> listaDocumento;
    
}
