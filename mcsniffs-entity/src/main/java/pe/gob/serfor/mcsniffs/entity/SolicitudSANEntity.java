package pe.gob.serfor.mcsniffs.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class SolicitudSANEntity extends AuditoriaEntity implements Serializable {
    private Integer idSolicitudSAN;
    private String codSolicitudSAN;
    private String subCSolicitudSAN;
    private String estadoSolicitudSAN;
    private Integer nroGestion;
    private String tipoPostulacion;
    private String procesoPostulacion;
    private String motivoSolicitud;
    private String asunto;
    private Integer idArchivo;
    private String descripcion;
    private String detalle;
    private String observacion;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaFinal;
    private Integer diasRestantes;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaFinalPlan;
    private Integer diasRestantesPlan;
    private String estadoPLan;
    private Integer diasAdicionales;
}
