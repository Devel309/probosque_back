package pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima;


public class ConsultaRequisitosDto {
    
    
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String nombres;
    private String tipoDocumento;
    private String nroDocumento;
    private String fechaEvaluacion;

    private String resultadoAntecedentePenal;
    private String resultadoAntecedenteJudicial;
    private String resultadoAntecedentePolicial;
    private String resultadoSancionVigenteOsce;
    private String resultadoInfractor;
    private String resultadoGeneral;

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getFechaEvaluacion() {
        return fechaEvaluacion;
    }

    public void setFechaEvaluacion(String fechaEvaluacion) {
        this.fechaEvaluacion = fechaEvaluacion;
    }

    public String getResultadoAntecedentePenal() {
        return resultadoAntecedentePenal;
    }

    public void setResultadoAntecedentePenal(String resultadoAntecedentePenal) {
        this.resultadoAntecedentePenal = resultadoAntecedentePenal;
    }

    public String getResultadoAntecedenteJudicial() {
        return resultadoAntecedenteJudicial;
    }

    public void setResultadoAntecedenteJudicial(String resultadoAntecedenteJudicial) {
        this.resultadoAntecedenteJudicial = resultadoAntecedenteJudicial;
    }

    public String getResultadoAntecedentePolicial() {
        return resultadoAntecedentePolicial;
    }

    public void setResultadoAntecedentePolicial(String resultadoAntecedentePolicial) {
        this.resultadoAntecedentePolicial = resultadoAntecedentePolicial;
    }

    public String getResultadoSancionVigenteOsce() {
        return resultadoSancionVigenteOsce;
    }

    public void setResultadoSancionVigenteOsce(String resultadoSancionVigenteOsce) {
        this.resultadoSancionVigenteOsce = resultadoSancionVigenteOsce;
    }

    public String getResultadoInfractor() {
        return resultadoInfractor;
    }

    public void setResultadoInfractor(String resultadoInfractor) {
        this.resultadoInfractor = resultadoInfractor;
    }

    public String getResultadoGeneral() {
        return resultadoGeneral;
    }

    public void setResultadoGeneral(String resultadoGeneral) {
        this.resultadoGeneral = resultadoGeneral;
    }
}
