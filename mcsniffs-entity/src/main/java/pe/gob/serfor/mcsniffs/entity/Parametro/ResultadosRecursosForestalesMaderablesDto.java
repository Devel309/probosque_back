package pe.gob.serfor.mcsniffs.entity.Parametro;

import java.util.List;

public class ResultadosRecursosForestalesMaderablesDto {

    private String areaTotalCensada;
    private String contadorEspecies;
    private  String numeroArbolesMaderables ;
    private String numeroArbolesNoMaderables ;
    private  String numeroArbolesTotal;
    private List<ResultadosRFMTabla> resultadosRFMTablas;

    public String getContadorEspecies() {
        return contadorEspecies;
    }

    public void setContadorEspecies(String contadorEspecies) {
        this.contadorEspecies = contadorEspecies;
    }

    public String getNumeroArbolesMaderables() {
        return numeroArbolesMaderables;
    }

    public void setNumeroArbolesMaderables(String numeroArbolesMaderables) {
        this.numeroArbolesMaderables = numeroArbolesMaderables;
    }

    public String getNumeroArbolesNoMaderables() {
        return numeroArbolesNoMaderables;
    }

    public void setNumeroArbolesNoMaderables(String numeroArbolesNoMaderables) {
        this.numeroArbolesNoMaderables = numeroArbolesNoMaderables;
    }

    public String getNumeroArbolesTotal() {
        return numeroArbolesTotal;
    }

    public void setNumeroArbolesTotal(String numeroArbolesTotal) {
        this.numeroArbolesTotal = numeroArbolesTotal;
    }

    public List<ResultadosRFMTabla> getResultadosRFMTablas() {
        return resultadosRFMTablas;
    }

    public void setResultadosRFMTablas(List<ResultadosRFMTabla> resultadosRFMTablas) {
        this.resultadosRFMTablas = resultadosRFMTablas;
    }
    public String getAreaTotalCensada() {
        return areaTotalCensada;
    }

    public void setAreaTotalCensada(String areaTotalCensada) {
        this.areaTotalCensada = areaTotalCensada;
    }
}
