package pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima;

import lombok.Data;

@Data
public class ListarCondicionMinimaDetalleDto {

    private Integer idCondicionMinimaDet;
    private String codigoTipoCondicionMinimaDet;
    private String resultado;
    private String fechaIngreso;
    private String fechaVigencia;
    private Integer idCondicionMinima;
    private String estado;

}
