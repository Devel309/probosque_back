package pe.gob.serfor.mcsniffs.entity.Dto.PlanManejoEvaluacion;

import java.io.Serializable;

public class CompiladoArchivoDto implements Serializable {

    private String nombreArchivo;
    private String tipoArchivo;
    private byte[] archivo;

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getTipoArchivo() {
        return tipoArchivo;
    }

    public void setTipoArchivo(String tipoArchivo) {
        this.tipoArchivo = tipoArchivo;
    }

    public byte[] getArchivo() {
        return archivo;
    }

    public void setArchivo(byte[] archivo) {
        this.archivo = archivo;
    }
}
