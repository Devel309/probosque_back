package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SolicitudBosqueLocalComiteTecnicoEntity extends AuditoriaEntity implements Serializable {
    private Integer idSolBosqueLocalComiteTecnico;
    private Integer idSolBosqueLocal;
    private Integer idPersona;
    private Integer idDocumentoAcreditacion;
    private Integer idcomiteTecnicoPersona;
    private Integer idBosqueLocal;
    private Integer idDocAcreditacion;
    private String estado;
    private String numDocumento;
    private String persona;
    private String telefono;
    private Integer idDepartamento;
    private String departamento;
    private Integer idProvincia;
    private String provincia;
    private Integer idDistrito;
    private String distrito;
    private Date fechaPresentacion;
    private String estadoSolicitud;
    private String resultadoSolicitud;
    private String perfil;
    private String email;
}
