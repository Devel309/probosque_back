package pe.gob.serfor.mcsniffs.entity.Dto.CondicionMinima;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data",
        "totalRegistro",
        "dataService",
        "message",
        "stackTrace",
        "codigo",
        "innerException",
        "success",
        "informacion",
        "messageExeption"
})
@Getter
@Setter

public class ConsultaTitularidadDto {

    @JsonProperty("data")
    private String data;
    @JsonProperty("totalRegistro")
    private Integer totalRegistro;
    @JsonProperty("dataService")
    private List<Titularidad> dataService;
    @JsonProperty("message")
    private String message;
    @JsonProperty("stackTrace")
    private String stackTrace;
    @JsonProperty("codigo")
    private String codigo;
    @JsonProperty("innerException")
    private String innerException;
    @JsonProperty("success")
    private boolean success;
    @JsonProperty("informacion")
    private String informacion;
    @JsonProperty("messageExeption")
    private String messageExeption;

    @Getter
    @Setter
    public static class Titularidad{


        private String libro;
        private String tipoDocumento;
        private String zona;
        private String razonSocial;
        private String estado;
        private String oficina;
        private String direccion;
        private String numeroDocumento;
        private String registro;
        private String numeroPartida;

    }

}