package pe.gob.serfor.mcsniffs.entity.Parametro;

import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

import java.io.Serializable;
import java.math.BigDecimal;

public class PlanManejoDto extends AuditoriaEntity implements Serializable {
    private Integer idContrato;
    private Integer idPlanManejo;
    private String codigoPlanManejo;
    private String tipoPlanManejo;
    private String codigoEstado;
    private Integer idTitular;
    private Boolean remitido;
    private Integer idPlanManejoPadre;

    private String tipoDocumento;
    private String nroDocumento;
    private String area;
    private String codigo;
    private String estado;
    private String nombrePlan;
    private Integer idDivisionAdministrativa;

    public Integer getIdDivisionAdministrativa() {
        return idDivisionAdministrativa;
    }

    public void setIdDivisionAdministrativa(Integer idDivisionAdministrativa) {
        this.idDivisionAdministrativa = idDivisionAdministrativa;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Override
    public String getEstado() {
        return estado;
    }

    @Override
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNombrePlan() {
        return nombrePlan;
    }

    public void setNombrePlan(String nombrePlan) {
        this.nombrePlan = nombrePlan;
    }

    public Integer getIdContrato() {
        return idContrato;
    }
    public String getTipoPlanManejo() {
        return tipoPlanManejo;
    }
    public void setTipoPlanManejo(String tipoPlanManejo) {
        this.tipoPlanManejo = tipoPlanManejo;
    }
    public String getCodigoPlanManejo() {
        return codigoPlanManejo;
    }
    public void setCodigoPlanManejo(String codigoPlanManejo) {
        this.codigoPlanManejo = codigoPlanManejo;
    }

    public void setIdContrato(Integer idContrato) {
        this.idContrato = idContrato;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public Integer getIdTitular() {
        return idTitular;
    }

    public void setIdTitular(Integer idTitular) {
        this.idTitular = idTitular;
    }

    public Integer getIdPlanManejo() {
        return idPlanManejo;
    }

    public void setIdPlanManejo(Integer idPlanManejo) {
        this.idPlanManejo = idPlanManejo;
    }

    public Boolean getRemitido() {
        return remitido;
    }

    public void setRemitido(Boolean remitido) {
        this.remitido = remitido;
    }

    public Integer getIdPlanManejoPadre() { return idPlanManejoPadre; }

    public void setIdPlanManejoPadre(Integer idPlanManejoPadre) { this.idPlanManejoPadre = idPlanManejoPadre; }
}
