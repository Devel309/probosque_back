package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;


public class ProteccionBosqueDemarcacionEntity extends AuditoriaEntity implements Serializable {
    private Integer idProBosqueDem;
    private PlanManejoEntity planManejo;
    private ParametroEntity parametro;
    private Boolean accion;
    private String descripcion;
    private String implementacion;

    public Integer getIdProBosqueDem() {
        return idProBosqueDem;
    }

    public void setIdProBosqueDem(Integer idProBosqueDem) {
        this.idProBosqueDem = idProBosqueDem;
    }

    public PlanManejoEntity getPlanManejo() {
        return planManejo;
    }

    public void setPlanManejo(PlanManejoEntity planManejo) {
        this.planManejo = planManejo;
    }

    public ParametroEntity getParametro() {
        return parametro;
    }

    public void setParametro(ParametroEntity parametro) {
        this.parametro = parametro;
    }

    public Boolean getAccion() {
        return accion;
    }

    public void setAccion(Boolean accion) {
        this.accion = accion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImplementacion() {
        return implementacion;
    }

    public void setImplementacion(String implementacion) {
        this.implementacion = implementacion;
    }
}