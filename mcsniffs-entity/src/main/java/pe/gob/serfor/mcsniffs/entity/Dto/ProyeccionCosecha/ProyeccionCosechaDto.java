package pe.gob.serfor.mcsniffs.entity.Dto.ProyeccionCosecha;

import lombok.Data;

@Data
public class ProyeccionCosechaDto {
    String nombreComun;
    String arbHaDap10a14;
    String arbHaDap15a19;
    String arbHaDap20a29;
    String arbHaDap30a39;
    String arbHaDap40a49;
    String arbHaDap50a59;
    String arbHaDap60a69;
    String arbHaDap70a79;
    String arbHaDap80a89;
    String arbHaDap90aMas;
    String arbHaDap70aMas;
    String arbHaTotalTipoBosque;
    String arbHaTotalPorArea;
    String abHaDap10a14;
    String abHaDap15a19;
    String abHaDap20a29;
    String abHaDap30a39;
    String abHaDap40a49;
    String abHaDap50a59;
    String abHaDap60a69;
    String abHaDap70a79;
    String abHaDap80a89;
    String abHaDap90aMas;
    String abHaDap70aMas;
    String abHaTotalTipoBosque;
    String abHaTotalPorArea;
    String volHaDap10a14;
    String volHaDap15a19;
    String volHaDap20a29;
    String volHaDap30a39;
    String volHaDap40a49;
    String volHaDap50a59;
    String volHaDap60a69;
    String volHaDap70a79;
    String volHaDap80a89;
    String volHaDap90aMas;
    String volHaDap70aMas;
    String volHaTotalTipoBosque;
    String volHaTotalPorArea;
    Integer bloque;
}
