package pe.gob.serfor.mcsniffs.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class PermisoForestalEntity extends AuditoriaEntity implements Serializable {
    private Integer idPermisoForestal;
    private Integer idSolicitud;
    private Integer idTipoProceso;
    private Integer idTipoPermiso;
    private short idTipoEscala;
    private String estadoPermiso;
    private String codigoEstado;
    private String descripcion;
    private String detalle;
    private String observacion;
    private Boolean remitido;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "America/Lima")
    private Date fechaSolicitudFinal;
    private Integer diasRestantes;
    private InformacionGeneralPermisoForestalEntity informacionGeneral;
    private String codigoUnico;
    private Integer idGeometria;
}
