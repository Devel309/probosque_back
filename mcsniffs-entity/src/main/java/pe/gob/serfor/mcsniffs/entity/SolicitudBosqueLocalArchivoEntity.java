package pe.gob.serfor.mcsniffs.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SolicitudBosqueLocalArchivoEntity extends AuditoriaEntity implements Serializable {

    private Integer idSolBosqueLocalArchivo;
    private Integer idSolBosqueLocal;
    private String codigoArchivo;
    private String codigoSubArchivo;
    private String codigoSeccion;
    private String codigoSubSeccion;
    private String descripcion;
    private String detalle;
    private String observacion;
    private Integer idArchivo;
    private String tipoDocumento;
    private String numeroDocumento;
    private String nombreArchivo;
    private String nombreGeneradoArchivo;
    private String extensionArchivo;
    private String rutaArchivo;
    private byte[] documento;
    private Boolean conforme;
    private String descripcionSeccion;
    private String tipoArchivo;
    private String asunto;
    private String seccion;
    private String codigoUbigeoMunicipio;
    private Date fechaDocumento;
    private Integer idDepartamentoMunicipio;
    private Integer idProvinciaMunicipio;
    private Integer idDistritoMunicipio;
    private String departamentoMunicipio;
    private String provinciaMunicipio;
    private String distritoMunicipio;
}
