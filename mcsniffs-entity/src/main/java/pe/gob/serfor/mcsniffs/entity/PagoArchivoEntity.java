package pe.gob.serfor.mcsniffs.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class PagoArchivoEntity extends AuditoriaEntity implements Serializable {
    private Integer idPagoArchivo;
    private String codigoArchivo;
    private String codigoSubArchivo;
    private String descripcion;
    private String detalle;
    private String observacion;
    private Integer idPago;
    private Integer idArchivo;
    private String codigoTipoDocumento;
    private String nombreArchivo;
    private String extensionArchivo;
    private byte[] documento;
}
