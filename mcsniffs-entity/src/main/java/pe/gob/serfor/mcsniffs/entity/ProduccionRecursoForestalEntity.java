package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ProduccionRecursoForestalEntity  extends AuditoriaEntity implements Serializable {
    private Integer idProdRecursoForestal;
    private String idTipoBosque;
    private String tipoBosque;
    private PlanManejoEntity planManejo;
    private String idEspecie;
    private String especie;
    private String idTipoProdRecursoForestal;

    public Integer getIdProdRecursoForestal() {
        return idProdRecursoForestal;
    }

    public void setIdProdRecursoForestal(Integer idProdRecursoForestal) {
        this.idProdRecursoForestal = idProdRecursoForestal;
    }

    public String getIdTipoBosque() {
        return idTipoBosque;
    }

    public void setIdTipoBosque(String idTipoBosque) {
        this.idTipoBosque = idTipoBosque;
    }

    public String getTipoBosque() {
        return tipoBosque;
    }

    public void setTipoBosque(String tipoBosque) {
        this.tipoBosque = tipoBosque;
    }

    public PlanManejoEntity getPlanManejo() {
        return planManejo;
    }

    public void setPlanManejo(PlanManejoEntity planManejo) {
        this.planManejo = planManejo;
    }

    public String getIdEspecie() {
        return idEspecie;
    }

    public void setIdEspecie(String idEspecie) {
        this.idEspecie = idEspecie;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getIdTipoProdRecursoForestal() {
        return idTipoProdRecursoForestal;
    }

    public void setIdTipoProdRecursoForestal(String idTipoProdRecursoForestal) {
        this.idTipoProdRecursoForestal = idTipoProdRecursoForestal;
    }
}
