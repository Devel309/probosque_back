package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class UnidadManejoEntity extends AuditoriaEntity implements Serializable {

	private Integer idUnidadManejo;
	private Integer idDistrito;
	private String cuenca;
	private Integer idPlanManejo;

	public Integer getIdUnidadManejo() {
		return idUnidadManejo;
	}

	public void setIdUnidadManejo(Integer idUnidadManejo) {
		this.idUnidadManejo = idUnidadManejo;
	}

	public Integer getIdDistrito() {
		return idDistrito;
	}

	public void setIdDistrito(Integer idDistrito) {
		this.idDistrito = idDistrito;
	}

	public String getCuenca() {
		return cuenca;
	}

	public void setCuenca(String cuenca) {
		this.cuenca = cuenca;
	}

	public Integer getIdPlanManejo() {
		return idPlanManejo;
	}

	public void setIdPlanManejo(Integer idPlanManejo) {
		this.idPlanManejo = idPlanManejo;
	}

    public UnidadManejoEntity() {
		super();
	}

}
