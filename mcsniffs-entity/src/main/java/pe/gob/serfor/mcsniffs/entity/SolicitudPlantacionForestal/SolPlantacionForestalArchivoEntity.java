package pe.gob.serfor.mcsniffs.entity.SolicitudPlantacionForestal;

import java.io.Serializable;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
public class SolPlantacionForestalArchivoEntity extends AuditoriaEntity implements Serializable {

    private Integer idSolPlantForestArchivo;
    private Integer idSolPlantForest;
    private String codigoArchivo;
    private String codigoSubArchivo;
    private String codigoSeccion;
    private String codigoSubSeccion;
    private String descripcion;
    private String detalle;
    private String observacion;
    private Integer idArchivo;
    private String tipoDocumento;
    private String nombreArchivo;
    private String nombreGeneradoArchivo;
    private String extensionArchivo;
    private String rutaArchivo;
    private byte[] documento;
    private Boolean conforme;
    private String descripcionSeccion;
    private String tipoArchivo;
    private Boolean conformeFiscalizacion;
    private String obsFiscalizacion;
}
