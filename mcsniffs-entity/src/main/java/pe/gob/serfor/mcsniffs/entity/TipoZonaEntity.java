package pe.gob.serfor.mcsniffs.entity;

public class TipoZonaEntity extends AuditoriaEntity {
    private short IdTipoZona;
    private String Descripcion;


    public short getIdTipoZona() {
        return IdTipoZona;
    }

    public void setIdTipoZona(short IdTipoZona) {
        IdTipoZona = IdTipoZona;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }
}
