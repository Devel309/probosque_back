package pe.gob.serfor.mcsniffs.entity;

public class ActividadesDesarrolloPFDMEntity {
    //value1 - en la tabla anexosPFDM
    private String actividad;
    //value2 - en la tabla anexosPFDM
    private String descripcion;
    //value3 - en la tabla anexosPFDM
    private String periodoAnios;
    //value4 - en la tabla anexosPFDM
    private String presupuesto;
    
    public String getActividad() {
        return actividad;
    }
    public void setActividad(String actividad) {
        this.actividad = actividad;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String getPeriodoAnios() {
        return periodoAnios;
    }
    public void setPeriodoAnios(String periodoAnios) {
        this.periodoAnios = periodoAnios;
    }
    public String getPresupuesto() {
        return presupuesto;
    }
    public void setPresupuesto(String presupuesto) {
        this.presupuesto = presupuesto;
    }
    
}
