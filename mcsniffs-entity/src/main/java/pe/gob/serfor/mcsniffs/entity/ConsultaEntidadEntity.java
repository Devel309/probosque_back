package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class ConsultaEntidadEntity extends AuditoriaEntity implements Serializable {
    private Integer IdConsultaEntidad;
    private String Descripcion;

    public Integer getIdConsultaEntidad() {
        return IdConsultaEntidad;
    }

    public void setIdConsultaEntidad(Integer idConsultaEntidad) {
        IdConsultaEntidad = idConsultaEntidad;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }
}
