package pe.gob.serfor.mcsniffs.entity.Parametro;

import java.io.Serializable;

import lombok.Data;
import pe.gob.serfor.mcsniffs.entity.AuditoriaEntity;

@Data
public class ContratoTHDto  extends AuditoriaEntity implements Serializable {
    private Integer idContrato;
    private String codigoContrato;
    private String ubigeo;
    private String tipoPlanManejo;
    private Integer tipoProceso;
    private String codigoTituloTh;
 
}
