package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;


@Data
@Getter
@Setter
public class PersonaDto extends AuditoriaEntity implements Serializable {
    private Integer idPersona;
    private String nombre;
    private String apellidoMaterno;
    private String apellidoPaterno;
    private String telefono;
    private String codTipoPersona;
    private String codTipoActor;
    private String idDistrito;
    private String codTipoCncc;
    private String codTipoDoc;
    private String numeroDocumento;
    private String email;
    private char esRepLegal;
    private Integer idRepLegal;
    private String razonSocial;
    private Integer idSolicitudAcceso;
    private Integer idUsuario;
    private String direccion;
    private String codIrena;
    private Integer idTipoRegente;
    private String codEvalSolic;
    private String codEvalEmpresa;
    private PersonaDto representanteLegal;
    
}