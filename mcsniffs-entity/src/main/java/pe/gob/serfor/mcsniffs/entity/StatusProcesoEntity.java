package pe.gob.serfor.mcsniffs.entity;

import java.io.Serializable;

public class StatusProcesoEntity extends AuditoriaEntity implements Serializable {
    private Integer IdStatusProceso;
    private String Descripcion;

    public Integer getIdStatusProceso() {
        return IdStatusProceso;
    }

    public void setIdStatusProceso(Integer idStatusProceso) {
        IdStatusProceso = idStatusProceso;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }
}
