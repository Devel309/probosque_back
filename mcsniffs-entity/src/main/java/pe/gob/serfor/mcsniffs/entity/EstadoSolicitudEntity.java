package pe.gob.serfor.mcsniffs.entity;

import java.util.Date;

public class EstadoSolicitudEntity extends AuditoriaEntity {
   Integer NU_ID_ESTADOSOLICITUD;
	String TX_NOMBRE;
	String TX_DESCRIPCION ;


    
    public Integer getNU_ID_ESTADOSOLICITUD() {
        return NU_ID_ESTADOSOLICITUD;
    }
    public void setNU_ID_ESTADOSOLICITUD(Integer nU_ID_ESTADOSOLICITUD) {
        NU_ID_ESTADOSOLICITUD = nU_ID_ESTADOSOLICITUD;
    }
    public String getTX_NOMBRE() {
        return TX_NOMBRE;
    }
    public void setTX_NOMBRE(String tX_NOMBRE) {
        TX_NOMBRE = tX_NOMBRE;
    }
    public String getTX_DESCRIPCION() {
        return TX_DESCRIPCION;
    }
    public void setTX_DESCRIPCION(String tX_DESCRIPCION) {
        TX_DESCRIPCION = tX_DESCRIPCION;
    }
   
    
}
