
<a name="0.0.1"></a>
## 0.0.1

> 2021-09-01

### Docs

* changelog.

### Feat

* [3.1.1 PGMF] Se agrega lógica para obtener y guardar archivos de PGMF
* [3.1.1 PGMF] Se agrega campos PGMFArchivoEntity
* [General] implementacion de service eliminar y obtener archivo
* [3.1.1 - HU02] Se agrega campo bloques quinquenales.
* [Información General] Se agrega endpoint crear/ obtener información General.
* [3.1.7 - HU10] Se agregan endpoints para registrar/editar/eliminar SistemaForestalManejoDetalle
* [3.1.7 - HU10] SistemaManejoForestalDetalleEntoty - se agrega campo
* Se cambia nombre servicio.
* [N3.1.2-HU08] Actividad Silvicultural Actualizar y Registrar Detalle
* [3.1.7 - HU10] Se termina end point para Registrar/Actualizar Sistema Manejo Forestal, Se terminó los endpoints necesarios para HU10.
* Se crea función para pasar valores nulos a los sps.
* [N3.1.2-HU08] Actividad Silvicultural Registrar
* [N3.1.2-HU08] Actividad Silvicultural Listar
* [3.1.7 - HU10] Se termina endpoint obtener Sistema Manejo Forestal.
* Se agregó/modificó Modelo SistemaManejoForestal y  SistemaManejoForestalDetalle
* [N3.1.2-HU08] Avance Actividad Silvicultural
* [3.1.7-HU10] Avance Controlador listar/guardar Sistema Manejo Forestal.
* se implementa mejoras en el servicio listar aprovechamiento y actividad
* Se terminó endpoint ObtenerHidrografia
* se implementa servicio de obtener Poblacion aledaña
* Se terminó endpoints: ObtenerTipoBosque, Obtener Fauna
* Avance endpoints: ObtenerTipobosque, ObtenerFauna
* se agrega campos a las entidades, para el manejo de fauna, infraestructura y tipo de bosques
* se implementa entidad, rep informacion basica
* se implementa guardado y actualizado de informacion general planificacion bosque
* se implementa servicios para informacion general , regente
* se implementa service para regente, informacion general
* se implementa servicio eliminar resumen Actividades del plan operativo anterior y se comenta el obtener id en el registro
* se modifica el registro de CEnso forestal detalle para el app movil y la carga masiva.
* se implementa servicio listar resumen Actividades del plan operativo anterior
* se implementa servicio para la historia resumen Actividades
* se agrega el registro tabla intermedia
* se implementa procedimiento de listar y registrar tabla intermedia
* se implemente campos nuevos para manejo de aprovechamiento en evaluacion
* se agrega campos para listar
* se implemente campo impacto para la tabla evaluacion ambiental
* se implemente campos adicionales para aprovechamiento
* se implemente sp para envio de aprovechamiento
* listar evaluacion tabla intermedia
* rescatar idActividad en registrar
* Registro Evaluacion ambiental Actividad
* Registro Evaluacion ambiental tabla intermedia
* Registro Actividad Evaluacion ambiental
* Evaluacion ambiental aprovechamiento listar y registrar
* listar con Detalle HU12
* registro y actualizacion detalle historia HU12
* implementacion de historia HU12, listar
* implementacion de historia HU12, actualizar y eliminar
* implementacion de historia HU12
* implementacion de historias HU02,HU03,HU06,HU09

### Fix

* Correccion servicio - JaquelineDB
* Se corrige tipo de dato de Usuario registro.
* [N3.1.2-HU08] cambio de dato bolean al registrar
* [N3.1.2-HU08] bolean fix default
* Se comenta Controlador POST [/listarTipoComboSistemaPlantacionForestal], ya que existen 2, corregir.
* No se olviden de poner punto y coma(;)
* [N3.1.2-HU08] Actividad Silvicultural listar valueof
* [N3.1.2-HU08] Actividad Silvicultural registrar , se lista el detalle en el response
* se cambia codesmell
* Se comenta endpoint: /listarManejoBosqueAprovechamientoCamino, genera error y no se puede levantar la app, hay 2 endpoint POST  con esa ruta.
* se agrega un campo requerido para el registrar regente
* se adiciona nuevos campos al listar actividad
* se corrige el listado con los nuevos campos
* se corrige el formato fecha en el entity
* se corrige el registrar participacion ciudadana
* se corrige la respuesta del controlador
* idActividad erroneo
* Setear Id Evaluiación Ambiental.
* Registro Evaluacion
* agregamos valor idCapacitacion a listar
* agregamos valor idCapacitacion a listar
* eliminar detalle feat: listar aumentamos parametro
* listar con Detalle HU12, feat: Participacion comunal avance
* implementacion de historia HU12, listar cambio de nombre

### Fxi

* log controller

### Refactor

* [General] refactor service eliminar y obtener archivo
* Se refactoriza controlador Sistema Manejo Forestal setear error.
* quitando librería no utilizada test.
* Se cambia nombre de clase PoblacionAledaña -> PoblacionAledania

### Upd

* marca de AnexoAdjunto
* ajustes
* ajustes
* ajustes SistemaManejoForestal
* ajustes capa eliminar
* ajustes
* se agrega servicios para cronograma actividad
* ajustes
* ajustes solicitud de acceso
* ajustes solicitud acceso
* obtener y actualizar revision SA
* solicitud acceso: se agrega distrito
* ajuste de permisos
* ajustes a Registro de solicitud
* ajuste registro
* ajustes solicitud
* ajustes servicio solicitud acceso
* merge
* Servicio ParametroValor y SolicitudAcceso
* solicitudAcceso

